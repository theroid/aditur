|  ![Aditur Logo](artwork/aditur_logo_blue.png)  |  &nbsp;  |
|  --------------------------------------------  |  ------  |
  &nbsp;
  &nbsp;
  &nbsp;
  

**_The Aditur is aimed at so-called "Jahrgänge", mostly in Germany 
and is therefore in German._**

Dieses Projekt entstand dadurch, dass wir in der Oberstufe selbst auf der Suche
nach einem Weg waren, Kommentare, Zitate und Bilder zu sammeln.  
Daraus entstand dann dieses doch sehr umfangreiche Projekt, das wir, nachdem es 
sich nach unserem Abitur bewährt hat, hier als Freie Software unter GPL jedem zur Verfügung 
stellen wollen.
  
  

# Funktionen

Die Funktionen umfassen (unter anderem):
*  Kommentare zu Schülern und Lehrern
*  Sammeln von Zitaten
*  Erstellen von Bildersammlungen/-gallerien
*  Umfragen
*  Verwaltung von Anzeigen (für die Abizeitung)
*  Export aller Daten im ODT-Format!

Die Kommentare können, sofern das gewollt ist, zensiert werden, wobei Schüler 
sich mit Email-Adresse und Passwort authentifizieren und Lehrer einen Link 
erhalten, mit dem sie die Kommentare zu ihnen zensieren können.

Außerdem können damit auch Lehrer die Schüler kommentieren.

Desweiteren können Ideen, In/Out, "Wir danken..." usw. gesammelt werden.

Da es doch sehr umfangreich ist, hier ein paar Screenshots:

| ![HQ](artwork/Screenshot Headquarter.png)         |  ![Das eigene Profil](artwork/Screenshot Eigenes Profil.png)     |
|  -----------------------------------------------  |  ------------------------------------------------------------  |
| ![Profilübersicht](artwork/Screenshot Profile.png)|  ![Profil im Detail](artwork/Screenshot Profil.png)            |
| ![Kommentare](artwork/Screenshot Kommentare.png)  |  ![Umfragen](artwork/Screenshot Umfragenergebnisse.png)         |
| ![Zitate](artwork/Screenshot Zitate.png)          |  ![Einstellungen](artwork/Screenshot Einstellungen.png)        |
  
  

# Installation
1.  Lade das [Installationspaket](aditur-installation-v1.0.zip) herunter.
2.  Entpacke die Daten (auch die versteckten) und lade sie auf deinen Webserver hoch (z.B. per FTP).
3.  Geh auf /install.php und folge den Anweisungen.

Wichtig: **Damit Aditur funktioniert, braucht Ihr unbedingt mod_rewrite.**



# Disclaimer
Auch wenn wir uns natürlich sicher sind, dass alles funktioniert ;) hier noch einmal die Erinnerung:
**Diese Software kommt ohne jegliche Garantie.**



**Wir hoffen, dass sich das Aditur als nützlich erweist, und freuen uns über Rückmeldungen!**
(Und vielleicht sogar Patches?) :smiley:
