<?php

echo "Create Installation Package\n\n";


// create ZIP Archive
$zip = new ZipArchive();

if ($zip -> open("aditur-installation-v". time() . ".zip", ZipArchive::CREATE) !== true) {
    exit("Cannot open destination.");
}

// collect files for the archive
$zip -> addFile("src/.htaccess", ".htaccess.install");
$zip -> addFile("src/.htaccess.installation", ".htaccess");

$zip -> addFile("aditur_raw.sql", "aditur.sql");

$zip -> addFile("src/index.php", "index.php");
$zip -> addFile("src/install.php", "install.php");

$zip -> addFile("src/LICENSE", "LICENSE");


// add whole folders: css, lib, modules, views
$folders = array("css", "data", "lib", "modules", "views");

foreach ($folders as $folder) {
    addRecursively($zip, "src/", $folder);
}


// add empty directories
$zip -> addEmptyDir("data/collections");
$zip -> addEmptyDir("data/downloads");
$zip -> addEmptyDir("config");
$zip -> addEmptyDir("upload");


// close zip archive
if ($zip -> close() !== true) {
    exit("ZIP Archive could not be written.");
}

echo "Package created.\n\n";


function addRecursively($zip, $prefix, $element) {
    echo "Adding recursively: " . $prefix . $element . "\n";
    
    if (is_dir($prefix . $element)) {
        $elements = array_diff(scandir($prefix . $element), array('..', '.'));
        
        $prefix = $prefix . $element . "/";
        
        foreach ($elements as $element) {
            addRecursively($zip, $prefix, $element);
        }
    }
    else {
        // remove "src/" from $prefix
        $parts = explode("/", $prefix);
        
        $cleanPrefix = "";
        
        for ($i = 1; $i < count($parts); $i++) {
            $cleanPrefix = $cleanPrefix . $parts[$i] . "/";
        }
        
        
        $zip -> addFile($prefix . $element, $cleanPrefix . $element);
    }
}


?>
