<?php
// View: Base


$this->addStylesheet("/aditur/home.css");


$this->printHeader();

if ($this->login->isUserLoggedIn()) {
    ?>
    
    <main class="MainContent">
    
    <?php
    
    $this->printErrorBox();
    $this->printNotificationBox();
    
    ?>
    
        <section class="ModuleOutput Group">
            <?php $this->loadModule("headquarter", "fromUrl"); ?>
        </section>
    </main>
    
    <?php
}
else {
    ?>
    
    <main class="MainContent">
        <?php $this->printLoginForm(); ?>
    </main>
    
    <?php
}

$this->printFooter();

?>