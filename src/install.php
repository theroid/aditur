<?php

if (version_compare(PHP_VERSION, '5.5.0', '<')) {
    exit("Sorry, Aditur requires PHP Version 5.5 or higher.");
}

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <!-- Aditur - written with blood, sweat and the power of a
                                                    ./
                                                    .7
                                        \       , //
                                        |\. -._/|//
                                        /\ ) ) ).'/
                                        /(  \  // /
                                        /(   J`((_/ \
                                    / ) | _\     /
                                    /|)  \  eJ    L
                                    |  \ L \   L   L
                                    /  \  J  `. J   L
                                    |  )   L   \/   \
                                /  \    J   (\   /
                _...._ _         |  \      \   \```
        ,.._.-'        ''' -...-||\     -. \   \
        .'.=.'                    `         `.\ [ Y
    /   /                                  \]  J
    Y / Y                                    Y   L
    | | |          \                         |   L
    | | |           Y                        A  J
    |   I           |                       /I\ /
    |    \          I             \        ( |]/|
    J     \         /._           /        -tI/ |
    L     )       /   /'- - - 'J           `'-:.
    J   .'      ,'  ,' ,     \   `'-.__          \
        \ T      ,'  ,'   )\    /|        ';'- -7   /
        \|    ,'L  Y...-' / _.' /         \   /   /
        J   Y  |  J    .'-'   /         , -.(   /
        L  |  J   L -'     .'         /  |    /\
        |  J.  L  J     .-;.-/       |    \ .' /
        J   L`-J   L____,.-'`        |  _.-'   |
            L  J   L  J                  ``  J    |
            J   L  |   L                     J    |
            L  J  L    \                    L    \
            |   L  ) _.'\                    ) _.'\
            L    \('`    \                  ('`    \
            ) _.'\`-....'                   `-....'
            ('`    \
            `-.___/
        -->


        <title>Installation</title>

        <!-- CSS links -->
        <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/aditur.css">
        <link rel="stylesheet" href="/css/style.css">

        <!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="/media/favicon.png">

        <!-- responsive viewport -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>

    <body>
        <header>
            <img id="logo" alt="Aditur" src="/data/images/aditurLogoBlue.png" title="Aditur ist ein freies Jahrgangs-CMS.">

            <div class="Header">
                <ul>
                    <li><a style="font-weight: 200;">Installation</a></li>
                </ul>
            </div>
        </header>

        <main class="MainContent">


            <!-- ErrorBox -->
            <section class="ErrorBox Group" style="display: none">
                <i class="fa fa-lg fa-times-circle Weak"></i>

                <ul>
                </ul>
            </section>

            <!-- NotificationBox -->
            <section class="NotificationBox Group" style="display: none">
                <i class="fa fa-lg fa-check Weak"></i>

                <ul>
                </ul>
            </section>


            <?php

        //check if required file permissions are granted
        if(!(is_writable_r("./data")
             && is_writable_r("./views")
             && is_writable_r("./css")

             && (is_writable("./data/collections/trash") || mkdir("./data/collections/trash", 0755, true))
             && (is_writable("./data/profilePics/thumbs") || mkdir("./data/profilePics/thumbs", 0755, true))
             && (is_writable("./data/profilePics/raw") || mkdir("./data/profilePics/raw", 0755, true))
             && (is_writable("./data/profilePics/originals") || mkdir("./data/profilePics/originals", 0755, true))
             && (is_writable("./data/teachers/thumbs") || mkdir("./data/teachers/thumbs", 0755, true))
             && (is_writable("./data/teachers/raw") || mkdir("./data/teachers/raw", 0755, true))
             && (is_writable("./data/teachers/originals") || mkdir("./data/teachers/originals", 0755, true))
             && (is_writable("./data/downloads") || mkdir("./data/downloads", 0755, true)))) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Keine ausreichenden Schreibrechte! PHP benötigt Zugriff auf <i>/css</i>, <i>/data</i> und <i>/views</i>.</li>
                    </ul>
                </section>

                <?php

            if (isset($_POST['db_send'])) {
                print_dbbox($_POST['host'], $_POST['user'], $_POST['dbname']);
            }
            else {
                print_dbbox();
            }

            return;
        }

        if (!is_dir("./config/")) {
            if(!mkdir("./config/", 0755, true)) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Die Konfigurationsdatei kann nicht erstellt werden. Bitte gib die nötigen Dateirechte frei.</li>
                    </ul>
                </section>

                <?php

                print_dbbox($host, $user, $dbname);

                return;
            }
        }

        
        if (isset($_POST['db_send'])) {
            $host = $_POST['host'];
            $user = $_POST['user'];
            $pass = $_POST['pass'];
            $dbname = $_POST['dbname'];

            $text = "<?php\n\ndefine('DB_HOST', '$host');\ndefine('DB_NAME', '$dbname');\ndefine('DB_USER', '$user');\ndefine('DB_PASS', '$pass');\n\n?>";

            
            // open the database-config file
            if(!$dbconf = fopen("./config/db.php", "w")) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Die Konfigurationsdatei kann nicht geöffnet werden. Bitte gib die nötigen Dateirechte frei.</li>
                    </ul>
                </section>

                <?php

                print_dbbox($host, $user, $dbname);

                return;
            }
        
            // set the database configuration
           if(!fwrite($dbconf, $text)) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Die Konfigurationsdatei kann nicht geöffnet werden. Bitte gib die nötigen Dateirechte frei.</li>
                    </ul>
                </section>

                <?php

                print_dbbox($host, $user, $dbname);

                return;
            }
            
            
            fclose($dbconf);

            // open database
            require_once("./config/db.php");

            $sqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            //check if settings are correct
            if ($sqli->connect_errno) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Die Datenbank kann nicht geöffnet werden! (
                            <?php echo $sqli -> connect_error; ?>)</li>
                    </ul>
                </section>

                <?php

                print_dbbox($host, $user, $dbname);

                return;
            }



            // check for existence of tables
            if ($sqli->query("SHOW TABLES LIKE 'aditur_modules'")->num_rows > 0) {
                ?>

                <section class="NotificationBox Group" style="display: block">
                    <i class="fa fa-lg fa-check Weak"></i>

                    <ul>
                        <li>Die Datenbank wurde erfolgreich eingerichtet!</li>
                    </ul>
                </section>

                <?php
            }
            else {
                //set up database

                $aditursql = file('./aditur.sql');

                $templine = '';

                foreach($aditursql as $line) {
                    // Skip it if it's a comment
                    if (substr($line, 0, 2) == '--' || substr($line, 0, 2) == '/*' || $line == '')
                        continue;

                    // Add this line to the current segment
                    $templine .= $line;
                    // If it has a semicolon at the end, it's the end of the query
                    if (substr(trim($line), -1, 1) == ';') {
                        if(!$sqli->query($templine)) {
                        ?>

                        <section class="ErrorBox Group" style="display: block">
                            <i class="fa fa-lg fa-times-circle Weak"></i>

                            <ul>
                                <li>Die Datenbank kann nicht beschrieben werden! (
                                    <?php echo $sqli -> error; ?>)</li>
                            </ul>
                        </section>

                        <?php

                        print_dbbox($host, $user, $dbname);

                        return;
                        }
                        $templine = '';
                    }

                }

                ?>

                <section class="NotificationBox Group" style="display: block">
                    <i class="fa fa-lg fa-check Weak"></i>

                    <ul>
                        <li>Die Datenbank wurde erfolgreich eingerichtet!</li>
                    </ul>
                </section>

                <?php
            }


            print_admin();
        }

        elseif (isset($_POST['admin_send'])) {
            $forename = $_POST['forename'];
            $lastname = $_POST['lastname'];
            $mail = $_POST['mail'];
            $mail2 = $_POST['mail2'];
            $pass = $_POST['pass'];
            $pass2 = $_POST['pass2'];
            $title = $_POST['title'];
            $domain = $_POST['domain'];

            //double-check mail and password
            if ($mail !== $mail2) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Die beiden Email-Adressen stimmen nicht überein!</li>
                    </ul>
                </section>

                <?php

                print_admin($forename, $lastname, $mail, $title, $domain);

                return;
            } else if ($pass !== $pass2) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Die beiden Passwörter stimmen nicht überein!</li>
                    </ul>
                </section>

                <?php

                print_admin($forename, $lastname, $mail, $title, $domain);

                return;
            }


            // open database
            require_once("./config/db.php");

            $sqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // check if settings are correct
            if ($sqli->connect_errno) {
                ?>

                <section class="ErrorBox Group" style="display: block">
                    <i class="fa fa-lg fa-times-circle Weak"></i>

                    <ul>
                        <li>Die Datenbank kann nicht geöffnet werden! (
                            <?php echo $sqli -> connect_error; ?>)</li>
                    </ul>
                </section>

                <?php

                print_admin($forename, $lastname, $mail, $title, $domain);

                return;
            }

            // set Hash-Options
            // set Cost
            $timeTarget = 0.3;
            $cost = 9;

            do {
                $cost++;

                $start = microtime(true);

                password_hash("test", PASSWORD_DEFAULT, array('cost'=>$cost));

                $end = microtime(true);
            } while (($end - $start) < $timeTarget);


            $query = $sqli->prepare("UPDATE aditur_config SET value=? WHERE config_key='COST'");
            $query->bind_param("i",$cost);
            $query->execute();

            //set Pepper
            $characters = '012345=?6789a]bcde$%}fghijklmn-o/(pqr{stuvw)xyzA+BCD*EFG#HIJKL[MNOPQRSTUVWXYZ!';
            $charactersLength = strlen($characters);
            $randomString = '';

            for ($i = 0; $i < 11; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }

            $query = $sqli->prepare("UPDATE aditur_config SET value=? WHERE config_key='PEPPER'");
            $query->bind_param("s",$randomString);
            $query->execute();


            // load config
            $query = $sqli->query("SELECT config_key, value FROM aditur_config");
            $configs = array();

            while ($object = $query -> fetch_object()) {
                $configs[$object->config_key] = $object->value;
            }
            
            
            
            // create an admin without password
            $query = $sqli->prepare("INSERT INTO aditur_members (forename, lastname, email, privileges, komitee) VALUES (?, ?, ?, 'administrator', 1)");
            $query->bind_param("sss",$forename, $lastname, $mail);
            $query->execute();
            
            
            // select id of created admin
            $query = $sqli -> prepare("SELECT id FROM aditur_members WHERE email=?");
            $query -> bind_param('s', $mail);
            $query -> bind_result($adminId);
            
            $query -> execute();
            
            $query -> fetch();
            
            $query -> close();
            
            
            // hash the password
            $hash = password_hash($adminId . $pass . $configs["PEPPER"], PASSWORD_DEFAULT, array('cost' => $configs["COST"]));

            $query = $sqli -> prepare("UPDATE aditur_members SET password=? WHERE id=?");
            $query -> bind_param('si', $hash, $adminId);
            
            $query -> execute();
            

            // set config
            // title
            $query = $sqli->prepare("UPDATE aditur_config SET value=? WHERE config_key='title'");
            $query->bind_param("s", $title);
            $query->execute();

            // domain
            $query = $sqli->prepare("UPDATE aditur_config SET value=? WHERE config_key='domain'");
            $query->bind_param("s",$domain);
            $query->execute();

            // admin-mail-adress
            $query = $sqli->prepare("UPDATE aditur_config SET value=? WHERE config_key='admin_email_address'");
            $query->bind_param("s", $mail);
            $query->execute();

            ?>

            <section class="NotificationBox Group" style="display: block">
                <i class="fa fa-lg fa-check Weak"></i>

                <ul>
                    <li>Der Administrator wurde erfolgreich angelegt!</li>
                </ul>
            </section>

            <?php

            rename("./.htaccess.install", "./.htaccess");
            unlink("./aditur.sql");

            ?>

            <section class="Success Box">
                <h1>Aditur wurde erfolgreich eingerichtet.</h1>

                <p>Log dich jetzt <a href="/">hier</a> ein.</p>
            </section>

            <?php

            class DeleteOnExit{
                function __destruct()
                {
                    unlink(__FILE__);
                }
            }

            $g_delete_on_exit = new DeleteOnExit();

        }

        else {
            if (file_exists("./config/db.php")) {
                require_once("./config/db.php");

                if (defined("DB_HOST") && defined("DB_USER") && defined("DB_PASS") && defined("DB_NAME")) {
                    $sqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

                    // check if settings are correct
                    if (!$sqli->connect_errno) {
                        if ($sqli->query("SHOW TABLES LIKE 'aditur_modules'")->num_rows > 0) {
                            print_admin();
                        }
                        else {
                            print_dbbox();
                        }
                    }
                    else {
                        print_dbbox();
                    }
                }
                else {
                    print_dbbox();
                }
            }
            else {
                print_dbbox();
            }
        }


        function  print_dbbox($h="", $u="", $d="") {
            ?>

            <section style="text-align: center;" class="Box">
                <img src="data/images/aditurLogoBlue.png">
                <h1>Willkommen im Aditur!</h1>
                <p>Dieser Installer wird Aditur für dich einrichten.</p>
                <form action="/install.php" method="post">
                    <h3>Datenbank</h3>
                    
                    <input type="text" value="<?php echo $h; ?>" name="host" placeholder="Servername" required>
                    
                    <input type="text" value="<?php echo $u; ?>" name="user" placeholder="Benutzername" required>
                    
                    <input type="password" name="pass" placeholder="Passwort" required>
                    
                    <input type="text" value="<?php echo $d; ?>" name="dbname" placeholder="Datenbankname" required>
                    <br>
                    <br>
                    <button type="submit" name="db_send" value="submit">
                        <i class="fa fa-lg fa-check"></i>&nbsp; Datenbank einrichten
                    </button>
                </form>
            </section>

            <?php
        }

        function print_admin($f="",$l="",$m="",$t="",$d="") {
            ?>

            <section style="text-align: center;" class="Box">
                <img src="data/images/aditurLogoBlue.png">
                <h1>Einrichtung abschließen</h1>
                <form action="/install.php" method="post">
                    <h2>Administrator einrichten</h2>
                    <p>Vorname:</p>
                    <input type="text" value="<?php echo $f; ?>" name="forename" required>
                    <p>Nachname:</p>
                    <input type="text" value="<?php echo $l; ?>" name="lastname" required>
                    <p>E-Mail:</p>
                    <input type="email" name="mail" value="<?php echo $m; ?>" required>
                    <p>E-Mail wiederholen:</p>
                    <input type="email" name="mail2" required>
                    <p>Passwort:</p>
                    <input type="password" name="pass" required>
                    <p>Passwort wiederholen:</p>
                    <input type="password" name="pass2" required>
                    <br>
                    <br>
                    <h2>Konfiguration</h2>
                    <p>Seitentitel:</p>
                    <input type="text" name="title" value="<?php echo $t; ?>" required>
                    <p>Domain:</p>
                    <input type="text" name="domain" value="<?php echo $d; ?>" required>
                    <br>
                    <br>
                    <button type="submit" name="admin_send" value="submit">
                        <i class="fa fa-lg fa-check"></i>&nbsp; Fertig
                    </button>
                </form>
            </section>

            <?php
        }

        function is_writable_r($dir) {
            if (is_dir($dir)) {
                if(is_writable($dir)) {
                    $objects = scandir($dir);

                    foreach ($objects as $object) {
                        if ($object != "." && $object != "..") {
                            if (!is_writable_r($dir."/".$object)) {
                                return false;
                            }
                            else {
                                continue;
                            }
                        }
                    }

                    return true;
                }
                else {
                    return false;
                }
            }
            else if (file_exists($dir)) {
                return (is_writable($dir));

            }
        }

        ?>

        </main>

        <footer>

        </footer>
    </body>
</html>
