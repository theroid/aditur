<?php
// Settings Base Class

abstract class Setting {
    protected $aditur = null;
    protected $db = null;


    public function __construct($aditur) {
        $this->aditur = $aditur;
        $this->db = $aditur->getDb();
    }


    // Default state when called by Aditur from URL
    abstract public function fromUrl($argv);

    // function that should display content at /settings
    abstract protected function index();
}

?>
