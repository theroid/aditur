<?php
// Module Base Class

abstract class Module {
    protected $aditur = null;
    protected $db = null;


    public function __construct($aditur, $function = null, $argv = array()) {
        $this->aditur = $aditur;
        $this->db = $aditur->getDb();
    }


    // Default method when called by Aditur from URL
    abstract public function fromUrl($argv);
}

?>
