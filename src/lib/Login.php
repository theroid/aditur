<?php

/** Class Login
 *
 * processes Login and Logout
 *
 * Automatically invoked by aditur main class.
 */
class Login
{
    // object of Aditur Main Class
    private $aditur = null;
    // object The database connection
    private $db = null;


    public function __construct($aditur)
    {
        $this->aditur = $aditur;


        // start session
        session_start();


        // check the possible login actions:
        // logout
        if (isset($_GET["logout"])) {
            $this->doLogout();
        }
        // login
        elseif (isset($_POST["login"])) {
            $this->dologinWithPostData();
        }
    }


    private function dologinWithPostData()
    {
        // check login form contents
        if (empty($_POST['user_name'])) {
            $this->aditur->error("Email nicht eingegeben.");
        }
        elseif (empty($_POST['user_password'])) {
            $this->aditur->error("Passwort nicht eingegeben.");
        }
        elseif (!empty($_POST['user_name']) && !empty($_POST['user_password'])) {
            $this->db = $this->aditur->getDB();

            // escape the POST stuff
            $userName = strtolower($_POST['user_name']);

            // database query, getting all the info of the selected user (allows login via email address in the
            // username field)
            $query = $this->db->prepare("SELECT id, forename, lastname, email, password, komitee, privileges FROM aditur_members WHERE email=?");
            $query -> bind_param('s', $userName);
            $query -> bind_result($id, $forename, $lastname, $email, $password, $komitee, $privileges);
            $query -> execute();

            $query -> store_result();


            // if this user exists
            if ($query -> num_rows > 0) {

                $query -> fetch();

                //check if account is blocked due to too many failed login-attempts

                //check if there are more than 3 failed login attempts
                $query = "SELECT COUNT(*) as total FROM aditur_login_attempts WHERE user_id=" . $id;


                if ($result = $this -> db -> query($query)) {
                    $data = $result -> fetch_assoc();

                } else {
                    $this->aditur->log($this->db->error);

                    $this->aditur->error("Datenbankfehler. Bitte kontaktiere den Administrator.");

                    return;
                }

                if ($data['total'] > 6) {
                    $this->aditur->error("Dein Account ist wegen zu vieler fehlgeschlagener Anmeldeversuche gesperrt!<br>Bitte ändere <a href=\"" . $this->aditur->path("/resetPassword") . "\">hier</a> deine Passwort.");

                    return;
                }


                // using PHP 5.5's password_verify() function to check if the provided password fits
                // the hash of that user's password
                if (password_verify($id.$_POST['user_password'].$this->aditur->config("PEPPER"), $password)) {
                    // User logged in successfully.

                    // write user data into PHP SESSION (a file on your server)
                    $_SESSION['id'] = $id;
                    $_SESSION['forename'] = $forename;
                    $_SESSION['lastname'] = $lastname;
                    $_SESSION['email'] = $email;
                    $_SESSION['komitee'] = $komitee;
                    $_SESSION['privileges'] = $privileges;
                    $_SESSION['user_login_status'] = 1;
                }
                else {
                    $this->aditur->error("Falsches Passwort. Versuch's nochmal!");

                    // log false login-attempts
                    $query = "INSERT INTO aditur_login_attempts (user_id) VALUES ($id)";

                    if (!$this->db->query($query)) {
                        $this->aditur->log($this->db->error);
                    }
                }
            }
            else {
                $this->aditur->error("Hast Du dich verschrieben?<br>Sieh dir deine E-Mail-Adresse an!");
            }
        }
        else {
            $this->aditur->error("Sorry, Aditur ist momentan nicht erreichbar. Bitte gib dem Admin Bescheid!");
        }
    }

    // logout
    public function doLogout()
    {
        // delete session
        $_SESSION = array();
        session_destroy();
    }

    // return login status
    public function isUserLoggedIn()
    {
        if (isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] == 1) {
            return true;
        }

        return false;
    }
}


?>
