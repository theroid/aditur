<?php
/** Aditur.php
 *
 *  Aditur is a "Jahrgangs"-CMS under GPLv3. See <http://aditur.de>.
 *
 *
 *  Copyright (C) 2016 Richard A. Sattel, Hannes P. Juchem.
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *
 *
 *  For ODT-Export PHP-ODT from php-odt.sourceforge.net is used. Thanks to Herch!
 *
**/


class Aditur {
    // file The log file
    private $logFile = null;


    // array Assoc Array Key/Value of cached config keys from aditur_config
    private $configs = array();


    // object of db instance
    private $db = null;

    // object of login handler instance
    public $login = null;


    // string URL
    private $url = "";

    // string Path of Working Directory
    private $pwd = "";


    // array Collection of log messages
    public $logs = array();

    // array Collection of error messages
    private $errors = array();

    // array Collection of success / neutral messages
    private $notes = array();


    // array Collection of Paths to CSS-Files that shall be included
    private $css = array();


    // string Website <title>
    private $title = "";


    // string Output of the module, cached by ob().
    private $moduleOutput = "";

    // string The whole <body>.
    private $body = "";



    // Following are parameters that decide which module will be included:

    // Here they are set to the default module "views" with its respective properties.
    // so if you want to change the default module, change the following variables!

    // string Name of the Module
    private $moduleName = "views";

    // array The part of the URL after the moduleName is splitted by '/' and collected in this array
    private $parameters = array("home");

    // string Description of the specific module, mostly a translation to german.
    private $description = "";

    // bool True if the script puts out html.
    private $html = true;

    // bool True if header/navigation/footer should be added.
    private $frame = false;

    // bool True if script access is requested, eg when a form is transmitted by JS.
    private $script = false;




    public function __construct() {
        // Check for version compatibility
        if (version_compare(PHP_VERSION, '5.5.0', '<')) {
            exit("Sorry, Aditur requires PHP Version 5.5 or higher.");
        }


        try {
            // open log file
            $this->logFile = fopen("data/private/log.txt", "a");


            // create a database connection
            // using the constants from config/db.php
            require_once("config/db.php");

            @$sqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);

            // change character set to utf8 and check it
            if (!$sqli->set_charset("utf8")) {
                $this->log($sqli -> error);
            }

            // if no connection errors (= working database connection)
            if ($sqli->connect_errno) {
                $this->log("DB connection failed.");

                return;
            }

            $this->db = $sqli;


            // if Database Connection FAILED
            if (is_null($this->db)) {
                header("HTTP/1.1 500 Internal Server Error");

                die("There is an error concerning the database connection. Please contact the Admin. He can help you.");
            }


            // login
            require_once("lib/Login.php");

            $this->login = new Login($this);


            // Module Base Class
            require_once("Module.php");

            // Settings Base Class
            require_once("lib/Setting.php");

            if(!$this->config("error_reporting")) {
                error_reporting(0);
            }
            

            // Begin by parsing the URL
            $this->parseUrl();


            // Handle Script Access...
            if ($this->script) {
                $this->handleScript();
            }
            // ...or a normal page request.
            else {
                $this->generatePage();
            }
        }
        catch (Exception $e) {
            ob_end_clean();

            header("HTTP/1.1 500 Internal Server Error");

            die("There is an internal error. Please contact the Admin. He can help you.");
        }
    }


    // boolean, string This can be used to get config Key/Value pairs from table:aditur_config
    public function config($key) {
        // if this key is already cached
        if (array_key_exists($key, $this->configs)) {
            return $this->configs[$key];
        }
        else {
            // consult db

            // config
            $query = $this->db->query("SELECT config_key, value FROM aditur_config WHERE config_key='" . $key . "'");

            if ($object = $query -> fetch_object()) {
                $query -> close();

                // cache in array
                $this->configs[$object->config_key] = $object->value;

                return $object -> value;
            }

            $query -> close();

            return "";
        }
    }


    // Methods for handling requests and page creation
    private function parseUrl() {
        // retrieve name of this script;
        $serverScriptName = $_SERVER['SCRIPT_NAME']; // SCRIPT_NAME = "/path/to/index.php"
        $explodedElements = explode("/", $serverScriptName);
        $thisScriptName = "/" . end($explodedElements);


        // retrieve part of URL after our script
        // or working directory if our script's name is not part of it
        $this->url = urldecode($_SERVER['REQUEST_URI']);


        // first: strip GET Query
        $this->url = explode("?", $this->url)[0];


        // working Directory as pwd
        $this->pwd = explode($thisScriptName, $serverScriptName)[0];


        // check if our script is in URL
        if (strpos($this->url, $thisScriptName) !== false) {
            // get string after our script
            $partURL = explode($thisScriptName, $this->url)[1];

        } else {
            // get string after working directory
            if ($this->pwd == "") {
                $partURL = explode("/", $this->url, 2)[1];
            }
            else {
                $partURL = explode($this->pwd . "/", $this->url)[1];
            }
        }


        // check if  a module is specified
        // if not or the user is NOT logged in, return
        if ($partURL === "") {

            return;
        }

        // split partURL by "/"
        $arguments = explode("/", $partURL);


        // first argument is the module name
        $moduleName = $arguments[0];
        $function = "fromUrl";

        // the other ones are parameters
        for ($i = 1; $i < count($arguments); $i++) {
            if (!empty($arguments[$i])) {
                $parameters[] = $arguments[$i];
            }
        }

        if (!isset($parameters)) $parameters = null;

        // shortcut for media module to reduce server load
        if ($moduleName === "media") {
            $this->moduleName = $moduleName;
            $this->parameters = $parameters;
            $this->description = "Medien";
            $this->html = 0;
            $this->frame = 0;

            return;
        }


        // Now get properties of the requested module from table:aditur_modules
        // If this process fails at any step, the default module is loaded.
        $query = $this -> db -> prepare("SELECT name, description, enabled, needs_login, html, frame, privileges, komitee FROM aditur_modules WHERE link=?");
        $query -> bind_param('s', $moduleName);
        $query -> execute();

        $query -> store_result();


        // check if module exists
        if ($query -> num_rows !== 1) {
            $query -> close();

            $this->parameters = array($moduleName);

            return;
        }


        $query -> bind_result($name, $description, $enabled, $needs_login, $html, $frame, $privileges, $komitee);

        $object = $query -> fetch();

        $query -> close();

        if (!$enabled) {
            return;
        }


        if ($needs_login && !$this->login->isUserLoggedIn()) {

            return;
        }


        if ($privileges != "") {
            if (!$this->checkPrivileges($privileges, $komitee)) {

                return;
            }
        }


        $this->moduleName = $name;
        $this->parameters = $parameters;
        $this->description = $description;
        $this->html = $html;
        $this->frame = $frame;


        $this->setTitle($this->description);


        // check if script access is requested
        if (isset($_GET['script'])) $this -> script = true;
    }

    private function handleScript() {
        // first: load Module
        $module = $this->loadModule($this->moduleName, null, null , false);

        // Check if it exists
        if ($module === false) {
            echo "-1 No such page.";

            return;
        }


        $status = false;

        // second: process POST values if "post"-method exists.
        if (method_exists($module, "post")) {
            $status = $module -> post();
        }

        if ($status) {
            echo "0 " . implode("\n", $this->notes);
        }
        else {
            echo "-1 " . implode("\n", $this->errors);
        }
    }

    private function generatePage() {
        // include  Aditur-Stylesheets before everything else
        $this->addStylesheet("aditur/aditur.css");
        $this->addStylesheet("aditur/style.css");

        // font-awesome
        $this->addStylesheet("aditur/font-awesome/css/font-awesome.min.css");


        // process POST data if the module provides the post()-method...
        if ($this->loadModule($this->moduleName, "post", $this->parameters, false)) {
            // ...and if this was successfull, apply POST/Redirect/GET:
            return $this->PRG();
        }


        // Now render the page.

        // if the requested module does not output any html,
        // just let the module do its stuff.
        if ($this->html == false) {
            $this->loadModule($this->moduleName, "fromUrl", $this->parameters);

            return;
        }


        // first: cache module output
        ob_start();

        $this->loadModule($this->moduleName, "fromUrl", $this->parameters, false);

        $this->moduleOutput = ob_get_clean();


        // second: add header, navigation and footer if requested
        // to complete <body>
        $this->renderBody();


        // And finally:
        // Output complete <html> page
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">

        <!-- Aditur - written with blood, sweat and the power of a
                                                           ./
                                                          .7
                                               \       , //
                                               |\. -._/|//
                                              /\ ) ) ).'/
                                             /(  \  // /
                                            /(   J`((_/ \
                                           / ) | _\     /
                                          /|)  \  eJ    L
                                         |  \ L \   L   L
                                        /  \  J  `. J   L
                                        |  )   L   \/   \
                                       /  \    J   (\   /
                     _...._ _         |  \      \   \```
              ,.._.-'        ''' -...-||\     -. \   \
            .'.=.'                    `         `.\ [ Y
           /   /                                  \]  J
          Y / Y                                    Y   L
          | | |          \                         |   L
          | | |           Y                        A  J
          |   I           |                       /I\ /
          |    \          I             \        ( |]/|
          J     \         /._           /        -tI/ |
           L     )       /   /'- - - 'J           `'-:.
           J   .'      ,'  ,' ,     \   `'-.__          \
            \ T      ,'  ,'   )\    /|        ';'- -7   /
             \|    ,'L  Y...-' / _.' /         \   /   /
              J   Y  |  J    .'-'   /         , -.(   /
               L  |  J   L -'     .'         /  |    /\
               |  J.  L  J     .-;.-/       |    \ .' /
               J   L`-J   L____,.-'`        |  _.-'   |
                L  J   L  J                  ``  J    |
                J   L  |   L                     J    |
                 L  J  L    \                    L    \
                 |   L  ) _.'\                    ) _.'\
                 L    \('`    \                  ('`    \
                  ) _.'\`-....'                   `-....'
                 ('`    \
                  `-.___/
        -->


        <title><?php echo $this->printTitle(); ?></title>

        <!-- CSS links -->
        <?php $this->printStylesheets(); ?>

        <!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->path("/media/images/favicon.png"); ?>">

        <!-- responsive viewport -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>
        <?php echo $this->body; ?>

    </body>
</html>

        <?php
    }

    private function renderBody() {
        ob_start();

        // if header, navigation and footer should be added
        if ($this->frame) {
            $this->printHeader();

            ?>

            <main>
                <?php

                $this->printErrorBox();

                $this->printNotificationBox();

                ?>

                <section class="Module Group">
                    <?php echo $this->moduleOutput; ?>

                </section>
            </main>

            <?php

            $this->printFooter();

        }
        else {
            echo $this->moduleOutput;
        }

        $this->body = ob_get_clean();
    }

    // These methods are used by renderBody() to assemble the <body>
    private function printHeader() {
        $loggedIn = $this->login->isUserLoggedIn();

        if ($loggedIn) {
            ?>

            <input id="navExpander" class="Expander" type="checkbox">
            <label for="navExpander" id="navLabel"><i class="fa fa-3x fa-bars"></i></label>

            <?php
        }

        ?>

        <header class="<?php if(!$loggedIn) echo "Shadow"; ?>">
            <a href="<?php echo $this->path("/headquarter"); ?>"><img id="logo" class="<?php if ($loggedIn) echo "Move"; ?>" alt="Aditur" src="<?php echo $this->pwd(); ?>/media/images/aditurLogoBlue.png" title="Aditur ist ein freies Jahrgangs-CMS."></a>

            <?php

            if ($loggedIn) {
                ?>

                <nav>
                    <div class="Header">
                        <ul>

                            <?php

                            $query = $this->db->query("SELECT link, description, icon, privileges, komitee FROM aditur_modules WHERE enabled=1 AND in_header=1");


                            while ($object = $query -> fetch_object()) {
                                // check for permissions
                                if (!empty($object->privileges)) {
                                    if (!$this->checkPrivileges($object->privileges, $object->komitee)) {
                                        continue;
                                    }
                                }

                                ?>

                                <li><a href="<?php echo $this->path("/" . $object->link); ?>"><i class="fa fa-lg fa-<?php echo $object->icon; ?>"></i>&nbsp;<?php echo $object->description; ?></a></li>

                                <?php
                            }

                            ?>

                    </ul>
                </div>

                <div class="Sidebar">
                    <ul>

                        <?php

                        $query = $this->db->query("SELECT link, description, icon, privileges, komitee FROM aditur_modules WHERE enabled=1 AND in_menu=1");


                        while ($object = $query -> fetch_object()) {
                            // check for permissions
                            if (!empty($object->privileges)) {
                                if (!$this->checkPrivileges($object->privileges, $object->komitee)) {
                                    continue;
                                }
                            }

                            ?>

                            <li><a href="<?php echo $this->path("/" . $object->link); ?>"><i class="fa fa-lg fa-<?php echo $object->icon; ?>"></i>&nbsp;<?php echo $object->description; ?></a></li>

                            <?php
                        }

                        ?>

                    </ul>
                </div>
            </nav>

            <?php
            }

            ?>

        </header>

        <?php
    }

    private function printFooter() {
        ?>

        <footer>
            <section>
                <input type="checkbox" class="Expander" id="impressumExpander">
                <label for="impressumExpander" class="ExpanderButton">Impressum/Datenschutz</label>
                <article id="impressum">
                    <label for="impressumExpander" class="Close"><i class="fa fa-lg fa-times-circle"></i></label>

                    <?php echo $this->config("impressum_data"); ?>

                    <h3>Haftungsausschluss</h3>

                    <h3>Haftungsbeschränkung</h3>
                    <p>Die Inhalte der Website werden mit größter Sorgfältigkeit erstellt, jedoch kann keine Gewähr für Richtigkeit, Vollständigkeit und Aktualität dieser übernommen werden. Fürgespeicherte Inhalte Dritter wird keine Haftung übernommen. Bei Bekanntwerden einer Rechtsverletzung werden wir diese umgehend entfernen. Die Nutzung der Inhalte der Website erfolgt auf eigene Gefahr des Nutzers.</p>

                    <h3>Externe Links</h3>
                    <p>Diese Website enthält Verlinkungen zu externen Websites (im Folgenden als „Seiten“ bezeichnet). Für die Inhalte dieser Seiten und für mögliche Schaden aus deren Benutzung haftet allein deren Betreiber. Auf die derzeitige und zukünftige Gestaltung besteht kein Einfluss. Der Anbieter distanziert sich daher ausdrücklich von den Inhalten der verlinkten Seiten. Die Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Solche waren zu diesem Zeitpunkt nicht erkenntlich. Eine dauerhafte Überprüfung ist für den Anbieter ohne konkreten Hinweis auf eine Rechtsverletzung nicht zumutbar. Bei Bekanntwerden einer Rechtsverletzung wird der Link umgehend gelöscht.</p>

                    <h3>Urheberrecht</h3>
                    <p>Die auf dieser Website veröffentlichten Inhalte und Werke unterliegen dem deutschen Urheberrecht. Jede Verwendung dieser Inhalte außerhalb der vom Urheberrecht zugelassenen Grenzen bedarf der schriftlichen Zustimmung des Anbieters oder des jeweiligen Rechteinhabers. Inhalte und Rechte Dritter werden als solche kenntlich gemacht.</p>
                    
                    <h3>Rechtswirksamkeit dieses Haftungsausschlusses</h3>
                    <p>Dieser Haftungsausschluss ist als Teil des Internetangebotes zu betrachten, von dem aus auf diese Seite verwiesen wurde. Sofern Teile oder einzelne Formulierungen dieses Textes der geltenden Rechtslage nicht, nicht mehr oder nicht vollständig entsprechen sollten, bleiben die übrigen Teile des Dokumentes in ihrem Inhalt und ihrer Gültigkeit davon unberührt.</p>
                    
                    
                    <h3>Datenschutzerklärung</h3>

                    <p>tl;dr: Nachfolgend kommt eine Standarddatenschutzerklärung für die DSGVO. Aditur trackt Dich nicht und benutzt auch keine Social-Media-Referenzen. Gespeichert werden eigentlich nur Dinge, die Du eingibst oder hochlädst, abgesehen von irgendwelchen logfiles natürlich. Cookies werden nur zur Authentifizierung benutzt ("Session-Cookie").<br><br></p>

                    <p>Diese Datenschutzerklärung klärt Sie über die Art, den Umfang und Zweck der Verarbeitung von personenbezogenen Daten (nachfolgend kurz „Daten“) innerhalb unseres Onlineangebotes und der mit ihm verbundenen Webseiten, Funktionen und Inhalte sowie externen Onlinepräsenzen, wie z.B. unser Social Media Profile auf (nachfolgend gemeinsam bezeichnet als „Onlineangebot“). Im Hinblick auf die verwendeten Begrifflichkeiten, wie z.B. „Verarbeitung“ oder „Verantwortlicher“ verweisen wir auf die Definitionen im Art. 4 der Datenschutzgrundverordnung (DSGVO).</p>

                    <h4>Arten der verarbeiteten Daten:</h4>

                    <ul>
                        <li>Bestandsdaten (z.B., Namen, Adressen)</li>
                        <li>Kontaktdaten (z.B., E-Mail, Telefonnummern)</li>
                        <li>Inhaltsdaten (z.B., Texteingaben, Fotografien, Videos)</li>
                        <li>Nutzungsdaten (z.B., besuchte Webseiten, Interesse an Inhalten, Zugriffszeiten)</li>
                        <li>Meta-/Kommunikationsdaten (z.B., Geräte-Informationen, IP-Adressen)</li>
                    </ul>

                    <h4>Kategorien betroffener Personen</h4>

                    <p>Besucher und Nutzer des Onlineangebotes (Nachfolgend bezeichnen wir die betroffenen Personen zusammenfassend auch als „Nutzer“).</p>

                    <h4>Zweck der Verarbeitung</h4>

                    <ul>
                        <li>Zurverfügungstellung des Onlineangebotes, seiner Funktionen und  Inhalte</li>
                        <li>Beantwortung von Kontaktanfragen und Kommunikation mit Nutzern</li>
                        <li>Sicherheitsmaßnahmen</li>
                        <li>Reichweitenmessung/Marketing</li>
                    </ul>

                    <h4>Verwendete Begrifflichkeiten </h4>

                    <p>„Personenbezogene Daten“ sind alle Informationen, die sich auf eine identifizierte oder identifizierbare natürliche Person (im Folgenden „betroffene Person“) beziehen; als identifizierbar wird eine natürliche Person angesehen, die direkt oder indirekt, insbesondere mittels Zuordnung zu einer Kennung wie einem Namen, zu einer Kennnummer, zu Standortdaten, zu einer Online-Kennung (z.B. Cookie) oder zu einem oder mehreren besonderen Merkmalen identifiziert werden kann, die Ausdruck der physischen, physiologischen, genetischen, psychischen, wirtschaftlichen, kulturellen oder sozialen Identität dieser natürlichen Person sind.</p>

                    <p>„Verarbeitung“ ist jeder mit oder ohne Hilfe automatisierter Verfahren ausgeführte Vorgang oder jede solche Vorgangsreihe im Zusammenhang mit personenbezogenen Daten. Der Begriff reicht weit und umfasst praktisch jeden Umgang mit Daten.</p>

                    <p>„Pseudonymisierung“ die Verarbeitung personenbezogener Daten in einer Weise, dass die personenbezogenen Daten ohne Hinzuziehung zusätzlicher Informationen nicht mehr einer spezifischen betroffenen Person zugeordnet werden können, sofern diese zusätzlichen Informationen gesondert aufbewahrt werden und technischen und organisatorischen Maßnahmen unterliegen, die gewährleisten, dass die personenbezogenen Daten nicht einer identifizierten oder identifizierbaren natürlichen Person zugewiesen werden.</p>

                    <p>„Profiling“ jede Art der automatisierten Verarbeitung personenbezogener Daten, die darin besteht, dass diese personenbezogenen Daten verwendet werden, um bestimmte persönliche Aspekte, die sich auf eine natürliche Person beziehen, zu bewerten, insbesondere um Aspekte bezüglich Arbeitsleistung, wirtschaftliche Lage, Gesundheit, persönliche Vorlieben, Interessen, Zuverlässigkeit, Verhalten, Aufenthaltsort oder Ortswechsel dieser natürlichen Person zu analysieren oder vorherzusagen.</p>

                    <p>Als „Verantwortlicher“ wird die natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet, bezeichnet.</p>

                    <p>„Auftragsverarbeiter“ eine natürliche oder juristische Person, Behörde, Einrichtung oder andere Stelle, die personenbezogene Daten im Auftrag des Verantwortlichen verarbeitet</p>

                    <h4>Maßgebliche Rechtsgrundlagen</h4>

                    <p>Nach Maßgabe des Art. 13 DSGVO teilen wir Ihnen die Rechtsgrundlagen unserer Datenverarbeitungen mit. Sofern die Rechtsgrundlage in der Datenschutzerklärung nicht genannt wird, gilt Folgendes: Die Rechtsgrundlage für die Einholung von Einwilligungen ist Art. 6 Abs. 1 lit. a und Art. 7 DSGVO, die Rechtsgrundlage für die Verarbeitung zur Erfüllung unserer Leistungen und Durchführung vertraglicher Maßnahmen sowie Beantwortung von Anfragen ist Art. 6 Abs. 1 lit. b DSGVO, die Rechtsgrundlage für die Verarbeitung zur Erfüllung unserer rechtlichen Verpflichtungen ist Art. 6 Abs. 1 lit. c DSGVO, und die Rechtsgrundlage für die Verarbeitung zur Wahrung unserer berechtigten Interessen ist Art. 6 Abs. 1 lit. f DSGVO. Für den Fall, dass lebenswichtige Interessen der betroffenen Person oder einer anderen natürlichen Person eine Verarbeitung personenbezogener Daten erforderlich machen, dient Art. 6 Abs. 1 lit. d DSGVO als Rechtsgrundlage.</p>

                    <h4>Sicherheitsmaßnahmen</h4>

                    <p>Wir treffen nach Maßgabe des Art. 32 DSGVO unter Berücksichtigung des Stands der Technik, der Implementierungskosten und der Art, des Umfangs, der Umstände und der Zwecke der Verarbeitung sowie der unterschiedlichen Eintrittswahrscheinlichkeit und Schwere des Risikos für die Rechte und Freiheiten natürlicher Personen, geeignete technische und organisatorische Maßnahmen, um ein dem Risiko angemessenes Schutzniveau zu gewährleisten.</p>

                    <p>Zu den Maßnahmen gehören insbesondere die Sicherung der Vertraulichkeit, Integrität und Verfügbarkeit von Daten durch Kontrolle des physischen Zugangs zu den Daten, als auch des sie betreffenden Zugriffs, der Eingabe, Weitergabe, der Sicherung der Verfügbarkeit und ihrer Trennung. Des Weiteren haben wir Verfahren eingerichtet, die eine Wahrnehmung von Betroffenenrechten, Löschung von Daten und Reaktion auf Gefährdung der Daten gewährleisten. Ferner berücksichtigen wir den Schutz personenbezogener Daten bereits bei der Entwicklung, bzw. Auswahl von Hardware, Software sowie Verfahren, entsprechend dem Prinzip des Datenschutzes durch Technikgestaltung und durch datenschutzfreundliche Voreinstellungen (Art. 25 DSGVO).</p>

                    <h4>Zusammenarbeit mit Auftragsverarbeitern und Dritten</h4>

                    <p>Sofern wir im Rahmen unserer Verarbeitung Daten gegenüber anderen Personen und Unternehmen (Auftragsverarbeitern oder Dritten) offenbaren, sie an diese übermitteln oder ihnen sonst Zugriff auf die Daten gewähren, erfolgt dies nur auf Grundlage einer gesetzlichen Erlaubnis (z.B. wenn eine Übermittlung der Daten an Dritte, wie an Zahlungsdienstleister, gem. Art. 6 Abs. 1 lit. b DSGVO zur Vertragserfüllung erforderlich ist), Sie eingewilligt haben, eine rechtliche Verpflichtung dies vorsieht oder auf Grundlage unserer berechtigten Interessen (z.B. beim Einsatz von Beauftragten, Webhostern, etc.).</p>

                    <p>Sofern wir Dritte mit der Verarbeitung von Daten auf Grundlage eines sog. „Auftragsverarbeitungsvertrages“ beauftragen, geschieht dies auf Grundlage des Art. 28 DSGVO.</p>

                    <h4>Rechte der betroffenen Personen</h4>

                    <p>Sie haben das Recht, eine Bestätigung darüber zu verlangen, ob betreffende Daten verarbeitet werden und auf Auskunft über diese Daten sowie auf weitere Informationen und Kopie der Daten entsprechend Art. 15 DSGVO.</p>

                    <p>Sie haben entsprechend. Art. 16 DSGVO das Recht, die Vervollständigung der Sie betreffenden Daten oder die Berichtigung der Sie betreffenden unrichtigen Daten zu verlangen.</p>

                    <p>Sie haben nach Maßgabe des Art. 17 DSGVO das Recht zu verlangen, dass betreffende Daten unverzüglich gelöscht werden, bzw. alternativ nach Maßgabe des Art. 18 DSGVO eine Einschränkung der Verarbeitung der Daten zu verlangen.</p>

                    <p>Sie haben das Recht zu verlangen, dass die Sie betreffenden Daten, die Sie uns bereitgestellt haben nach Maßgabe des Art. 20 DSGVO zu erhalten und deren Übermittlung an andere Verantwortliche zu fordern.</p>

                    <p>Sie haben ferner gem. Art. 77 DSGVO das Recht, eine Beschwerde bei der zuständigen Aufsichtsbehörde einzureichen.</p>

                    <h4>Widerrufsrecht</h4>

                    <p>Sie haben das Recht, erteilte Einwilligungen gem. Art. 7 Abs. 3 DSGVO mit Wirkung für die Zukunft zu widerrufen</p>

                    <h4>Widerspruchsrecht</h4>

                    <p>Sie können der künftigen Verarbeitung der Sie betreffenden Daten nach Maßgabe des Art. 21 DSGVO jederzeit widersprechen. Der Widerspruch kann insbesondere gegen die Verarbeitung für Zwecke der Direktwerbung erfolgen.</p>

                    <h4>Cookies und Widerspruchsrecht bei Direktwerbung</h4>

                    <p>Als „Cookies“ werden kleine Dateien bezeichnet, die auf Rechnern der Nutzer gespeichert werden. Innerhalb der Cookies können unterschiedliche Angaben gespeichert werden. Ein Cookie dient primär dazu, die Angaben zu einem Nutzer (bzw. dem Gerät auf dem das Cookie gespeichert ist) während oder auch nach seinem Besuch innerhalb eines Onlineangebotes zu speichern. Als temporäre Cookies, bzw. „Session-Cookies“ oder „transiente Cookies“, werden Cookies bezeichnet, die gelöscht werden, nachdem ein Nutzer ein Onlineangebot verlässt und seinen Browser schließt. In einem solchen Cookie kann z.B. der Inhalt eines Warenkorbs in einem Onlineshop oder ein Login-Status gespeichert werden. Als „permanent“ oder „persistent“ werden Cookies bezeichnet, die auch nach dem Schließen des Browsers gespeichert bleiben. So kann z.B. der Login-Status gespeichert werden, wenn die Nutzer diese nach mehreren Tagen aufsuchen. Ebenso können in einem solchen Cookie die Interessen der Nutzer gespeichert werden, die für Reichweitenmessung oder Marketingzwecke verwendet werden. Als „Third-Party-Cookie“ werden Cookies bezeichnet, die von anderen Anbietern als dem Verantwortlichen, der das Onlineangebot betreibt, angeboten werden (andernfalls, wenn es nur dessen Cookies sind spricht man von „First-Party Cookies“).</p>

                    <p>Wir können temporäre und permanente Cookies einsetzen und klären hierüber im Rahmen unserer Datenschutzerklärung auf.</p>

                    <p>Falls die Nutzer nicht möchten, dass Cookies auf ihrem Rechner gespeichert werden, werden sie gebeten die entsprechende Option in den Systemeinstellungen ihres Browsers zu deaktivieren. Gespeicherte Cookies können in den Systemeinstellungen des Browsers gelöscht werden. Der Ausschluss von Cookies kann zu Funktionseinschränkungen dieses Onlineangebotes führen.</p>

                    <p>Ein genereller Widerspruch gegen den Einsatz der zu Zwecken des Onlinemarketing eingesetzten Cookies kann bei einer Vielzahl der Dienste, vor allem im Fall des Trackings, über die US-amerikanische Seite <a href="http://www.aboutads.info/choices/">http://www.aboutads.info/choices/</a> oder die EU-Seite <a href="http://www.youronlinechoices.com/">http://www.youronlinechoices.com/</a> erklärt werden. Des Weiteren kann die Speicherung von Cookies mittels deren Abschaltung in den Einstellungen des Browsers erreicht werden. Bitte beachten Sie, dass dann gegebenenfalls nicht alle Funktionen dieses Onlineangebotes genutzt werden können.</p>

                    <h4>Löschung von Daten</h4>

                    <p>Die von uns verarbeiteten Daten werden nach Maßgabe der Art. 17 und 18 DSGVO gelöscht oder in ihrer Verarbeitung eingeschränkt. Sofern nicht im Rahmen dieser Datenschutzerklärung ausdrücklich angegeben, werden die bei uns gespeicherten Daten gelöscht, sobald sie für ihre Zweckbestimmung nicht mehr erforderlich sind und der Löschung keine gesetzlichen Aufbewahrungspflichten entgegenstehen. Sofern die Daten nicht gelöscht werden, weil sie für andere und gesetzlich zulässige Zwecke erforderlich sind, wird deren Verarbeitung eingeschränkt. D.h. die Daten werden gesperrt und nicht für andere Zwecke verarbeitet. Das gilt z.B. für Daten, die aus handels- oder steuerrechtlichen Gründen aufbewahrt werden müssen.</p>

                    <h4>Registrierfunktion</h4>

                    <p>Nutzer können ein Nutzerkonto anlegen. Im Rahmen der Registrierung werden die erforderlichen Pflichtangaben den Nutzern mitgeteilt und auf Grundlage des Art. 6 Abs. 1 lit. b DSGVO zu Zwecken der Bereitstellung des Nutzerkontos verarbeitet. Zu den verarbeiteten Daten gehören insbesondere die Login-Informationen (Name, Passwort sowie eine E-Mailadresse). Die im Rahmen der Registrierung eingegebenen Daten werden für die Zwecke der Nutzung des Nutzerkontos und dessen Zwecks verwendet.</p>

                    <p>Die Nutzer können über Informationen, die für deren Nutzerkonto relevant sind, wie z.B. technische Änderungen, per E-Mail informiert werden. Wenn Nutzer ihr Nutzerkonto gekündigt haben, werden deren Daten im Hinblick auf das Nutzerkonto, vorbehaltlich einer gesetzlichen Aufbewahrungspflicht, gelöscht. Es obliegt den Nutzern, ihre Daten bei erfolgter Kündigung vor dem Vertragsende zu sichern. Wir sind berechtigt, sämtliche während der Vertragsdauer gespeicherten Daten des Nutzers unwiederbringlich zu löschen.</p>

                    <h4>Kommentare und Beiträge</h4>

                    <p>Wenn Nutzer Kommentare oder sonstige Beiträge hinterlassen, können ihre IP-Adressen auf Grundlage unserer berechtigten Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO für 7 Tage gespeichert werden. Das erfolgt zu unserer Sicherheit, falls jemand in Kommentaren und Beiträgen widerrechtliche Inhalte hinterlässt (Beleidigungen, verbotene politische Propaganda, etc.). In diesem Fall können wir selbst für den Kommentar oder Beitrag belangt werden und sind daher an der Identität des Verfassers interessiert.</p>

                    <p>Des Weiteren behalten wir uns vor, auf Grundlage unserer berechtigten Interessen gem. Art. 6 Abs. 1 lit. f. DSGVO, die Angaben der Nutzer zwecks Spamerkennung zu verarbeiten.</p>

                    <p>Auf derselben Rechtsgrundlage behalten wir uns vor, im Fall von Umfragen die IP-Adressen der Nutzer für deren Dauer zu speichern und Cookis zu verwenden, um Mehrfachabstimmungen zu vermeiden.</p>

                    <p>Die im Rahmen der Kommentare und Beiträge angegebenen Daten, werden von uns bis zum Widerspruch der Nutzer dauerhaft gespeichert.</p>

                    <h4>Kontaktaufnahme</h4>

                    <p>Bei der Kontaktaufnahme mit uns (z.B. per Kontaktformular, E-Mail, Telefon oder via sozialer Medien) werden die Angaben des Nutzers zur Bearbeitung der Kontaktanfrage und deren Abwicklung gem. Art. 6 Abs. 1 lit. b) DSGVO verarbeitet.</p>

                    <p>Wir löschen die Anfragen, sofern diese nicht mehr erforderlich sind. Wir überprüfen die Erforderlichkeit alle zwei Jahre; Ferner gelten die gesetzlichen Archivierungspflichten</p>

                    <h4>Hosting und E-Mail-Versand</h4>

                    <p>Die von uns in Anspruch genommenen Hosting-Leistungen dienen der Zurverfügungstellung der folgenden Leistungen: Infrastruktur- und Plattformdienstleistungen, Rechenkapazität, Speicherplatz und Datenbankdienste, E-Mail-Versand, Sicherheitsleistungen sowie technische Wartungsleistungen, die wir zum Zwecke des Betriebs dieses Onlineangebotes einsetzen.</p>

                    <p>Hierbei verarbeiten wir, bzw. unser Hostinganbieter Bestandsdaten, Kontaktdaten, Inhaltsdaten, Vertragsdaten, Nutzungsdaten, Meta- und Kommunikationsdaten von Kunden, Interessenten und Besuchern dieses Onlineangebotes auf Grundlage unserer berechtigten Interessen an einer effizienten und sicheren Zurverfügungstellung dieses Onlineangebotes gem. Art. 6 Abs. 1 lit. f DSGVO i.V.m. Art. 28 DSGVO (Abschluss Auftragsverarbeitungsvertrag).</p>

                    <h4>Erhebung von Zugriffsdaten und Logfiles</h4>

                    <p>Wir, bzw. unser Hostinganbieter, erhebt auf Grundlage unserer berechtigten Interessen im Sinne des Art. 6 Abs. 1 lit. f. DSGVO Daten über jeden Zugriff auf den Server, auf dem sich dieser Dienst befindet (sogenannte Serverlogfiles). Zu den Zugriffsdaten gehören Name der abgerufenen Webseite, Datei, Datum und Uhrzeit des Abrufs, übertragene Datenmenge, Meldung über erfolgreichen Abruf, Browsertyp nebst Version, das Betriebssystem des Nutzers, Referrer URL (die zuvor besuchte Seite), IP-Adresse und der anfragende Provider.</p>

                    <p>Logfile-Informationen werden aus Sicherheitsgründen (z.B. zur Aufklärung von Missbrauchs- oder Betrugshandlungen) für die Dauer von maximal 7 Tagen gespeichert und danach gelöscht. Daten, deren weitere Aufbewahrung zu Beweiszwecken erforderlich ist, sind bis zur endgültigen Klärung des jeweiligen Vorfalls von der Löschung ausgenommen.</p>

                    <h4>Einbindung von Diensten und Inhalten Dritter</h4>

                    <p>Wir setzen innerhalb unseres Onlineangebotes auf Grundlage unserer berechtigten Interessen (d.h. Interesse an der Analyse, Optimierung und wirtschaftlichem Betrieb unseres Onlineangebotes im Sinne des Art. 6 Abs. 1 lit. f. DSGVO) Inhalts- oder Serviceangebote von Drittanbietern ein, um deren Inhalte und Services, wie z.B. Videos oder Schriftarten einzubinden (nachfolgend einheitlich bezeichnet als “Inhalte”).</p>

                    <p>Dies setzt immer voraus, dass die Drittanbieter dieser Inhalte, die IP-Adresse der Nutzer wahrnehmen, da sie ohne die IP-Adresse die Inhalte nicht an deren Browser senden könnten. Die IP-Adresse ist damit für die Darstellung dieser Inhalte erforderlich. Wir bemühen uns nur solche Inhalte zu verwenden, deren jeweilige Anbieter die IP-Adresse lediglich zur Auslieferung der Inhalte verwenden. Drittanbieter können ferner so genannte Pixel-Tags (unsichtbare Grafiken, auch als "Web Beacons" bezeichnet) für statistische oder Marketingzwecke verwenden. Durch die "Pixel-Tags" können Informationen, wie der Besucherverkehr auf den Seiten dieser Website ausgewertet werden. Die pseudonymen Informationen können ferner in Cookies auf dem Gerät der Nutzer gespeichert werden und unter anderem technische Informationen zum Browser und Betriebssystem, verweisende Webseiten, Besuchszeit sowie weitere Angaben zur Nutzung unseres Onlineangebotes enthalten, als auch mit solchen Informationen aus anderen Quellen verbunden werden.</p>

                    <p><br>Vom Websiteinhaber angepasst</p>
                    
                    <p><br><br><a href="https://datenschutz-generator.de" rel="nofollow" target="_blank">Erstellt mit Datenschutz-Generator.de von RA Dr. Thomas Schwenke</a></p>
                </article>
            </section>

            <section>
                <p><a href="mailto:<?php echo $this->config("admin_email_address"); ?>" target="_blank"><?php echo $this->config("admin_email_address"); ?></a></p>
            </section>

            <section>
                <p class="copyright">&copy; <?php echo $this->config("title"); ?></p>
            </section>
        </footer>

        <?php
    }



    // With this method any script can set its <title> manually,
    // even if normally its description from table:aditur_modules is used.
    public function setTitle($string) {
        $this->title = $string;
    }

    // And this renders it into the <title>-tag.
    private function printTitle() {
        if ($this->title !== "") {
            echo "Aditur - " . $this->title;
        }
        else {
            echo $this->config("title");
        }
    }


    // This method adds stylesheets to the list of stylesheets that...
    public function addStylesheet($stylesheet) {
        if (in_array($stylesheet, $this->css)) return;


        $this->css[] = $stylesheet;
    }

    // ...will be added in the <head>.
    private function printStylesheets() {
        foreach ($this->css as $stylesheet) {
            ?>

            <link rel="stylesheet" href="<?php echo $this->path("/css/" . $stylesheet); ?>">

            <?php
        }
    }



    // object Loads a module and adds its stylesheets.
    // If no function is passed, returns the instance of the loaded module,
    // elseif a function is given returns what is returned by the function.
    public function loadModule($name, $function = null, $argv = null, $callPost = true) {
        // check for existence of that Module
        if (in_array($name, scandir("modules")) && file_exists("modules/" . $name . "/module.php")) {
            // include module stylesheet(s)

            // scan for files
            $files = array_diff(scandir("modules/" . $name . "/"), array('..', '.'));

            // filter *.css files
            foreach ($files as $file) {
                $parts = explode(".", $file);
                $suffix = end($parts);


                if (strtolower($suffix) === "css") {
                    $this->addStylesheet($name . "/" . $file);
                }
            }



            // include class file
            include_once("modules/" . $name . "/module.php");

            $module = new $name($this, $function, $argv);


            // if POST data should be processed automatically
            if ($callPost) {
                if (method_exists($module, "post")) {
                    $module -> post($argv);
                }
            }


            // if no function is passed, simply return module instance.
            if (is_null($function)) {
                return $module;
            }
            // Otherwise call either the specified method...
            elseif (method_exists($module, $function)) {
                return $module -> $function($argv);
            }
            // ...or return false because that method does not exist.
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }



    // object of DB
    public function getDb() {
        return $this->db;
    }


    // string Returns Request URI
    public function url() {
        return $this->url;
    }


    // string Returns Admin's E-Mail-Address from table:aditur_config.
    public function getAdminsEmailAddress() {
        return $this->config("admin_email_address");
    }


    // string This method returns working directory
    public function pwd() {
            return $this->pwd;
    }

    public function path($path) {
        return $this->pwd . $path;
    }


    // This method enables to include from ServerRoot from anywhere.
    public function aditur_include($path) {
        include($path);
    }



    // With this method error messages for the user are collected...
    public function error($error) {
        $this->errors[] = $error;
    }

    // ...and rendered into a red box.
    public function printErrorBox() {
        ?>

        <!-- ErrorBox -->
        <section class="ErrorBox Group" id="errorBox" style="display: <?php echo count($this->errors) == 0 ? "none" : "block"; ?>">
            <i class="fa fa-lg fa-times-circle Weak"></i>

            <ul>
                <?php

                if (count($this->errors) > 0) {
                    foreach ($this->errors as $error) {
                        ?>

                        <li><?php echo $error; ?></li>

                        <?php
                    }
                }

                ?>

            </ul>
        </section>

        <?php
    }

    // With this method neutral/success messages for the user are collected...
    public function note($note) {
        $this->notes[] = $note;
    }

    // and rendered into a green box.
    public function printNotificationBox() {
        // If notes have been set by PRG(), include them first.
        if (!empty($_SESSION['notes'])) {
            $this->notes = array_merge($_SESSION['notes'], $this->notes);

            unset($_SESSION['notes']);
        }


        ?>

        <!-- NotificationBox -->
        <section class="NotificationBox Group" id="notificationBox" style="display: <?php echo count($this->notes) == 0 ? "none" : "block"; ?>">
            <i class="fa fa-lg fa-check Weak"></i>

            <ul>
                <?php

                if (count($this->notes) > 0) {
                    foreach ($this->notes  as $note) {
                        ?>
                        <li><?php echo $note; ?></li>
                        <?php
                    }
                }

                ?>

            </ul>
        </section>

        <?php
    }

    // With this method log messages are collected throughout all scripts.
    // TODO: create a real good logger.
    public function log($logMessage) {
        // write to log file, including timestamp
        fwrite($this->logFile, time() . "\t" . trim($logMessage) . "\n");
    }



    // Print a common LoginForm
    public function printLoginForm() {
        ?>

        <!-- login form box -->
        <form class="LoginForm" method="post" action="/headquarter" name="loginform">
            <?php $this->printErrorBox(); ?>

            <input id="login_input_username" class="login_input" type="email" value="<?php if (isset($_POST['user_name'])) echo $_POST['user_name']; ?>" placeholder="E-Mail-Adresse" name="user_name" <?php if (!isset($_POST['user_name'])) echo "autofocus"; ?> required>

            <input id="login_input_password" class="login_input" type="password" name="user_password" placeholder="Passwort" autocomplete="off" <?php if (isset($_POST['user_name'])) echo "autofocus"; ?> required >

            <button type="submit" name="login">
                <i class="fa fa-lg fa-sign-in"></i>&nbsp;Login
            </button>

            <p style="text-align: center;"><br><a id="iresetPassword" href="<?php echo $this->path("/resetPassword"); ?>">Passwort vergessen</a></p>
        </form>

        <?php
    }

    // And one that appears when clicking a toggle button.
    public function printTogglingLoginForm() {
        if ($this->login->isUserLoggedIn()) {
            ?>

            <a class="GoToAditurButton" href="<?php echo $this->path("/headquarter"); ?>">Aditur</a>

            <?php
        }
        else {
            ?>

            <div class="TogglingLogin">
                <!-- login toggle -->
                <input type="checkbox" class="Expander" id="loginExpander" <?php if (isset($_POST['user_name'])) echo "checked"; ?>>
                <label for="loginExpander" class="Button" id="open">Login</label>

                <!-- login form box -->
                <form class="LoginForm" method="post" action="/headquarter" name="loginform">
                    <label for="loginExpander" class="Close"><i class="fa fa-lg fa-times-circle"></i></label>

                    <?php $this->printErrorBox(); ?>

                    <input id="login_input_username" class="login_input" type="email" value="<?php if (isset($_POST['user_name'])) echo $_POST['user_name']; ?>" placeholder="E-Mail-Adresse" name="user_name" <?php if (!isset($_POST['user_name'])) echo "autofocus"; ?> required>

                    <input id="login_input_password" class="login_input" type="password" name="user_password" placeholder="Passwort" autocomplete="off" <?php if (isset($_POST['user_name'])) echo "autofocus"; ?> required >

                    <button type="submit" name="login">
                        <i class="fa fa-lg fa-sign-in"></i>&nbsp;Login
                    </button>

                    <p style="text-align: center;"><br><a id="iresetPassword" href="<?php echo $this->path("/resetPassword"); ?>">Passwort vergessen</a></p>
                </form>
            </div>

            <?php
        }
    }



    // Check privileges by providing a key from table:aditur_rights.
    public function mayI($operation_key) {
        if (!isset($_SESSION['privileges'])) {
            return false;
        }

        $result = $this->db->prepare("SELECT privileges, komitee from aditur_rights WHERE operation_key='$operation_key'");
        $result -> execute();
        $result -> bind_result($requiredPrivileges, $requiredKomitee);
        $result -> fetch();

        return $this->checkPrivileges($requiredPrivileges, $requiredKomitee);
    }

    // Or provide them yourself.
    //komitee must be checked for 1 and 0 because both stands for no komitee
    public function checkPrivileges($requiredPrivileges, $requiredKomitee = 0) {
        $privilege = $_SESSION['privileges'];

        switch ($requiredPrivileges) {
            case "normal":
                if ($privilege === "normal" && ($_SESSION['komitee'] == $requiredKomitee || $requiredKomitee == 0 || $requiredKomitee == 1)) return true;
            case "speaker":
                if ($privilege === "speaker" && ($_SESSION['komitee'] == $requiredKomitee || $requiredKomitee == 0 || $requiredKomitee == 1)) return true;
            case "president":
                if ($privilege === "president") return true;
            case "administrator":
                if ($privilege === "administrator") return true;
        }

        return false;
    }



    // Send an HTML Mail from Admin's Email Address
    public function sendEmail($receiver, $subject, $message) {
        $message = "<!DOCTYPE html>
        <html>
            <head>
                <meta charset=\"utf-8\">
            </head>
            <body>"
                . $message .
            "</body>
        </html>";


        $header  = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $header .= 'From: ' . $this->config("admin_email_address") . "\r\n";
        $header .= 'Reply-To: ' . $this->config("admin_email_address");


        return mail($receiver, $subject, $message, $header);
    }


    // image object Provide a function to create an image object from JPG, PNG or GIF respectively.
    public function imagecreatefromfile($filename) {
        if (!file_exists($filename)) {
            // throw new InvalidArgumentException('File "'.$filename.'" not found.');
            return;
        }
        switch (getimagesize($filename)['mime']) {
            case 'image/jpeg':
            case 'image/jpg':
                return imagecreatefromjpeg($filename);

            case 'image/png':
                return imagecreatefrompng($filename);

            case 'image/gif':
                return imagecreatefromgif($filename);

            default:
                // throw new InvalidArgumentException('File "'.$filename.'" is not valid jpg, png or gif image.');

                $this->error("Dieses Bildformat wird nicht unterstützt. Bitte lade deine Bilder als JPG oder PNG hoch!");

                return false;
        }
    }


    // Implement POST/Redirect/GET model
    public function PRG() {
        // by saving notes in SESSION and redirecting to the same URI.
        $_SESSION['notes'] = $this->notes;

        header('Location: ' . $this->url());
    }


    public function __destruct() {
        // close DB connection
        // $this -> db -> close();

        // close log file
        fclose($this->logFile);
    }
}


?>
