<?php
// Module: Template


class Boxes extends Module {
    // Default state when called by Aditur from URL
    public function fromUrl($argv) {
        ?>

        <section class="Module Boxes">
            <h2><i class="fa fa-lg fa-th-large"></i>&nbsp;Boxen</h2>

        <?php

        if (!empty($argv)) {
            $this->renderBox($argv[0]);
        }
        else {
            $this->renderIndex();
        }

        ?>

        </section>

       <?php
    }


    protected function op() {
        if (isset($_POST['entry_operation'])){
            if (!empty($_POST['new_entry']) && !empty($_POST['box_id'])) {
                if ($_POST['entry_operation'] == "submit") {
                    // check if box exists
                    $query = $this->db->prepare("SELECT id FROM aditur_boxes WHERE id=?");
                    $query -> bind_param('i', $_POST['box_id']);
                    $query -> bind_result($id);
                    $query -> execute();

                    $query -> store_result();

                    if ($query -> num_rows == 0) {
                        $query -> close();

                        return;
                    }

                    $query -> fetch();

                    $query -> close();


                    $entry = htmlspecialchars($_POST['new_entry']);

                    $query = $this -> db -> prepare("INSERT INTO aditur_box_entries (author_id, box_id, text) VALUES (?, ?, ?)");
                    $query -> bind_param('iis', $_SESSION['id'], $id, $entry);

                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);

                        $this->aditur->error("Dein Eintrag konnte leider nicht gespeichert werden. Bitte wende Dich an den Admin!");

                        return;
                    }

                    $this->aditur->note("Eintrag wurde gespeichert. Danke für Deine Kreativität!");
                }
            }
        }

        // delete entry
        if ($this->aditur->mayI("deleteBoxEntries") && isset($_POST['delete_entry_operation']) && !empty($_POST['entry_id'])) {
            $query = $this->db->prepare("DELETE FROM aditur_box_entries WHERE id=?");
            $query -> bind_param('i', $_POST['entry_id']);

            if ($query -> execute()) {
                $this->aditur->note("Eintrag gelöscht.");
            }
            else {
                $this->aditur->error("Eintrag konnte nicht gelöscht werden.");
            }

            $query -> close();
        }



        // create new box
        if ($this->aditur->mayI("createBoxes") && isset($_POST['new_box_operation']) && !empty($_POST['new_box_name'])) {
            $name = htmlspecialchars($_POST['new_box_name']);
            $isSecret = isset($_POST['new_box_is_secret']) ? true : false;

            $query = $this->db->prepare("INSERT INTO aditur_boxes (creator_id, name, secret, closed) VALUES (?, ?, ?, 0)");
            $query -> bind_param('isi', $_SESSION['id'], $name, $isSecret);

            if ($query -> execute()) {
                $this->aditur->note("Die Box wurde erstellt.");
            }
            else {
                $this->aditur->error("Die Box konnte nicht erstellt werden. Bitte wende Dich an den Admin.");
            }
        }


        // delete box
        if ($this->aditur->mayI("createBoxes") && isset($_POST['delete_box_operation']) && !empty($_POST['box_id'])) {
            // check if box is closed
            $query = $this->db->prepare("SELECT closed FROM aditur_boxes WHERE id=?");
            $query -> bind_param('i', $_POST['box_id']);
            $query -> execute();
            $query -> bind_result($isClosed);

            $query -> store_result();

            if ($query -> num_rows == 0) {
                $query -> close();

                return;
            }

            $query -> fetch();

            $query -> close();


            if (!$isClosed) {
                $this->aditur->error("Eine offene Box kann nicht gelöscht werden. Bitte schließe sie erst oder wende Dich an den Admin.");

                return;
            }


            $query = $this->db->prepare("DELETE FROM aditur_boxes WHERE id=?");
            $query -> bind_param('i', $_POST['box_id']);

            if ($query -> execute()) {
                $this->aditur->note("Die Box wurde gelöscht.");
            }
            else {
                $this->aditur->error("Die Box konnte nicht gelöscht werden. Bitte wende Dich an den Admin.");
            }
        }


        // open box
        if ($this->aditur->mayI("createBoxes") && isset($_POST['open_box_operation']) && !empty($_POST['box_id'])) {
            $query = $this->db->prepare("UPDATE aditur_boxes SET closed=0 WHERE id=?");
            $query -> bind_param('i', $_POST['box_id']);

            if ($query -> execute()) {
                $this->aditur->note("Box wurde geöffnet.");
            }
            else {
                $this->aditur->error("Die Box konnte nicht geöffnet werden. Bitte wende Dich an den Admin.");
            }
        }


        // close box
        if ($this->aditur->mayI("createBoxes") && isset($_POST['close_box_operation']) && !empty($_POST['box_id'])) {
            $query = $this->db->prepare("UPDATE aditur_boxes SET closed=1 WHERE id=?");
            $query -> bind_param('i', $_POST['box_id']);

            if ($query -> execute()) {
                $this->aditur->note("Box wurde geschlossen.");
            }
            else {
                $this->aditur->error("Die Box konnte nicht geschlossen werden. Bitte wende Dich an den Admin.");
            }
        }
    }


    protected function renderIndex() {
        $this->op();


        $mayICreateBoxes = $this->aditur->mayI("createBoxes");

        $mayISeeBoxEntries = $this->aditur->mayI("seeBoxEntries");
        $mayIDeleteBoxEntries = $this->aditur->mayI("deleteBoxEntries");


        if ($mayICreateBoxes) {
            ?>

            <div class="Box">
                <h3>Neue Box erstellen</h3>

                <form class="SubmitNewBoxForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <p>Boxen ermöglichen die Sammlung von Inhalten.</p>

                    <input type="text" name="new_box_name" placeholder="Titel der Box" required>

                    <p><input type="checkbox" id="newBoxIsPrivate" name="new_box_is_secret"><label for="newBoxIsPrivate">Geschlossene Box. Inhalte sind nicht öffentlich sichtbar.</label></p>

                    <button type="submit" name="new_box_operation"><i class="fa fa-lg fa-upload"></i>&nbsp;Erstellen</button>
                </form>
            </div>

            <?php
        }


        $query = $this->db->query("SELECT id, name, creator_id, secret, closed FROM aditur_boxes");

        $boxes = array();
        while ($object = $query -> fetch_object()) {
            $boxes[] = $object;
        }

        if (empty($boxes)) {
            ?>

            <section class="Box">
                <p class="Empty">Noch keine Boxen erstellt.</p>
            </section>

            <?php
        }
        else {
            // enumerate Boxes
            $nBoxes = 0;

            foreach($boxes as $box) {
                ?>

                <div class="Box">
                    <h3><?php echo $box->name; ?></h3>

                    <?php

                    if (!$box->secret || $mayISeeBoxEntries || $_SESSION['id'] == $box->creator_id) {
                        $query = $this->db->query("SELECT id, text FROM aditur_box_entries WHERE box_id=" . $box->id);

                        ?>

                        <ul>
                            <?php

                            $nEntries = 0;

                            while ($entry = $query -> fetch_object()) {
                                ?>

                                <li><p><?php echo $entry->text; ?></p><?php
                                if ($mayIDeleteBoxEntries) {
                                    ?>
                                    <input type="checkbox" class="Expander EntryExpander" id="entryExpander<?php echo $box->id . $nEntries; ?>">
                                    <label class="Delete Toggler" for="entryExpander<?php echo $box->id . $nEntries; ?>">
                                        <i class="fa fa-lg fa-times-circle"></i>
                                    </label>
                                    <form class="DeleteEntryForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                                        <label class="Close" for="entryExpander<?php echo $box->id . $nEntries; ?>">
                                            <i class="fa fa-lg fa-times-o"></i>
                                        </label>

                                        <input type="hidden" name="entry_id" value="<?php echo $entry->id; ?>">

                                        <p>Diesen Eintrag wirklich löschen?</p>

                                        <button type="submit" name="delete_entry_operation">
                                            <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                                        </button>
                                    </form>

                                    <?php
                                }

                                ?>
                                </li>

                                <?php

                                $nEntries++;
                            }


                            if ($nEntries === 0) {
                                ?>

                                <li><p class="Empty">Bisher wurde noch kein Eintrag verfasst.</p></li>

                                <?php
                            }

                            ?>
                        </ul>

                        <?php
                    }

                    if ($box->closed && $mayICreateBoxes) {
                        ?>

                        <p class="Closed">Box geschlossen.</p>


                        <form class="OpenBoxForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                            <input type="hidden" name="box_id" value="<?php echo $box->id; ?>">

                            <button type="submit" name="open_box_operation">
                                <i class="fa fa-lg fa-envelope-open"></i>&nbsp;Öffnen
                            </button>
                        </form>


                        <div class="Control">
                            <input type="checkbox" class="Expander" id="deleteBoxExpander<?php echo $nBoxes; ?>">
                            <label class="Button ToggleButton" for="deleteBoxExpander<?php echo $nBoxes; ?>">
                                <i class="fa fa-lg fa-trash-o"></i>&nbsp;Löschen
                            </label>

                            <form class="DeleteBoxForm SubmitForm Expandable" action="<?php echo $this->aditur->url(); ?>" method="post">
                                <input type="hidden" name="box_id" value="<?php echo $box->id; ?>">

                                <p>Willst Du diese Box wirklich löschen?</p>

                                <button type="submit" name="delete_box_operation">
                                    <i class="fa fa-lg fa-check"></i>&nbsp;Bestätigen
                                </button>
                            </form>
                        </div>


                        <?php
                    }
                    else if ($box->closed) {
                        ?>

                        <p class="Closed">Box geschlossen.</p>

                        <?php
                    }
                    else {
                        ?>

                        <!-- EntrySubmitForm -->
                        <form class="SubmitForm EntrySubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                            <input type="hidden" name="box_id" value="<?php echo $box->id; ?>">

                            <div>
                                <textarea name="new_entry" placeholder="Ein guter Eintrag wäre..." required></textarea>
                                <button type="submit" name="entry_operation" value="submit">
                                    <i class="fa fa-lg fa-check"></i>&nbsp;Senden
                                </button>
                            </div>
                        </form>

                        <?php

                        if ($mayICreateBoxes) {
                            ?>

                            <form class="CloseBoxForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                                <input type="hidden" name="box_id" value="<?php echo $box->id; ?>">

                                <button type="submit" name="close_box_operation">
                                    <i class="fa fa-lg fa-envelope"></i>&nbsp;Schließen
                                </button>
                            </form>

                            <?php
                        }
                    }

                    ?>

                </div>

                <?php

                $nBoxes++;
            }
        }
    }


    public function renderBox($name) {
        $this->op();


        $mayISeeBoxEntries = $this->aditur->mayI("seeBoxEntries");
        $mayIDeleteBoxEntries = $this->aditur->mayI("deleteBoxEntries");


        $query = $this->db->query("SELECT id, name, creator_id, secret, anonymous FROM aditur_boxes WHERE name=" . $name);

        if (!$object = $query -> fetch_object()) {

            return;
        }

        $box = $object;

        ?>

        <div class="Box">
            <h3><?php echo $box->name; ?></h3>

            <?php

            if (!$box->secret || $mayISeeBoxEntries || $_SESSION['id'] == $box->creator_id) {
                $query = $this->db->query("SELECT id, text FROM aditur_box_entries WHERE box_id=" . $box->id);

                ?>

                <ul>
                    <?php

                        $nEntries = 0;
                        while ($entry = $query -> fetch_object()) {
                            ?>

                            <li><p><?php echo $entry->text; ?></p><?php
                            if ($mayIDeleteBoxEntries) {
                                ?>
                                <input type="checkbox" class="Expander EntryExpander" id="entryExpander<?php echo $nEntries; ?>">
                                <label class="Delete Toggler" for="entryExpander<?php echo $nEntries; ?>">
                                    <i class="fa fa-lg fa-times-circle"></i>
                                </label>
                                <form class="DeleteEntryForm SubmitForm" action="<?php echo $this->aditur->url() . "#entrybox"; ?>" method="post">
                                    <label class="Close" for="entryExpander<?php echo $nEntries; ?>">
                                        <i class="fa fa-lg fa-times-o"></i>
                                    </label>

                                    <input type="hidden" name="entry_id" value="<?php echo $object->id; ?>">

                                    <p>Diesen Eintrag wirklich löschen?</p>

                                    <button type="submit" name="delete_entry_operation">
                                        <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                                    </button>
                                </form>
                                <?php
                            }

                            ?>
                            </li>

                            <?php

                            $nEntries++;
                        }


                        if ($nEntries === 0) {
                            ?>

                            <li><p class="Empty">Bisher wurde noch kein Eintrag verfasst.</p></li>

                            <?php
                        }

                        ?>
                </ul>

            <?php
            }

            if ($box->closed && $mayICreateBoxes) {
                ?>

                <p class="Closed">Box geschlossen.</p>

                <div class="Controls">
                    <form class="OpenBoxForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input type="hidden" name="box_id" value="<?php echo $box->id; ?>">

                        <button type="submit" name="open_box_operation">
                            <i class="fa fa-lg fa-envelope-open"></i>&nbsp;Öffnen
                        </button>
                    </form>

                    <form class="DeleteBoxForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input type="hidden" name="box_id" value="<?php echo $box->id; ?>">

                        <button type="submit" name="delete_box_operation">
                            <i class="fa fa-lg fa-times-circle"></i>&nbsp;Löschen
                        </button>
                    </form>
                </div>

                <?php
            }
            else if ($box->closed) {
                ?>

                <p class="Closed">Box geschlossen.</p>

                <?php
            }
            else {
                ?>

                <!-- EntrySubmitForm -->
                <form class="SubmitForm EntrySubmitForm" action="<?php echo $this->aditur->url() . "#entrybox"; ?>" method="post">
                    <div>
                        <textarea name="new_entry" placeholder="Ein guter Eintrag wäre..." required></textarea>
                        <button type="submit" name="entry_operation" value="submit">
                            <i class="fa fa-lg fa-check"></i>&nbsp;Senden
                        </button>
                    </div>
                </form>

                <?php

                if ($mayICreateBoxes) {
                    ?>

                    <form class="CloseBoxForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input type="hidden" name="box_id" value="<?php echo $box->id; ?>">

                        <button type="submit" name="close_box_operation">
                            <i class="fa fa-lg fa-envelope"></i>&nbsp;Schließen
                        </button>
                    </form>

                    <?php
                }
            }

            ?>

        </div>

        <?php
    }
}

?>
