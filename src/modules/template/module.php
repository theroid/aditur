<?php
// Module: Template


// Every module that should be accessible, must be in table:aditur_modules.
class Template extends Module {
    // when the module is requested by the user:
    public function fromUrl($argv) {
        if (!empty($argv) && $argv[0] == "detail") {
            $this->showDetail($argv[1]);
        }
        else {
            $this->displayIndex();
        }
    }


    // process POST Data
    // @return true on success, false on error
    public function post() {
        $success = false;

        if (isset($_POST['certain_operation'])) {
            // check necessary POST-variables
            if (empty($_POST['id'])) return false;


            $success = $this->doSomething($_POST['id']);

            unset($_POST['certain_operation']);
        }


        return $success;
    }

    private function doSomething($id) {
        $q = $this -> db -> prepare("SELECT forename, lastname FROM aditur_members WHERE id=?");
        $q -> bind_param('i', $id);
        $q -> bind_result($forename, $lastname);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Konnte ID nicht verarbeiten. Bitte wende Dich an den Admin.");

            return false;
        }

        $q -> fetch();

        $q -> close();


        $this->aditur->note("User ID: " . $id . " = " . $forename . " " . $lastname);

        return true;
    }


    private function showDetail($param) {
        ?>

        <h2>Template</h2>

        <div class="Box">
            <h3>Param:</h3>

            <p class="Param"><?php echo $param; ?></p>
        </div>


        <?php
    }


    private function displayIndex() {
        ?>

        <h2>Template</h2>

        <form class="SubmitParamForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
            <p>
                <label for="id"><i class="fa fa-lg fa-user"></i>&nbsp;ID: </label>
                <input type="number" name="id" id="id" value="" placeholder="id" required>
            </p>

            <button type="submit" name="certain_operation">
                <i class="fa fa-lg fa-check"></i>&nbsp;Absenden
            </button>
        </form>

        <?php
    }
}

?>
