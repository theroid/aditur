<?php
// Module: Downloads


class Downloads extends Module {
    public function fromUrl($argv) {
        if (empty($argv)) {
            ?>

            <h2>Downloads</h2>

            <?php

            if ($this->aditur->mayI("uploadFiles")) {
                $this->printUploadForm();
            }

            $this->displayIndex();
        }
        else {
            $this->deliver($argv[0]);
        }
    }

    protected function printUploadForm() {
        $this->op();

        ?>

        <section class="Settings Box">
            <h3>Neue Datei hochladen</h3>

            <form class="SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="new_download_file">

                <?php

                if ($this->aditur->checkPrivileges("president")) {
                    ?>
                    <div class="UploadForm">
                        <select class="UploadFormLeft" name="privileges" required>
                            <option value="" disabled="" selected="" hidden="">Benutzerrechte</option>
                            <option value="normal">Alle</option>
                            <option value="speaker">Komiteesprecher</option>
                            <option value="president">Jahrgangssprecher</option>

                            <?php if ($this->aditur->checkPrivileges("administrator")){
                                ?>

                                <option value="administrator">Administrator</option>

                                <?php
                            }

                            ?>

                        </select>

                        <select class="UploadFormRight" name="komitee" required>
                            <option value="" disabled="" selected="" hidden="">Komitee</option>
                            <option value="1">Alle</option>
                            <?php

                            $query = $this->db->query("SELECT id, name FROM aditur_komitees");
                            $object = $query -> fetch_object();

                            while ($object = $query -> fetch_object()) {
                                ?>
                                    <option value="<?php echo $object->id; ?>">
                                        <?php echo $object->name; ?>
                                    </option>
                                    <?php
                            }

                            ?>

                        </select>
                    </div>

                    <input type="text" name="directory" list="dir" placeholder="Verzeichnis (optional)">

                    <datalist id="dir">
                        <?php

                        $query = $this->db->query("SELECT DISTINCT directory FROM aditur_downloads");

                        while ($object = $query -> fetch_object()) {
                            ?>

                            <option value="<?php echo $object->directory; ?>">

                            <?php
                        }

                        ?>

                    </datalist>

                    <?php
                }
                else {
                    ?>

                    <input type="hidden" name="privileges" value="o">
                    <input type="hidden" name="komitee" value="o">
                    <input type="hidden" name="directory" value="o">

                    <p>Du kannst nur Dateien für dein Komitee freigeben.</p>

                    <?php
                }

                ?>

                <button type="submit" name="upload_file_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Hochladen
                </button>
            </form>
        </section>

        <?php
    }

    protected function displayIndex() {
        ?>

        <section class="Downloads">

        <?php

        // select all file entrys from database
        $query = $this->db->query("SELECT id, link, path, privileges, komitee, directory FROM aditur_downloads ORDER BY directory");

        // write all files the user is allowed to see in an array grouped by the directory
        $directories = [];
        while ($row = $query->fetch_object()) {
            if ($this->aditur->checkPrivileges($row->privileges, $row->komitee) && file_exists($row->path)) {
                $dir = $row->directory;
                $directories[$dir][] = $row;
            }
        }

        // say if there are no files
        if (count($directories) == 0) {
            ?>

            <div class="Box">
                <p class="Empty">Derzeit gibt es keine Downloads.</p>
            </div>

            <?php
        }

        // otherwise list the files
        foreach($directories as $current_dir => $dir_rows) {
            ?>

            <h3 class="Flex"><?php echo $current_dir; ?></h3>

            <div class="Flex">
                <?php

                foreach($dir_rows as $dir) {

                    // retrieve file type to set icon appropriately
                    $mime = mime_content_type($dir -> path);

                    $type = explode("/", $mime)[0];

                    $icon = "";

                    switch ($type) {
                        case "image":
                            $icon = "file-image-o";

                            break;
                        case "video":
                            $icon = "file-video-o";

                            break;
                        case "text":
                            $icon = "file-text";

                            break;
                        case "application":
                            if ($mime == "application/pdf") {
                                $icon = "file-pdf-o";
                            }
                            else {
                                $icon = "file";
                            }

                            break;
                        default:
                            $icon = "file-o";
                    }

                    ?>

                    <a class="Box File" href="<?php echo $this->aditur->path("/downloads/" . $dir->link); ?>">
                        <div class="">
                            <?php

                            // if you are an admin, display form to delete file
                            if ($this->aditur->mayI("deleteFile")) {
                                ?>

                                <div class="DeleteFile">
                                    <input type="checkbox" class="Expander" id="deleteFileExpander<?php echo $dir->id; ?>">
                                    <label for="deleteFileExpander<?php echo $dir->id; ?>" class="ToggleButton Delete HideByExpander">
                                        <i class="fa fa-lg fa-times-circle-o Close"></i>
                                    </label>

                                    <form class="DeleteFileForm SubmitForm Expandable" action="<?php echo $this->aditur->url(); ?>" method="post">
                                        <label for="deleteFileExpander<?php echo $dir->id; ?>" class="ToggleButton Delete">
                                            <i class="fa fa-lg fa-times-circle-o"></i>
                                        </label>

                                        <input type="hidden" name="file_id" value="<?php echo $dir->id; ?>">

                                        <p>Diese Datei wirklich löschen?</p>

                                        <button type="submit" name="delete_file_operation">
                                            <i class="fa fa-lg fa-trash-o"></i>&nbsp;Löschen
                                        </button>
                                    </form>
                                </div>

                                <div class="EditFile">
                                    <input type="checkbox" class="Expander" id="editFileExpander<?php echo $dir->id; ?>">
                                    <label for="editFileExpander<?php echo $dir->id; ?>" class="ToggleButton Edit HideByExpander">
                                        <i class="fa fa-lg fa-pencil"></i>
                                    </label>

                                    <form class="DeleteFileForm SubmitForm Expandable" action="<?php echo $this->aditur->url(); ?>" method="post">
                                        <label for="editFileExpander<?php echo $dir->id; ?>" class="ToggleButton EditClose">
                                            <i class="fa fa-lg fa-times-circle-o"></i>
                                        </label>

                                        <input type="hidden" name="file_id" value="<?php echo $dir->id; ?>">

                                        <select name="privileges" required>
                                            <?php

                                            $right = array("normal"=>"Alle", "speaker"=>"Komiteesprecher", "president"=>"Jahrgangssprecher", "administrator"=>"Administrator")

                                            ?>

                                            <option value="<?php echo $dir->privileges; ?>" selected="" hidden="">
                                                <?php echo $right[$dir->privileges]; ?>
                                            </option>
                                            <option value="normal">Alle</option>
                                            <option value="speaker">Komiteesprecher</option>

                                            <option value="president">Jahrgangssprecher</option>
                                            <?php
                                            if ($this->aditur->checkPrivileges("administrator")){
                                                ?>

                                                <option value="administrator">Administrator</option>

                                                <?php
                                            }

                                            ?>

                                        </select>

                                        <select name="komitee" required>
                                            <option value="<?php echo $dir->komitee; ?>" selected="" hidden="">
                                                <?php if($dir->komitee==1){echo "Alle";}else{echo $this->db->query("SELECT name FROM aditur_komitees WHERE id=$dir->komitee") -> fetch_object() -> name;} ?>
                                            </option>
                                            <option value="1">Alle</option>

                                            <?php

                                            $query = $this->db->query("SELECT id, name FROM aditur_komitees");
                                            $object = $query -> fetch_object();

                                            while ($object = $query -> fetch_object()) {
                                                ?>

                                                <option value="<?php echo $object->id; ?>">
                                                    <?php echo $object->name; ?>
                                                </option>

                                                <?php
                                            }

                                            ?>
                                        </select>

                                        <input type="text" name="directory" list="dir" value="<?php echo $dir->directory; ?>" placeholder="Verzeichnis (optional)">

                                        <button type="submit" name="changeFile">
                                            <i class="fa fa-lg fa-pencil"></i>Ändern
                                        </button>
                                    </form>
                                </div>

                                <?php
                            }

                            ?>

                            <div class="Icon">
                                <i class="fa fa-lg fa-<?php echo $icon; ?>"></i>
                            </div>
                            <div class="Name">
                                <?php echo $dir->link; ?>
                            </div>
                        </div>
                    </a>

                    <?php
                }

                ?>

            </div>

            <?php
        }

        ?>

        </section>

        <?php
    }

    private function op() {
        if ($this->aditur->mayI("uploadFiles") && isset($_POST['upload_file_operation']) && !empty($_FILES['new_download_file']['tmp_name']) && isset($_POST['privileges']) && isset($_POST['komitee'])) {

            //remove special chars from the filename
            //only the filename without extension is used and "." is replaced so multiple extensions aren't possible
            $filename = htmlspecialchars(str_replace(array('/', '\\', '\"', ';', '(', ')', '.'), "", pathinfo($_FILES['new_download_file']['name'], PATHINFO_FILENAME)));

            //president & administrator can choose options
            if ($this->aditur->checkPrivileges("president")) {

                //check if komitee exists
                $komitee = $_POST['komitee'];
                $query = $this->db->prepare("SELECT name FROM aditur_komitees WHERE id=?");
                $query -> bind_param("i", $komitee);
                $query -> execute();
                $query -> store_result();
                if ($query->num_rows !== 1) {
                    $this->aditur->error("Datei konnte nicht gespeichert werden.");
                    return;

                }
                $query -> close();

                //check if privilege exists
                $privilege = $_POST['privileges'];
                $validprivileges = array("normal", "speaker", "president", "administrator");
                if (!in_array($privilege, $validprivileges)) {
                    $this->aditur->error("Datei konnte nicht gespeichert werden.");
                    return;
                }
                //set directory
                if ($komitee != 1 && ($privilege == "normal" || $privilege == "speaker")) {
                    $directory = $this->db->query("SELECT name FROM aditur_komitees WHERE id=$komitee") -> fetch_object() -> name . "-Komitee";
                }
                else if (!empty($_POST['directory'])) {
                    //remove special chars from the directory
                    $directory = htmlspecialchars(str_replace(array('/', '\\', '\"', '..', ';', '(', ')'), "", $_POST['directory']));
                }
                else {
                    $directory = "";
                }
            }
            //speaker can only submit for his komitee
            else {
                $komitee = $_SESSION['komitee'];
                $privilege = "normal";
                $directory = $this->db->query("SELECT name FROM aditur_komitees WHERE id=$komitee") -> fetch_object() -> name . "-Komitee";
            }


            //check the filetype and protect from malicious files
            $valid_file_types = array(//Bilder
                                      "png" => "image/png",                                                              //.png
                                      "jpg" => "image/jpeg",                                                             //.jpeg .jpg
                                      "pdf" => "application/pdf",                                                        //.pdf
                                      //Dokumente
                                      "doc" => "application/msword",                                                     //.doc
                                      "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",//.docx
                                      "odt" => "application/vnd.oasis.opendocument.text",                                //.odt
                                      //Tabellen
                                      "xls" => "application/vnd.ms-excel",                                               //.xls
                                      "xlsx" => "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",     //.xlsx
                                      "ods" => "application/vnd.oasis.opendocument.spreadsheet",                         //.ods
                                      //Video
                                      "mp4" => "video/mp4",                                                              //.mp4
                                      //Audio
                                      "mp3" => "audio/mpeg",                                                             //.mp3
                                     );

            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $filetype = finfo_file($finfo, $_FILES['new_download_file']['tmp_name']);
            if (!in_array($filetype, $valid_file_types)) {
                $this->aditur->error("Unzulässige Datei!");
                return;
            }

            //make sure that the file has the right extension
            $extension = array_search($filetype, $valid_file_types);
            $basename = $filename.".".$extension;
            $path = "data/downloads/".$basename;


            //save file at data/downloads
            if(!move_uploaded_file($_FILES['new_download_file']['tmp_name'], $path)) {
                $this->aditur->log("ERROR move_uploaded_file returned false!");

                $this->aditur->error("Datei konnte nicht gespeichert werden.");
                return;
            }

            //write file-entry to the database
            $query = $this->db->prepare("INSERT INTO aditur_downloads (link, path, directory, privileges, komitee) VALUES (?, ?, ?, ?, ?)");
            $query -> bind_param("ssssi", $basename, $path, $directory, $privilege, $komitee);
            if(!$query -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Datei konnte nicht gespeichert werden.");
                return;
            }

            $this->aditur->note("Datei wurde gespeichert.");
        }

        if ($this->aditur->mayI("deleteFile") && isset($_POST['changeFile']) && isset($_POST['file_id']) && isset($_POST['directory']) && isset($_POST['privileges']) && isset($_POST['komitee'])) {

            //check if file exists
            $file_id = $_POST['file_id'];
            $query = $this->db->prepare("SELECT privileges FROM aditur_downloads WHERE id=?");
            $query->bind_param("i", $file_id);
            $query->execute();
            $query -> store_result();
            if ($query->num_rows !== 1) {
                $this->aditur->error("Datei konnte nicht geändert werden.");
                return;
            }
            $query->bind_result($fileprivileges);
            $query->fetch();
            //and if the user has the right to change it
            if (!$this->aditur->checkPrivileges($fileprivileges)) {
                $this->aditur->error("Datei konnte nicht geändert werden.");
                return;
            }
            $query -> close();


            //check if komitee exists
            $komitee = $_POST['komitee'];
            $query = $this->db->prepare("SELECT name FROM aditur_komitees WHERE id=?");
            $query -> bind_param("i", $komitee);
            $query -> execute();
            $query -> store_result();
            if ($query->num_rows !== 1) {
                $this->aditur->error("Datei konnte nicht geändert werden.");
                return;
            }
            $query -> close();

            //check if privilege exists
            $privilege = $_POST['privileges'];
            $validprivileges = array("normal", "speaker", "president", "administrator");
            if (!in_array($privilege, $validprivileges)) {
                $this->aditur->error("Datei konnte nicht geändert werden.");
                return;
            }
            //set directory
            if ($komitee != 1 && ($privilege == "normal" || $privilege == "speaker")) {
                $directory = $this->db->query("SELECT name FROM aditur_komitees WHERE id=$komitee") -> fetch_object() -> name . "-Komitee";
            }
            else if (!empty($_POST['directory'])) {
                //remove special chars from the directory
                $directory = htmlspecialchars(str_replace(array('/', '\\', '\"', '..', ';', '(', ')'), "", $_POST['directory']));
            }
            else {
                $directory = "";
            }

            //Update database entry
            $query = $this->db->prepare("UPDATE aditur_downloads SET privileges=?, komitee=?, directory=? WHERE id=?");
            $query -> bind_param("sisi", $privilege, $komitee, $directory, $file_id);
            if(!$query -> execute()) {
                $this->aditur->error("Datei konnte nicht geändert werden.");
                return;
            }

            $this->aditur->note("Datei wurde geändert.");

        }

        if ($this->aditur->mayI("deleteFile") && isset($_POST['delete_file_operation']) && isset($_POST['file_id'])) {

            //check if file exists
            $file_id = $_POST['file_id'];
            $query = $this->db->prepare("SELECT privileges, path FROM aditur_downloads WHERE id=?");
            $query->bind_param("i", $file_id);
            if (!$query -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Datei konnte nicht gelöscht werden.");

                return;
            }
            $query -> store_result();
            if ($query->num_rows !== 1) {
                $this->aditur->error("Datei konnte nicht gelöscht werden.");
                return;
            }
            $query->bind_result($fileprivileges, $path);
            $query->fetch();
            //and if the user has the right to change it
            if (!$this->aditur->checkPrivileges($fileprivileges)) {
                $this->aditur->error("Datei konnte nicht gelöscht werden.");
                return;
            }
            $query->free_result();
            $query -> close();

            //delete database entry
            $delquery = $this->db->prepare("DELETE FROM aditur_downloads WHERE id=?");
            $delquery->bind_param("i", $file_id);
            if(!$delquery -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Datei konnte nicht gelöscht werden.");
                return;
            }

            //delete File
            if (file_exists($path)) {
                if (!unlink($path)) {
                    $this->aditur->log("ERROR Datei kann nicht gelöscht werden: " . $path);

                    return;
                }
            }

            $this->aditur->note("Datei wurde gelöscht.");

        }
    }

    protected function deliver($link) {
        $query = $this->db->prepare("SELECT path, privileges, komitee FROM aditur_downloads WHERE link=?");
        $query -> bind_param('s', $link);
        $query -> bind_result($file, $privileges, $komitee);
        $query -> execute();

        $query -> store_result();

        if ($query -> num_rows == 0) {
            $query -> close();

            $this->displayIndex();

            return;
        }

        $query -> fetch();

        $query -> close();


        if (!$this->aditur->checkPrivileges($privileges, $komitee)) {
            $this->displayIndex();

            return;
        }

        if (file_exists($this->aditur->path($file))) {
            while (ob_get_level()) ob_end_clean();

            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));

            readfile($this->aditur->path($file));

            exit;
        }
        else {
            while (ob_get_level()) ob_end_clean();

            header("HTTP/1.1 404 Not Found");

            exit;
        }
    }
}


?>
