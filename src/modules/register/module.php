<?php
// Module: register


class register extends Module {

    // Default state when called by Aditur from URL
    public function fromUrl($argv = null) {
        
        if ($this->aditur->login->isUserLoggedIn()) {
            $this->aditur->loadModule("headquarter", "fromUrl");

            return;
        }
        
        $this->op();
        
        $query = $this->db->query("SELECT id, name FROM aditur_komitees");

        $komitees = array();
        while ($object = $query -> fetch_object()) {
            $komitees[] = $object;
        }

        $query -> close();
        ?>
        
        <section class="NewProfile">
            <form class="CreateNewUserForm SubmitForm Box" action="<?php echo $this->aditur->url(); ?>" method="post">
                <h3>Registriere dich:</h3>
                
                <label for="forename">Vorname:</label>
                <input id="forename" type="text" name="new_profile_forename" value="" placeholder="Vorname" required>
                
                <label for="lastname">Nachname:</label>
                <input id="lastname" type="text" name="new_profile_lastname" value="" placeholder="Nachname" required>
                
                <p>ACHTUNG: Dein Name kann nur von einem Admin geändert werden! Also nimm gefälligst deinen richtigen Namen, damit die anderen auch wissen wen sie da kommentieren!</p>
                <br>
                <label for="mail">E-Mail:</label>
                <input id="mail" type="email" name="new_profile_email" value="" placeholder="E-Mail-Adresse" required>
                
                <input type="email_check" name="new_profile_email_check" value="" placeholder="E-Mail-Adresse bestätigen" required>
                
                <label for="komitee">Komitee auswählen:</label>
                <select id="komitee" name="new_profile_komitee" required>
                    <?php

        foreach ($komitees as $komitee) {
                    ?>
                    <option value="<?php echo $komitee->id; ?>"><?php echo $komitee->name; ?></option>
                    <?php
        }

                    ?>
                </select>

                <button type="submit" name="new_profile_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

    	<?php

    }


    protected function op() {
        // Profile Management
        if (isset($_POST['new_profile_operation'])
        && !empty($_POST['new_profile_forename'])
        && !empty($_POST['new_profile_lastname'])
        && !empty($_POST['new_profile_email'])
        && !empty($_POST['new_profile_email_check'])
        && !empty($_POST['new_profile_komitee'])) {
            // set privileges
            $privileges = "normal";

            // check komitee
            $query = $this->db->prepare("SELECT id FROM aditur_komitees WHERE id=?");
            $query -> bind_param('i', $_POST['new_profile_komitee']);
            $query -> execute();
            $query -> bind_result($komiteeId);
            $query -> store_result();

            if ($query -> num_rows !== 1) {
                $query -> close();

                $this->aditur->error("Das angegebene Komitee existiert nicht.");

                return;
            }

            $query -> fetch();
            $query -> close();

            if ($_POST['new_profile_email'] != $_POST['new_profile_email_check']) {
                
                $this->aditur->error("Die E-Mail Adressen stimmen nicht überein!");

                return;
            }

            // lowercase email
            $newEmail = filter_var(trim(strtolower($_POST['new_profile_email'])), FILTER_SANITIZE_EMAIL);
            $newForename = trim(htmlspecialchars($_POST['new_profile_forename']));
            $newLastname = trim(htmlspecialchars($_POST['new_profile_lastname']));

            if (!filter_var($newEmail, FILTER_VALIDATE_EMAIL)) {
                
                $this->aditur->error("Bitte gib eine gültige E-Mail ein!");

                return;
            }

            // insert values
            $query = $this->db->prepare("INSERT INTO aditur_members (forename, lastname, email, privileges, komitee) VALUES (?, ?, ?, ?, ?)");
            $query -> bind_param('sssss', $newForename,
                             $newLastname,
                             $newEmail,
                             $privileges,
                             $komiteeId);
            if (!$query -> execute()) {
                $this->aditur->error("Profil konnte nicht hinzugefügt werden. Bitte gib dem Administrator Bescheid oder check ob du dich schon registriert hast. <a href=\"http://" . $this->aditur->config("domain") . "/resetPassword/\">" . $this->aditur->config("domain") . "/resetPassword/</a>");

                return;
            }

            $query -> close();


            // send password reset email
            $query = $this->db->prepare("SELECT id, forename, email FROM aditur_members WHERE email=?");
            $query -> bind_param('s', $_POST['new_profile_email']);
            $query -> execute();
            $query -> bind_result($id, $forename, $email);
            $query -> fetch();
            $query -> close();

            $hash = md5($forename . time());
            $timestamp = time();

            $query = "INSERT INTO aditur_password_reset (user_id, token, timestamp) VALUES ($id, '$hash', $timestamp)";
            $this->db->query($query);

            $message = "<h1>Anmeldung im Aditur</h1><p><br><br>Um Dich im Aditur anmelden zu können, klick auf Diesen Link und erstelle ein Passwort!</p><a href=\"http://" . $this->aditur->config("domain") . "/resetPassword/" . $hash . "\">Passwort erstellen</a><p><br><br>Die Gültigkeit dieses Links endet in 48 Stunden. Danach klick einfach unter Login auf \"Passwort vergessen\", um Dein Passwort zu erstellen.</p>";

            if (!$this->aditur->sendEmail($email, "Aditur Registrierung", $message)) {
                $this->aditur->error("Es gab leider einen Fehler im Mailversand, bitte wende dich an einen Administrator.");
            }
            else {
                $this->aditur->note("Dein Profil wurde erstellt, bitte schau in deine Mails(auch in den Spamordner), dort müsstest du einen Link finden um dein Passwort zu setzen.");
            }
        }
    }
}



?>
