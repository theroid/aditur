<?php
// Module: Sites


class Sites extends Module {
    public function fromUrl($argv) {
        if (empty($argv)) {
            $this->showIndex();
        }
        else {
            if ($argv[0] == "create") {
                $this->createSite();
            }
            else {
                $this->showArticle($argv[0]);
            }
        }
    }


    protected function showArticle($link) {
        $query = $this->db->prepare("SELECT heading, content FROM aditur_sites WHERE link=?");
        $query -> bind_param('s', $link);
        $query -> execute();
        $query -> bind_result($heading, $content);

        if (!$query -> fetch()) {
            $query -> close();

            $this->showIndex();
        }
        else {
            $query -> close();

            ?>

            <a class="Back" href="<?php echo $this->aditur->path("/sites"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

            <h2 class="Gap"><?php echo $heading; ?></h2>

            <section class="Box">
                <article>
                    <p><?php echo $content; ?></p>
                </article>
            </section>

            <?php
        }
    }


    protected function showIndex() {
        $this->op();

        if ($this->aditur->mayI("createSite")) {
            ?>

            <a class="Back" href="<?php echo $this->aditur->path("/sites/create"); ?>"><i class="fa fa-lg fa-plus"></i>&nbsp;Neue&nbsp;Seite&nbsp;erstellen</a>

            <?php
        }

        $mayIDelete = $this->aditur->mayI("deleteSite");

        $query = $this->db->query("SELECT id, link, heading, content FROM aditur_sites ORDER BY heading");

        ?>
        <h2>Seiten</h2>

        <div class="Index">
        <?php

        while ($object = $query -> fetch_object()) {
            ?>

            <a href="<?php echo $this->aditur->path("/sites/".$object->link); ?>">
                <div class="Entry">
                    <h3><?php echo $object->heading; ?></h3>

                    <?php

                    if ($mayIDelete) {
                        ?>

                        <input type="checkbox" class="Expander DeleteSiteExpander" id="deleteSiteExpander<?php echo $object->id; ?>">
                        <label class="Toggler Close" for="deleteSiteExpander<?php echo $object->id; ?>">
                            <i class="fa fa-lg fa-trash"></i>
                        </label>
                        <form class="DeleteSiteForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                            <i class="fa fa-lg fa-chevron-up"></i>

                            <input type="hidden" name="site_id" value="<?php echo $object->id; ?>">

                            <p>Diese Seite wirklich löschen?</p>

                            <button type="submit" name="delete_site_operation">
                                <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                            </button>
                        </form>

                        <?php
                    }

                    ?>

                    <div class="Preview"><?php echo substr($object->content, 0, 111)."..."; ?></div>
                </div>
            </a>

            <?php
        }

        ?>
        </div>

        <?php

        $query -> close();
    }


    public function createSite() {
        $this->op();

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/sites"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <h3>Neue Seite erstellen</h3>

        <section class="NewSite Box">
            <form class="CreateNewSiteForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input type="text" name="new_site_heading" placeholder="Überschrift" required>

                <input type="text" name="new_site_link" placeholder="Link/URL" required>

                <textarea name="new_site_content" placeholder="Inhalt" required></textarea>

                <button type="submit" name="new_site_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

        <?php
    }


    private function op() {
        if (isset($_POST['new_site_operation']) && !empty($_POST['new_site_heading'])
            && !empty($_POST['new_site_link'])
            && !empty($_POST['new_site_content'])
            && $this->aditur->mayI("createSite")) {
            $query = $this->db->prepare("INSERT INTO aditur_sites (heading, link, content) VALUES (?, ?, ?)");
            $query -> bind_param('sss', $_POST['new_site_heading'], $_POST['new_site_link'], $_POST['new_site_content']);

            if ($query -> execute()) {
                $this->aditur->note("Seite wurde erstellt.");
            }
            else {
                $this->aditur->error("Seite konnte nicht erstellt werden. Bitte wende Dich an den Admin!");
            }

            $query -> close();
        }

        if (isset($_POST['delete_site_operation']) && !empty($_POST['site_id']) && $this->aditur->mayI("deleteSite")) {
            $query = $this->db->prepare("DELETE FROM aditur_sites WHERE id=?");
            $query -> bind_param('i', $_POST['site_id']);

            if ($query -> execute()) {
                $this->aditur->note("Seite wurde gelöscht.");
            }
            else {
                $this->aditur->error("Seite konnte nicht gelöscht werden. Bitte wende Dich an den Admin!");
            }

            $query -> close();
        }
    }
}

?>
