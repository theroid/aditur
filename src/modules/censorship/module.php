<?php
// Module: Censorship


class Censorship extends Module {
    public function fromUrl($argv) {
        $this->op();

        ?>

        <h2>Kommentare zensieren</h2>

        <div class="Censorship">

        <?php

        $mayIcensor = $this->aditur->mayI('censorComments');

        // check if censorship by komitee is enabled
        $query = $this->db->query("SELECT value FROM aditur_config WHERE config_key='censorship_komitee_enabled'");

        $censorshipKomiteeEnabled = $query -> fetch_object() -> value == 0 ? false : true;

        // check if censorship by students is enabled
        $query = $this->db->query("SELECT value FROM aditur_config WHERE config_key='censorship_students_enabled'");

        $censorshipStudentsEnabled = $query -> fetch_object() -> value == 0 ? false : true;


        if (count($argv) >= 1) {
            if ($argv[0] == "students" && $censorshipKomiteeEnabled && $mayIcensor) {
                $this->censorStudents();
            }
            else if ($argv[0] == "teachers" && $censorshipKomiteeEnabled && $mayIcensor) {
                $this->censorTeachers();
            }
            else if ($argv[0] == "fromteachers" && $censorshipKomiteeEnabled && $mayIcensor) {
                $this->censorCommentsFromTeachers();
            }
            else {
                if ($censorshipKomiteeEnabled && $mayIcensor) {
                    ?>

                    <div class="Index">
                        <a class="Box" href="<?php echo $this->aditur->path("/censorship/students"); ?>"><i class="fa fa-lg fa-users"></i>&nbsp;Schülerkommentare zensieren</a>

                        <a class="Box" href="<?php echo $this->aditur->path("/censorship/teachers"); ?>"><i class="fa fa-lg fa-graduation-cap"></i>&nbsp;Kommentare <strong>zu</strong> Lehrern zensieren</a>

                        <a class="Box" href="<?php echo $this->aditur->path("/censorship/fromteachers"); ?>"><i class="fa fa-lg fa-eye-slash"></i>&nbsp;Kommentare <strong>von</strong> Lehrern zensieren</a>
                    </div>

                    <?php
                }

                if ($censorshipStudentsEnabled) {
                    $this->censorComments($_SESSION['id'], false);

                    $this->printScript();
                }
            }
        }
        else {
            if ($censorshipKomiteeEnabled && $mayIcensor) {
                ?>

                <div class="Index">
                    <a class="Box" href="<?php echo $this->aditur->path("/censorship/students"); ?>"><i class="fa fa-lg fa-users"></i>&nbsp;Schülerkommentare zensieren</a>

                    <a class="Box" href="<?php echo $this->aditur->path("/censorship/teachers"); ?>"><i class="fa fa-lg fa-graduation-cap"></i>&nbsp;Kommentare <strong>zu</strong> Lehrern zensieren</a>

                    <a class="Box" href="<?php echo $this->aditur->path("/censorship/fromteachers"); ?>"><i class="fa fa-lg fa-eye-slash"></i>&nbsp;Kommentare <strong>von</strong> Lehrern zensieren</a>
                </div>

                <?php
            }

            if ($censorshipStudentsEnabled) {
                $this->censorComments($_SESSION['id'], false);

                $this->printScript();
            }
        }

        ?>

        </div>

        <?php
    }


    protected function op() {
        // for Students
        if ($this->aditur->login->isUserLoggedIn()) {
            if (isset($_POST['censor_comment_operation']) && !empty($_POST['comment_id']) && !empty($_POST['reason'])) {
                $query = $this->db->prepare("SELECT comment_on_id FROM aditur_comments WHERE id=?");
                $query -> bind_param('i', $_POST['comment_id']);
                $query -> execute();
                $query -> bind_result($comment_on_id);
                $query -> store_result();


                if ($query -> num_rows !== 1) {
                    return;
                }


                $query -> fetch();
                $query -> close();


                if ($_SESSION['id'] != $comment_on_id && !$this->aditur->mayI('censorComments')) {
                    return;
                }
                if ($_SESSION['id'] != $comment_on_id && $this->aditur->mayI('censorComments')) {
                    $query = $this->db->prepare("UPDATE aditur_comments SET precensored=1, reason=? WHERE id=?");
                }
                else {
                    $query = $this->db->prepare("UPDATE aditur_comments SET censored=1, reason=? WHERE id=?");
                }


                $query -> bind_param('si', htmlspecialchars($_POST['reason']), $_POST['comment_id']);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }

            if (isset($_POST['uncensor_comment_operation']) && !empty($_POST['comment_id'])) {
                $query = $this->db->prepare("SELECT comment_on_id FROM aditur_comments WHERE id=?");
                $query -> bind_param('i', $_POST['comment_id']);
                $query -> execute();
                $query -> bind_result($comment_on_id);
                $query -> store_result();

                if ($query -> num_rows !== 1) {
                    return;
                }

                $query -> fetch();
                $query -> close();


                if ($_SESSION['id'] != $comment_on_id && !$this->aditur->mayI('censorComments')) {
                    return;
                }
                if ($_SESSION['id'] != $comment_on_id && $this->aditur->mayI('censorComments')) {
                    $query = $this->db->prepare("UPDATE aditur_comments SET precensored=0, reason='' WHERE id=?");
                }
                else {
                    $query = $this->db->prepare("UPDATE aditur_comments SET censored=0, reason='' WHERE id=?");
                }

                $query -> bind_param('i', $_POST['comment_id']);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }



        }


        // for Teachers
        if ($this->aditur->mayI("censorComments")) {
            if (isset($_POST['censor_teachers_comment_operation']) && !empty($_POST['comment_id']) && !empty($_POST['reason'])) {
                $id = $_POST['comment_id'];

                $query = $this->db->prepare("SELECT comment_on_id FROM aditur_teachers_comments WHERE id=?");
                $query -> bind_param('i', $id);
                $query -> execute();
                $query -> bind_result($comment_on_id);
                $query -> store_result();


                if ($query -> num_rows !== 1) {
                    if (isset($_GET['wrapper'])) {
                        ob_end_clean();

                        echo "-1";

                        exit;
                    }

                    return;
                }


                $query -> fetch();
                $query -> close();


                if ($id != $comment_on_id && !$this->aditur->mayI('censorComments')) {
                    if (isset($_GET['wrapper'])) {
                        ob_end_clean();

                        echo "-1";

                        exit;
                    }

                    return;
                }
                if ($id != $comment_on_id && $this->aditur->mayI('censorComments')) {
                    $query = $this->db->prepare("UPDATE aditur_teachers_comments SET precensored=1, reason=? WHERE id=?");
                }
                else {
                    $query = $this->db->prepare("UPDATE aditur_teachers_comments SET censored=1, reason=? WHERE id=?");
                }

                $reason = htmlspecialchars($_POST['reason']);

                $query -> bind_param('si', $reason, $id);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }

            if (isset($_POST['uncensor_teachers_comment_operation']) && !empty($_POST['comment_id'])) {
                $query = $this->db->prepare("SELECT comment_on_id FROM aditur_teachers_comments WHERE id=?");
                $query -> bind_param('i', $_POST['comment_id']);
                $query -> execute();
                $query -> bind_result($comment_on_id);
                $query -> store_result();

                if ($query -> num_rows !== 1) {
                    return;
                }

                $query -> fetch();
                $query -> close();

                if ($id != $comment_on_id && !$this->aditur->mayI('censorComments')) {
                    if (isset($_GET['wrapper'])) {
                        ob_end_clean();

                        echo "-1";

                        exit;
                    }

                    return;
                }
                if ($id != $comment_on_id && $this->aditur->mayI('censorComments')) {
                    $query = $this->db->prepare("UPDATE aditur_teachers_comments SET precensored=0, reason='' WHERE id=?");
                }
                else {
                    $query = $this->db->prepare("UPDATE aditur_teachers_comments SET censored=0, reason='' WHERE id=?");
                }

                $query -> bind_param('i', $_POST['comment_id']);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }
        }


        // comments FROM teachers
        if ($this->aditur->mayI("censorComments")) {
            if (isset($_POST['censor_comment_from_teacher_operation']) && !empty($_POST['comment_id']) && !empty($_POST['reason'])) {
                $query = $this->db->prepare("UPDATE aditur_comments_from_teachers SET censored=1, reason=? WHERE id=?");
                $query -> bind_param('si', htmlspecialchars($_POST['reason']), $_POST['comment_id']);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }

            if (isset($_POST['uncensor_comment_from_teacher_operation']) && !empty($_POST['comment_id'])) {
                $query = $this->db->prepare("UPDATE aditur_comments_from_teachers SET censored=0, reason='' WHERE id=?");
                $query -> bind_param('i', $_POST['comment_id']);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }

        }
    }


    protected function opTeachers($access_code) {
        // for Teachers
        if ($access_code != "" OR $this->aditur->mayI("censorComments")) {
            $query = $this->db->prepare("SELECT id FROM aditur_teachers WHERE access_code=?");
            $query -> bind_param('s', $access_code);
            $query -> execute();
            $query -> bind_result($id);
            $query -> store_result();

            if ($query -> num_rows !== 1) {
                if (isset($_GET['wrapper'])) {
                    $query -> close();

                    ob_end_clean();

                    echo $access_code;
                    echo "-1";

                    exit;
                }

                return;
            }

            $query -> fetch();
            $query -> close();


            if (isset($_POST['censor_teachers_comment_operation']) && !empty($_POST['comment_id']) && !empty($_POST['reason'])) {
                $query = $this->db->prepare("SELECT comment_on_id FROM aditur_teachers_comments WHERE id=?");
                $query -> bind_param('i', $_POST['comment_id']);
                $query -> execute();
                $query -> bind_result($comment_on_id);
                $query -> store_result();


                if ($query -> num_rows !== 1) {
                    if (isset($_GET['wrapper'])) {
                        ob_end_clean();

                        echo $access_code;
                        echo "-1";

                        exit;
                    }

                    return;
                }


                $query -> fetch();
                $query -> close();


                if ($id != $comment_on_id && !$this->aditur->mayI('censorComments')) {
                    if (isset($_GET['wrapper'])) {
                        ob_end_clean();

                        echo $access_code;
                        echo "-1";

                        exit;
                    }

                    return;
                }


                $query = $this->db->prepare("UPDATE aditur_teachers_comments SET censored=1, reason=? WHERE id=?");
                $query -> bind_param('si', htmlspecialchars($_POST['reason']), $_POST['comment_id']);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }

            if (isset($_POST['uncensor_teachers_comment_operation']) && !empty($_POST['comment_id'])) {
                $query = $this->db->prepare("SELECT comment_on_id FROM aditur_teachers_comments WHERE id=?");
                $query -> bind_param('i', $_POST['comment_id']);
                $query -> execute();
                $query -> bind_result($comment_on_id);
                $query -> store_result();

                if ($query -> num_rows !== 1) {
                    return;
                }

                $query -> fetch();
                $query -> close();


                if ($id != $comment_on_id && !$this->aditur->mayI('censorComments')) {
                    return;
                }

                $query = $this->db->prepare("UPDATE aditur_teachers_comments SET censored=0, reason='' WHERE id=?");
                $query -> bind_param('i', $_POST['comment_id']);

                $success = false;

                if ($query -> execute()) {
                    $success = true;
                }

                $query -> close();

                if (isset($_GET['wrapper'])) {
                    ob_end_clean();

                    if ($success) {
                        echo "0";
                    }
                    else {
                        echo "-1";
                    }

                    exit;
                }

                return;
            }
        }


        // redirect for Teachers
        if (isset($_POST['submit_access_code_operation']) && !empty($_POST['access_code'])) {
            ob_end_clean();

            header("Location: " . $this->aditur->config("domain") . $this->aditur->url() . "/" . $_POST['access_code']);

            exit;
        }
    }


    public function censorStudents() {
        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/censorship"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <?php

        // display Students
        $query = $this->db->query("SELECT id FROM aditur_members ORDER BY forename");

        $ids = array();
        while ($object = $query -> fetch_object()) {
            $ids[] = $object -> id;
        }

        $query -> close();

        foreach($ids as $id) {
            $this->censorComments($id, true);
        }

        $this->printScript();
    }


    public function censorTeachers() {
        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/censorship"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <?php

        // display Teachers
        $query = $this->db->query("SELECT access_code, title, name FROM aditur_teachers ORDER BY name");

        $teachers = array();
        while ($object = $query -> fetch_object()) {
            $pair = array();
            $pair['title'] = $object->title;
            $pair['name'] = $object->name;
            $pair['code'] = $object->access_code;

            $teachers[] = $pair;
        }

        $query -> close();

        foreach ($teachers as $teacher) {
            ?>

            <h3><?php echo $teacher['title'] . " " . $teacher['name']; ?></h3>

            <?php

            $this->censorTeacher($teacher['code']);
        }

        $this->printScript();
    }


    public function censorComments($id, $precensoring) {
        if ($precensoring) {
            $query = $this->db->query("SELECT id, comment, precensored, censored, reason FROM aditur_comments WHERE comment_on_id=" . $id);
        }
        else {
            $query = $this->db->query("SELECT id, comment, precensored, censored, reason FROM aditur_comments WHERE comment_on_id=" . $id . " AND precensored=0");
        }


        ?>

        <p style="margin: 1rem 0;">Es wurden <?php echo $query -> num_rows; ?> Kommentare zu <?php if($id === $_SESSION['id']) { echo "Dir"; } else { echo "dieser Person"; } ?> verfasst.</p>

        <p>Klick auf einen Kommentar um ihn zu <span class="Censored">zensieren!</span><br><br></p>

        <div class="Censorship Box">
            <?php

            $first = true;
            while ($object = $query -> fetch_object()) {

                if (!$first) {
                    ?>

                    <i class="Separator fa fa-circle-o"></i>

                    <?php
                }

                ?>

                <div class="CommentBox">
                    <button class="Comment<?php if ($object->censored || $object->precensored) echo " Censored"; ?>" type="button" onclick="showCensorshipForm(this);"><?php echo $object->comment; ?></button>

                <?php
                    if (!($object->censored || $object->precensored)) {
                        ?>

                    <form class="CensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                            <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                            <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                                <i class="fa fa-lg fa-chevron-up"></i>
                                <i class="fa fa-lg fa-times-circle"></i>
                            </button>

                            <p>Diesen Kommentar zensieren?</p>

                            <input type="text" name="censorship_reason" value="" placeholder="Begründung" required>

                            <button type="button" name="censor_comment_operation" onclick="censorComment(this); return false;"><i class="fa fa-lg fa-eye-slash"></i>&nbsp;Zensieren</button>
                        </form>

                        <?php
                    }
                    else {
                        ?>

                        <form class="UncensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                            <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                            <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                                <i class="fa fa-lg fa-chevron-up"></i>
                                <i class="fa fa-lg fa-times-circle"></i>
                            </button>

                            <p>Zensiert mit der Begründung: <strong><?php echo $object->reason; ?></strong></p>

                            <p>Diesen Kommentar doch nicht zensieren?</p>

                            <button type="button" name="uncensor_comment_operation" onclick="uncensorComment(this); return false;"><i class="fa fa-lf fa-check"></i>&nbsp;Nicht mehr zensieren</button>
                        </form>

                        <?php
                    }

                    ?>
                </div>

                <?php

                $first = false;
            }

            ?>
        </div>

        <?php
    }


    public function censorTeachersComments($argv) {
        $id = $argv[0];
        $access_code = $argv[1];


        $this->opTeachers($access_code);


        $query = $this->db->query("SELECT id, comment, censored, reason FROM aditur_teachers_comments WHERE comment_on_id=" . $id . " AND precensored=0");

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/lehrer/" . $access_code); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <p style="margin: 1rem 0;">Es wurden <?php echo $query -> num_rows; ?> Kommentare zu Ihnen verfasst.</p>


        <h2>Kommentare zensieren</h2>

        <p>Klicken Sie auf einen Kommentar um ihn zu <span class="Censored">zensieren!</span><br><br></p>


        <div class="Censorship Box">
            <?php

                $first = true;
                while ($object = $query -> fetch_object()) {

                    if (!$first) {
            ?>

            <i class="Separator fa fa-circle-o"></i>

            <?php
                    }

            ?>

            <div class="CommentBox">
                <button class="Comment<?php if ($object->censored) echo " Censored"; ?>" type="button" onclick="showCensorshipForm(this);"><?php echo $object->comment; ?></button>

                <?php
                    if (!$object->censored) {
                ?>

                <form class="CensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                    <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                    <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                        <i class="fa fa-lg fa-chevron-up"></i>
                        <i class="fa fa-lg fa-times-circle"></i>
                    </button>

                    <p>Diesen Kommentar zensieren?</p>

                    <input type="text" name="censorship_reason" value="" placeholder="Begründung" required>

                    <button type="button" name="censor_teachers_comment_operation" onclick="censorTeachersComment(this); return false;"><i class="fa fa-lg fa-eye-slash"></i>&nbsp;Zensieren</button>
                </form>

                <?php
                    }
                    else {
                ?>

                <form class="UncensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                    <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                    <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                        <i class="fa fa-lg fa-chevron-up"></i>
                        <i class="fa fa-lg fa-times-circle"></i>
                    </button>

                    <p>Zensiert mit der Begründung: <strong><?php echo $object->reason; ?></strong></p>

                    <p>Diesen Kommentar doch nicht zensieren?</p>

                    <button type="button" name="uncensor_teachers_comment_operation" onclick="uncensorTeachersComment(this); return false;"><i class="fa fa-lf fa-check"></i>&nbsp;Nicht mehr zensieren</button>
                </form>

                <?php
                    }

                ?>
            </div>

                <?php

                $first = false;
            }

            $this->printScript($access_code);

        ?>
        </div>

        <?php
    }


    public function censorTeacher($access_code) {
        $query = $this->db->prepare("SELECT id FROM aditur_teachers WHERE access_code=?");
        $query -> bind_param('s', $access_code);
        $query -> execute();
        $query -> bind_result($id);
        $query -> store_result();

        if ($query -> num_rows !== 1) {
            $query -> close();

            return;
        }

        $query -> fetch();
        $query -> close();


        $query = $this->db->query("SELECT id, comment, precensored, censored, reason FROM aditur_teachers_comments WHERE comment_on_id=" . $id);

        ?>

        <p style="margin: 1rem 0;">Es wurden <?php echo $query -> num_rows; ?> Kommentare verfasst.</p>

        <div class="Censorship Box">
            <?php

                $first = true;
                while ($object = $query -> fetch_object()) {

                    if (!$first) {
            ?>

            <i class="Separator fa fa-circle-o"></i>

            <?php
                    }

            ?>

            <div class="CommentBox">
                <button class="Comment<?php if ($object->censored || $object->precensored) echo " Censored"; ?>" type="button" onclick="showCensorshipForm(this);"><?php echo $object->comment; ?></button>

                <?php
                    if (!($object->censored || $object->precensored)) {
                        ?>

                        <form class="CensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                            <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                            <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                                <i class="fa fa-lg fa-chevron-up"></i>
                                <i class="fa fa-lg fa-times-circle"></i>
                            </button>

                            <p>Diesen Kommentar zensieren?</p>

                            <input type="text" name="censorship_reason" value="" placeholder="Begründung" required>

                            <button type="button" name="censor_teachers_comment_operation" onclick="censorTeachersComment(this); return false;"><i class="fa fa-lg fa-eye-slash"></i>&nbsp;Zensieren</button>
                        </form>

                        <?php
                    }
                    else {
                        ?>

                        <form class="UncensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                            <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                            <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                                <i class="fa fa-lg fa-chevron-up"></i>
                                <i class="fa fa-lg fa-times-circle"></i>
                            </button>

                            <p>Zensiert mit der Begründung: <strong><?php echo $object->reason; ?></strong></p>

                            <p>Diesen Kommentar doch nicht zensieren?</p>

                            <button type="button" name="uncensor_teachers_comment_operation" onclick="uncensorTeachersComment(this); return false;"><i class="fa fa-lf fa-check"></i>&nbsp;Nicht mehr zensieren</button>
                        </form>

                        <?php
                    }

                    ?>
                </div>

                <?php

                $first = false;
            }

        ?>

        </div>

        <?php
    }


    public function printScript($access_code = null) {
        ?>

        <script>
            function censorComment(element) {
                var ajaxRequest,
                    commentId = element.parentNode.getElementsByTagName("input")[0].value,
                    reason = element.parentNode.getElementsByTagName("input")[1].value;

                if (window.XMLHttpRequest) {
                    ajaxRequest = new XMLHttpRequest();
                }
                else {
                    // code for IE6, IE5
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }

                ajaxRequest.onreadystatechange = function () {
                    if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                        var response = ajaxRequest.responseText;

                        if (response == 0) {
                            element.parentNode.parentNode.getElementsByClassName("Comment")[0].className += " Censored";

                            var div = element.parentNode.parentNode;

                            div.removeChild(element.parentNode);

                            div.innerHTML = div.innerHTML +
                                "<form class=\"UncensorCommentForm ToggleForm\" action=\"<?php echo $this->aditur->url(); ?>\" method=\"post\" style=\"display: none;\"><input type=\"hidden\" name=\"comment_id\" value=\"" + commentId + "\"><button type=\"button\" class=\"Toggler\" onclick=\"this.parentNode.style.display = 'none';\"><i class=\"fa fa-lg fa-chevron-up\"></i><i class=\"fa fa-lg fa-times-circle\"></i></button><p>Zensiert mit der Begründung: <strong>" + reason + "</strong></p><p>Diesen Kommentar doch nicht zensieren?</p><button type=\"submit\" name=\"uncensor_comment_operation\" onclick=\"uncensorComment(this); return false;\"><i class=\"fa fa-lf fa-check\"></i>&nbsp;Nicht mehr zensieren</button></form>";
                        }
                        else {
                            // show error message!
                            // TODO
                        }
                    }
                }

                ajaxRequest.open("POST", "<?php echo $this->aditur->url() . "?wrapper"; ?>", true);
                ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxRequest.send("censor_comment_operation&comment_id=" + commentId + "&reason=" + reason);
            }

            function uncensorComment(element) {
                var ajaxRequest,
                    commentId = element.parentNode.getElementsByTagName("input")[0].value;

                if (window.XMLHttpRequest) {
                    ajaxRequest = new XMLHttpRequest();
                }
                else {
                    // code for IE6, IE5
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }

                ajaxRequest.onreadystatechange = function () {
                    if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                        var response = ajaxRequest.responseText;

                        if (response == 0) {
                            element.parentNode.parentNode.getElementsByClassName("Comment")[0].className = "Comment";

                            var div = element.parentNode.parentNode;

                            div.removeChild(element.parentNode);

                            div.innerHTML = div.innerHTML +
                                "<form class=\"CensorCommentForm ToggleForm\" action=\"<?php echo $this->aditur->url(); ?>\" method=\"post\" style=\"display: none;\"><input type=\"hidden\" name=\"comment_id\" value=\"" + commentId + "\"><button type=\"button\" class=\"Toggler\" onclick=\"this.parentNode.style.display = 'none';\"><i class=\"fa fa-lg fa-chevron-up\"></i><i class=\"fa fa-lg fa-times-circle\"></i></button><p>Diesen Kommentar zensieren?</p><input type=\"text\" name=\"censorship_reason\" value=\"\" placeholder=\"Begründung\" required><button type=\"submit\" name=\"censor_comment_operation\" onclick=\"censorComment(this); return false;\"><i class=\"fa fa-lg fa-eye-slash\"></i>&nbsp;Zensieren</button></form>";
                        }
                        else {
                            // show error message!
                            // TODO
                        }
                    }
                }

                ajaxRequest.open("POST", "<?php echo $this->aditur->url() . "?wrapper"; ?>", true);
                ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxRequest.send("uncensor_comment_operation&comment_id=" + commentId);
            }



            function censorTeachersComment(element) {
                var ajaxRequest,
                    commentId = element.parentNode.getElementsByTagName("input")[0].value,
                    reason = element.parentNode.getElementsByTagName("input")[1].value,
                    teacher = "<?php if (!is_null($access_code)) echo $access_code; ?>";

                if (window.XMLHttpRequest) {
                    ajaxRequest = new XMLHttpRequest();
                }
                else {
                    // code for IE6, IE5
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }

                ajaxRequest.onreadystatechange = function () {
                    if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                        var response = ajaxRequest.responseText;

                        console.log(response);

                        if (response == 0) {
                            element.parentNode.parentNode.getElementsByClassName("Comment")[0].className += " Censored";

                            var div = element.parentNode.parentNode;

                            div.removeChild(element.parentNode);

                            div.innerHTML = div.innerHTML +
                                "<form class=\"UncensorCommentForm ToggleForm\" action=\"<?php echo $this->aditur->url(); ?>\" method=\"post\" style=\"display: none;\"><input type=\"hidden\" name=\"comment_id\" value=\"" + commentId + "\"><button type=\"button\" class=\"Toggler\" onclick=\"this.parentNode.style.display = 'none';\"><i class=\"fa fa-lg fa-chevron-up\"></i><i class=\"fa fa-lg fa-times-circle\"></i></button><p>Zensiert mit der Begründung: <strong>" + reason + "</strong></p><p>Diesen Kommentar doch nicht zensieren?</p><button type=\"submit\" name=\"uncensor_teachers_comment_operation\" onclick=\"uncensorTeachersComment(this); return false;\"><i class=\"fa fa-lf fa-check\"></i>&nbsp;Nicht mehr zensieren</button></form>";
                        }
                        else {
                            // show error message!
                            // TODO
                        }
                    }
                }

                ajaxRequest.open("POST", "<?php echo $this->aditur->url() . "?wrapper"; ?>", true);
                ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxRequest.send("censor_teachers_comment_operation&comment_id=" + commentId + "&reason=" + reason);
            }

            function uncensorTeachersComment(element) {
                var ajaxRequest,
                    commentId = element.parentNode.getElementsByTagName("input")[0].value,
                    teacher = "<?php if (!is_null($access_code)) echo $access_code; ?>";

                if (window.XMLHttpRequest) {
                    ajaxRequest = new XMLHttpRequest();
                }
                else {
                    // code for IE6, IE5
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }

                ajaxRequest.onreadystatechange = function () {
                    if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                        var response = ajaxRequest.responseText;

                        if (response == 0) {
                            element.parentNode.parentNode.getElementsByClassName("Comment")[0].className = "Comment";

                            var div = element.parentNode.parentNode;

                            div.removeChild(element.parentNode);

                            div.innerHTML = div.innerHTML +
                                "<form class=\"CensorCommentForm ToggleForm\" action=\"<?php echo $this->aditur->url(); ?>\" method=\"post\" style=\"display: none;\"><input type=\"hidden\" name=\"comment_id\" value=\"" + commentId + "\"><button type=\"button\" class=\"Toggler\" onclick=\"this.parentNode.style.display = 'none';\"><i class=\"fa fa-lg fa-chevron-up\"></i><i class=\"fa fa-lg fa-times-circle\"></i></button><p>Diesen Kommentar zensieren?</p><input type=\"text\" name=\"censorship_reason\" value=\"\" placeholder=\"Begründung\" required><button type=\"submit\" name=\"censor_teachers_comment_operation\" onclick=\"censorComment(this); return false;\"><i class=\"fa fa-lg fa-eye-slash\"></i>&nbsp;Zensieren</button></form>";
                        }
                        else {
                            // show error message!
                            // TODO
                        }
                    }
                }

                ajaxRequest.open("POST", "<?php echo $this->aditur->url() . "?wrapper"; ?>", true);
                ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxRequest.send("uncensor_teachers_comment_operation&comment_id=" + commentId);
            }


            function censorCommentFromTeacher(element) {
                var ajaxRequest,
                    commentId = element.parentNode.getElementsByTagName("input")[0].value,
                    reason = element.parentNode.getElementsByTagName("input")[1].value;

                if (window.XMLHttpRequest) {
                    ajaxRequest = new XMLHttpRequest();
                }
                else {
                    // code for IE6, IE5
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }

                ajaxRequest.onreadystatechange = function () {
                    if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                        var response = ajaxRequest.responseText;

                        if (response == 0) {
                            element.parentNode.parentNode.getElementsByClassName("Comment")[0].className += " Censored";

                            var div = element.parentNode.parentNode;

                            div.removeChild(element.parentNode);

                            div.innerHTML = div.innerHTML +
                                "<form class=\"UncensorCommentForm ToggleForm\" action=\"<?php echo $this->aditur->url(); ?>\" method=\"post\" style=\"display: none;\"><input type=\"hidden\" name=\"comment_id\" value=\"" + commentId + "\"><button type=\"button\" class=\"Toggler\" onclick=\"this.parentNode.style.display = 'none';\"><i class=\"fa fa-lg fa-chevron-up\"></i><i class=\"fa fa-lg fa-times-circle\"></i></button><p>Zensiert mit der Begründung: <strong>" + reason + "</strong></p><p>Diesen Kommentar doch nicht zensieren?</p><button type=\"submit\" name=\"uncensor_comment_from_teacher_operation\" onclick=\"uncensorCommentFromTeacher(this); return false;\"><i class=\"fa fa-lf fa-check\"></i>&nbsp;Nicht mehr zensieren</button></form>";
                        }
                        else {
                            // show error message!
                            // TODO
                        }
                    }
                }

                ajaxRequest.open("POST", "<?php echo $this->aditur->url() . "?wrapper"; ?>", true);
                ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxRequest.send("censor_comment_from_teacher_operation&comment_id=" + commentId + "&reason=" + reason);
            }

            function uncensorCommentFromTeacher(element) {
                var ajaxRequest,
                    commentId = element.parentNode.getElementsByTagName("input")[0].value;

                if (window.XMLHttpRequest) {
                    ajaxRequest = new XMLHttpRequest();
                }
                else {
                    // code for IE6, IE5
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                }

                ajaxRequest.onreadystatechange = function () {
                    if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                        var response = ajaxRequest.responseText;

                        if (response == 0) {
                            element.parentNode.parentNode.getElementsByClassName("Comment")[0].className = "Comment";

                            var div = element.parentNode.parentNode;

                            div.removeChild(element.parentNode);

                            div.innerHTML = div.innerHTML +
                                "<form class=\"CensorCommentForm ToggleForm\" action=\"<?php echo $this->aditur->url(); ?>\" method=\"post\" style=\"display: none;\"><input type=\"hidden\" name=\"comment_id\" value=\"" + commentId + "\"><button type=\"button\" class=\"Toggler\" onclick=\"this.parentNode.style.display = 'none';\"><i class=\"fa fa-lg fa-chevron-up\"></i><i class=\"fa fa-lg fa-times-circle\"></i></button><p>Diesen Kommentar zensieren?</p><input type=\"text\" name=\"censorship_reason\" value=\"\" placeholder=\"Begründung\" required><button type=\"submit\" name=\"censor_comment_from_teacher_operation\" onclick=\"censorCommentFromTeacher(this); return false;\"><i class=\"fa fa-lg fa-eye-slash\"></i>&nbsp;Zensieren</button></form>";
                        }
                        else {
                            // show error message!
                            // TODO
                        }
                    }
                }

                ajaxRequest.open("POST", "<?php echo $this->aditur->url() . "?wrapper"; ?>", true);
                ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                ajaxRequest.send("uncensor_comment_from_teacher_operation&comment_id=" + commentId);
            }



            function showCensorshipForm(element) {
                if (element.parentNode.getElementsByClassName("ToggleForm")[0].style.display == "none") {
                    var forms = document.getElementsByClassName("ToggleForm");

                    for (var i = 0; i < forms.length; i++) {
                        forms[i].style.display = "none";
                    }



                    element.parentNode.getElementsByClassName("ToggleForm")[0].style.display = "block";
                }
                else {
                    element.parentNode.getElementsByClassName("ToggleForm")[0].style.display = "none";
                }
            }
        </script>

        <?php
    }


    public function censorCommentsFromTeachers() {
        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/censorship"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <?php

        // display Students
        $query = $this->db->query("SELECT id FROM aditur_members ORDER BY forename");

        $ids = array();
        while ($object = $query -> fetch_object()) {
            $ids[] = $object -> id;
        }

        $query -> close();

        foreach($ids as $id) {
            $query = $this->db->query("SELECT id, comment, censored, reason FROM aditur_comments_from_teachers WHERE comment_on_id=" . $id);

            ?>

            <p style="margin: 1rem 0;">Es wurden <?php echo $query -> num_rows; ?> Kommentare zu <?php if($id === $_SESSION['id']) { echo "Dir"; } else { echo "dieser Person"; } ?> verfasst.</p>

            <p>Klick auf einen Kommentar um ihn zu <span class="Censored">zensieren!</span><br><br></p>

            <div class="Censorship Box">
                <?php

                $first = true;
                while ($object = $query -> fetch_object()) {

                    if (!$first) {
                ?>

                <i class="Separator fa fa-circle-o"></i>

                <?php
                    }

                ?>

                <div class="CommentBox">
                    <button class="Comment<?php if ($object->censored) echo " Censored"; ?>" type="button" onclick="showCensorshipForm(this);"><?php echo $object->comment; ?></button>

                    <?php

                    if (!$object->censored) {
                        ?>

                    <form class="CensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                        <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                        <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                            <i class="fa fa-lg fa-chevron-up"></i>
                            <i class="fa fa-lg fa-times-circle"></i>
                        </button>

                        <p>Diesen Kommentar zensieren?</p>

                        <input type="text" name="censorship_reason" value="" placeholder="Begründung" required>

                        <button type="button" name="censor_comment_from_teacher_operation" onclick="censorCommentFromTeacher(this); return false;"><i class="fa fa-lg fa-eye-slash"></i>&nbsp;Zensieren</button>
                    </form>

                        <?php
                    }
                    else {
                        ?>

                    <form class="UncensorCommentForm ToggleForm" action="<?php echo $this->aditur->url(); ?>" method="post" style="display: none;">
                        <input type="hidden" name="comment_id" value="<?php echo $object->id; ?>">

                        <button type="button" class="Toggler" onclick="this.parentNode.style.display = 'none';">
                            <i class="fa fa-lg fa-chevron-up"></i>
                            <i class="fa fa-lg fa-times-circle"></i>
                        </button>

                        <p>Zensiert mit der Begründung: <strong><?php echo $object->reason; ?></strong></p>

                        <p>Diesen Kommentar doch nicht zensieren?</p>

                        <button type="button" name="uncensor_comment_from_teacher_operation" onclick="uncensorCommentFromTeacher(this); return false;"><i class="fa fa-lf fa-check"></i>&nbsp;Nicht mehr zensieren</button>
                    </form>

                        <?php
                    }

                    ?>
                </div>

                <?php

                $first = false;
            }

            ?>
            </div>

            <?php
        }

        $this->printScript();
    }
}

?>
