<?php
// Settings: Announcements
// Create, delete Announcements


class AnnouncementsSettings extends Setting {
    public function fromUrl($argv) {
        $this->op();


        $query = $this->db->query("SELECT id, title, content FROM aditur_announcements ORDER BY timestamp DESC");

        $announcements = array();
        while ($object = $query -> fetch_object()) {
            $announcements[] = $object;
        }

        $query -> close();

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/settings"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Einstellungen</a>

        <h2>Ankündigungen</h2>

        <section class="Announcements Box">
            <h3>Neue Ankündigung erstellen</h3>

            <form class="CreateNewAnnouncementForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input type="text" name="new_announcement_title" placeholder="Überschrift" required>

                <textarea name="new_announcement_content" placeholder="Inhalt" required></textarea>

                <button type="submit" name="new_announcement_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

        <section class="AnnouncementManagement Box">
            <h3>Ankündigungen</h3>

            <ul class="Announcements" id="announcements">
                <?php

                $nAnnouncement = 0;
                foreach ($announcements as $announcement) {
                    ?>

                    <li>
                        <p class="Title"><?php echo $announcement->title; ?></p>
                        <input type="checkbox" class="Expander AnnouncementExpander" id="AnnouncementExpander<?php echo $nAnnouncement; ?>">
                        <label class="Toggler" for="AnnouncementExpander<?php echo $nAnnouncement; ?>">
                            <i class="fa fa-lg fa-times-circle"></i>
                        </label>
                        <form class="DeleteAnnouncementForm SubmitForm" action="<?php echo $this->aditur->url() . "#announcements"; ?>" method="post">
                            <input type="hidden" name="announcement_id" value="<?php echo $announcement->id; ?>">

                            <p>Diese Ankündigung wirklich löschen?</p>

                            <button type="submit" name="delete_announcement_operation">
                                <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                            </button>
                        </form>
                    </li>

                    <?php

                    $nAnnouncement++;
                }

                ?>
            </ul>
        </section>

        <?php
    }


    private function op() {
        // Announcement Management
        if (isset($_POST['new_announcement_operation']) && !empty($_POST['new_announcement_title']) && !empty($_POST['new_announcement_content']))  {
            $time = time();

            $query = $this->db->prepare("INSERT INTO aditur_announcements (title, content, timestamp) VALUES (?, ?, ?)");
            $query -> bind_param('sss', $_POST['new_announcement_title'], $_POST['new_announcement_content'], $time);
            if ($query -> execute()) {
                $this->aditur->note("Ankündigung wurde erstellt.");
            }
            else {
                $this->aditur->error("Die neue Ankündigung konnte nicht hinzugefügt werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['delete_announcement_operation']) && !empty($_POST['announcement_id'])) {
            $query = $this->db->prepare("DELETE FROM aditur_announcements WHERE id=?");
            $query -> bind_param('i', $_POST['announcement_id']);
            if ($query -> execute()) {
                $this->aditur->note("Ankündigung wurde gelöscht.");
            }
            else {
                $this->aditur->error("Ankündigung konnte nicht gelöscht werden! Bitte wende Dich an den Admin!");
            }
        }
    }


    public function index() {
        ?>

        <a class="Setting Box" href="<?php echo $this->aditur->path("/settings/announcements"); ?>">
            <div>
                <h3>&nbsp;<i class="fa fa-lg fa-info"></i>&nbsp;&nbsp;&nbsp;Ankündigungen</h3>
            </div>
        </a>

        <?php
    }
}

?>
