<?php
// Module: Announcements


class Announcements extends Module {
    public function fromUrl($argv) {
        $this->aditur->loadModule("headquarter", "fromUrl");

        return;
    }


    public function showCurrent() {
        $query = $this->db->query("SELECT title, content FROM aditur_announcements ORDER BY timestamp DESC");


        while ($object = $query -> fetch_object()) {
            $this->render($object->title, $object->content);
        }

        $query -> close();
    }


    private function render($title, $content) {
        ?>

        <section class="Announcements Box Group">
            <h2><?php echo $title; ?></h2>

            <i class="fa fa-lg fa-bullhorn Weak" id="announcementBullhorn"></i>

            <article>
                <?php echo $content; ?>
            </article>
        </section>

        <?php
    }
}



?>
