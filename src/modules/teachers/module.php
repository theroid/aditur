<?php
// Module: Teachers


class Teachers extends Module {
    public function __construct($aditur, $function, $argv = array()) {
        parent::__construct($aditur, $function, $argv);
        $aditur->addStylesheet("comments/base.css");
        $aditur->addStylesheet("profiles/base.css");
    }




    public function fromUrl($argv) {
        if (empty($argv)) {
            $this->allProfiles();
            $this->printYourComments();
        }
        else {
            $this->profile($argv[0]);
        }
    }


    // method called by __construct() where Operations should be made
    protected function op($id = null) {
        if (isset($_POST['teachers_comment_operation'])) {
            if (!empty($_POST['new_comment'])) {

                if ($_POST['teachers_comment_operation'] == "submit") {
                    $comment = htmlspecialchars($_POST['new_comment']);

                    $query = $this -> db -> prepare("INSERT INTO aditur_teachers_comments (author_id, comment_on_id, comment)
                    VALUES (?, ?, ?)");
                    $query -> bind_param('iis', $_SESSION['id'], $id, $comment);
                    if (!$query -> execute()) {
                        $this->aditur->error("Dein Kommentar konnte leider nicht gespeichert werden. Bitte wende Dich an den Admin!");
                        return;
                    }
                }
            }


            if (!empty($_POST['comment_id'])) {
                // get author_id
                $id = $_POST['comment_id'];


                $query = $this->db->prepare("SELECT author_id FROM aditur_teachers_comments WHERE id=?");
                $query -> bind_param('i', $id);
                $query -> bind_result($author_id);

                if (!$query -> execute()) {
                    // TODO show error
                    $this->aditur->log($this -> db -> error);
                    $query -> close();

                    return;
                }

                $query -> fetch();

                $query -> close();


                // check if it is the author or a privileged user
                if ($_SESSION['id'] == $author_id) {



                    // perform changes
                    if ($_POST['teachers_comment_operation'] == "delete") {
                        $query = $this->db->prepare("DELETE FROM aditur_teachers_comments WHERE id=?");
                        $query -> bind_param('i', $id);
                    }
                    else if ($_POST['teachers_comment_operation'] == "edit") {
                        $comment = htmlspecialchars($_POST['new_comment']);

                        $query = $this->db->prepare("UPDATE aditur_teachers_comments SET comment=? WHERE id=?");
                        $query -> bind_param('si', $comment, $id);
                    }
                    else {
                        $query -> close();

                        return;
                    }
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);
                        $this->aditur->error("Dein Kommentar konnte nicht bearbeitet werden. Probiere es später noch einmal!");
                    }
                    else {
                        $this->aditur->note("Dein Kommentar wurde geändert.");
                    }

                    $query -> close();
                }
            }
        }
    }



    public function printStatistics() {
        // how many comments you wrote
        $query = "SELECT COUNT(id) FROM aditur_teachers_comments WHERE author_id=" . $_SESSION['id'];
        $result = $this -> db -> query($query);
        $nWrittenComments = $result -> fetch_row();

        // how many comments were written in summary
        $query = "SELECT COUNT(id) FROM aditur_teachers_comments";
        $result = $this -> db -> query($query);
        $nComments = $result -> fetch_row();

?>Du hast bisher <?php echo $nWrittenComments[0]; ?> von <?php echo $nComments[0]; ?> Kommentaren insgesamt verfasst.<?php
    }



    public function printYourComments() {
        $this->op();


        $query = "SELECT title, name, aditur_teachers_comments.id, comment_on_id, comment FROM aditur_teachers_comments, aditur_teachers WHERE author_id=" . $_SESSION['id'] . " AND aditur_teachers.id=comment_on_id ORDER BY name";

        if (!$result = $this -> db -> query($query)) {
            $this->aditur->log($this -> db -> error);
        }

        if ($result->num_rows > 0) {
?>

<div class="Comments">

<h2>Von Dir verfasste Kommentare</h2>

<p><br>Klick auf einen Kommentar um ihn zu bearbeiten.</p>

<?php
            $lastId = -1;
            $expanderCount = 0;

            while ($comment = $result->fetch_object()) {
                if ($comment->comment_on_id != $lastId) {
                    if ($expanderCount > 0) {
?>
</div>

<?php
                    }
?>

<h3>Deine Kommentare zu <?php echo $comment->title . " " . $comment->name; ?></h3>

<div class="CommentAuthor">
    <a href="<?php echo $this->aditur->path("/teachers/" . $comment->comment_on_id . "/" . $comment->title . " " . $comment->name); ?>">
       <div class="ImageSizer">
           <img alt="" src="<?php echo $this->aditur->path("/media/teachers/thumbs/" . $comment->comment_on_id . "/" . $comment->title . " " . $comment->name . ".jpg"); ?>">
        </div>
        <p><?php echo $comment->title . " " . $comment->name; ?></p>
    </a>
</div>

<div class="CommentList">
    <?php
                }

    ?>

    <div class="Comment">
        <input type="checkbox" class="Expander BlackBoxExpander" id="blackBoxExpander<?php echo $expanderCount; ?>">
        <label class="Comment" for="blackBoxExpander<?php echo $expanderCount; ?>"><?php echo $comment->comment; ?></label>
        <form class="ChangeComment" action="<?php echo $this->aditur->url(); ?>" method="post">
            <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">
            <textarea name="new_comment" placeholder="Verfasse einen schönen Kommentar!"><?php echo $comment->comment; ?></textarea>
            <button class="ButtonEdit" type="submit" value="edit" name="teachers_comment_operation"><i class="fa fa-lg fa-pencil"></i>&nbsp;Bearbeiten</button>
            <label class="Button" for="blackBoxExpander<?php echo $expanderCount; ?>"><i class="fa fa-lg fa-remove"></i>&nbsp;Abbrechen</label>
        </form>

        <form class="DeleteComment" action="<?php echo $this->aditur->url(); ?>" method="post">
            <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">
            <button class="ButtonDelete" type="submit" value="delete" name="teachers_comment_operation"><i class="fa fa-lg fa-eraser"></i>&nbsp;Löschen</button>
        </form>
    </div>

    <?php
                $expanderCount++;

                $lastId = $comment->comment_on_id;
            }
    ?>
</div>
</div>
<?php
        }
        else {
?><p>Du hast noch keine Kommentare verfasst.</p><?php
        }
    }



    public function printYourCommentsTo($id) {
        $this->op($id);
?>

<div class="Comments">

    <?php
        ob_start();

        // print comments you commented on

        // get id of $commentedOn
        $query = $this->db->prepare("SELECT title, name FROM aditur_teachers WHERE id=?");
        $query -> bind_param('i', $id);
        $query -> bind_result($title, $name);
        if (!$query->execute()) {
            echo $this -> db -> error;
        }
        $query -> fetch();
        $query -> close();
    ?>

    <h3>Deine Kommentare zu <?php echo $title . " " . $name; ?></h3>

    <p><br>Klick auf einen Kommentar um ihn zu bearbeiten.</p>

    <?php



        // get comments
        if (!$result = $this -> db -> query("SELECT id, comment FROM aditur_teachers_comments WHERE
     comment_on_id=$id AND author_id=" . $_SESSION['id'])) {
            echo $this -> db -> error;
        }

        $nComments = 0;
    ?>
    <div class="CommentAuthor">
        <a href="<?php echo $this->aditur->path("/me"); ?>">
            <div class="ImageSizer">
                <img alt="" src="<?php echo $this->aditur->path("/media/profilePics/thumbs/" . $_SESSION['id'] . "/" . $_SESSION['forename'] . " " . $_SESSION['lastname'] . ".jpg"); ?>">
            </div>
        </a>
    </div>


    <div class="CommentList">
        <?php
        while ($comment = $result -> fetch_object()) {
            $nComments++;
        ?>

        <div class="Comment">
            <input type="checkbox" class="Expander BlackBoxExpander" id="blackBoxExpander<?php echo $nComments; ?>">
            <label class="Comment" for="blackBoxExpander<?php echo $nComments; ?>"><?php echo $comment->comment; ?></label>
            <form class="ChangeComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">
                <textarea name="new_comment" placeholder="Verfasse einen schönen Kommentar!"><?php echo $comment->comment; ?></textarea>
                <button class="ButtonEdit" type="submit" value="edit" name="teachers_comment_operation"><i class="fa fa-lg fa-pencil"></i>&nbsp;Bearbeiten</button>
                <label class="Button" for="blackBoxExpander<?php echo $nComments; ?>"><i class="fa fa-lg fa-remove"></i>&nbsp;Abbrechen</label>
            </form>

            <form class="DeleteComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">
                <button class="ButtonDelete" type="submit" value="delete" name="teachers_comment_operation"><i class="fa fa-lg fa-eraser"></i>&nbsp;Löschen</button>
            </form>
        </div>

        <?php
        }
            ?>
    </div>

    <?php
        if ($nComments > 0) {
            ob_end_flush();
        }
        else {
            ob_end_clean();
    ?>

    <p style="margin-top: 2rem;">Noch keine Kommentare verfasst.</p>

    <?php
        }
    ?>
</div>
<?php
    }



    public function printSubmitForm() {
?>
<!-- CommentSubmitForm -->
<form class="SubmitForm CommentSubmitForm Box" action="<?php echo $this->aditur->url(); ?>" method="post">
    <div>
        <textarea name="new_comment" placeholder="Verfasse einen schönen Kommentar!" required></textarea>
        <button type="submit" name="teachers_comment_operation" value="submit">
            <i class="fa fa-lg fa-check"></i>&nbsp; Senden
        </button>
    </div>
</form>
<?php
    }



    public function allProfiles() {
        // show all profiles
        $query = "SELECT id, title, name FROM aditur_teachers ORDER BY name ASC";
        if (!$result = $this->db->query($query)) {
            echo $this->db->error;
            $this->aditur->log($this -> db -> error);
        }
?>
<h2>Alle Profile</h2>

<?php $this->printStatistics(); ?>

<div class="ProfileFrames">
    <?php
        while ($object = $result->fetch_object()) {
    ?>
    <div class="ProfileFrame">
        <a href="<?php echo $this->aditur->path("/teachers/" . $object->id . "/" . $object->name); ?>">
            <div class="ImageSizer">
                <img alt="<?php echo $object->title . " " . $object->name; ?>'s Profilbild" src="<?php echo $this->aditur->path("/media/teachers/thumbs/" . $object->id . "/" . $object->title . " " . $object->name . ".jpg"); ?>">
            </div>
            <p><?php echo $object->title . " " . $object->name; ?></p>
        </a>
    </div>
    <?php } ?>
</div>
<?php
    }



    public function profile($id) {
        // display a specific profile detailed


        // check if $username exists
        $query = $this -> db -> prepare("SELECT title, name FROM aditur_teachers WHERE id=?");
        $query -> bind_param('i', $id);
        $query -> execute();
        $query -> bind_result($title, $name);
        $query -> store_result();

        // if there is != 1 user or the selected user is == SESSION
        if ($query->num_rows != 1) {
            // if user does not exist display all users
            $this->aditur->error("Dieser Lehrer existiert nicht.");
            $query->close();

            $this->allProfiles();

            return;
        }

        $query -> fetch();
        $query -> close();
?>
<div class="ProfileDetail">
    <div class="displayRow">
        <a class="Back" href="<?php echo $this->aditur->path("/teachers"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp; Zurück</a>

        <h2><?php echo $title . " " . $name; ?>'s Profil</h2>
    </div>
    <div class="ImageSizer">
        <img class="ProfileImage" alt="<?php echo $title . " " . $name; ?>'s Profilbild" src="<?php echo $this->aditur->path("/media/teachers/originals/" . $id . "/" . $title . " " . $name . ".jpg"); ?>">
    </div>

    <?php
            // load existing comments
            $this->aditur->loadModule("teachers", "printYourCommentsTo", $id);

            // load comments submit form
            $this->aditur->loadModule("teachers", "printSubmitForm");

    ?>
</div>
<?php
    }
}


?>
