<?php
// Settings: Teachers
// Lehrerverwaltung


class TeachersSettings extends Setting {
    public function fromUrl($argv) {
        $this->op();

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/settings"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Einstellungen</a>


        <h2>Lehrer</h2>

        <section class="NewTeacher">
            <form class="CreateNewTeacherForm SubmitForm Box" action="<?php echo $this->aditur->url(); ?>" method="post" enctype="multipart/form-data">
                <h3>Neuen Lehrer erstellen</h3>

                <input type="text" name="new_teacher_title" value="" placeholder="Anrede" required>

                <input type="text" name="new_teacher_name" value="" placeholder="Name" required>

                <input type="hidden" name="MAX_FILE_SIZE" value="8000000">

                <label for="newTeacherPic">Lehrerbild hochladen</label>
                <input type="file" name="new_teacher_pic" id="newTeacherPic">

                <button type="submit" name="new_teacher_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

        <section class="ManageTeachers">
            <h3>Lehrer</h3>

            <p>Klick auf die <span class="Properties">Eigenschaften</span>, die Du ändern möchtest!</p>

            <ul class="TeacherList" id="form-1">
                <?php

                $query = $this->db->query("SELECT id, title, name FROM aditur_teachers ORDER BY name");

                $nForms = 0;
                while ($object = $query -> fetch_object()) {
                ?>

                <li class="Teacher Box" id="form<?php echo $nForms; ?>">
                    <div class="ProfileFrame">
                        <div class="ImageSizer">
                            <img src="<?php echo $this->aditur->path("/media/teachers/thumbs/" . $object->id . "/" . $object->title . " " . $object->name) . ".jpg"; ?>" alt="">
                        </div>
                        <p class="TeacherName"><?php echo $object->title . " " . $object->name; ?></p>
                    </div>

                    <div class="Forms">
                        <div class="ChangeName">
                            <input type="checkbox" class="Expander NameExpander" id="nameExpander<?php echo $nForms; ?>">
                            <label class="Data" for="nameExpander<?php echo $nForms; ?>">
                                <?php echo $object->title . " " . $object->name; ?>
                            </label>
                            <form class="ChangeName SubmitForm" action="<?php echo $this->aditur->url() . "#form" . $nForms; ?>" method="post">
                                <i class="fa fa-lg fa-chevron-up"></i>

                                <label class="Close" for="nameExpander<?php echo $nForms; ?>">
                                    <i class="fa fa-lg fa-times-circle"></i>
                                </label>

                                <p>Lehrernamen ändern</p>

                                <input type="hidden" name="teacher_id" value="<?php echo $object->id; ?>">

                                <input type="text" name="new_teacher_title" value="<?php echo $object->title; ?>" placeholder="Neue Anrede" required>
                                <input type="text" name="new_teacher_name" value="<?php echo $object->name; ?>" placeholder="Neuer Name" required>

                                <button type="submit" name="change_teacher_name_operation">
                                    <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                                </button>
                            </form>
                        </div>

                        <div class="ChangePic">
                            <input type="checkbox" class="Expander ChangePicExpander" id="picExpander<?php echo $nForms; ?>">
                            <label class="Data" for="picExpander<?php echo $nForms; ?>">
                                Lehrerbild ändern
                            </label>
                            <form class="ChangePicForm SubmitForm" action="<?php echo $this->aditur->url() . "#form" . $nForms; ?>" method="post" enctype="multipart/form-data">
                                <i class="fa fa-lg fa-chevron-up"></i>

                                <label class="Close" for="picExpander<?php echo $nForms; ?>">
                                    <i class="fa fa-lg fa-times-circle"></i>
                                </label>

                                <p>Lehrerbild ändern</p>

                                <input type="hidden" name="teacher_id" value="<?php echo $object->id; ?>">

                                <input type="hidden" name="MAX_FILE_SIZE" value="8000000">

                                <input type="file" name="new_teacher_pic" id="newTeacherPic">

                                <button type="submit" name="new_teacher_operation">
                                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                                </button>

                            </form>
                        </div>
                    </div>

                    <div class="DeleteTeacher">
                        <input type="checkbox" class="Expander DeleteTeacherExpander" id="deleteTeacherExpander<?php echo $nForms; ?>">
                        <label class="Toggler Close" for="deleteTeacherExpander<?php echo $nForms; ?>">
                            <i class="fa fa-lg fa-trash"></i>
                        </label>
                        <form class="DeleteTeacherForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                            <i class="fa fa-lg fa-chevron-up"></i>

                            <label class="Close" for="deleteTeacherExpander<?php echo $nForms; ?>">
                                <i class="fa fa-lg fa-times-circle"></i>
                            </label>

                            <input type="hidden" name="teacher_id" value="<?php echo $object->id; ?>">

                            <p>Diesen Lehrer wirklich löschen?</p>

                            <button type="submit" name="delete_teacher_operation">
                                <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                            </button>
                        </form>
                    </div>
                </li>

                <?php

                    $nForms++;
                }

                ?>
            </ul>
        </section>

        <?php
    }


    private function op() {
        // Teacher Management
        if (isset($_POST['new_teacher_operation'])
            && !empty($_POST['new_teacher_title'])
            && !empty($_POST['new_teacher_name'])) {

            $newTitle = trim(htmlspecialchars($_POST['new_teacher_title']));
            $newName = trim(htmlspecialchars($_POST['new_teacher_name']));

            $access_code = substr(md5( $newTitle. $newName . rand()), 0, 8);

            // insert values
            $query = $this->db->prepare("INSERT INTO aditur_teachers (title, name, access_code) VALUES (?, ?, ?)");
            $query -> bind_param('sss', $newTitle, $newName, $access_code);
            if (!$query -> execute()) {
                $query -> close();

                $this->aditur->error("Lehrer konnte nicht hinzugefügt werden. Bitte gib dem Administrator Bescheid.");

                return;
            }

            $query -> close();


            if (!empty($_FILES['new_teacher_pic']['tmp_name'])) {
                $query = $this->db->prepare("SELECT id FROM aditur_teachers WHERE name=?");

                $query -> bind_param('s', $newName);
                $query -> execute();
                $query -> bind_result($id);
                $query -> fetch();

                $query -> close();

                $this -> uploadTeacherPic($id);
            }
        }

        if (!empty($_POST['teacher_id'])) {
            $id = $_POST['teacher_id'];

            if (isset($_POST['change_teacher_name_operation']) && !empty($_POST['new_teacher_title']) && !empty($_POST['new_teacher_name'])) {
                $newTeacherTitle = trim(htmlspecialchars($_POST['new_teacher_title']));
                $newTeacherName = trim(htmlspecialchars($_POST['new_teacher_name']));

                $query = $this->db->prepare("UPDATE aditur_teachers SET title=?, name=? WHERE id=?");
                $query -> bind_param('ssi', $newTeacherTitle, $newTeacherName, $id);
                if (!$query -> execute()) {
                    $this->aditur->error("Der Lehrername konnte nicht geändert werden. Bitte wende Dich an den Admin.");

                    $query -> close();
                    return;
                }

                $query -> close();

                $this->aditur->note("Der Lehrername wurde geändert.");
            }

            if (isset($_POST['delete_teacher_operation'])) {
                $query = $this->db->prepare("SELECT title, name FROM aditur_teachers WHERE id=?");
                $query -> bind_param('i', $_POST['teacher_id']);
                $query -> bind_result($title, $name);
                $query -> execute();
                $query -> store_result();

                if ($query -> num_rows !== 1) {
                    $query -> close();

                    $this->aditur->error("Der zu löschende Lehrer existiert nicht!");

                    return;
                }

                $query -> fetch();
                $query -> close();


                $query = $this->db->prepare("DELETE FROM aditur_teachers WHERE id=?");

                $query -> bind_param('i', $_POST['teacher_id']);

                if (!$query -> execute()) {
                    $query -> close();

                    $this->aditur->error("Der zu löschende Lehrer konnte nicht gelöscht werden! Bitte wende Dich an den Admin.");

                    return;
                }

                $query -> close();

                $this->aditur->note("Der Lehrer \"$title $name\" (ID: " . $_POST['teacher_id'] . ") wurde gelöscht.");
            }


            if (!empty($_FILES['new_teacher_pic']['tmp_name'])) {
                $this -> uploadTeacherPic($id);
            }
        }
    }


    public function index() {
        ?>

        <a class="Setting Box" href="<?php echo $this->aditur->path("/settings/teachers"); ?>">
            <div>
                <h3><i class="fa fa-lg fa-graduation-cap"></i>Lehrerverwaltung</h3>
            </div>
        </a>

        <?php
    }


    private function uploadTeacherPic($id) {
        if (!empty($_FILES['new_teacher_pic']['tmp_name']) && !is_null($id)) {
            $thumbnailMaxWidth = 130;
            $thumbnailMaxHeight = 100;

            $originalMaxWidth = 700;
            $originalMaxHeight = 400;


            $path = "data/teachers/raw/" . $id . ".jpg";


            if (file_exists($path)) {
                // remove the old one
                if (!unlink("data/teachers/raw/" . $id . ".jpg")) {
                    $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

                    return;
                }
                if (!unlink("data/teachers/originals/" . $id . ".jpg")) {
                    $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

                    return;
                }
                if (!unlink("data/teachers/thumbs/" . $id . ".jpg")) {
                    $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

                    return;
                }
            }


            $image = $this->aditur->imagecreatefromfile($_FILES['new_teacher_pic']['tmp_name']);


            if (is_null($image)) {
                $this->aditur->error("Neues Lehrerbild konnte nicht gespeichert werden!");

                return;
            }


            // create thumbnail
            if (imagesx($image) > imagesy($image)) {
                $thumbnailHeight = imagesy($image) * $thumbnailMaxWidth / imagesx($image);

                $thumbnail = imagecreatetruecolor($thumbnailMaxWidth, $thumbnailHeight);

                imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailMaxWidth, $thumbnailHeight, imagesx($image), imagesy($image));
            }
            else {
                $thumbnailWidth = imagesx($image) * $thumbnailMaxHeight / imagesy($image);

                $thumbnail = imagecreatetruecolor($thumbnailWidth, $thumbnailMaxHeight);

                imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailWidth, $thumbnailMaxHeight, imagesx($image), imagesy($image));
            }

            if (!imagejpeg($thumbnail, $this->aditur->path("data/teachers/thumbs/" . $id . ".jpg"))) {
                $this->aditur->error("Neues Lehrerbild konnte nicht gespeichert werden!");

                return;
            }

            // create fitting originals size
            if (imagesx($image) > imagesy($image)) {
                $originalHeight = imagesy($image) * $originalMaxWidth / imagesx($image);

                $original = imagecreatetruecolor($originalMaxWidth, $originalHeight);

                imagecopyresampled($original, $image, 0, 0, 0, 0, $originalMaxWidth, $originalHeight, imagesx($image), imagesy($image));
            }
            else {
                $originalWidth = imagesx($image) * $originalMaxHeight / imagesy($image);

                $original = imagecreatetruecolor($originalWidth, $originalMaxHeight);

                imagecopyresampled($original, $image, 0, 0, 0, 0, $originalWidth, $originalMaxHeight, imagesx($image), imagesy($image));
            }

            if (!imagejpeg($original, $this->aditur->path("data/teachers/originals/" . $id . ".jpg"))) {
                $this->aditur->error("Neues Lehrerbild konnte nicht gespeichert werden!");

                return;
            }


            if (imagejpeg($image, $this->aditur->path("data/teachers/raw/" . $id . ".jpg"))) {
                $this->aditur->note("Lehrerbild wurde gespeichert.");

                unlink($_FILES['new_teacher_pic']['tmp_name']);
            } else {
                $this->aditur->error("Neues Lehrerbild konnte nicht gespeichert werden!");

                return;
            }
        }
    }
}

?>
