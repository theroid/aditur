<?php
// Module: Headquarter


class Headquarter extends Module {
    public function fromUrl($argv) {
        ?>

        <div class="Headquarter">
            <div class="Box">
                <h1 style="text-align: center; font-size: 32pt;">Willkommen im Aditur!</h1>
            </div>

            <?php

            // include modules you want to display in your headquarter

            $this->aditur->loadModule("announcements", "showCurrent");

            $this->aditur->loadModule("finances", "showStatus");

            $this->aditur->loadModule("comments", "printCommentKing");

            $this->aditur->loadModule("citations", "printSubmitForm");

            $this->aditur->loadModule("ideabox", "printIdeas");

            $this->aditur->loadModule("comments", "printNotCommentedProfiles");

            ?>

        </div>

        <?php
    }
}



?>
