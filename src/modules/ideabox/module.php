<?php
// Module: Template


class Ideabox extends Module {
    // Default state when called by Aditur from URL
    public function fromUrl($argv = null) {
        if (!$this->aditur->login->isUserLoggedIn()){
            $this->aditur->printNotificationBox();
            $this->aditur->printErrorBox();
        }

        ?>

        <section class="Box">
            <h2 class="Idea">Ideenbox</h2>

            <?php $this->printIdeaSubmitForm(); ?>

        </section>

        <?php
    }



    protected function op() {
        if (isset($_POST['idea_operation'])){
            if (!empty($_POST['new_idea'])) {
                if ($_POST['idea_operation'] == "submit") {
                    $idea = htmlspecialchars($_POST['new_idea']);
                    $query = $this -> db -> prepare("INSERT INTO aditur_ideas (idea) VALUES (?)");
                    $query -> bind_param('s', $idea);
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);
                        $this->aditur->error("Deine Idee konnte leider nicht gespeichert werden. Bitte wende Dich an den Admin!");
                        return;
                    }

                    $this->aditur->note("Idee wurde gespeichert. Danke für Deine Kreativität!");
                }
            }
        }

        // delete id
        if ($this->aditur->mayI("deleteIdeas") && isset($_POST['delete_idea_operation']) && !empty($_POST['idea_id'])) {
            $query = $this->db->prepare("DELETE FROM aditur_ideas WHERE id=?");
            $query -> bind_param('i', $_POST['idea_id']);
            if ($query -> execute()) {
                $this->aditur->note("Idee gelöscht.");
            }
            else {
                $this->aditur->error("Idee konnte nicht gelöscht werden.");
            }

            $query -> close();
        }
    }


    public function printIdeaSubmitForm() {
        $this->op();

        ?>
        <!-- IdeaSubmitForm -->
        <form class="SubmitForm IdeaSubmitForm" action="<?php echo $this->aditur->url() . "#ideabox"; ?>" method="post">
            <div>
                <textarea name="new_idea" placeholder="Eine geniale Idee wäre..." required></textarea>

                <button type="submit" name="idea_operation" value="submit">
                    <i class="fa fa-lg fa-check"></i>&nbsp; Senden
                </button>
            </div>
        </form>
        <?php
    }

    public function printIdeas() {
        $query = $this->db->query("SELECT enabled FROM aditur_modules WHERE name='ideabox'");

        $enabled = $query -> fetch_object() -> enabled;

        if (!$enabled) {
            return;
        }


        ob_start();

        $this->printIdeaSubmitForm();
        $ideabuffer = ob_get_contents();

        ob_end_clean();


        $query = "SELECT id, idea FROM aditur_ideas";
        $result = $this->db->query($query);

        ?>

        <section class="Box" id="ideabox">
            <h2>Ideen</h2>

            <div class="IdeaFrame">
                <ul class="IdeaList">
                    <?php

                    $nIdeas = 0;
                    while ($object = $result->fetch_object()) {
                        ?>

                        <li>
                            <p><?php echo $object->idea; ?></p>

                            <?php

                            if ($this->aditur->mayI("deleteIdeas")) {
                                ?>
                                <input type="checkbox" class="Expander IdeaExpander" id="ideaExpander<?php echo $nIdeas; ?>">
                                <label class="Delete Toggler" for="ideaExpander<?php echo $nIdeas; ?>">
                                    <i class="fa fa-lg fa-times-circle"></i>
                                </label>
                                <form class="DeleteIdeaForm SubmitForm" action="<?php echo $this->aditur->url() . "#ideabox"; ?>" method="post">
                                    <label class="Close" for="ideaExpander<?php echo $nIdeas; ?>">
                                        <i class="fa fa-lg fa-times-o"></i>
                                    </label>

                                    <input type="hidden" name="idea_id" value="<?php echo $object->id; ?>">

                                    <p>Diese Idee wirklich löschen?</p>

                                    <button type="submit" name="delete_idea_operation">
                                        <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                                    </button>
                                </form>

                                <?php
                            }

                            ?>

                        </li>

                        <?php

                        $nIdeas++;
                    }

                    if ($nIdeas == 0) {
                        ?>
                        <li>Bisher wurden noch keine Ideen verfasst.</li>
                        <?php
                    }

                    ?>

                </ul>
            </div>

            <?php

            echo $ideabuffer;

            ?>

            <p>Um schnell eine Idee verfassen zu können, kannst du auch auf <a href="<?php echo $this->aditur->path("/ideabox"); ?>"><?php echo $this->aditur->config("domain"); ?>/ideabox</a> gehen, dafür musst Du dich nicht einmal anmelden!</p>

        </section>

        <?php
    }
}

?>
