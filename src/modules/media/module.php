<?php
// Module: Media


class Media extends Module {
    public function fromUrl($argv) {
        // if a folder is requested send 404
        if (strpos(end($argv), '.') === false) {
            ob_end_clean();
            header("HTTP/1.1 404 Not Found");

            exit;
        }

        // login is required for profilePics
        // if ("profilePics" == $argv[0]) {
        switch ($argv[0]) {
            case "profilePics":
                // Because we need teacher's access...
                /*
                if (!$this->aditur->login->isUserLoggedIn()) {
                    header("HTTP/1.1 403 Forbidden");

                    exit;
                }

                */

                $path = "data/profilePics/" . $argv[1] . "/" . $argv[2] . ".jpg";

                if (!file_exists($path)) {
                    if ($argv[1] == "thumbs") {
                        $path = "data/profilePics/anonymousThumb.jpg";
                    }
                    else {
                        $path = "data/profilePics/anonymousOriginal.jpg";
                    }
                }

                $this->show($path);

                break;

            case "teachers":
                if (!$this->aditur->login->isUserLoggedIn()) {
                    header("HTTP/1.1 403 Forbidden");

                    exit;
                }

                $path = "data/teachers/" . $argv[1] . "/" . $argv[2] . ".jpg";

                if (!file_exists($path)) {
                    if ($argv[1] == "thumbs") {
                        $path = "data/teachers/anonymousThumb.jpg";
                    }
                    else {
                        $path = "data/teachers/anonymousOriginal.jpg";
                    }
                }

                $this->show($path);

                break;

            case "paper":
                if (!$this->aditur->login->isUserLoggedIn()) {
                    header("HTTP/1.1 403 Forbidden");

                    exit;
                }

                $path = "data/" . implode("/", $argv);

                if ($argv[2] != $_SESSION['id']) {
                    header("HTTP/1.1 403 Forbidden");

                    exit;
                }

                $this->show($path);

                break;

            case "babyPics":
                if (!$this->aditur->login->isUserLoggedIn()) {
                    header("HTTP/1.1 403 Forbidden");

                    exit;
                }

                if ($argv[1] != $_SESSION['id']) {
                    header("HTTP/1.1 403 Forbidden");

                    exit;
                }

                $path = "data/babyPics/" . $argv[1] . ".jpg";

                if (!file_exists($path)) {
                    $path = "data/babyPics/baby.jpg";
                }

                $this->show($path);

                break;

            case "private":
                ob_end_clean();
                header("HTTP/1.1 404 Not Found");

                exit;

            default:
                $this->show("data/" . implode("/", $argv));
                break;
        }
    }


    public function show($file) {
        if (file_exists($this->aditur->path($file))) {
            ob_end_clean();

            header("Cache-Control: private, max-age=604800"); // 7 days
            header('Expires: '.gmdate('D, d M Y H:i:s', time()+604800).' GMT');
            header('Content-Type: '.mime_content_type($this->aditur->path($file)));

            readfile($this->aditur->path($file));

            exit;
        }
        else {
            ob_end_clean();
            header("HTTP/1.1 404 Not Found");

            exit;
        }
    }
}


?>
