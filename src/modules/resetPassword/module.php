<?php
// Module: resetPassword


class resetPassword extends Module {
    private $success = false;

    // Default state when called by Aditur from URL
    public function fromUrl($argv = null) {
        if (is_null($argv)) {
            $this->success = $this->op();
        }
        else {
            $this->success = $this->op($argv[0]);
        }


        if (is_null($argv)) {
            $this->printOrderPasswordResetForm();
        }
        else {
            $this->printPasswordResetForm($argv[0]);
        }
    }


    protected function printOrderPasswordResetForm() {
        if (isset($_POST['order_password_reset_operation']) && $this->success) {
            // $this->aditur->note("E-Mail wurde versandt.");
        }
        else {
            ?>
            <div class="PasswordResetForm Box">
                <form class="SubmitForm PasswordResetForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <p>Gib hier Deine E-Mail-Adresse ein, die mit Deinem Account verknüpft ist und wir schicken Dir eine E-Mail, mit der Du Dein Passwort zurücksetzen kannst.<br></p>

                    <input type="email" name="email" placeholder="E-Mail-Adresse">

                    <button type="submit" name="order_password_reset_operation">
                        <i class="fa fa-lg fa-check"></i>&nbsp;Bestätigen
                    </button>
                </form>

                <p style="text-align: center;"><br><a href="<?php echo $this->aditur->path("/"); ?>"><i class="fa fa-l fa-arrow-left"></i>&nbsp;Zurück</a></p>
            </div>

            <?php
        }
    }


    protected function printPasswordResetForm($token) {
        // check token
        $query = $this->db->prepare("SELECT id, user_id, timestamp FROM aditur_password_reset WHERE token=?");
        $query -> bind_param('s', $token);
        $query -> bind_result($resetId, $userId, $timestamp);
        $query -> execute();

        $query -> store_result();

        $this->aditur->log($query -> num_rows);

        if ($query -> num_rows != 1 && !$this->success) {
            $this->aditur->error("Dein Link ist abgelaufen. Bitte fordere einen neuen an.");

            $query -> close();

            $this->printOrderPasswordResetForm();

            return false;
        }

        if ($this->success && isset($_POST['reset_password_operation'])) {
            $this->aditur->note("Passwort wurde geändert. Du kannst Dich nun <a href=\"" . $this->aditur->path("/headquarter") . "\">hier</a> wieder anmelden.");

            return;
        }

        ?>

        <div class="PasswordReset Box">
            <form class="SubmitForm NewPasswordForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <p>Gib hier Dein neues Passwort ein</p>

                <input type="password" name="new_password" placeholder="Neues Passwort">

                <input type="password" name="new_password_twice" placeholder="Neues Passwort bestätigen">

                <button type="submit" name="reset_password_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Bestätigen
                </button>
            </form>
        </div>

        <?php
    }


    protected function removeReset($token) {
        //$validtime = date("Y-m-d H:i:s", time()-(48 * 60 * 60));
        $validtime = (time()-(48 * 60 * 60));
        $query = "DELETE FROM aditur_password_reset WHERE timestamp<$validtime";
        $this->db->query($query);
    }


    protected function validatepassword($password, $id) {
        //password must be longer than a specified length
        if (strlen($password) < 6) return false;
        //define invalid passwords
        $invalidstrings = array(
                            "password",
                            "1234",
                            "passwort"
                        );
        //add obvious things like name or mail
        $query = $this->db->prepare("SELECT forename, lastname, email FROM aditur_members WHERE id=?");
        $query -> bind_param('i', $id);
        $query -> bind_result($forn, $lastn, $mail);
        $query -> execute();
        $query -> fetch();
        $query -> close();
        $invalidstrings[] = $forn;
        $invalidstrings[] = $lastn;
        $invalidstrings[] = strstr($mail, "@", true);
        //check if there´s an invalid string in the password
        foreach ($invalidstrings as $invalidstring) {
            $pos = stripos($password, $invalidstring);
            if ($pos!==false) return false;
        }
        return true;
    }


    protected function op($token = null) {
        
        $this->removeReset($token);
        
        if (isset($_POST['order_password_reset_operation']) && !empty($_POST['email'])) {
            $email = strtolower($_POST['email']);

            // check for right email
            $query = $this->db->prepare("SELECT forename, id FROM aditur_members WHERE email=?");
            $query -> bind_param('s', $email);
            $query -> bind_result($forename, $id);
            $query -> execute();

            $query -> store_result();

            if ($query -> num_rows !== 1) {
                $this->aditur->error("Diese Email ist ungültig!");
                $query -> close();
                return false;
            }

            $query -> fetch();
            $query -> close();

            $hash = md5($forename . time());
            
            $timestamp = time();

            $query = "INSERT INTO aditur_password_reset (user_id, token, timestamp) VALUES ($id, '$hash', $timestamp)";
            if (!$this->db->query($query)) $this->aditur->log($this->db->error);

            $message = "<h1>Reset des Aditurpassworts</h1><p><br><br>Um Dein Passwort zurückzusetzen, klick auf Diesen Link!</p><a href=\"http://" . $this->aditur->config("domain") . "/resetPassword/" . $hash . "\">Passwort zurücksetzen</a><p><br><br>Die Gültigkeit dieses Links endet in 48 Stunden.</p>";

            if (!$this->aditur->sendEmail($email, "Aditur Passwortreset", $message)) {
                $this->aditur->error("Deine E-Mail konnte nicht versandt werden. Bitte wende Dich an den Admin!");
            }
            else {
                $this->aditur->note("E-Mail wurde versandt.");
            }

            return true;
        }
        else if (isset($_POST['reset_password_operation']) && !empty($_POST['new_password']) && !empty($_POST['new_password_twice']) && !empty($token)) {
            if ($_POST['new_password'] !== $_POST['new_password_twice']) {
                $this->aditur->error("Die beiden Passwörter stimmten nicht überein.");
                return false;
            }

            // check token
            $query = $this->db->prepare("SELECT id, user_id, timestamp FROM aditur_password_reset WHERE token=?");
            $query -> bind_param('s', $token);
            $query -> bind_result($resetId, $userId, $timestamp);
            $query -> execute();

            $query -> store_result();

            if ($query -> num_rows !== 1) {
                $this->aditur->error("Dein Link ist abgelaufen. Bitte fordere <a href=\"" . $this->aditur->path("/resetPassword") . "\">hier</a> einen neuen an.");

                $query -> close();
                return false;
            }

            $query -> fetch();
            $query -> close();

            // check if link is still valid
            // timeDifference should be smaller than 48 hours
            $timeDifference = $timestamp - time();

            if ($timeDifference > (48 * 60 * 60)) {
                $this->aditur->error("Dein Link ist abgelaufen. Bitte fordere <a href=\"" . $this->aditur->path("/resetPassword") . "\">hier</a> einen neuen an.");

                return false;
            }

            //check if password is valid
            if (!$this->validatepassword($_POST['new_password'], $userId)) {
                $this->aditur->error("Dein Passwort ist nicht sicher! Es muss mindestens 6 Zeichen lang sein und darf keine offensichtlichen Bestandteile, wie 'passwort' oder deinen Namen enthalten.");
                return false;
            }


            // execute Update
            $hash = password_hash($userId . $_POST['new_password'] . $this->aditur->config("PEPPER"), PASSWORD_DEFAULT, array('cost' => $this->aditur->config("COST")));

            $query = "UPDATE aditur_members SET password='$hash' WHERE id=$userId";
            if (!$this->db->query($query)) {
                $this->aditur->error("Es ist ein Fehler aufgetreten. Bitte wende dich an " . "<a href=\"mailto:" . $this->aditur->getAdminsEmailAddress . "\">" . $this->aditur->getAdminsEmailAddress() . "</a>");

                $this->aditur->log($this->db->error);

                return false;
            }
            
            // delete token
            $query = $this->db->prepare("DELETE FROM aditur_password_reset WHERE token=?");
            $query -> bind_param('s', $token);
            $query -> execute();
            $query -> close();


            $query = "DELETE FROM aditur_login_attempts WHERE user_id=$userId";
            if (!$this->db->query($query)) $this->aditur->log($this->db->error);



            return true;
        }
    }
}



?>
