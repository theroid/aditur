<?php
// Module: Collections


class Collections extends Module {
    public function op($collection = null) {
        if (!empty($_POST['new_collection_name'])) {
            if ($this->aditur->mayI("createCollection")) {
                if (strpos($_POST['new_collection_name'], "/") === false && strpos($_POST['new_collection_name'], "(") === false && strpos($_POST['new_collection_name'], ")") === false && strpos($_POST['new_collection_name'], ";") === false && strpos($_POST['new_collection_name'], "\"") === false && strpos($_POST['new_collection_name'], "..") === false && strpos($_POST['new_collection_name'], "\\") === false) {
                    // check for existance
                    if (!is_dir(htmlspecialchars("data/collections/" . $_POST['new_collection_name']))) {
                        // create new collection
                        if (!mkdir(htmlspecialchars("data/collections/" . $_POST['new_collection_name']))
                         || !mkdir(htmlspecialchars("data/collections/" . $_POST['new_collection_name'] . "/raw"))
                         || !mkdir(htmlspecialchars("data/collections/" . $_POST['new_collection_name'] . "/originals"))
                         || !mkdir(htmlspecialchars("data/collections/" . $_POST['new_collection_name'] . "/thumbs")))
                        {
                            $this->aditur->error("Diese Sammlung konnte nicht angelegt werden.");
                        }
                    }
                    else {
                        $this->aditur->error("Diese Sammlung gibt es schon!");
                    }
                }
                else {
                    $this->aditur->error("Dieser Name ist ungültig. '/', \\', '\"', '(' & ')' sind nicht erlaubt!");
                }
            }
        }

        if (isset($_POST['delete_collection_operation']) && !empty($_POST['collection_to_delete'])) {
            if ($this->aditur->mayI("deleteCollection")) {
                if ($_POST['collection_to_delete'] === "trash") {
                    return;
                }
                
                $collectiontodelete = str_replace(array('/', '\\', '\"', '..', ';', '(', ')'), "", $_POST['collection_to_delete']);
                if (is_dir(htmlspecialchars("data/collections/" . $collectiontodelete))) {
                    if (is_dir(htmlspecialchars("data/collections/trash/" . $collectiontodelete))) {
                        $date = date("Y-m-d-H-i-s", time());

                        rename(htmlspecialchars("data/collections/trash/" . $collectiontodelete), htmlspecialchars("data/collections/trash/" . $collectiontodelete . "-" . $date));
                    }

                    if (rename(htmlspecialchars("data/collections/" . $collectiontodelete), htmlspecialchars("data/collections/trash/" . $collectiontodelete))) {
                        $this->aditur->note(htmlspecialchars("Die Sammlung \"" . $collectiontodelete . "\" wurde in den Papierkorb verschoben."));
                    }
                }
            }
        }

        if (!is_null($collection) && ($this->aditur->config("upload_images_enabled") || $this->aditur->checkPrivileges('administrator'))) {
            if (!empty($_FILES)) {
                $thumbnailMaxWidth = 142;

                $originalMaxWidth = 315;

                
                $filename = htmlspecialchars($collection . "_" . date("y-m-d-H-i-s", time()) . "." . sprintf('%04d', $_SESSION['id']) . ".jpg");
                $path = htmlspecialchars("data/collections/" . $collection . "/raw/" . $filename);


                if (file_exists($path)) {
                    
                    $this->aditur->error("Bild konnte nicht gespeichert werden!");
                    return;
                    
                    // rename the old one
                    /*$date = date("Y-m-d-H-i-s", time());

                    if (!rename(htmlspecialchars("data/collections/" . $collection . "/raw/" . $_FILES['file']['name']), htmlspecialchars("data/collections/" . $collection . "/raw/" . $_FILES['file']['name'] . "-$date.jpg"))) {
                        $this->aditur->error("Bild konnte nicht gespeichert werden!");

                        return;
                    }
                    if (!rename(htmlspecialchars("data/collections/" . $collection . "/originals/" . $_FILES['file']['name']), htmlspecialchars("data/collections/" . $collection . "/originals/" . $_FILES['file']['name'] . "-$date.jpg"))) {
                        $this->aditur->error("Bild konnte nicht gespeichert werden!");

                        return;
                    }
                    if (!rename(htmlspecialchars("data/collections/" . $collection . "/thumbs/" . $_FILES['file']['name']), htmlspecialchars("data/collections/" . $collection . "/thumbs/" . $_FILES['file']['name'] . "-$date.jpg"))) {
                        $this->aditur->error("Bild konnte nicht gespeichert werden!");

                        return;
                    }*/
                }


                $image = $this->aditur->imagecreatefromfile($_FILES['file']['tmp_name']);


                // create thumbnail
                $thumbnailHeight = imagesy($image) * $thumbnailMaxWidth / imagesx($image);

                $thumbnail = imagecreatetruecolor($thumbnailMaxWidth, $thumbnailHeight);

                imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailMaxWidth, $thumbnailHeight, imagesx($image), imagesy($image));


                if (!imagejpeg($thumbnail, $this->aditur->path(htmlspecialchars("data/collections/" . $collection . "/thumbs/" . $filename)))) {
                    $this->aditur->error("Bild konnte nicht gespeichert werden!");

                    return;
                }

                // create fitting originals size
                $originalHeight = imagesy($image) * $originalMaxWidth / imagesx($image);

                $original = imagecreatetruecolor($originalMaxWidth, $originalHeight);

                imagecopyresampled($original, $image, 0, 0, 0, 0, $originalMaxWidth, $originalHeight, imagesx($image), imagesy($image));


                if (!imagejpeg($original, $this->aditur->path(htmlspecialchars("data/collections/" . $collection . "/originals/" . $filename)))) {
                    $this->aditur->error("Bild konnte nicht gespeichert werden!");

                    return;
                }


                if (imagejpeg($image, $this->aditur->path(htmlspecialchars("data/collections/" . $collection . "/raw/" . $filename)))) {
                    $this->aditur->note("Bild(er) erfolgreich hochgeladen.");

                    unlink($_FILES['file']['tmp_name']);
                } else {
                    $this->aditur->error("Bild konnte nicht gespeichert werden. Bitte gib dem Administrator Bescheid!");

                    return;
                }

                return;
            }
            else if (isset($_POST['delete_file_operation']) && !empty($_POST['file_to_delete'])) {
                if ($this->aditur->mayI("deleteCollectionFile") || explode(".", $_POST['file_to_delete'])[1] == sprintf('%04d', $_SESSION['id'])) {
                    $filetodelete = str_replace(array('/', '\\', '..', ';', '(', ')'), "", $_POST['file_to_delete']);
                    if (file_exists(htmlspecialchars("data/collections/" . $collection . "/raw/" . $filetodelete))) {
                        unlink(htmlspecialchars("data/collections/" . $collection . "/raw/" . $filetodelete));

                        unlink(htmlspecialchars("data/collections/" . $collection . "/thumbs/" . $filetodelete));

                        unlink(htmlspecialchars("data/collections/" . $collection . "/originals/" . $filetodelete));
                    }
                }
            }
        }
    }

    // Default state when called by Aditur from URL
    public function fromUrl($argv = null) {
        if (!is_null($argv)) {
            $this->op($argv[0]);
        }
        else {
            $this->op();
        }

        if (!is_null($argv[0])) {
            $this->printCollection($argv[0]);
            return;
        }

        if ($this->aditur->mayI("createCollection")) {
            $this->printCreateNewCollectionForm();
        }

        $this->printCollections();
    }


    public function printCollections() {
        $collections = array_diff(scandir("data/collections"), array('..', '.', 'trash'));

        if (empty($collections)) {
            $this->aditur->error("Es wurde noch keine Sammlung angelegt.");
            return;
        }

        ?>
        <h1>Bildersammlungen</h1>

        <div class="Collections">

        <?php

        $nCollections = 0;
        foreach ($collections as $collection) {

            $files = array_diff(scandir("data/collections/" . $collection . "/raw/"), array('..', '.'));


            // render collections preview
            ?>

            <div class="Collection">
                <?php

                if ($this->aditur->mayI("deleteCollection")) {
                    ?>
                    <button type="button" class="ToggleButton" onclick="if (document.getElementById('deleteCollection<?php echo $nCollections; ?>').style.display === 'none') { document.getElementById('deleteCollection<?php echo $nCollections; ?>').style.display = 'block' } else { document.getElementById('deleteCollection<?php echo $nCollections; ?>').style.display = 'none' }">
                        <i class="fa fa-times-circle"></i>
                    </button>

                    <form action="<?php echo $this->aditur->url(); ?>" method="post" id="deleteCollection<?php echo $nCollections; ?>" style="display: none;">
                        <input type="hidden" name="collection_to_delete" value="<?php echo $collection; ?>">

                        <p>Diese Sammlung wirklich löschen?</p>

                        <button type="submit" name="delete_collection_operation">
                            <i class="fa fa-trash"></i>&nbsp;Löschen
                        </button>
                    </form>

                    <?php
                }

                ?>
                <a href="<?php echo $this->aditur->path("/collections/" . $collection); ?>">
                    <div class="Preview divideBy<?php echo count($files) > 1 ? "2" : count($files); ?>"><?php

                if (!empty($files)) {
                    $nFiles = 0;
                    foreach ($files as $file) {
                        // render 4 previews at maximum
                        if ($nFiles >= 4) {
                            break;
                        }

                        ?>
                        <div class="ImageSizer Icon">
                            <img alt="" src="<?php echo $this->aditur->path("/media/collections/" . $collection . "/thumbs/" . $file); ?>">
                        </div>
                        <?php

                        $nFiles++;
                    }
                }
                else {
                    ?><p class="Empty">leer</p><?php
                }

                ?>
                    </div>

                    <p class="Name"><?php echo $collection; ?></p>
                </a>
            </div>

            <?php

            $nCollections++;
        }

        ?>

        </div>

        <?php
    }


    public function printCreateNewCollectionForm() {
        ?>

        <div class="CreateCollection Box">
            <form class="SubmitForm" action="<?php echo $this->aditur->path("/collections/"); ?>" method="post">
                <h3>Eine neue Bildersammlung anlegen</h3>

                <input type="text" name="new_collection_name" autocomplete="off" placeholder="Name der neuen Sammlung">

                <button name="collection_operation" type="submit"><i class="fa fa-lg fa-check"></i>&nbsp;Erstellen</button>
            </form>
        </div>

        <?php
    }


    public function printCollection($collection) {
        if (!in_array($collection, scandir($this->aditur->path("data/collections/")))) {
            $this->printCollections();
            return;
        }

        ?>
        <a href="<?php echo $this->aditur->path("/collections"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <h1>Bildersammlung: <?php echo $collection; ?></h1>

        <?php

        $files = array_diff(scandir($this->aditur->path("data/collections/" . $collection . "/raw")), array('..', '.'));

        if (empty($files)) {
            ?>

            <div class="Collection"><p class="Empty">leer</p></div>

            <?php
        }
        else {
        ?>

        <div class="Collection divideBy<?php echo count($files) > 1 ? "2" : count($files); ?>">
            <?php $i=0; foreach ($files as $file) {
                $i++;
            ?>
            <div class="File">
                <?php

                if ($this->aditur->mayI("deleteCollectionFile") || explode(".", $file)[1] == sprintf('%04d', $_SESSION['id'])) {
                    ?>
                <button type="button" class="ToggleButton" onclick="if (document.getElementById('delete<?php echo $file; ?>').style.display === 'none') { document.getElementById('delete<?php echo $file; ?>').style.display = 'block' } else { document.getElementById('delete<?php echo $file; ?>').style.display = 'none' }">
                        <i class="fa fa-times-circle"></i>
                    </button>

                    <form action="<?php echo $this->aditur->url(); ?>" method="post" id="delete<?php echo $file; ?>" style="display: none;">
                        <input type="hidden" name="file_to_delete" value="<?php echo $file; ?>">

                        <p>Diese Datei löschen?</p>

                        <button type="submit" name="delete_file_operation">
                            <i class="fa fa-trash"></i>&nbsp;Löschen
                        </button>
                    </form>

                    <?php
                }

                ?>
                <a href="<?php echo $this->aditur->path("/media/collections/" . $collection ."/raw/" . $file); ?>">
                    <img alt="" src="<?php echo $this->aditur->path("/media/collections/" . $collection . "/originals/" . $file); ?>">
                    <p class="Name"><?php echo $collection."_$i"; ?></p>
                </a>
            </div>

            <?php
            }
            ?>
        </div>
            <?php
        }
        
        if ($this->aditur->config("upload_images_enabled") || $this->aditur->checkPrivileges('administrator')) {
            ?>

            <div class="Upload">
                <form action="<?php echo $this->aditur->url(); ?>" method="post" id ="dropzone" class="dropzone" enctype="multipart/form-data" multiple>
                    <div class="fallback">
                        <input name="file" type="file" multiple>
                        <button type="submit"><i class="fa fa-lg fa-upload"></i>&nbsp;Hochladen</button>
                    </div>
                </form>
            </div>

            <script src="<?php echo $this->aditur->path("/media/scripts/dropzone.js"); ?>"></script>

            <?php
        }
    }
}



?>
