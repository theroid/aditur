<?php
// Module: Members


// Every module that should be accessible, must be in table:aditur_modules.
class Members extends Module {
    // when the module is requested by the user:
    public function fromUrl($argv) {
        if (!empty($argv[0])) {
            if ($argv[0] == "mail") {
                $this->showMail($argv);
            }
            else {
                $this->showIndex();
            }
        }
        else {
            $this->showIndex();
        }
    }


    // process POST Data
    // @return true on success, false on error
    public function post() {
        $success = false;

        if (isset($_POST['send_email_operation'])) {
            // check necessary POST-variables
            if (empty($_POST['email_body'])) return false;
            if (empty($_POST['email_subject'])) return false;


            // parse target
            $target = explode("/", $this->aditur->url())[3];

            if ($target != "members" && $target != "speakers" && $target != "komitee") {
                return false;
            }


            $komiteeId= null;

            if ($target == "komitee") {
                $komiteeId = explode("/", $this->aditur->url())[4];
            }


            $success = $this->sendEmailTo($target, $_POST['email_subject'], $_POST['email_body'], $komiteeId);

            unset($_POST['send_email_operation']);
        }


        return $success;
    }

    private function sendEmailTo($target, $subject, $text, $komiteeId = null) {
        if ($target == "komitee") {
            $q = $this -> db -> prepare("SELECT id FROM aditur_komitees WHERE id=?");
            $q -> bind_param('i', $komiteeId);
            $q -> bind_result($komiteeId);

            if (!$q -> execute()) {
                $q -> close();

                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Mit der Datenbank stimmt etwas nicht. Bitte wende Dich an den Admin.");

                return false;
            }

            $q -> store_result();

            if ($q -> num_rows != 1) {
                $this->aditur->error("Dieses Komitee existiert nicht.");

                return false;
            }

            $q -> fetch();

            $q -> close();
        }


        // select email addresses
        $addresses = array();

        if ($target == "members") {
            $q = $this -> db -> query("SELECT email FROM aditur_members");


        }
        elseif ($target == "speakers") {
            $q = $this -> db -> query("SELECT email FROM aditur_members WHERE privileges='speakers'");
        }
        elseif ($target == "komitee") {
            $q = $this -> db -> query("SELECT email FROM aditur_members WHERE komitee=" . $komiteeId);
        }
        else {
            $q -> close();

            $this->aditur->log("ERROR sendEmailTo() called with invalid target!");

            return false;
        }

        while ($object = $q -> fetch_object()) {
            if ($object -> email == "") {
                continue;
            }

            $addresses[] = $object->email;
        }

        $q -> close();


        // create email address string from address array
        $receiver = $this->aditur->config("admin_email_address");


        // create message's HTML
        $message = "<!DOCTYPE html>
        <html>
            <head>
                <meta charset=\"utf-8\">
            </head>
            <body>"
            . $text .
            "</body>
        </html>";


        $header  = 'MIME-Version: 1.0' . "\r\n";
        $header .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $header .= 'From: ' . $_SESSION['email'] . "\r\n";
        $header .= 'Reply-To: ' . $_SESSION['email'] . "\r\n";
        $header .= 'Bcc: ' . implode(",\r\n", $addresses);


        if (mail($receiver, $subject, $message, $header)) {
            $this->aditur->note("E-Mail wurde erfolgreich versandt.");

            return true;
        }
        else {
            $this->aditur->log("ERROR sendEmailTo konnte E-Mail nicht senden!");

            $this->aditur->error("E-Mail konnte nicht versandt werden. Bitte wende Dich an den Admin.");

            return false;
        }
    }


    private function showIndex() {
        $isPresident = $this->aditur->checkPrivileges("president");

        ?>

        <h2><i class="fa fa-lg fa-envelope"></i>&nbsp;Mitglieder</h2>

        <section class="Members">
            <?php

            if ($isPresident) {
                ?>

                <div class="MailToAll Box">
                    <a class="MailLink" href="<?php echo $this->aditur->path("/members/mail/members"); ?>">
                        <i class="fa fa-lg fa-envelope"></i>&nbsp;E-Mail&nbsp;an&nbsp;alle&nbsp;Jahrgangsmitglieder
                    </a>

                </div>

                <?php
            }

            // print all komitees with their respective members

            // if admin, select all komitees
            // otherwise select only own komitee
            if ($isPresident) {
                $q = $this -> db -> query("SELECT id, name FROM aditur_komitees ORDER BY name ASC");
            }
            else {
                $q = $this -> db -> query("SELECT id, name FROM aditur_komitees WHERE id=" . $_SESSION['komitee']);
            }

            $komitees = array();

            while ($object = $q -> fetch_object()) {
                $komitees[] = $object;
            }

            $q -> close();


            foreach ($komitees as $komitee) {
                $q = $this -> db -> query("SELECT id, forename, lastname, email FROM aditur_members WHERE komitee=" . $komitee->id);

                $members = array();

                while ($object = $q -> fetch_object()) {
                    $members[] = $object;
                }

                $q -> close();


                ?>

                <div class="Komitee Box">
                    <h3><?php if ($komitee->name == "/") { echo "/ Keinem Komitee zugeordnet"; } else { echo $komitee->name; } ?></h3>

                    <table class="Members">
                        <tr>
                            <th>Name</th>
                            <th>E-Mail</th>
                        </tr>

                        <?php

                        foreach ($members as $member) {
                            ?>

                            <tr>
                                <td><?php echo $member->forename . " " . $member->lastname; ?></td>
                                <td><?php echo $member->email; ?></td>
                            </tr>

                            <?php
                        }

                        ?>

                    </table>

                    <a class="MailLink" href="<?php echo $this->aditur->path("/members/mail/komitee/" . $komitee->id . "/" . $komitee->name); ?>">
                        <i class="fa fa-lg fa-envelope"></i>&nbsp;E-Mail&nbsp;an&nbsp;dieses&nbsp;Komitee
                    </a>
                </div>

                <?php
            }


            ?>

            <div class="Speakers Box">
                <h3>Komiteesprecher</h3>

                <?php

                // select all speakers
                $q = $this -> db -> query("SELECT aditur_members.id, forename, lastname, email, aditur_members.komitee, aditur_komitees.name FROM aditur_members, aditur_komitees WHERE privileges='speaker' AND aditur_komitees.id=aditur_members.komitee");


                $speakers = array();

                while ($object = $q -> fetch_object()) {
                    $speakers[] = $object;
                }


                ?>

                <table class="Speakers">
                    <tr>
                        <th>Komitee</th>
                        <th>Name</th>
                        <th>E-Mail</th>
                    </tr>

                    <?php

                    foreach ($speakers as $speaker) {
                        ?>

                        <tr>
                            <td><?php echo $speaker->name; ?></td>
                            <td><?php echo $speaker->forename . " " . $speaker->lastname; ?></td>
                            <td><?php echo $speaker->email; ?></td>
                        </tr>

                        <?php
                    }

                    ?>

                </table>

                <a class="MailLink" href="<?php echo $this->aditur->path("/members/mail/speakers"); ?>">
                    <i class="fa fa-lg fa-envelope"></i>&nbsp;E-Mail&nbsp;an&nbsp;alle&nbsp;Komiteesprecher
                </a>
            </div>
        </section>

        <?php
    }


    private function showMail($parameters) {
        // set heading respectively to email target
        $heading = "";

        switch ($parameters[1]) {
            case "members":
                $heading = "E-Mail an den Jahrgang senden";

                break;
            case "speakers":
                $heading = "E-Mail an die Komiteesprecher senden";

                break;
            case "komitee":
                $q = $this -> db -> prepare("SELECT id, name FROM aditur_komitees WHERE id=?");
                $q -> bind_param('i', $parameters[2]);
                $q -> bind_result($id, $name);

                if (!$q -> execute()) {
                    $q -> close();

                    $this->aditur->log($this -> db -> error);

                    $this->aditur->error("Etwas stimmt mit der Datenbank nicht. Bitte wende Dich an den Admin.");

                    return false;
                }

                $q -> store_result();

                if ($q -> num_rows != 1) {
                    $this->aditur->error("Dieses Komitee existiert nicht.");

                    return false;
                }

                $q -> fetch();

                $q -> close();

                if ($name == "/") {
                    $name = "alle ohne Komitee";
                }

                $heading = "E-Mail an " . $name . " senden";

                break;
            default:
                $this->showIndex();

                return false;
        }

        ?>

        <h2><i class="fa fa-lg fa-envelope"></i>&nbsp;Mitglieder</h2>

        <section class="Members">
            <a class="Back" href="<?php echo $this->aditur->path("/members"); ?>">
                <i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück
            </a>

            <div class="SendEmail Box">
                <form class="SendEmailForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <h3><?php echo $heading; ?></h3>

                    <input type="text" name="email_subject" value="" placeholder="Betreff" required>

                    <textarea name="email_body" placeholder="E-Mail-Text" required></textarea>
                    <p>HTML wird unterstützt. Das ist dein &lt;body&gt;.</p>

                    <button type="submit" name="send_email_operation">
                        <i class="fa fa-lg fa-paper-plane"></i>&nbsp;Senden
                    </button>
                </form>
                <?php



                ?>

            </div>
        </section>

        <?php
    }
}

?>
