<?php
// Module: Css


class Css extends Module {
    public function fromUrl($argv) {
        if (strpos(end($argv), '.') === false) {
            header("HTTP/1.1 404 Not Found");
            exit;
        }


        if ("aditur" === $argv[0]) {
            unset($argv[0]);
            $dir = "css/" . implode("/", $argv);
        }
        else {
            $dir = "modules/" . implode("/", $argv);
        }

        if (file_exists($this->aditur->path($dir))) {
            header("Content-Type: text/css");
            readfile($this->aditur->path($dir));
            exit;
        }
        else {
            header("HTTP/1.1 404 Not Found");
            exit;
        }
    }
}


?>
