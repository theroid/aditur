<?php
// Module: Teachers


class imageGallery extends Module {
    public function fromUrl($argv) {
        //if (isset($argv[0]) && isset($argv[2]) && $argv[0] === "getImageList") {
        if ($argv[0] == "getImageList") {
            $this->getImageList($argv[2]);
        }
        else if (isset($argv[0])) {
            $this->printScript();
        }
        else {
            // TODO
            // $this->aditur->loadModule("home", "fromUrl");
        }
    }


    protected function getImageList($event) {
        // turn off output buffering and delete cache
        ob_end_clean();


        //only for testing!
        //this file should be replaced
        //when concerning functionality is implemented officially!

        //returns list of images in specified directory

        $eventDir   = "./data/photos/";

        $thumbs     = "thumbs";
        $originals  = "originals";


        //check if the event exists. Not really safe but better than nothing.
        $possibleEvents = array_diff(scandir($eventDir), array('..', '.'));

        if ($event > count($possibleEvents)) {
            die("Event does NOT exist!");
        }


        //return list of files
        $files = array_diff(scandir($eventDir.$event."/".$thumbs), array('..', '.'));

        foreach ($files as $path) {
            echo $path."\n";
        }

        exit;
    }


    protected function printScript() {
        ?>

        <!-- Image Gallery -->
        <section id="imageViewer">
           <i id="imageViewerClose" class="fa fa-lg fa-times Control" onclick="closeImageViewer()"></i>

            <section id="imageViewerThumbs">

            </section>

            <section id="imageViewerFullScreenGallery">
                <div>
                    <img id="imageViewerBackgroundImage" alt="Gro&szlig;es Bild wird geladen, einen Moment..." src="">
                </div>
                <div id="imageViewerStream"></div>

                <div id="imageViewerFrontLayer">
                    <div class="Controls">
                        <i id="imageViewerControlLeft" class="fa fa-lg fa-chevron-left Control" onclick="changePicture(-1)"></i>
                        <i id="imageViewerControlRight" class="fa fa-lg fa-chevron-right Control" onclick="changePicture(1)"></i>
                        <i id="imageViewerControlClose" class="fa fa-lg fa-times Control" onclick="closeFullScreenGallery()"></i>

                        <i id="imageViewerControlDownload" class="fa fa-lg fa-download Control" onclick="downloadImage()"></i>
                    </div>

                    <div id="imageViewerInfoBar">
                        <!-- comments, favourites, ... -->
                    </div>
                </div>
            </section>

            <script>
                //ImageViewer script

                var imageViewer = document.getElementById("imageViewer"),
                    thumbs = document.getElementById("imageViewerThumbs"),

                    fullScreenGallery   = document.getElementById("imageViewerFullScreenGallery"),
                    backgroundImage     = document.getElementById("imageViewerBackgroundImage"),
                    imageStream         = document.getElementById("imageViewerStream"),
                    frontLayer          = document.getElementById("imageViewerFrontLayer"),

                    controlLeft         = document.getElementById("imageViewerControlLeft"),
                    controlRight        = document.getElementById("imageViewerControlRight"),

                    infoBar = document.getElementById("imageViewerInfoBar"),
                    lastEventId = -1,
                    thumbsList = null,
                    currentEvent = -1,

                    //current image being displayed in FullScreenGallery
                    currentImage = -1,

                    imagesLoaded = 0,
                    thumbsSize = 200;

                    fullscreenImage = null;

                    //paths
                    imageDirectory  = "/media",
                    photosDirectory = "/media/photos/";


                function initImageViewer(segmentId) {
                    "use strict";

                    //console.log("initImageViewer() invoked");

                    document.body.onresize = function () { imageViewerResizeHandler(); };

                    //download list of thumbs and call insertThumbsList() when ready

                    var ajaxRequest;


                    //testing, pls remove when works
                    //console.log(segmentId);


                    //if the last loaded gallery is requested, just show it again
                    if (segmentId === lastEventId) {
                        //console.log("same EventId");
                        showThumbsList();
                        return;
                    }


                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        ajaxRequest=new XMLHttpRequest();
                    }
                    else {
                        // code for IE6, IE5
                        ajaxRequest=new ActiveXObject("Microsoft.XMLHTTP");
                    }

                    ajaxRequest.onreadystatechange=function() {
                        if (ajaxRequest.readyState==4 && ajaxRequest.status==200) {
                            lastEventId = segmentId;

                            insertThumbsList(ajaxRequest.responseText, segmentId);

                            //console.log("ReadyState");
                        }
                    }

                    ajaxRequest.open("GET", "/imageGallery/getImageList/event/" + segmentId, true);
                    ajaxRequest.send();


                    showThumbsList();
                }

                function showThumbsList() {
                    "use strict";

                    //console.log("showThumbsList() invoked");

                    //open/show #ImageViewerThumbs
                    imageViewer.style.display = "block";
                    thumbs.style.display = "block";

                    document.getElementsByTagName("body")[0].style.overflowY = "hidden";
                }

                function insertThumbsList(list, event) {
                    "use strict";

                    //console.log("insertThumbsList() invoked");
                    //console.log(list);


                    //first, remove old thumbs list
                    while (thumbs.firstChild) {
                        thumbs.removeChild(thumbs.lastChild);
                    }

                    //then
                    //insert downloaded list in #ImageViewerThumbs
                    thumbsList = list.split("\n");
                    currentEvent = event;

                    thumbs.onscroll = function () { appendThumbs(0); };

                    appendThumbs(0);
                }

                function appendThumbs(n) {
                    if (n == 0) {
                        windowWidth = window.innerWidth;
                        windowHeight = window.innerHeight;

                        imagesInARow = Math.floor(windowWidth / thumbsSize);

                        imagesToLoad = Math.floor((windowHeight + thumbs.scrollTop) / thumbsSize * imagesInARow - imagesLoaded) + imagesLoaded;
                    }
                    else {
                        imagesToLoad = imagesLoaded + n;
                    }



                    for ( ; imagesLoaded <= imagesToLoad && imagesLoaded < thumbsList.length-1; imagesLoaded++) {   //list.length-1 because last item is empty
                        var newImage = document.createElement("img");

                        newImage.src = photosDirectory + currentEvent + "/thumbs/" + thumbsList[imagesLoaded].replace("\n", "");

                        newImage.onclick = function () { showInFullScreenGallery(this); };

                        thumbs.appendChild(newImage);

                        //console.log(newImage + " " + newImage.src);
                    }
                }

                function showInFullScreenGallery(imageElement) {
                    "use strict";

                    //console.log("showInFullScreenGallery() invoked");


                    //open/show fullscreen gallery
                    fullScreenGallery.style.display = "block";

                    showImage(imageElement);
                }

                function changePicture(direction) {
                    "use strict";

                    //console.log("changePicture() invoked");

                    //append next fullscreen image to image stream
                    //move current pic in opposite direction
                    //and let next picture come in

                    if (direction < 0) {    //move left
                        showImage(currentImage.previousElementSibling);
                    }
                    else {                  //move right
                        // append next thumbsnail to thumbs
                        appendThumbs(1);

                        showImage(currentImage.nextElementSibling);
                    }
                }

                function showImage(imageElement) {
                    "use strict";

                    //console.log("showImage() invoked");
                    //console.log(imageElement);


                    if (imageElement === null) return;

                    //set thumbnail in background layer
                    backgroundImage.src = imageElement.src;

                    //testing
                    //remove old image
                    while (imageStream.firstChild) {
                        imageStream.removeChild(imageStream.lastChild);
                    }


                    //create new segment in imageStream
                    var newSegment = document.createElement("div");

                    newSegment.className = "Segment";

                    //and var image in front layer
                    var newImage = document.createElement("img");

                    newImage.src = imageElement.src.replace("thumbs", "originals");

                    newSegment.appendChild(newImage);


                    imageStream.appendChild(newSegment);


                    //perhaps also show comments and rating


                    //save state in currentImage
                    currentImage = imageElement;

                    fullscreenImage = newImage;
                }

                function closeFullScreenGallery() {
                    "use strict";

                    //hide FullScreenGallery
                    fullScreenGallery.style.display = "none";
                }

                function closeImageViewer() {
                    "use strict";

                    //hide ImageViewer
                    thumbs.style.display = "none";
                    imageViewer.style.display = "none";

                    document.getElementsByTagName("body")[0].style.overflowY = "scroll";

                    document.body.onresize = null;
                }

                function downloadImage() {
                    window.open(fullscreenImage.src.replace("originals", "raw"));
                }

                function imageViewerResizeHandler() {
                    "use strict";

                    console.log("Window resized!");
                }
            </script>
        </section>

        <?php
    }
};


?>
