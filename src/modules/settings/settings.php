<?php
// Settings: Settings (in general)
// domain, email, impressum, anonymous pictures & favicon


class SettingsSettings extends Setting {
    private $title = "";
    private $domain = "";
    private $adminEmailAddress = "";
    private $impressumInfo = "";

    public function fromUrl($argv) {
        $this->title = $this->aditur->config("title");
        $this->domain = $this->aditur->config("domain");
        $this->adminEmailAddress = $this->aditur->config("admin_email_address");
        $this->impressumInfo = $this->aditur->config("impressum_data");


        $this->op();

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/settings"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Einstellungen</a>

        <h2>Allgemeines</h2>

        <p style="margin: 1rem 0;">Klick auf die Eigenschaften, die Du ändern möchtest!</p>

        <section class="Settings Box">
            <form class="SubmitForm PropertyForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <p class="Left">Titel: </p>

                <input type="checkbox" class="Expander PropertyExpander" id="titleExpander">
                <label for="titleExpander"><?php echo $this->title; ?></label>
                <div class="Expansion">
                    <input type="text" id="newTitle" name="new_title" value="<?php echo $this->title; ?>" placeholder="Neuer Titel" required>

                    <button type="submit" name="change_title_operation">
                        <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                    </button>
                </div>
            </form>

            <form class="SubmitForm PropertyForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <p class="Left">Domain: </p>

                <input type="checkbox" class="Expander PropertyExpander" id="domainExpander">
                <label for="domainExpander"><?php echo $this->domain; ?></label>
                <div class="Expansion">
                    <input type="text" id="newDomain" name="new_domain" value="<?php echo $this->domain; ?>" placeholder="Neue Domain" required>

                    <button type="submit" name="change_domain_operation">
                        <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                    </button>
                </div>
            </form>

            <form class="SubmitForm PropertyForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <p class="Left">Kontakt-E-Mail: </p>

                <input type="checkbox" class="Expander PropertyExpander" id="adminEmailExpander">
                <label for="adminEmailExpander"><?php echo $this->adminEmailAddress; ?></label>
                <div class="Expansion">
                    <input type="email" id="newAdminEmail" name="new_admin_email_address" value="<?php echo $this->adminEmailAddress; ?>" placeholder="Neue Kontakt-E-Mail-Adresse" required>

                    <button type="submit" name="change_admin_email_address_operation">
                        <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                    </button>
                </div>
            </form>

            <form class="SubmitForm PropertyForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <p>Impressum</p>

                <input type="checkbox" class="Expander PropertyExpander" id="impressumInfoExpander">
                <label class="FullWidth" for="impressumInfoExpander"><?php echo $this->impressumInfo; ?></label>
                <div class="Expansion FullWidth">
                    <textarea name="new_impressum_info" placeholder="Kontaktdaten, ..." required><?php echo $this->impressumInfo; ?></textarea>

                    <button type="submit" name="change_impressum_info_operation">
                        <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                    </button>

                    <label Class="Button Reset" for="impressumInfoExpander">
                        Abbrechen
                    </label>
                </div>
            </form>
        </section>

        <section class="Settings Box">
            <h3>Standard-Profilbild</h3>

            <form class="SetAnonymousProfilePic SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="new_anonymous_profile_pic">

                <button type="submit" name="change_anonymous_profile_pic_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                </button>

                <button class="Reset" name="reset_anonymous_profile_pic_operation">
                    Zurücksetzen
                </button>
            </form>
        </section>

        <section class="Settings Box">
            <h3>Standard-Profilbild für Lehrer</h3>

            <form class="SetAnonymousTeacherPic SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="new_anonymous_teacher_pic">

                <button type="submit" name="change_anonymous_teacher_pic_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                </button>

                <button class="Reset" name="reset_anonymous_teacher_pic_operation">
                    Zurücksetzen
                </button>
            </form>
        </section>

        <section class="Settings Box">
            <h3>Favicon</h3>

            <p style="padding-left: 3rem;">Das Favicon ist das kleine Bild, das oben im Tab angezeigt wird.</p>


            <form class="SetFavicon SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post" enctype="multipart/form-data">
                <input type="file" name="new_favicon">

                <button type="submit" name="change_favicon_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                </button>

                <button class="Reset" name="reset_favicon_operation">
                    Zurücksetzen
                </button>
            </form>
        </section>

        <?php
    }


    private function op() {
        if (isset($_POST['change_title_operation']) && !empty($_POST['new_title'])) {
            $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key='title'");
            $query -> bind_param('s', $_POST['new_title']);

            if ($query -> execute()) {
                $this->aditur->note("Titel wurde geändert.");

                $this->title = $_POST['new_title'];
            }
            else {
                $this->aditur->error("Titel konnte nicht geändert werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['change_domain_operation']) && !empty($_POST['new_domain'])) {
            $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key='domain'");
            $query -> bind_param('s', $_POST['new_domain']);

            if ($query -> execute()) {
                $this->aditur->note("Domain wurde geändert.");

                $this->domain = $_POST['new_domain'];
            }
            else {
                $this->aditur->error("Domain konnte nicht geändert werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['change_admin_email_address_operation']) && !empty($_POST['new_admin_email_address'])) {
            $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key='admin_email_address'");
            $query -> bind_param('s', $_POST['new_admin_email_address']);

            if ($query -> execute()) {
                $this->aditur->note("Kontakt-E-Mail-Adresse wurde geändert.");

                $this->adminEmailAddress = $_POST['new_admin_email_address'];
            }
            else {
                $this->aditur->error("Kontakt-E-Mail-Adresse konnte nicht geändert werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['change_impressum_info_operation']) && !empty($_POST['new_impressum_info'])) {
            $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key='impressum_data'");
            $query -> bind_param('s', $_POST['new_impressum_info']);

            if ($query -> execute()) {
                $this->aditur->note("Impressum wurde geändert.");

                $this->impressumInfo = $_POST['new_impressum_info'];
            }
            else {
                $this->aditur->error("Impressum konnte nicht geändert werden. Bitte wende Dich an den Admin!");
            }
        }


        if (isset($_POST['change_anonymous_profile_pic_operation']) && !empty($_FILES['new_anonymous_profile_pic']['tmp_name'])) {
            $thumbnailMaxWidth = 130;
            $thumbnailMaxHeight = 100;

            $originalMaxWidth = 700;
            $originalMaxHeight = 400;


            $path = "data/profilePics/anonymous.jpg";


            if (file_exists($path)) {
                // remove the old one
                if (!unlink("data/profilePics/anonymous.jpg")) {
                    $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                    return;
                }
                if (!unlink("data/profilePics/anonymousOriginal.jpg")) {
                    $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                    return;
                }
                if (!unlink("data/profilePics/anonymousThumb.jpg")) {
                    $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                    return;
                }
            }


            $image = $this->aditur->imagecreatefromfile($_FILES['new_anonymous_profile_pic']['tmp_name']);


            if (is_null($image)) {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }


            // create thumbnail
            if (imagesx($image) > imagesy($image)) {
                $thumbnailHeight = imagesy($image) * $thumbnailMaxWidth / imagesx($image);

                $thumbnail = imagecreatetruecolor($thumbnailMaxWidth, $thumbnailHeight);

                imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailMaxWidth, $thumbnailHeight, imagesx($image), imagesy($image));
            }
            else {
                $thumbnailWidth = imagesx($image) * $thumbnailMaxHeight / imagesy($image);

                $thumbnail = imagecreatetruecolor($thumbnailWidth, $thumbnailMaxHeight);

                imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailWidth, $thumbnailMaxHeight, imagesx($image), imagesy($image));
            }

            if (!imagejpeg($thumbnail, $this->aditur->path("data/profilePics/anonymousThumb.jpg"))) {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }

            // create fitting originals size
            if (imagesx($image) > imagesy($image)) {
                $originalHeight = imagesy($image) * $originalMaxWidth / imagesx($image);

                $original = imagecreatetruecolor($originalMaxWidth, $originalHeight);

                imagecopyresampled($original, $image, 0, 0, 0, 0, $originalMaxWidth, $originalHeight, imagesx($image), imagesy($image));
            }
            else {
                $originalWidth = imagesx($image) * $originalMaxHeight / imagesy($image);

                $original = imagecreatetruecolor($originalWidth, $originalMaxHeight);

                imagecopyresampled($original, $image, 0, 0, 0, 0, $originalWidth, $originalMaxHeight, imagesx($image), imagesy($image));
            }

            if (!imagejpeg($original, $this->aditur->path("data/profilePics/anonymousOriginal.jpg"))) {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }


            if (imagejpeg($image, $this->aditur->path("data/profilePics/anonymous.jpg"))) {
                $this->aditur->note("Neues Standardbild wurde gespeichert.");

                unlink($_FILES['new_anonymous_profile_pic']['tmp_name']);
            } else {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }
        }


        if (isset($_POST['change_anonymous_teacher_pic_operation']) && !empty($_FILES['new_anonymous_teacher_pic']['tmp_name'])) {
            $thumbnailMaxWidth = 130;
            $thumbnailMaxHeight = 100;

            $originalMaxWidth = 700;
            $originalMaxHeight = 400;


            $path = "data/teachers/anonymous.jpg";


            if (file_exists($path)) {
                // remove the old one
                if (!unlink("data/teachers/anonymous.jpg")) {
                    $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                    return;
                }
                if (!unlink("data/teachers/anonymousOriginal.jpg")) {
                    $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                    return;
                }
                if (!unlink("data/teachers/anonymousThumb.jpg")) {
                    $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                    return;
                }
            }


            $image = $this->aditur->imagecreatefromfile($_FILES['new_anonymous_teacher_pic']['tmp_name']);


            if (is_null($image)) {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }


            // create thumbnail
            if (imagesx($image) > imagesy($image)) {
                $thumbnailHeight = imagesy($image) * $thumbnailMaxWidth / imagesx($image);

                $thumbnail = imagecreatetruecolor($thumbnailMaxWidth, $thumbnailHeight);

                imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailMaxWidth, $thumbnailHeight, imagesx($image), imagesy($image));
            }
            else {
                $thumbnailWidth = imagesx($image) * $thumbnailMaxHeight / imagesy($image);

                $thumbnail = imagecreatetruecolor($thumbnailWidth, $thumbnailMaxHeight);

                imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailWidth, $thumbnailMaxHeight, imagesx($image), imagesy($image));
            }

            if (!imagejpeg($thumbnail, $this->aditur->path("data/teachers/anonymousThumb.jpg"))) {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }

            // create fitting originals size
            if (imagesx($image) > imagesy($image)) {
                $originalHeight = imagesy($image) * $originalMaxWidth / imagesx($image);

                $original = imagecreatetruecolor($originalMaxWidth, $originalHeight);

                imagecopyresampled($original, $image, 0, 0, 0, 0, $originalMaxWidth, $originalHeight, imagesx($image), imagesy($image));
            }
            else {
                $originalWidth = imagesx($image) * $originalMaxHeight / imagesy($image);

                $original = imagecreatetruecolor($originalWidth, $originalMaxHeight);

                imagecopyresampled($original, $image, 0, 0, 0, 0, $originalWidth, $originalMaxHeight, imagesx($image), imagesy($image));
            }

            if (!imagejpeg($original, $this->aditur->path("data/teachers/anonymousOriginal.jpg"))) {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }


            if (imagejpeg($image, $this->aditur->path("data/teachers/anonymous.jpg"))) {
                $this->aditur->note("Neues Standardbild wurde gespeichert.");

                unlink($_FILES['new_anonymous_teacher_pic']['tmp_name']);
            } else {
                $this->aditur->error("Neues Standardbild konnte nicht gespeichert werden!");

                return;
            }
        }


        if (isset($_POST['change_favicon_operation']) && !empty($_FILES['new_favicon']['tmp_name'])) {
            $path = "data/images/favicon.png";


            if (file_exists($path)) {
                // remove the old one
                if (!unlink("data/images/favicon.png")) {
                    $this->aditur->error("Neues Favicon konnte nicht gespeichert werden!");

                    return;
                }
            }


            $image = $this->aditur->imagecreatefromfile($_FILES['new_favicon']['tmp_name']);


            if (is_null($image)) {
                $this->aditur->error("Neues Favicon konnte nicht gespeichert werden!");

                return;
            }


            if (imagejpeg($image, $this->aditur->path("data/images/favicon.png"))) {
                $this->aditur->note("Neues Favicon wurde gespeichert.");

                unlink($_FILES['new_favicon']['tmp_name']);
            } else {
                $this->aditur->error("Neues Favicon konnte nicht gespeichert werden!");

                return;
            }
        }


        if (isset($_POST['reset_anonymous_profile_pic_operation'])) {
            if (copy("data/private/profilePics/anonymous.jpg", "data/profilePics/anonymous.jpg")
                && copy("data/private/profilePics/anonymousOriginal.jpg", "data/profilePics/anonymousOriginal.jpg")
                && copy("data/private/profilePics/anonymousThumb.jpg", "data/profilePics/anonymousThumb.jpg")) {
                $this->aditur->note("Standardbild wurde zurückgesetzt.");
            }
            else {
                $this->aditur->error("Standardbild konnte nicht zurückgesetzt werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['reset_anonymous_teacher_pic_operation'])) {
            if (copy("data/private/teachers/anonymous.jpg", "data/teachers/anonymous.jpg")
                && copy("data/private/teachers/anonymousOriginal.jpg", "data/teachers/anonymousOriginal.jpg")
                && copy("data/private/teachers/anonymousThumb.jpg", "data/teachers/anonymousThumb.jpg")) {
                $this->aditur->note("Standardbild wurde zurückgesetzt.");
            }
            else {
                $this->aditur->error("Standardbild konnte nicht zurückgesetzt werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['reset_favicon_operation'])) {
            if (copy("data/private/favicon.png", "data/images/favicon.png")) {
                $this->aditur->note("Favicon wurde zurückgesetzt.");
            }
            else {
                $this->aditur->error("Favicon konnte nicht zurückgesetzt werden. Bitte wende Dich an den Admin!");
            }
        }
    }


    public function index() {
        ?>

        <a class="Setting Box" href="<?php echo $this->aditur->path("/settings/settings"); ?>">
            <div>
                <h3>&nbsp;<i class="fa fa-lg fa-cog"></i>&nbsp;Allgemeines</h3>
            </div>
        </a>

        <?php
    }
}

?>
