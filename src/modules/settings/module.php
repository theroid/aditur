<?php
// Module: Settings


class Settings extends Module {
    public function fromUrl($argv) {
        if (!empty($argv[0])
           && file_exists("modules/" . $argv[0] . "/settings.php")) {
            $parameters = $argv;
            unset($parameters[0]);

            include_once("modules/" . $argv[0] . "/settings.php");

            $name = $argv[0] . "Settings";

            $setting = new $name($this->aditur);

            $setting->fromUrl($parameters);
        }
        else {
            // $this->op();

            ?>

            <h2>Einstellungen</h2>

            <section class="Settings">

            <?php

            // search for settings.php in every module folder

            // get all module-folders
            $modules = array_diff(scandir("modules"), array('.', '..'));

            foreach ($modules as $module) {
                $path = "modules/$module/settings.php";

                if (file_exists($path)) {
                    include_once($path);

                    $name = $module . "Settings";

                    $setting = new $name($this->aditur);

                    $setting->index();
                }
            }

            ?>

            </section>

            <?php

            // De/Activate Modules
            $q = $this -> db -> query("SELECT id, name, description, enabled, can_be_disabled FROM aditur_modules WHERE can_be_disabled=1");

            $modules = array();

            while ($object = $q -> fetch_object()) {
                // handle profiles, comments, teachers, censorship and lehrer separately
                if ($object->name == "profiles") {
                    $profiles = $object;
                }
                elseif ($object->name == "comments") {
                    $comments = $object;
                }
                elseif ($object->name == "teachers") {
                    $teachers = $object;
                }
                elseif ($object->name == "censorship") {
                    $censorship = $object;
                }
                elseif ($object->name == "lehrer") {
                    $lehrer = $object;
                }
                else {
                    $modules[] = $object;
                }
            }

            $q -> close();


            // Check for consistency

            // censorship
            $query = $this->db->query("SELECT enabled, privileges, komitee FROM aditur_modules WHERE name='censorship'");

            $object = $query -> fetch_object();

            $censorshipModuleEnabled = $object -> enabled;
            $censorshipRequiredPrivileges = $object -> privileges;
            $censorshipRequiredKomitee = $object -> komitee;

            $idAbizeitung = $this->db->query("SELECT id FROM aditur_komitees WHERE name='Abizeitung'") -> fetch_object() -> id;


            // single groups
            $censorshipKomiteeEnabled = $this->aditur->config("censorship_komitee_enabled");

            $censorshipStudentsEnabled = $this->aditur->config("censorship_students_enabled");

            $censorshipTeachersEnabled = $this->aditur->config("censorship_teachers_enabled");

            // check for consistency
            // if module is enabled although it is not needed, deactivate it
            if ($censorshipModuleEnabled && !$censorshipKomiteeEnabled && !$censorshipStudentsEnabled) {
                $this->db->query("UPDATE aditur_modules SET enabled=0 WHERE name='censorship'");
                
            }

            // if only censorship for komitee is enabled adjust module properties accordingly
            if ($censorshipKomiteeEnabled && !$censorshipStudentsEnabled && ($censorshipRequiredPrivileges != "speaker" || $censorshipRequiredKomitee != $idAbizeitung || !$censorshipModuleEnabled)) {
                $this->db->query("UPDATE aditur_modules SET enabled=1, privileges='speaker', komitee=" . $idAbizeitung . " WHERE name='censorship'");
            }

            // if censorship for students is enabled adjust module properties accordingly
            if ($censorshipStudentsEnabled && ($censorshipRequiredPrivileges != "" || $censorshipRequiredKomitee != 0 || !$censorshipModuleEnabled)) {
                $this->db->query("UPDATE aditur_modules SET enabled=1, privileges='', komitee=0 WHERE name='censorship'");
            }


            // check lehrer module consistency
            $commentsFromTeachersEnabled = $this->aditur->config("comments_from_teachers_enabled");

            // if either subfunction for teachers is enabled, enable the module itself
            if (!$lehrer->enabled && ($commentsFromTeachersEnabled || $censorshipTeachersEnabled)) {
                $this->db->query("UPDATE aditur_modules SET enabled=1 WHERE name='lehrer'");
            }

            // disable module if no subfunction is needed
            if ($lehrer->enabled && !$commentsFromTeachersEnabled && !$censorshipTeachersEnabled) {
                $this->db->query("UPDATE aditur_modules SET enabled=0 WHERE name='lehrer'");
            }

            

            ?>

            <section class="ModuleManagement Box">
                <h3><i class="fa fa-lg fa-toggle-on"></i>&nbsp;De-/Aktivieren</h3>

                <h4>Kommentare</h4>

                <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <input type="hidden" name="target" value="<?php echo $comments->name; ?>">

                    <p><strong>Schüler kommentieren Schüler</strong> ist <?php
                        if ($comments->enabled) {
                            ?><span class="activated">aktiviert</span><?php
                        }
                        else {
                            ?><span class="deactivated">deaktiviert</span><?php
                        }

                        ?>.
                    </p>

                    <button type="submit" class="<?php echo !$comments->enabled ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$comments->enabled ? "activate" : "deactivate"; ?>">
                        <?php echo !$comments->enabled ? "Aktivieren" : "Deaktivieren"; ?>
                    </button>
                </form>

                <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <input type="hidden" name="target" value="<?php echo $teachers->name; ?>">

                    <p><strong>Schüler kommentieren Lehrer</strong> ist <?php
                        if ($teachers->enabled) {
                            ?><span class="activated">aktiviert</span><?php
                        }
                        else {
                            ?><span class="deactivated">deaktiviert</span><?php
                        }

                        ?>.
                    </p>

                    <button type="submit" class="<?php echo !$teachers->enabled ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$teachers->enabled ? "activate" : "deactivate"; ?>">
                        <?php echo !$teachers->enabled ? "Aktivieren" : "Deaktivieren"; ?>
                    </button>
                </form>

                <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <input type="hidden" name="target" value="comments_from_teachers">

                    <p><strong>Lehrer kommentieren Schüler</strong> ist <?php
                        if ($this->aditur->config("comments_from_teachers_enabled")) {
                            ?><span class="activated">aktiviert</span><?php
                        }
                        else {
                            ?><span class="deactivated">deaktiviert</span><?php
                        }

                        ?>.
                    </p>

                    <button type="submit" class="<?php echo !$this->aditur->config("comments_from_teachers_enabled") ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$this->aditur->config("comments_from_teachers_enabled") ? "activate" : "deactivate"; ?>">
                        <?php echo !$this->aditur->config("comments_from_teachers_enabled") ? "Aktivieren" : "Deaktivieren"; ?>
                    </button>
                </form>


                <h4>Zensur</h4>

                <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <input type="hidden" name="target" value="censorship_komitee">

                    <p><strong>Zensur durch das Abizeitungskomitee</strong> ist <?php
                        if ($this->aditur->config("censorship_komitee_enabled")) {
                            ?><span class="activated">aktiviert</span><?php
                        }
                        else {
                            ?><span class="deactivated">deaktiviert</span><?php
                        }

                        ?>.
                    </p>

                    <button type="submit" class="<?php echo !$this->aditur->config("censorship_komitee_enabled") ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$this->aditur->config("censorship_komitee_enabled") ? "activate" : "deactivate"; ?>">
                        <?php echo !$this->aditur->config("censorship_komitee_enabled") ? "Aktivieren" : "Deaktivieren"; ?>
                    </button>
                </form>

                <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <input type="hidden" name="target" value="censorship_students">

                    <p><strong>Zensur der eigenen Kommentare</strong> ist <?php
                        if ($this->aditur->config("censorship_students_enabled")) {
                            ?><span class="activated">aktiviert</span><?php
                        }
                        else {
                            ?><span class="deactivated">deaktiviert</span><?php
                        }

                        ?>.
                    </p>

                    <button type="submit" class="<?php echo !$this->aditur->config("censorship_students_enabled") ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$this->aditur->config("censorship_students_enabled") ? "activate" : "deactivate"; ?>">
                        <?php echo !$this->aditur->config("censorship_students_enabled") ? "Aktivieren" : "Deaktivieren"; ?>
                    </button>
                </form>

                <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <input type="hidden" name="target" value="censorship_teachers">

                    <p><strong>Zensur durch die Lehrer</strong> ist <?php
                        if ($this->aditur->config("censorship_teachers_enabled")) {
                            ?><span class="activated">aktiviert</span><?php
                        }
                        else {
                            ?><span class="deactivated">deaktiviert</span><?php
                        }

                        ?>.
                    </p>

                    <button type="submit" class="<?php echo !$this->aditur->config("censorship_teachers_enabled") ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$this->aditur->config("censorship_teachers_enabled") ? "activate" : "deactivate"; ?>">
                        <?php echo !$this->aditur->config("censorship_teachers_enabled") ? "Aktivieren" : "Deaktivieren"; ?>
                    </button>
                </form>


                <h4>Bilder</h4>
                
                
                <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <input type="hidden" name="target" value="upload_images">

                    <p><strong>Hochladen von Bildern</strong> ist <?php
                        if ($this->aditur->config("upload_images_enabled")) {
                            ?><span class="activated">aktiviert</span><?php
                        }
                        else {
                            ?><span class="deactivated">deaktiviert</span><?php
                        }

                        ?>.
                    </p>

                    <button type="submit" class="<?php echo !$this->aditur->config("upload_images_enabled") ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$this->aditur->config("upload_images_enabled") ? "activate" : "deactivate"; ?>">
                        <?php echo !$this->aditur->config("upload_images_enabled") ? "Aktivieren" : "Deaktivieren"; ?>
                    </button>
                </form>

                               
                                
                <h4>Module</h4>

                <?php

                foreach ($modules as $module) {
                    ?>

                    <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input type="hidden" name="target" value="<?php echo $module->name; ?>">

                        <p><strong><?php echo $module->description; ?></strong> ist <?php
                            if ($module->enabled) {
                                ?><span class="activated">aktiviert</span><?php
                            }
                            else {
                                ?><span class="deactivated">deaktiviert</span><?php
                            }

                            ?>.
                        </p>

                        <button type="submit" class="<?php echo !$module->enabled ? "Activate" : "Deactivate"; ?>" name="activation_operation" value="<?php echo !$module->enabled ? "activate" : "deactivate"; ?>">
                            <?php echo !$module->enabled ? "Aktivieren" : "Deaktivieren"; ?>
                        </button>
                    </form>

                    <?php
                }

                ?>

            </section>


            <section class="AccessCodes Box">
                <h3><i class="fa fa-lg fa-barcode"></i>&nbsp;Zugangscodes für Lehrer</h3>

                <form action="<?php echo $this->aditur->url(); ?>" method="post">
                    <button type="submit" name="download_teachers_access_codes">
                        <i class="fa fa-lg fa-download"></i>&nbsp;Herunterladen
                    </button>
                </form>
            </section>

            <section class="Export Box">
                <h3><i class="fa fa-lg fa-file-text"></i>&nbsp;Inhalte exportieren</h3>

                <form action="<?php echo $this->aditur->path("/export"); ?>" method="post">
                    <p>Bilder und Kommentare zu Lehrern und Schülern, sowie die gesammelten Zitate, Umfragen und Boxen werden in einer ODT-Datei zusammengefasst und heruntergeladen.</p>

                    <p><br>WARNUNG: Dieser Prozess braucht einiges an Zeit und Arbeitsspeicher und die heruntergeladene Datei ist sehr groß!<br>Bitte nur einmal auf Herunterladen drücken und warten, bis die Seite geladen wurde!<br><br>Bei Fehlern sollte das Einbetten von Bildern deaktiviert werden.<br><br></p>

                    <p><input type="checkbox" name="embed_pictures" id="embedPictures"> <label for="embedPictures">Bilder einbetten</label><br><br></p>

                    <p><input type="checkbox" name="black_bars" id="blackBars"> <label for="blackBars">Zensierte Kommentare als Schwarze Balken einfügen</label><br><br></p>

                    <button type="submit" name="download_export">
                        <i class="fa fa-lg fa-download"></i>&nbsp;Herunterladen
                    </button>
                </form>
            </section>

            <?php
        }
    }

    public function post() {
        $success = false;

        if (isset($_POST['download_teachers_access_codes'])) {
            $success = $this->downloadTeachersAccessCodes();

            unset($_POST['download_teachers_access_codes']);
        }


        if (!empty($_POST['activation_operation'])) {
            if (empty($_POST['target'])) return false;

            $success = $this->processActivation($_POST['activation_operation'], $_POST['target']);

            unset($_POST['activation_operation']);
        }


        return $success;
    }

    private function downloadTeachersAccessCodes() {
        $query = $this->db->query("SELECT id, title, name, access_code FROM aditur_teachers ORDER BY name");

        $teachers = array();
        while ($object = $query -> fetch_object()) {
            $teachers[] = $object;
        }

        $query -> close();


        // PHPODT
        $this->aditur->aditur_include("lib/phpodt/phpodt.php");

        $odt = ODT::getInstance();


        $headingPStyle = new ParagraphStyle("HeadingPStyle1");
        $headingPStyle -> setTextAlign(StyleConstants::CENTER);
        $headingPStyle -> setVerticalMargin("0.65cm", "0.65cm");

        $headingStyle = new TextStyle("HeadingStyle1");
        $headingStyle -> setBold();
        $headingStyle -> setFontSize("18pt");


        $breakPageStyle = new ParagraphStyle("Paragraph1");
        $breakPageStyle -> setTextAlign(StyleConstants::CENTER);
        $breakPageStyle -> setBreakAfter(StyleConstants::PAGE);


        $boldText = new TextStyle("BoldText");
        $boldText -> setBold();


        foreach ($teachers as $teacher) {
            $p = new Paragraph($headingPStyle);
            $p -> addText("Zugangscode", $headingStyle);
            $p -> addLineBreak();
            $p -> addLineBreak();
            $p -> addLineBreak();
            $p -> addLineBreak();


            $p = new Paragraph($breakPageStyle);
            $p -> addText("Für: ");
            $p -> addText($teacher->title . " " . $teacher->name, $boldText);

            $p -> addLineBreak();
            $p -> addLineBreak();

            $p -> addText("Zugangscode: ");
            $p -> addText($teacher->access_code, $boldText);

            $p -> addLineBreak();
            $p -> addLineBreak();
            $p -> addLineBreak();

            $p -> addText("Um Ihre Kommentare zu zensieren, gehen Sie bitte auf:");
            $p -> addLineBreak();
            $p -> addHyperlink("http://" . $this->aditur->config("domain") . "/lehrer/" . $teacher->access_code, "http://" . $this->aditur->config("domain") . "/lehrer/" . $teacher->access_code, "Klicken Sie auf diesen Link, um Ihre Kommentare zu zensieren.");

            $p -> addLineBreak();
            $p -> addLineBreak();
            $p -> addLineBreak();
            $p -> addLineBreak();

            $p -> addText("Bei Fragen und Problemen wenden Sie sich gern an uns!");
        }


        $file = $this->aditur->path("data/private/Zugangscodes.odt");

        $odt -> output($file);

        ob_end_clean();

        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));

        readfile($file);

        exit;
    }

    private function processActivation($modus, $target) {
        $modus = $modus == "activate" ? 1 : 0;

        // if this is concerning censorship that is controlled by aditur_config
        // if $target begins with "censorship" at char 0.
        if (strpos($target, "censorship") === 0) {
            switch ($target) {
                case "censorship_komitee":
                    $target = "censorship_komitee_enabled";

                    break;
                case "censorship_students":
                    $target = "censorship_students_enabled";

                    break;
                case "censorship_teachers":
                    $target = "censorship_teachers_enabled";

                    break;
                default:
                    return false;
            }


            $q = $this -> db -> prepare("UPDATE aditur_config SET value=? WHERE config_key=?");
            $q -> bind_param('is', $modus, $target);

            if (!$q -> execute()) {
                $q -> close();

                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Zensur konnte nicht " . ($modus ? "aktiviert" : "deaktiviert") . " werden.");

                return false;
            }

            $q -> close();


            $this->aditur->note("Zensur wurde erfolgreich " . ($modus ? "aktiviert" : "deaktiviert") . ".");

            return true;
        }
        elseif ($target == "comments_from_teachers") {
            $target = "comments_from_teachers_enabled";

            $q = $this -> db -> prepare("UPDATE aditur_config SET value=? WHERE config_key=?");
            $q -> bind_param('is', $modus, $target);

            if (!$q -> execute()) {
                $q -> close();

                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Kommentare von Lehrern konnten nicht " . ($modus ? "aktiviert" : "deaktiviert") . " werden.");

                return false;
            }

            $q -> close();


            $this->aditur->note("Kommentare von Lehrern wurden erfolgreich " . ($modus ? "aktiviert" : "deaktiviert") . ".");

            return true;
        }
        elseif ($target == "upload_images") {
            $target = "upload_images_enabled";

            $q = $this -> db -> prepare("UPDATE aditur_config SET value=? WHERE config_key=?");
            $q -> bind_param('is', $modus, $target);

            if (!$q -> execute()) {
                $q -> close();

                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Hochladen von Bildern konnten nicht " . ($modus ? "aktiviert" : "deaktiviert") . " werden.");

                return false;
            }

            $q -> close();


            $this->aditur->note("Hochladen von Bildern wurde erfolgreich " . ($modus ? "aktiviert" : "deaktiviert") . ".");

            return true;
        }


        // or it is just an ordinary module
        $q = $this -> db -> prepare("UPDATE aditur_modules SET enabled=? WHERE name=?");
        $q -> bind_param('is', $modus, $target);

        if (!$q -> execute()) {
            $q -> close();

            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Modul: " . $target . " konnte nicht " . ($modus ? "aktiviert" : "deaktiviert") . " werden.");

            return false;
        }

        $q -> close();

        $this->aditur->note("Modul: " . $target . " wurde " . ($modus ? "aktiviert" : "deaktiviert") . ".");

        return true;
    }
}


?>
