<?php
// Module: Comments


class Comments extends Module {
    public function fromUrl($argv) {
        // your Comments
        $this->printYourComments();

        // who you have not commented on
        $this->printNotCommentedProfiles();
    }


    protected function op($id = null) {
        if (isset($_POST['comment_operation'])) {
            if (!empty($_POST['new_comment'])) {
                if ($_POST['comment_operation'] == "submit") {
                    $comment = htmlspecialchars($_POST['new_comment']);

                    $query = $this -> db -> prepare("INSERT INTO aditur_comments (author_id, comment_on_id, comment)
                    VALUES (?, ?, ?)");
                    $query -> bind_param('iis', $_SESSION['id'], $id, $comment);
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);
                        $this->aditur->error("Dein Kommentar konnte leider nicht gespeichert werden. Bitte wende Dich an den Admin!");
                        return;
                    }
                }
            }


            if (!empty($_POST['comment_id'])) {
                // get author_id
                $id = $_POST['comment_id'];


                $query = $this->db->prepare("SELECT author_id FROM aditur_comments WHERE id=?");
                $query -> bind_param('i', $id);
                $query -> bind_result($author_id);

                if (!$query -> execute()) {
                    // TODO show error
                    $this->aditur->log($this -> db -> error);
                    $query -> close();
                    return;
                }

                $query -> fetch();

                $query -> close();


                // check if it is the author or a privileged user
                if ($_SESSION['id'] == $author_id) {



                    // perform changes
                    if ($_POST['comment_operation'] == "delete") {
                        $query = $this->db->prepare("DELETE FROM aditur_comments WHERE id=?");
                        $query -> bind_param('i', $id);
                    }
                    else if ($_POST['comment_operation'] == "edit") {
                        $comment = htmlspecialchars($_POST['new_comment']);

                        $query = $this->db->prepare("UPDATE aditur_comments SET comment=? WHERE id=?");
                        $query -> bind_param('si', $comment, $id);
                    }
                    else {
                        $query -> close();
                        return;
                    }
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);
                        $this->aditur->error("Dein Kommentar konnte nicht bearbeitet werden. Probiere es später noch einmal!");
                    }
                    else {
                        $this->aditur->note("Dein Kommentar wurde geändert.");
                    }

                    $query -> close();
                }
            }
        }
    }



    public function printStatistics() {
        // how many comments you wrote
        $query = "SELECT COUNT(id) FROM aditur_comments WHERE author_id=" . $_SESSION['id'];
        $result = $this -> db -> query($query);
        $nWrittenComments = $result -> fetch_row();

        // how many comments were written in summary
        $query = "SELECT COUNT(id) FROM aditur_comments";
        $result = $this -> db -> query($query);
        $nComments = $result -> fetch_row();

        ?>Du hast bisher <?php echo $nWrittenComments[0]; ?> von <?php echo $nComments[0]; ?> Kommentaren insgesamt verfasst.<?php
    }


    public function printYourComments() {
        $this->op();


        $query = "SELECT forename, lastname, aditur_comments.id, comment_on_id, comment FROM aditur_comments, aditur_members WHERE author_id=" . $_SESSION['id'] . " AND aditur_members.id=comment_on_id ORDER BY forename";

        if (!$result = $this -> db -> query($query)) {
            $this->aditur->log($this -> db -> error);
        }

        if ($result->num_rows > 0) {
            ?>

            <div class="Comments">
                <h2>Von Dir verfasste Kommentare</h2>

                <p><?php $this->printStatistics(); ?></p>

                <p><br>Klick auf einen Kommentar um ihn zu bearbeiten.</p>
                <?php
                        $lastId = -1;
                        $expanderCount = 0;

                        while ($comment = $result->fetch_object()) {
                            if ($comment->comment_on_id != $lastId) {
                                if ($expanderCount > 0) {
                ?>
            </div>

            <?php
                    }
    ?>

    <h3>Deine Kommentare zu <?php echo $comment->forename; ?></h3>

    <div class="CommentAuthor">
        <a href="<?php echo $this->aditur->path("/profiles/" . $comment->comment_on_id . "/" . $comment->forename . " " . $comment->lastname); ?>">
           <div class="ImageSizer">
                <img alt="" src="<?php echo $this->aditur->path("/media/profilePics/thumbs/" . $comment->comment_on_id . "/" . $comment->forename . " " . $comment->lastname . ".jpg"); ?>">
            </div>
            <p><?php echo $comment->forename; ?></p>
        </a>
    </div>

    <div class="CommentList">
    <?php
                }

    ?>

         <div class="Comment">
            <input type="checkbox" class="Expander BlackBoxExpander" id="blackBoxExpander<?php echo $expanderCount; ?>">
            <label class="Comment" for="blackBoxExpander<?php echo $expanderCount; ?>"><?php echo $comment->comment; ?></label>
            <form class="ChangeComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">
                <textarea name="new_comment" placeholder="Verfasse einen schönen Kommentar!"><?php echo $comment->comment; ?></textarea>
                <button class="Button Edit" type="submit" value="edit" name="comment_operation"><i class="fa fa-lg fa-pencil"></i>&nbsp;Bearbeiten</button>
                <label class="Button" for="blackBoxExpander<?php echo $expanderCount; ?>"><i class="fa fa-lg fa-remove"></i>&nbsp;Abbrechen</label>
            </form>

            <form class="DeleteComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">
                <button class="ButtonDelete" type="submit" value="delete" name="comment_operation"><i class="fa fa-lg fa-eraser"></i>&nbsp;Löschen</button>
            </form>
        </div>

            <?php
                $expanderCount++;

                $lastId = $comment->comment_on_id;
            }
            ?>
    </div>
</div>
           <?php
        }
        else {
            ?><p>Du hast noch keine Kommentare verfasst.</p><?php
        }
    }



    public function printNotCommentedProfiles() {
        // get all member ids
        $query = "SELECT id FROM aditur_members";
        $allIdsResult = $this -> db -> query($query);
        while ($allObject = $allIdsResult -> fetch_object()) {
            $allIds[] = $allObject -> id;
        }

        // get ids from those you have already commented on
        $query = "SELECT DISTINCT comment_on_id FROM aditur_comments WHERE author_id=" . $_SESSION['id'];
        $result = $this -> db -> query($query);

        $n = 0;
        $alreadyCommented = array();
        while ($object = $result -> fetch_object()) {
            $alreadyCommented[] = $object -> comment_on_id;
            $n++;
        }


        // delete the ones you have commented on from $allIds
        foreach ($alreadyCommented as $cId) {
            if (in_array($cId, $allIds)) {
                unset($allIds[array_search($cId, $allIds)]);
            }
        }

        if (empty($allIds)) {
            return;
        }
        else {
            // get username from everyone you have not commented

            $profiles = $allIds;

            shuffle($profiles);


            ?>
            <section class="Box">
                <h2>Von Dir noch nicht kommentiert</h2>
                <input type="checkbox" class="Expander" id="notCommentedExpander">
                <div class="NotCommented Group">
                    <?php $this->aditur->loadModule("profiles", "profileFrame", $profiles); ?>
                </div>
                <label for="notCommentedExpander" class="Button" id="showMore">
                    <i class="fa fa-lg fa-angle-double-down"></i>&nbsp;alle&nbsp;anzeigen</label>
                <div class="Group"></div>
            </section>

            <?php
        }
    }



    public function printYourCommentsTo($id) {
        $query = $this->db->query("SELECT enabled FROM aditur_modules WHERE name='comments'");
        $enabled = $query -> fetch_object() -> enabled;

        if ($enabled) {
            $this->op($id);
        }
?>

<div class="Comments">

    <?php
        ob_start();


        // print comments you commented on

        // get $commentedOn of $id
        $query = $this->db->prepare("SELECT forename, lastname FROM aditur_members WHERE id=?");
        $query -> bind_param('i', $id);
        $query -> bind_result($forename, $lastname);
        if (!$query->execute()) {
            echo $this -> db -> error;
        }
        $query -> fetch();
        $query -> close();


        // get count of comments
        $query = "SELECT COUNT(id) FROM aditur_comments WHERE comment_on_id=$id AND author_id=" . $_SESSION['id'];
        $result = $this->db->query($query);
        $count = $result->fetch_row()[0];


    ?>

    <h3>Deine Kommentare zu <?php echo $forename; ?> (<?php echo $count; ?>)</h3>
    <p><br>Klick auf einen Kommentar um ihn zu bearbeiten.</p>

    <?php

        // get comments
        if (!$result = $this -> db -> query("SELECT id, comment FROM aditur_comments WHERE comment_on_id=$id AND author_id=" . $_SESSION['id'])) {
            echo $this -> db -> error;
        }

        $nComments = 0;
    ?>
    <div class="CommentAuthor">
        <a href="<?php echo $this->aditur->path("/me"); ?>">
            <div class="ImageSizer">
                <img alt="Dein Profilbild" src="<?php echo $this->aditur->path("/media/profilePics/thumbs/" . $_SESSION['id'] . "/" . $forename . " " . $lastname . ".jpg"); ?>">
            </div>
        </a>
    </div>


    <div class="CommentList">
    <?php
        while ($comment = $result -> fetch_object()) {
            $nComments++;
            ?>

            <div class="Comment">
                <?php

                if ($enabled) {
                    ?>

                    <input type="checkbox" class="Expander BlackBoxExpander" id="blackBoxExpander<?php echo $nComments; ?>">
                    <label class="Comment" for="blackBoxExpander<?php echo $nComments; ?>"><?php echo $comment->comment; ?></label>
                    <form class="ChangeComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">
                        <textarea name="new_comment" placeholder="Verfasse einen schönen Kommentar!"><?php echo $comment->comment; ?></textarea>
                        <button class="Button Edit" type="submit" value="edit" name="comment_operation"><i class="fa fa-lg fa-pencil"></i>&nbsp;Bearbeiten</button>
                        <label class="Button" for="blackBoxExpander<?php echo $nComments; ?>"><i class="fa fa-lg fa-remove"></i>&nbsp;Abbrechen</label>
                    </form>

                    <form class="DeleteComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input value="<?php echo $comment->id; ?>" name="comment_id" type="hidden">

                        <button class="ButtonDelete" type="submit" value="delete" name="comment_operation">
                           <i class="fa fa-lg fa-eraser"></i>&nbsp;Löschen
                        </button>
                    </form>

                    <?php
                }
                else {
                    ?>

                    <label class="Comment"><?php echo $comment->comment; ?></label>

                    <?php
                }

                ?>
            </div>

            <?php
        }

        ?>

    </div>

    <?php
        if ($nComments > 0) {
            ob_end_flush();
        }
        else {
            ob_end_clean();
        ?>

        <p style="margin-top: 2rem;">Noch keine Kommentare verfasst.</p>

        <?php
        }
    ?>
</div>
<?php
    }



    public function printSubmitForm() {
        $query = $this->db->query("SELECT enabled FROM aditur_modules WHERE name='comments'");
        $enabled = $query -> fetch_object() -> enabled;

        if (!$enabled) {
            return;
        }

        ?>

        <div class="Box">
            <!-- CommentSubmitForm -->
            <form class="SubmitForm CommentSubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <div>
                    <textarea name="new_comment" placeholder="Verfasse einen schönen Kommentar!" required></textarea>
                    <button type="submit" name="comment_operation" value="submit">
                        <i class="fa fa-lg fa-check"></i>&nbsp; Senden
                    </button>
                </div>
            </form>
        </div>

        <?php
    }

    public function printCommentKing() {
        // find out who has written most of the comments
        $query = "SELECT id FROM aditur_members";
        $result = $this->db->query($query);

        while ($object = $result->fetch_object()) {
            $ids[] = $object->id;
        }

        $king = array('id' => $ids[0], 'count' => 0);

        foreach ($ids as $id) {
            $query = "SELECT COUNT(author_id) FROM aditur_comments WHERE author_id=" . $id;
            $result = $this->db->query($query);

            if ($row = $result->fetch_row()) {
                if ($row[0] > $king['count']) {
                    $king['id'] = $id;
                    $king['count'] = $row[0];
                }
            }
        }

        $query = "SELECT forename, lastname FROM aditur_members WHERE id=" . $king['id'];
        $result = $this->db->query($query);
        $object = $result->fetch_object();

        $query = "SELECT DISTINCT comment_on_id FROM aditur_comments WHERE author_id=" . $king['id'];
        $result = $this->db->query($query);

        $count = 0;

        while ($result->fetch_object()) {
            $count++;
        }

        if ($count == count($ids)) {
            $count = "ALLEN";
        }

        ?>

       <section class="Box">
           <div class="Comments CommentKing">
               <a href="<?php echo $this->aditur->path("/profiles/" . $king['id'] . "/" . $object->forename . " " . $object->lastname); ?>">
                   <div class="ImageSizer">
                       <img id="crown" alt="Die Krone des Kommentarkönigs" src="<?php echo $this->aditur->path("/media/images/crown.png"); ?>">
                       <img alt="Der Kommentarkönig!" title="<?php echo $object->forename; ?> hat die meisten Kommentare verfasst." src="<?php echo $this->aditur->path("/media/profilePics/raw/" . $king['id'] . "/" . $object->forename . " " . $object->lastname . ".jpg"); ?>">
                       <p><?php echo $object->forename; ?></p>
                   </div>

                   <p>hat <span style="font-weight: bold"><?php echo $king['count']; ?> Kommentare</span> zu <span style="font-weight: bold;"><?php echo $count; ?> Personen</span> und damit die meisten Kommentare verfasst.</p>
               </a>
           </div>
       </section>

        <?php
    }
}




?>
