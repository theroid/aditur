<?php
// Module: Surveys


class Surveys extends Module {
    // When the module is called explicitly by user
    public function fromUrl($argv) {
        if (isset($argv) && $argv[0] == "results") {
            $this->printResults();
        }
        else {
            $this->printAllSurveys();
        }
    }


    // process POST Data
    public function post() {
        $success = false;

        // switch different operations

        // create a new survey
        if (isset($_POST['new_survey_operation'])) {
            // check if user has privileges to create a survey
            if ($this->aditur->mayI('createSurvey') == false) {
                return false;
            }
            if (empty($_POST['new_survey_name'])) {
                $this->aditur->error("Kein Umfragetitel gesetzt!");

                return false;
            }
            if (empty($_POST['new_answer_index_0'])
                && !isset($_POST['add_members'])
                && !isset($_POST['add_teachers'])
                && !isset($_POST['suggestion'])) {
                $this->aditur->error("Keine Antwortmöglichkeit gesetzt!");

                return false;
            }


            // check if members and/or teachers should be added
            $addMembers = false;
            $addTeachers = false;

            if (isset($_POST['add_members'])) {
                $addMembers = true;
            }

            if (isset($_POST['add_teachers'])) {
                $addTeachers = true;
            }


            // check if answers can be suggested
            $suggestion = false;

            if (isset($_POST['suggestion'])) {
                $suggestion = true;
            }


            // collect answers
            $answers = array();

            for ($i = 0; !empty($_POST['new_answer_index_' . $i]); $i++) {
                $answers[] = $_POST['new_answer_index_' . $i];
            }

            if (empty($answers)) {
                $answers = null;
            }


            // check if dropdown answers are enabled.
            $dropdown = false;

            if (isset($_POST['dropdown_answers'])) {
                $dropdown = true;
            }


            // check if this is a survey has a category attribute.
            $category = "";

            if (!empty($_POST['category'])) {
                $category = $_POST['category'];
            }
            // new_category is taken even if an existing category is selected.
            if (!empty($_POST['new_category'])) {
                $category = $_POST['new_category'];
            }


            $success = $this -> createNewSurvey($_POST['new_survey_name'], $category, $suggestion, $answers, $addMembers, $addTeachers, $dropdown);

            unset($_POST['new_survey_operation']);
        }


        // add answers to existing surveys
        if (isset($_POST['new_survey_answer_operation'])) {
            if (empty($_POST['survey_id'])) {
                $this->aditur-> error("Keine Umfrage ausgewählt!");

                return false;
            }

            $surveyId = $_POST['survey_id'];


            // check if suggestion is enabled for this survey
            $suggestion = false;

            $q = $this -> db -> prepare("SELECT suggestion FROM aditur_surveys WHERE id=?");
            $q -> bind_param('i', $_POST['survey_id']);
            $q -> bind_result($suggestion);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Zu dieser Umfrage können keine Antwortmöglichkeiten hinzugefügt werden.");

                $q -> close();

                return false;
            }

            $q -> fetch();

            $q -> close();


            if ($suggestion) {
                $accepted = $this -> aditur -> mayI('createSurvey');


                $answers = array();
                for ($i = 0; !empty($_POST['new_answer_index_' . $i]); $i++) {
                    $answers[] = $_POST['new_answer_index_' . $i];
                }


                $success = $this -> addAnswers($surveyId, $answers, $accepted);

                unset($_POST['new_survey_answer_operation']);
            }
        }


        // delete a survey
        if (isset($_POST['delete_survey_operation'])) {
            if (isset($_POST['survey_id']) == false) return;
            if ($this -> aditur -> mayI('deleteSurvey') == false) return false;


            $success = $this -> deleteSurvey($_POST['survey_id']);

            unset($_POST['delete_survey_operation']);
        }


        // submit a vote
        if (isset($_POST['submit_vote_operation'])) {
            if (empty($_POST['survey_id'])) return false;
            if (empty($_POST['answer_id'])) return false;


            $success = $this -> submitVote($_POST['survey_id'], $_POST['answer_id']);

            unset($_POST['submit_vote_operation']);
        }


        // (re-)open a survey
        if (isset($_POST['open_survey_operation'])) {
            if (empty($_POST['survey_id'])) return false;


            // Check if this is the user who created the survey.
            $q = $this -> db -> prepare("SELECT id, komitee FROM aditur_surveys WHERE id=?");
            $q -> bind_param('i', $_POST['survey_id']);
            $q -> bind_result($id, $komitee);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $q -> close();

                return false;
            }

            $q -> store_result();

            if ($q -> num_rows !== 1) {
                $q -> close();

                return false;
            }

            $q -> fetch();
            $q -> close();


            // check privileges
            if ($this->aditur->checkPrivileges("speaker", $komitee) == false) {
                return false;
            }


            $success = $this -> openSurvey($_POST['survey_id']);

            unset($_POST['open_survey_operation']);
        }


        // close a survey
        if (isset($_POST['close_survey_operation'])) {
            if (empty($_POST['survey_id'])) return false;


            // Check if this is the user who created the survey.
            $q = $this -> db -> prepare("SELECT id, komitee FROM aditur_surveys WHERE id=?");
            $q -> bind_param('i', $_POST['survey_id']);
            $q -> bind_result($id, $komitee);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $q -> close();

                return false;
            }

            $q -> store_result();

            if ($q -> num_rows != 1) {
                $q -> close();

                return false;
            }

            $q -> fetch();
            $q -> close();


            // check privileges
            if ($this->aditur->checkPrivileges("speaker", $komitee) == false) {
                return false;
            }


            $succes = $this -> closeSurvey($_POST['survey_id']);

            unset($_POST['close_survey_operation']);
        }


        // disable suggestion feature
        if (isset($_POST['disable_suggestion_operation'])) {
            if (empty($_POST['survey_id'])) return false;


            // Check if this is the user who created the survey.
            $q = $this -> db -> prepare("SELECT id, komitee FROM aditur_surveys WHERE id=?");
            $q -> bind_param('i', $_POST['survey_id']);
            $q -> bind_result($id, $komitee);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $q -> close();

                return false;
            }

            $q -> store_result();

            if ($q -> num_rows !== 1) {
                $q -> close();

                return false;
            }

            $q -> fetch();
            $q -> close();


            // check privileges
            if ($this->aditur->checkPrivileges("speaker", $komitee) == false) {
                return false;
            }


            $succes = $this -> disableSuggestion($_POST['survey_id']);

            unset($_POST['disable_suggestion_operation']);
        }


        // accept a suggested answer possibility
        if (isset($_POST['accept_answer_operation'])) {
            if (isset($_POST['answer_id']) == false) return;


            // Check if this is the user who created the survey.

            // get survey id
            $q = $this -> db -> prepare("SELECT id, survey_id FROM aditur_surveys_answers WHERE id=?");
            $q -> bind_param('i', $_POST['answer_id']);
            $q -> bind_result($id, $surveyId);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $q -> close();

                return false;
            }

            $q -> fetch();

            $q -> close();


            $q = $this -> db -> prepare("SELECT id, komitee FROM aditur_surveys WHERE id=?");
            $q -> bind_param('i', $surveyId);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                return false;
            }

            $q -> bind_result($surveyId, $komitee);

            $q -> store_result();

            if ($q -> num_rows !== 1) {
                $q -> close();

                return false;
            }

            $q -> fetch();
            $q -> close();


            // check privileges
            if ($this->aditur->checkPrivileges("speaker", $komitee) == false) {
                return false;
            }


            $succes = $this -> acceptAnswer($id);

            unset($_POST['accept_answer_operation']);
        }


        // or decline a suggested answer possibility
        if (isset($_POST['decline_answer_operation'])) {
            if (isset($_POST['answer_id']) == false) return;


            // Check if this is the user who created the survey.

            // get survey id
            $q = $this -> db -> prepare("SELECT id, survey_id FROM aditur_surveys_answers WHERE id=?");
            $q -> bind_param('i', $_POST['answer_id']);
            $q -> bind_result($id, $surveyId);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $q -> close();

                return false;
            }

            $q -> fetch();

            $q -> close();


            $q = $this -> db -> prepare("SELECT id, komitee FROM aditur_surveys WHERE id=?");
            $q -> bind_param('i', $surveyId);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                return false;
            }

            $q -> bind_result($surveyId, $komitee);

            $q -> store_result();

            if ($q -> num_rows !== 1) {
                $q -> close();

                return false;
            }

            $q -> fetch();
            $q -> close();


            // check privileges
            if ($this->aditur->checkPrivileges("speaker", $komitee) == false) {
                return false;
            }


            $succes = $this -> declineAnswer($id);

            unset($_POST['decline_answer_operation']);
        }


        return $success;
    }

    // The following methods are used by post() to perform specific operations
    private function createNewSurvey($name, $category = "", $suggestion = false, $answers = null, $addMembers = false, $addTeachers = false, $dropdown = false) {
        $name = htmlspecialchars($name);

        // set to default category if empty
        if ($category == "") {
            $category = "Allgemeines";
        }

        $category = htmlspecialchars($category);


        // check if dropdown answers are enabled.
        // this is the case automatically if members or teachers are added.
        if (isset($_POST['add_members']) || isset($_POST['add_teachers'])) {
            $dropdown = true;
        }


        // perform query
        $q = $this -> db -> prepare("INSERT INTO aditur_surveys (question, komitee, closed, category, dropdown, suggestion) VALUES (?, ?, 0, ?, ?, ?)");
        $q -> bind_param('sisii', $name, $_SESSION['komitee'], $category, $dropdown, $suggestion);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Neue Umfrage konnte nicht erstellt werden. Bitte wende Dich an den Admin!");

            $q -> close();

            return false;
        }

        $q -> close();


        // give feedback: success.
        $this->aditur->note("Neue Umfrage \"" . $name . "\" wurde erstellt.");


        // get id of the created survey for addAnswers()
        $q = $this -> db -> prepare("SELECT id FROM aditur_surveys WHERE question=?");
        $q -> bind_param('s', $name);
        $q -> bind_result($surveyId);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Neue Umfrage konnte nicht erstellt werden. Bitte wende Dich an den Admin!");

            $q -> close();

            return false;
        }

        $q -> fetch();

        $q -> close();


        // process answers
        if (is_null($answers)) {
            $answers = array();
        }

        if ($addMembers) {
            $q = $this -> db -> query("SELECT forename, lastname FROM aditur_members ORDER BY forename");

            $objects = array();
            while ($object = $q -> fetch_object()) {
                $objects[] = $object;
            }

            $q -> close();

            // add to $answers either only forename or if there a multiple guys with this forename, also add the lastname.
            for ($i = 0; $i < count($objects); $i++) {
                $object = $objects[$i];

                // if two people share the same forename
                if ($i > 0 && $objects[$i-1]->forename == $object->forename || $i < count($objects)-1 && $object->forename == $objects[$i+1]->forename) {
                    $answers[] = $object->forename . " " . $object->lastname;
                }
                else {
                    $answers[] = $object->forename;
                }
            }
        }

        if ($addTeachers) {
            $q = $this -> db -> query("SELECT title, name FROM aditur_teachers ORDER BY name");

            while ($object = $q -> fetch_object()) {
                $answers[] = $object -> title . " " . $object -> name;
            }

            $q -> close();
        }


        // add answers
        return $this -> addAnswers($surveyId, $answers, 1);
    }

    private function addAnswers($surveyId, $answers, $accepted) {
        foreach ($answers as $answer) {
            $answer = htmlspecialchars($answer);


            // perform query
            $q = $this -> db -> prepare("INSERT INTO aditur_surveys_answers (survey_id, author_id, answer, accepted) VALUES (?, ?, ?, ?)");
            $q -> bind_param('iisi', $surveyId, $_SESSION['id'], $answer, $accepted);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Antwortmöglichkeit \"" . $answer . "\" konnte nicht gespeichert werden!");

                return false;
            }

            $q -> close();
        }

        return true;
    }

    private function deleteSurvey($surveyId) {
        $q = $this -> db -> prepare("DELETE FROM aditur_surveys_votes WHERE survey_id=?");
        $q -> bind_param('i', $_POST['survey_id']);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Umfragenstimmen konnte nicht gelöscht werden!");
        }

        $q -> close();


        $q = $this -> db -> prepare("DELETE FROM aditur_surveys_answers WHERE survey_id=?");
        $q -> bind_param('i', $_POST['survey_id']);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Umfragenantworten konnte nicht gelöscht werden!");
        }

        $q -> close();


        $q = $this -> db -> prepare("DELETE FROM aditur_surveys WHERE id=?");
        $q -> bind_param('i', $_POST['survey_id']);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Umfrage konnte nicht gelöscht werden!");

            $q -> close();

            return false;
        }

        $this->aditur->note("Umfrage wurde gelöscht.");

        $q -> close();


        return true;
    }

    private function submitVote($surveyId, $answerId) {
        // check if there is already a vote
        $q = $this -> db -> prepare("SELECT id FROM aditur_surveys_votes WHERE survey_id=? AND author_id=?");
        $q -> bind_param("ii", $_POST['survey_id'], $_SESSION['id']);
        $q -> bind_result($id);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Deine Stimme konnte nicht gespeichert werden. Bitte wende Dich an den Admin!");

            $q -> close();

            return false;
        }

        $q -> store_result();

        if ($q -> num_rows > 0) {
            $q -> fetch();

            $q -> close();


            // remove older vote(s)
            $q = $this -> db -> prepare("DELETE FROM aditur_surveys_votes WHERE survey_id=? AND author_id=?");
            $q -> bind_param("ii", $_POST['survey_id'], $_SESSION['id']);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Deine Stimme konnte nicht gespeichert werden. Bitte wende Dich an den Admin!");

                $q -> close();

                return false;
            }
        }

        $q -> close();


        // insert new vote
        $q = $this -> db -> prepare("INSERT INTO aditur_surveys_votes (survey_id, author_id, answer_id) VALUES (?, ?, ?)");
        $q -> bind_param("iii", $surveyId, $_SESSION['id'], $answerId);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Deine Stimme konnte nicht gespeichert werden. Bitte wende Dich an den Admin!");

            return false;
        }

        $q -> close();


        return true;
    }

    private function openSurvey($id) {
        $q = $this -> db -> query("UPDATE aditur_surveys SET closed=0 WHERE id=" . $id);

        if (!$q) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Umfrage konnte nicht geöffnet werden. Bitte wende Dich an den Admin.");

            return false;
        }

        return true;
    }

    private function closeSurvey($id) {
        $q = $this -> db -> query("UPDATE aditur_surveys SET closed=1 WHERE id=" . $id);

        if (!$q) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Umfrage konnte nicht geschlossen werden. Bitte wende Dich an den Admin.");

            return false;
        }

        return true;
    }

    private function disableSuggestion($id) {
        $q = $this -> db -> query("UPDATE aditur_surveys SET suggestion=0 WHERE id=" . $id);

        if (!$q) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Vorschläge konnten nicht deaktiviert werden. Bitte wende Dich an den Admin.");

            return false;
        }

        return true;
    }

    private function acceptAnswer($id) {
        $q = $this -> db -> query("UPDATE aditur_surveys_answers SET accepted=1 WHERE id=" . $id);

        if (!$q) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Antwortmöglichkeit konnte nicht akzeptiert werden. Bitte wende Dich an den Admin.");

            return false;
        }

        return true;
    }

    private function declineAnswer($id) {
        $q = $this -> db -> query("DELETE FROM aditur_surveys_answers WHERE id=" . $id);

        if (!$q) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Antwortmöglichkeit konnte nicht gelöscht werden. Bitte wende Dich an den Admin.");

            return false;
        }

        return true;
    }


    public function printAllSurveys() {
        // if you are the president or the speaker of the "Abizeitungskomitee",
        // print "Results"-button.
        if ($this->aditur->mayI("seeResults")) {
            ?>

            <div class="Surveys SurveyResults Box">
                <a class="Button" href="<?php echo $this->aditur->path("/surveys/results"); ?>"><i class="fa fa-lg fa-result"></i>&nbsp;&nbsp;Ergebnisse</a>
            </div>

            <?php
        }

        $this->printCreateNewSurveyForm();

        ?>

        <h2>Alle Umfragen</h2>

        <?php

        // select all surveys
        $query = "SELECT id, question, komitee, category, dropdown, suggestion FROM aditur_surveys WHERE closed=0 ORDER BY category, suggestion DESC, id";

        if (!$result = $this -> db -> query($query)) {
            $this->aditur->log($this -> db -> error);
        }

        $surveys = array(array());
        for ($i = 0; $object = $result -> fetch_object(); $i++) {
            $surveys[$i]['id'] = $object -> id;
            $surveys[$i]['question'] = $object -> question;
            $surveys[$i]['komitee']  = $object -> komitee;
            $surveys[$i]['category'] = $object -> category;
            $surveys[$i]['dropdown'] = $object -> dropdown;
            $surveys[$i]['suggestion'] = $object -> suggestion;
        }

        // if no survey is open at the moment
        if (empty($surveys[0])) {
            ?>

            <div class="Box" style="width: 100%; margin-top: 1rem;">
                <p style="text-align: center;">Derzeit gibt es keine Umfragen.</p>
            </div>

            <?php
        }
        else {
            ?>

            <div class="Surveys">
                <?php

                // enumerate surveys
                $nSurvey = 1;

                // create headings for categories
                $lastCategory = "";

                foreach ($surveys as $survey) {
                    // If this  survey is the first of a new category,
                    // print the heading first.
                    if ($survey['category'] !== $lastCategory) {
                        ?>

                        <h3><?php echo $survey['category']; ?></h3>

                        <?php

                        $lastCategory = $survey['category'];
                    }

                    $allowed = $this->aditur->checkPrivileges("speaker", $survey['komitee']);

                    // get answers
                    if ($allowed) {
                        $query = "SELECT id, answer, accepted FROM aditur_surveys_answers WHERE survey_id=" . $survey['id'] . " ORDER BY accepted DESC, id";
                    }
                    else {
                        $query = "SELECT id, answer, accepted FROM aditur_surveys_answers WHERE survey_id=" . $survey['id'] . " AND accepted=1";
                    }

                    $result = $this -> db -> query($query);

                    $answers = array(array());

                    for ($i = 0; $object = $result -> fetch_object(); $i++) {
                        $answers[$i]['id'] = $object -> id;
                        $answers[$i]['answer'] = $object -> answer;
                        $answers[$i]['accepted'] = $object -> accepted;
                    }


                    // get our vote
                    $query = "SELECT answer_id FROM aditur_surveys_votes WHERE survey_id=" . $survey['id'] . " AND author_id=" . $_SESSION['id'];

                    $result = $this -> db -> query($query);


                    $voteId = -1;
                    if ($result -> num_rows > 0) {
                        $object = $result -> fetch_object();
                        $voteId = $object -> answer_id;
                    }
                    elseif ($result -> num_rows > 1) {
                        $this->aditur->log("ERROR in table Survey Votes: multiple votes for " . $_SESSION['id'] . " at survey " . $survey['id']);
                    }




                    // start OUTPUT
                    ?>

                    <div id="survey<?php echo $nSurvey; ?>" class="Survey">
                        <?php

                        if ($allowed) {
                            ?>

                            <div class="Controls">
                                <div class="CloseSurvey">
                                    <input type="checkbox" class="Expander" id="surveyCloseExpander<?php echo $nSurvey; ?>">
                                    <label for="surveyCloseExpander<?php echo $nSurvey; ?>" class="ToggleButton HideByExpander">
                                        <i class="fa fa-envelope-o"></i>
                                    </label>


                                    <form class="CloseSurveyForm Expandable" action="<?php echo $this->aditur->url(); ?>#survey<?php echo $nSurvey; ?>" method="post" id="closeSurvey<?php echo $survey['id']; ?>">
                                        <label for="surveyCloseExpander<?php echo $nSurvey; ?>" class="ToggleButton">
                                            <i class="fa fa-lg fa-times-circle-o Close"></i>
                                        </label>

                                        <input type="hidden" name="survey_id" value="<?php echo $survey['id']; ?>">

                                        <button type="submit" name="close_survey_operation">
                                            <i class="fa fa-envelope-o"></i>&nbsp;Schließen
                                        </button>
                                    </form>
                                </div>

                                <?php

                                if ($survey['suggestion']) {
                                    ?>

                                    <div class="DisableSuggestion">
                                        <input type="checkbox" class="Expander" id="disableSuggestionExpander<?php echo $nSurvey; ?>">
                                        <label for="disableSuggestionExpander<?php echo $nSurvey; ?>" class="ToggleButton HideByExpander">
                                            <i class="fa fa-cog"></i>
                                        </label>


                                        <form class="DisableSuggestionForm Expandable" action="<?php echo $this->aditur->url(); ?>#survey<?php echo $nSurvey; ?>" method="post" id="disableSuggestion<?php echo $survey['id']; ?>">
                                            <label for="disableSuggestionExpander<?php echo $nSurvey; ?>" class="ToggleButton">
                                                <i class="fa fa-lg fa-times-circle-o Close"></i>
                                            </label>

                                            <input type="hidden" name="survey_id" value="<?php echo $survey['id']; ?>">

                                            <h4>Vorschläge deaktivieren</h4>

                                            <p>Vorsicht: Diese Aktion kann nicht rückgängig gemacht werden.</p>

                                            <button type="submit" name="disable_suggestion_operation">
                                                <i class="fa fa-stop-circle"></i>&nbsp;Deaktivieren
                                            </button>
                                        </form>
                                    </div>

                                    <?php
                                }

                                ?>

                            </div>

                            <?php
                        }

                        ?>


                        <h3><?php echo $survey['question']; ?></h3>

                        <div class="Answers">
                            <form action="<?php echo $this->aditur->url(); ?>#survey<?php echo $nSurvey; ?>" method="post">
                                <input type="hidden" name="survey_id" value="<?php echo $survey['id']; ?>">

                                <?php

                                $i = 0;

                                if ($survey['dropdown']) {
                                    ?>

                                    <select name="answer_id" required onchange="submitVoteFromDropdown(<?php echo $survey['id']; ?>, this)">
                                        <option value="" disabled selected hidden>Antwort wählen</option>

                                        <?php

                                        for ( ; ($i < count($answers)) && ($answers[$i]['accepted'] == 1); $i++) {
                                            $answer = $answers[$i];

                                            ?>

                                            <option class="SurveyAnswer<?php
                                            if ($answer['id'] == $voteId) {
                                                ?> OriginalVote<?php
                                            } ?>" <?php
                                            if ($answer['id'] == $voteId) {
                                                    ?>selected<?php
                                            } ?> value="<?php echo $answer['id']; ?>">
                                                <?php echo $answer['answer']; ?>
                                            </option>

                                            <?php
                                        }

                                        ?>
                                    </select>

                                    <?php
                                }
                                else {
                                    for ( ; ($i < count($answers)) && ($answers[$i]['accepted'] == 1); $i++) {
                                        $answer = $answers[$i];

                                        ?>

                                        <p>
                                            <input type="radio" id="surveyAnswer<?php echo $answer['id']; ?>" <?php

                                                if ($answer['id'] == $voteId) {
                                                   ?>checked<?php
                                                }

                                            ?> name="answer_id" value="<?php echo $answer['id']; ?>">

                                            <label class="SurveyAnswer<?php

                                                if ($answer['id'] == $voteId) {
                                                    ?> OriginalVote<?php
                                                }

                                            ?>" for="surveyAnswer<?php echo $answer['id']; ?>" onclick="submitVote(<?php echo $survey['id']; ?>, <?php echo $answer['id']; ?>, this)"><?php echo $answer['answer']; ?></label>
                                        </p>

                                        <?php
                                    }
                                }

                                ?>

                                <button class="SubmitVote HideByJS" type="submit"  id="submitButton<?php echo $survey['id']; ?>" name="submit_vote_operation">
                                    <i class="fa fa-times-circle-o"></i>&nbsp;Abstimmen
                                </button>
                            </form>
                        </div>


                        <?php

                        // If there are not yet accepted answers left, print them
                        // in a <form> to accept or decline them.
                        if ($allowed && $i < count($answers)) {
                            ?>

                            <h4>Vorgeschlagene Antworten</h4>

                            <?php

                            for ( ; $i < count($answers); $i++) {
                                $answer = $answers[$i];

                                ?>

                                <form class="AcceptAnswersForm SubmitForm" action="<?php echo $this->aditur->url(); ?>#survey<?php echo $nSurvey; ?>" method="post">
                                    <input type="hidden" name="answer_id" value="<?php echo $answer['id']; ?>">

                                    <p><?php echo $answer['answer']; ?></p>

                                    <button name="accept_answer_operation" type="submit">
                                        <i class="fa fa-lg fa-check"></i>&nbsp;Akzeptieren
                                    </button>

                                    <button name="decline_answer_operation" type="submit">
                                        <i class="fa fa-lg fa-thumbs-down"></i>&nbsp;Ablehnen
                                    </button>
                                </form>

                                <?php
                            }
                        }


                        // If suggestions are enabled, print a form to submit new answers.
                        if ($survey['suggestion']) {
                            ?>

                            <hr>

                            <?php

                            $this->printAnswerSubmitForm(false, $survey['id']);
                        }

                        ?>
                    </div>

                    <?php

                    $nSurvey++;
                }

                ?>
            </div>

            <script>
                // Hides buttons from forms that are now handled by AJAX.
                function hideButtons() {
                    var buttons = document.getElementsByClassName("HideByJS");

                    for (var i = 0; i < buttons.length; i++) {
                        buttons[i].style.display = "none";
                    }
                }


                // AJAX function to transmit Votes
                function submitVote(surveyId, answerId, element) {
                    var ajaxRequest;

                    if (window.XMLHttpRequest) {
                        ajaxRequest = new XMLHttpRequest();
                    }
                    else {
                        // code for IE6, IE5
                        ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }

                    ajaxRequest.onreadystatechange = function() {
                        if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                            var response = ajaxRequest.responseText;

                            if (response == 0) {
                                if (element.parentNode.parentNode.tagName == "FORM") {
                                    var answers = element.parentNode.parentNode.children;

                                    // every child is a form whose <label> in a <p> we want to access.

                                    // remove other siblings SurveyVote
                                    // and set this element's class to SurveyVote
                                    for (i = 0; i < answers.length; i++) {
                                        if (answers[i].tagName != "P") {
                                            continue;
                                        }


                                        var label = answers[i].children[1];

                                        label.className = "SurveyAnswer";
                                    }

                                    element.className += " SurveyVote";
                                }
                            }
                            else {
                                // Show error message in ErrorBox
                                var errorBox = document.getElementById("errorBox"),
                                    // create a new <li> element for the errorBox
                                    newListElement = document.createElement("li"),
                                    // parse response
                                    message = response.split(" ", 1)[1];

                                newListElement.innerHTML = message;

                                errorBox.getElementsByTagName("ul")[0].appendChild(newListElement);
                            }
                        }
                    }

                    ajaxRequest.open("POST", "./surveys/?script", true);
                    ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    ajaxRequest.send("submit_vote_operation&survey_id=" + surveyId + "&answer_id=" + answerId);
                }


                // same function as above, just for dropdown answers
                function submitVoteFromDropdown(surveyId, element) {
                    var ajaxRequest,
                        // get value of selected option
                        answerId = element.options[element.selectedIndex].value;

                    if (window.XMLHttpRequest) {
                        ajaxRequest = new XMLHttpRequest();
                    }
                    else {
                        // code for IE6, IE5
                        ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }

                    ajaxRequest.onreadystatechange = function() {
                        if (ajaxRequest.readyState == 4 && ajaxRequest.status == 200) {
                            var response = ajaxRequest.responseText;

                            if (response == 0) {
                                if (element.tagName == "SELECT") {
                                    // remove all children's SurveyVote
                                    // and set the selected option's class to SurveyVote
                                    var answers = element.children;

                                    for (i = 0; i < answers.length; i++) {
                                        answers[i].className = "SurveyAnswer";
                                    }


                                    answers[element.selectedIndex].className += " SurveyVote";
                                }
                            }
                            else {
                                // Show error message in ErrorBox
                                var errorBox = document.getElementById("errorBox"),
                                    // create a new <li> element for the errorBox
                                    newListElement = document.createElement("li"),
                                    // parse response
                                    message = response.split(" ", 1)[1];

                                newListElement.innerHTML = message;

                                errorBox.getElementsByTagName("ul")[0].appendChild(newListElement);
                            }
                        }
                    }

                    ajaxRequest.open("POST", "./surveys/?script", true);
                    ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    ajaxRequest.send("submit_vote_operation&survey_id=" + surveyId + "&answer_id=" + answerId);
                }



                // init
                hideButtons();
            </script>

            <?php
        }
    }


    public function printCreateNewSurveyForm() {
        // if you are the president or the speaker of the Abizeitungskomitee :D
        if ($this->aditur->mayI("createSurvey")) {
            // print submitForm for NEW Survey


            // get existing categories
            $q = $this -> db -> query("SELECT DISTINCT category FROM aditur_surveys");

            $categories = array();
            while ($object = $q -> fetch_object()) {
                $categories[] = $object -> category;
            }

            $q -> close();

            // remove "Allgemeines"
            unset($categories[array_search("Allgemeines", $categories)]);
            // remove "Abizeitung"
            unset($categories[array_search("Abizeitung", $categories)]);


            ?>

            <div class="Box">
                <form class="SubmitNewSurveyForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                    <h3>Neue Umfrage erstellen</h3>

                    <input type="text" name="new_survey_name" placeholder="Titel der Umfrage" required>

                    <p class="ChooseCategory Group">
                        <label for="newSurveyCategory">Kategorie</label>
                        <select id="newSurveyCategory" name="category">
                            <option value="" disabled selected hidden>auswählen</option>
                            <option value="Allgemeines">Allgemeines</option>
                            <option value="Abizeitung">Abizeitung</option>
                            <?php

                            foreach($categories as $category) {
                                ?>

                                <option value="<?php echo $category; ?>"><?php echo $category; ?></option>

                                <?php
                            }

                            ?>

                        </select>

                        <input type="text" name="new_category" placeholder="oder neue Kategorie eingeben.">
                    </p>

                    <?php $this->printAnswerSubmitForm(); ?>

                    <button type="submit" name="new_survey_operation"><i class="fa fa-lg fa-upload"></i>&nbsp;Erstellen</button>
                </form>
            </div>

            <?php
        }
    }


    public function printAnswerSubmitForm($onCreation = true, $surveyId = null) {
        // this method is called by printCreateNewSurveyForm with $onCreation = true
        // and if the user can suggest answers, with $onCreation = false obviously.
        // So, if this is not part of the NewSurveyForm, include <form> and submit-<button>.
        // Additionally hide add_members and add_teachers as they are only available to admins who create a survey.
        if (!$onCreation) {
            ?>

            <form class="SubmitNewAnswerForSurveyForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input type="hidden" name="survey_id" value="<?php echo $surveyId; ?>">
            <?php
        }


        ?>

        <div id="createSurveyForm">
            <?php

            if ($onCreation) {
                ?>

                <h4>Vorschläge</h4>

                <p><input type="checkbox" name="suggestion" id="suggestion"><label for="suggestion">aktivieren</label></p>

                <p>Jeder kann Antwortmöglichkeiten vorschlagen, die dann von Dir akzeptiert oder abgelehnt werden können.</p>


                <h4>Darstellung der Antwortmöglichkeiten</h4>

                <p><label><input type="checkbox" name="dropdown_answers">Dropdown-Antworten</label></p>

                <p>Wenn es viele Antwortmöglichkeiten gibt, empfiehlt es sich Dropdown-Antworten auszuwählen. Ist platzsparender.</p>


                <h4>Antwortmöglichkeiten hinzufügen</h4>

                <p><input type="checkbox" name="add_members" id="addMembers"><label for="addMembers">Alle Jahrgangsmitglieder</label></p>

                <p><input type="checkbox" name="add_teachers" id="addTeachers"><label for="addTeachers">Alle Lehrer</label></p>

                <?php
            }
            else {
                ?>

                <h4>Antwortmöglichkeiten hinzufügen</h4>

                <?php
            }

            ?>

            <input type="text" <?php if ($onCreation) { ?>onkeyup="checkForInput(this)"<?php } else { ?>required<?php } ?> name="new_answer_index_0" placeholder="Neue Antwortmöglichkeit">
        </div>

        <?php

        if (!$onCreation) {
            ?>
                <button type="submit" name="new_survey_answer_operation">
                    <i class="fa fa-lg fa-plus-circle"></i>&nbsp;Vorschlagen
                </button>
            </form>

            <?php
        }
        else {
            ?>

            <script>
                var nCreatedElements = 0,
                    form = document.getElementById("createSurveyForm");

                function checkForInput(element) {
                    // get index of input element
                    var index = element.name.split("new_answer_index_")[1];


                    // if element is filled, create new input with incremented index
                    if (element.value !== "" && nCreatedElements == index) {
                        newInput = document.createElement("input");

                        newInput.type = "text";
                        newInput.name = "new_answer_index_" + (nCreatedElements+1);
                        newInput.onkeyup = function () { checkForInput(this); };
                        newInput.placeholder = "Noch eine neue Antwortmöglichkeit";

                        form.appendChild(newInput);

                        nCreatedElements += 1;
                    }
                }
            </script>

            <?php
        }
    }


    public function printResults() {
        // if you are the president or the speaker of the "Abizeitungskomitee"
        if ($this->aditur->mayI("seeResults") == false) {
            $this->printAllSurveys();

            return;
        }


        // get count of members
        $query = "SELECT COUNT(id) FROM aditur_members";
        $result = $this->db->query($query);
        $memberCount = $result->fetch_row()[0];

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/surveys"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <h2>Ergebnisse</h2>

        <div class="Surveys Results">
            <?php

            // get all surveys
            $query = "SELECT id, question, komitee, closed, category, dropdown FROM aditur_surveys ORDER BY category, id";
            $result = $this->db->query($query);

            $surveys = array();

            while ($row = $result->fetch_assoc()) {
                $surveys[] = $row;
            }

            if (empty($surveys)) {
                ?>

                <div class="Box" style="width: 100%; margin-top: 1rem;">
                    <p style="text-align: center;">Keine Umfragen erstellt.</p>
                </div>

                <?php

                return;
            }


            // Check privileges and cache them
            $allowedToDelete = $this->aditur->mayI("deleteSurvey");


            // enumerate surveys
            $nSurvey = 1;

            // create headings for categories
            $lastCategory = "";

            foreach ($surveys as $survey) {
                // If this  survey is the first of a new category,
                // print the heading first.
                if ($survey['category'] !== $lastCategory) {
                    ?>

                    <h3><?php echo $survey['category']; ?></h3>

                    <?php

                    $lastCategory = $survey['category'];
                }


                ?>

                <div class="Result Survey">
                    <?php

                    // Select all answers
                    $query = "SELECT id, answer FROM aditur_surveys_answers WHERE survey_id=" . $survey['id'];
                    $result = $this->db->query($query);

                    $answers = array();
                    while ($row = $result -> fetch_assoc()) {
                        $answers[] = $row;
                    }


                    if ($allowedToDelete) {
                        ?>

                        <div class="DeleteSurvey">
                            <input type="checkbox" class="Expander" id="deleteSurveyExpander<?php echo $nSurvey; ?>">

                            <label for="deleteSurveyExpander<?php echo $nSurvey; ?>" class="Button ToggleButton" style="margin: 0 auto; width: 13.8rem;">
                                <i class="fa fa-trash-o"></i>&nbsp;Löschen
                            </label>

                            <form class="DeleteSurveyForm SubmitForm Expandable" action="<?php echo $this->aditur->url(); ?>" method="post" id="deleteSurvey<?php echo $survey['id']; ?>">
                                <input type="hidden" name="survey_id" value="<?php echo $survey['id']; ?>">

                                <p style="padding: 0 0.5rem; font-weight: bold; text-align: center;">Diese Aktion kann nicht rückgängig gemacht werden.</p>

                                <button type="submit" name="delete_survey_operation"><i class="fa fa-check"></i>&nbsp;Bestätigen</button>
                            </form>
                        </div>

                        <?php
                    }


                    $allowedToOpen = $this->aditur->checkPrivileges("speaker", $survey['komitee']);

                    if ($survey['closed'] && $allowedToOpen) {
                        ?>

                        <form class="OpenSurveyForm" action="<?php echo $this->aditur->url(); ?>" method="post" id="closeSurvey<?php echo $survey['id']; ?>">
                            <input type="hidden" name="survey_id" value="<?php echo $survey['id']; ?>">

                            <button type="submit" name="open_survey_operation"><i class="fa fa-envelope-open"></i>&nbsp;Öffnen</button>
                        </form>

                        <?php
                    }

                    ?>

                    <h3><?php echo $survey['question']; ?></h3>

                    <?php

                    $countAll = 0;

                    $votes = array();

                    // get votes for each answer
                    foreach ($answers as $answer) {
                        $query = "SELECT COUNT(id) FROM aditur_surveys_votes WHERE survey_id=" . $survey['id'] . " AND answer_id=" . $answer['id'];
                        $result = $this -> db -> query($query);
                        $count  = $result -> fetch_row()[0];
                        $countAll += $count;

                        $votes[] = array('count' => $count, 'answer' => $answer['answer']);
                    }


                    // sort answers descending depending on how many votes they have
                    $counts = array();
                    $answers = array();

                    foreach ($votes as $key => $row) {
                        $counts[$key] = $row['count'];
                        $answers[$key] = $row['answer'];
                    }

                    array_multisort($counts, SORT_DESC, $answers, SORT_ASC, $votes);

                    ?>

                    <table>
                        <?php

                        foreach ($votes as $key => $row) {
                            // skip answers with 0 votes
                            if ($row['count'] == 0) {
                                continue;
                            }

                            ?>

                            <tr class="SurveyResults Group">
                                <td class="Count"><?php echo $row['count']; ?></td>
                                <td class="Answer"><?php echo $row['answer']; ?></td>
                            </tr>

                            <?php
                        }

                        ?>

                    </table>

                    <p>
                        <br>Abgestimmt haben:<br><br>
                        <?php echo $countAll . " von " . $memberCount . " (" . substr($countAll / $memberCount * 100, 0, 5) . "%)"; ?>
                    </p>
                </div>

                <?php

                $nSurvey++;
            }
        ?>
        </div>

        <?php
    }
}

?>
