<?php
// Module: commercials


class commercials extends Module {
    public function fromUrl($argv) {
        $this->printcommercials();
    }

    // method called by __construct() where Operations should be made
    protected function op($name = null) {
        if (isset($_POST['ad_operation'])) {



                if ($_POST['ad_operation'] == "submit") {
                    $advertiser = htmlspecialchars($_POST['new_advertiser']);
                    $responsible = $_SESSION['id'];

                    $query = $this -> db -> prepare("INSERT INTO aditur_commercials (advertiser, responsible) VALUES (?, ?)");
                    $query -> bind_param('si', $advertiser, $responsible);
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);

                        $this->aditur->error("Dein Eintrag konnte leider nicht gespeichert werden. Bitte gib dem Administrator Bescheid!");

                        return;
                    }
                }

                if (($_POST['ad_operation'] == "edit") AND
            ($this->aditur->mayI("changeAdInfo"))) {
                    $format = htmlspecialchars($_POST['new_format']);

                    $query = $this -> db -> prepare("UPDATE aditur_commercials SET format=? WHERE id=?");
                    $query -> bind_param('si', $format, $_POST['ad_id']);
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);

                        $this->aditur->error("Dein Eintrag konnte leider nicht gespeichert werden. Bitte gib dem Administrator Bescheid!");

                        return;
                    }
                }

        }

        // alle dürfen ändern
        if ((isset($_POST['marked'])) AND
            (isset($_POST['ad_id']))  AND
            ($this->aditur->mayI("changeAdInfo")))
        {
            if ($_POST['marked'] == "no") {

                $column = htmlspecialchars($_POST['column']);

                $query = $this->db->prepare("UPDATE aditur_commercials SET $column='yes' WHERE id=?");
                $query -> bind_param('i', $_POST['ad_id']);


            }
            else if ($_POST['marked'] == "yes") {

                $column = htmlspecialchars($_POST['column']);
                $query = $this->db->prepare("UPDATE aditur_commercials SET $column='no' WHERE id=?");
                $query -> bind_param('i', $_POST['ad_id']);

            }

            if (!$query -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Dein Eintrag konnte nicht bearbeitet werden. Bitte gib dem Administrator Bescheid!");
            }

        }

        // nur Komiteesprecher Abizeitung dürfen ändern
        if ((isset($_POST['marked_s'])) AND
            (isset($_POST['ad_id']))    AND
            ($this->aditur->mayI("changeAdInfo")))
        {
            if ($_POST['marked_s']=="no") {

                $column = htmlspecialchars($_POST['column']);

                $query = $this->db->prepare("UPDATE aditur_commercials SET $column='yes' WHERE id=?");
                $query -> bind_param('i', $_POST['ad_id']);


            }
            else if ($_POST['marked_s']=="yes") {

                $column = htmlspecialchars($_POST['column']);

                $query = $this->db->prepare("UPDATE aditur_commercials SET $column='no' WHERE id=?");
                $query -> bind_param('i', $_POST['ad_id']);

            }

            if (!$query -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Dein Eintrag konnte nicht bearbeitet werden. Bitte gib dem Administrator Bescheid!");
            }
        }

        // nur Komiteesprecher Finanzen darf ändern
        if ((isset($_POST['marked_a'])) AND
            (isset($_POST['ad_id']))    AND
            ($this->aditur->mayI("changeAdFinances")))
        {
            if ($_POST['marked_a']=="no") {

                $column = htmlspecialchars($_POST['column']);

                $query = $this->db->prepare("UPDATE aditur_commercials SET $column='yes' WHERE id=?");
                $query -> bind_param('i', $_POST['ad_id']);


            }
            else if ($_POST['marked_a'] == "yes") {

                $column = htmlspecialchars($_POST['column']);

                $query = $this->db->prepare("UPDATE aditur_commercials SET $column='no' WHERE id=?");
                $query -> bind_param('i', $_POST['ad_id']);

            }

            if (!$query -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Dein Eintrag konnte nicht bearbeitet werden. Bitte gib dem Administrator Bescheid!");
            }
        }




    }



    public function printcommercials() {
        $this->op();


        $query = "SELECT id, forename, lastname FROM aditur_members ORDER BY forename";
                if (!$result = $this->db->query($query)) {
                    $this->aditur->log($this -> db -> error);
                }

                $objec = array();

                while ($obj = $result->fetch_object()) {
                    $objec[] =$obj;
                }
        $result->close();



        $query = "SELECT * FROM aditur_commercials";

        if (!$result = $this -> db -> query($query)) {
            $this->aditur->log($this -> db -> error);
        }

        if ($result->num_rows > 0) {
?>

    <h2>Anzeigen</h2>

    <?php

    // get some stats
    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials");
    $ncommercials = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE commitment='yes'");
    $ncommercialsComitted = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE payed='yes'");
    $ncommercialsPayed = $query -> fetch_row()[0];
    $query -> close();

    // calculate page of DIN A4 of commercials
    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A3'");
    $nA3 = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A4'");
    $nA4 = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A5'");
    $nA5 = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A6'");
    $nA6 = $query -> fetch_row()[0];
    $query -> close();


    $pagesOfcommercials = $nA3 * 2 + $nA4 + $nA5 / 2 + $nA6 / 4;

    // calculate page of DIN A4 of commercials
    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A3' AND payed='yes'");
    $nA3 = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A4' AND payed='yes'");
    $nA4 = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A5' AND payed='yes'");
    $nA5 = $query -> fetch_row()[0];
    $query -> close();

    $query = $this->db->query("SELECT COUNT(id) FROM aditur_commercials WHERE format='A6' AND payed='yes'");
    $nA6 = $query -> fetch_row()[0];
    $query -> close();

    $cash = $this->aditur->config("priceOfA3") * $nA3
          + $this->aditur->config("priceOfA4") * $nA4
          + $this->aditur->config("priceOfA5") * $nA5
          + $this->aditur->config("priceOfA6") * $nA6;

    ?>

    <p style="margin: 1rem">
        <?php echo $ncommercials; ?> Einträge, davon <?php echo $ncommercialsComitted; ?> zugesagt und <?php echo $ncommercialsPayed; ?> bezahlt, ergeben <?php echo $pagesOfcommercials; ?> DIN A4 Seiten voller Werbung.<br>
        Bezahlt sind bisher <?php echo $cash; ?>€.
    </p>

    <?php

    $this->printcommercialsubmitForm();

    ?>

<section class="Commercials">

    <?php

    while ($object = $result->fetch_object()) {
                ?>
        <div class="Ad Box" id="<?php echo $object->id; ?>">
            <h3>#<?php echo $object->id; ?>: <?php echo $object->advertiser; ?></h3>

            <p class="Responsible">Verantwortlicher: <strong><?php


                $i = array_search($object->responsible, array_column($objec, 'id'));

                $obj = $objec[$i];

                $displayName = $obj->forename;

                // if two people share the same forename
                if ($i > 0 && $objec[$i-1]->forename == $obj->forename || $i < count($objec)-1 && $obj->forename == $objec[$i+1]->forename) {
                    $displayName .= " " . $obj->lastname;
                }

                echo $displayName;


                ?></strong></p>

            <?php

            // if you have rights to edit, display form, if not just a paragraph
            if ($this->aditur->mayI("changeAdInfo"))
            {
            ?>

            <form class="ChangeFormat" action="<?php echo $this->aditur->url() . "#" . $object->id; ?>" method="post">
                <input type="hidden" name="ad_id" value="<?php echo $object->id; ?>">

                <p>
                    <label for="format">Format: DIN </label>
                    <select class="ad_form" name="new_format" id="format">
                        <option><?php echo $object->format; ?></option>
                        <option>A3</option>
                        <option>A4</option>
                        <option>A5</option>
                        <option>A6</option>
                    </select>

                    <button class="ad_form" type="submit" value="edit" name="ad_operation">
                        <i class="fa fa-lg fa-chevron-right"></i>&nbsp;Ändern
                    </button>
                </p>
            </form>
            <?php
            }
            else {
            ?>

            <p> Format: DIN <?php echo $object->format; ?></p>

            <?php

            }

            // if you have rights to edit, display form, if not just a paragraph
            if ($this->aditur->mayI("changeAdInfo") || $object->responsible == $_SESSION['id'])
            {
            ?>

            <form class="Commercials" action="<?php echo $this->aditur->url() . "#" . $object->id; ?>" method="post">
                <input type="hidden" name="ad_id" value="<?php echo $object->id; ?>">

                <input type="hidden" name="column" value="asked">

                <p class="<?php echo $object->asked === "yes" ? "Yes" : "No"; ?>">
                   <?php if ($object->asked === "yes") { ?>
                    <i class="fa fa-lg fa-check"></i>&nbsp;Angefragt<?php } else { ?>
                    <i class="fa fa-lg fa-times"></i>&nbsp;Noch nicht angefragt<?php } ?>

                    <button type="submit" name="marked" value="<?php echo $object->asked; ?>">
                        <i class="fa fa-lg fa-chevron-right"></i>&nbsp;Ändern
                    </button>
                </p>
            </form>

            <?php
            }
            else {
            ?>

            <p class="NoForm <?php echo $object->asked === "yes" ? "Yes" : "No"; ?>"><?php if ($object->asked === "yes") { ?><i class="fa fa-lg fa-check"></i>&nbsp;Angefragt<?php } else { ?><i class="fa fa-lg fa-times"></i>&nbsp;Noch nicht angefragt<?php } ?></p>

            <?php
            }

            // if you have rights to edit, display form, if not just a paragraph
            if ($this->aditur->mayI("changeAdInfo"))
            {
            ?>

            <form class="Commercials" action="<?php echo $this->aditur->url() . "#" . $object->id; ?>" method="post">
                <input type="hidden" name="ad_id" value="<?php echo $object->id; ?>">

                <input type="hidden" name="column" value="commitment">

                <p class="<?php echo $object->commitment === "yes" ? "Yes" : "No"; ?>">
                    <?php if ($object->commitment === "yes") { ?>
                    <i class="fa fa-lg fa-check"></i>&nbsp;Zugesagt<?php } else { ?>
                    <i class="fa fa-lg fa-times"></i>&nbsp;Noch nicht zugesagt<?php } ?>

                    <button type="submit" name="marked" value="<?php echo $object->commitment; ?>">
                        <i class="fa fa-lg fa-chevron-right"></i>&nbsp;Ändern
                    </button>
                </p>
            </form>

            <?php
            }
            else {
            ?>

            <p class="NoForm <?php echo $object->commitment === "yes" ? "Yes" : "No"; ?>">
                <?php if ($object->commitment === "yes") { ?>
                <i class="fa fa-lg fa-check"></i>&nbsp;Zugesagt<?php } else { ?>
                <i class="fa fa-lg fa-times"></i>&nbsp;Noch nicht zugesagt<?php } ?>
            </p>

            <?php
            }


            if ($this->aditur->mayI("changeAdInfo"))
            {
            ?>
            <form class="Commercials" action="<?php echo $this->aditur->url() . "#" . $object->id; ?>" method="post">
                <input type="hidden" name="ad_id" value="<?php echo $object->id; ?>">

                <input type="hidden" name="column" value="ad_in">

                <p class="<?php echo $object->ad_in === "yes" ? "Yes" : "No"; ?>">
                    <?php if ($object->ad_in === "yes") { ?>
                    <i class="fa fa-lg fa-check"></i>&nbsp;Anzeige eingegangen<?php } else { ?>
                    <i class="fa fa-lg fa-times"></i>&nbsp;Anzeige noch nicht eingegangen<?php } ?>

                    <button type="submit" name="marked_s" value="<?php echo $object->ad_in; ?>">
                        <i class="fa fa-lg fa-chevron-right"></i>&nbsp;Ändern
                    </button>
                </p>
            </form>

            <?php
            }
            else {
            ?>

            <p class="NoForm <?php $object->ad_in === "yes" ? "Yes" : "No"; ?>">
                <?php if ($object->ad_in === "yes") { ?>
                <i class="fa fa-lg fa-check"></i>&nbsp;Anzeige eingegangen<?php } else { ?>
                <i class="fa fa-lg fa-times"></i>&nbsp;Anzeige noch nicht eingegangen<?php } ?>
            </p>

            <?php
            }

            if (($this->aditur->mayI("changeAdFinances")))
            {
            ?>

            <form class="Commercials" action="<?php echo $this->aditur->url() . "#" . $object->id; ?>" method="post">
                <input type="hidden" name="ad_id" value="<?php echo $object->id; ?>">

                <input type="hidden" name="column" value="payed">

                <p class="<?php echo $object->payed === "yes" ? "Yes" : "No"; ?>">
                    <?php if ($object->payed === "yes") { ?>
                    <i class="fa fa-lg fa-check"></i>&nbsp;Bezahlt<?php } else { ?>
                    <i class="fa fa-lg fa-times"></i>&nbsp;Noch nicht bezahlt<?php } ?>

                    <button type="submit" name="marked_a" value="<?php echo $object->payed; ?>">
                        <i class="fa fa-lg fa-chevron-right"></i>&nbsp;Ändern
                    </button>
                </p>
            </form>

            <?php
            }
            else {
            ?>

            <p class="NoForm <?php echo $object->payed === "yes" ? "Yes" : "No"; ?>">
                <?php if ($object->payed === "yes") { ?>
                <i class="fa fa-lg fa-check"></i>&nbsp;Bezahlt<?php } else { ?>
                <i class="fa fa-lg fa-times"></i>&nbsp;Noch nicht bezahlt<?php } ?>
            </p>

            <?php
            }


            if ($this->aditur->mayI("changeAdInfo"))
            {
            ?>

            <form class="Commercials" action="<?php echo $this->aditur->url() . "#" . $object->id; ?>" method="post">
                <input type="hidden" name="ad_id" value="<?php echo $object->id; ?>">

                <input type="hidden" name="column" value="copy" />

                <p class="<?php echo $object->copy === "yes" ? "Yes" : "No"; ?>">
                    <?php if ($object->copy === "yes") { ?>
                    <i class="fa fa-lg fa-check"></i>&nbsp;Belegexemplar erhalten<?php } else { ?>
                    <i class="fa fa-lg fa-times"></i>&nbsp;Belegexemplar noch nicht erhalten<?php } ?>

                    <button type="submit" name="marked_s" value="<?php echo $object->copy; ?>">
                        <i class="fa fa-lg fa-chevron-right"></i>&nbsp;Ändern
                    </button>
                </p>
            </form>

            <?php
            }
            else {
            ?>

            <p class="NoForm <?php echo $object->copy === "yes" ? "Yes" : "No"; ?>">
                <?php if ($object->copy === "yes") { ?>
                <i class="fa fa-lg fa-check"></i>&nbsp;Belegexemplar erhalten<?php } else { ?>
                <i class="fa fa-lg fa-times"></i>&nbsp;Belegexemplar noch nicht erhalten<?php } ?>
            </p>

            <?php
            }

            ?>
        </div>

        <?php } ?>

</section>

<?php

        }
        else {
            ?>
            <p>Es wurden noch keine Anzeigen erstellt.</p>
            <?php

            $this->printcommercialsubmitForm();
        }
    }

    public function printcommercialsubmitForm() {
        ?>
        <form class="SubmitForm Box" action="<?php echo $this->aditur->url(); ?>" method="post">
            <h3>Neue Anzeige eintragen</h3>

            <input type="text" name="new_advertiser" placeholder="Wen hast du angefragt?" required>

            <button type="submit" name="ad_operation" value="submit">                    <i class="fa fa-lg fa-check"></i>&nbsp;Absenden
            </button>
        </form>
        <?php
    }
}




?>
