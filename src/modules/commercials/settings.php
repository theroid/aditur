<?php
// Settings: commercials
// Adjust prices


class commercialsSettings extends Setting {
    private $priceOfA3 = 0;
    private $priceOfA4 = 0;
    private $priceOfA5 = 0;
    private $priceOfA6 = 0;


    public function fromUrl($argv) {
        $this->priceOfA3 = $this->aditur->config("priceOfA3");
        $this->priceOfA4 = $this->aditur->config("priceOfA4");
        $this->priceOfA5 = $this->aditur->config("priceOfA5");
        $this->priceOfA6 = $this->aditur->config("priceOfA6");

        $this->op();

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/settings"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Einstellungen</a>

        <h2>Anzeigen</h2>

        <section class="AdjustPrices">
            <form class="AdjustPricesForm SubmitForm Box" action="<?php echo $this->aditur->url(); ?>" method="post">
                <h3>Anzeigenpreise ändern</h3>

                <p>
                    <label for="priceOfA3" class="Format">DIN A3 für</label>
                    <input type="number" id="priceOfA3" class="Price" name="price_of_a3" placeholder="<?php echo $this->priceOfA3; ?>">
                    <span class="Euro">€</span>
                </p>

                <p>
                    <label for="priceOfA3" class="Format">DIN A4 für</label>
                    <input type="number" id="priceOfA4" class="Price" name="price_of_a4" placeholder="<?php echo $this->priceOfA4; ?>">
                    <span class="Euro">€</span>
                </p>

                <p>
                    <label for="priceOfA3" class="Format">DIN A5 für</label>
                    <input type="number" id="priceOfA5" class="Price" name="price_of_a5" placeholder="<?php echo $this->priceOfA5; ?>">
                    <span class="Euro">€</span>
                </p>

                <p>
                    <label for="priceOfA6" class="Format">DIN A6 für</label>
                    <input type="number" id="priceOfA6" class="Price" name="price_of_a6" placeholder="<?php echo $this->priceOfA6; ?>">
                    <span class="Euro">€</span>
                </p>

                <button type="submit" name="adjust_prices_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                </button>
            </form>
        </section>

        <?php
    }


    private function op() {
        if (isset($_POST['adjust_prices_operation'])) {
            $key = "";
            $value = 0;

            if (!empty($_POST['price_of_a3'])) {
                $key = "priceOfA3";
                $value = $_POST['price_of_a3'];

                $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key=?");
                $query -> bind_param('is', $value, $key);

                if ($query -> execute()) {
                    $this->aditur->note("Preis von A3 wurde geändert.");
                }
                else {
                    $this->aditur->error("Preise konnten nicht geändert werden. Bitte wende Dich an den Admin!");
                }

                $query -> close();

                $this->priceOfA3 = $value;
            }
            if (!empty($_POST['price_of_a4'])) {
                $key = "priceOfA4";
                $value = $_POST['price_of_a4'];

                $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key=?");
                $query -> bind_param('is', $value, $key);

                if ($query -> execute()) {
                    $this->aditur->note("Preis von A4 wurde geändert.");
                }
                else {
                    $this->aditur->error("Preise konnten nicht geändert werden. Bitte wende Dich an den Admin!");
                }

                $query -> close();

                $this->priceOfA4 = $value;
            }
            if (!empty($_POST['price_of_a5'])) {
                $key = "priceOfA5";
                $value = $_POST['price_of_a5'];

                $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key=?");
                $query -> bind_param('is', $value, $key);

                if ($query -> execute()) {
                    $this->aditur->note("Preis von A5 wurde geändert.");
                }
                else {
                    $this->aditur->error("Preise konnten nicht geändert werden. Bitte wende Dich an den Admin!");
                }

                $query -> close();

                $this->priceOfA5 = $value;
            }
            if(!empty($_POST['price_of_a6'])) {
                $key = "priceOfA6";
                $value = $_POST['price_of_a6'];

                $query = $this->db->prepare("UPDATE aditur_config SET value=? WHERE config_key=?");
                $query -> bind_param('is', $value, $key);

                if ($query -> execute()) {
                    $this->aditur->note("Preis von A6 wurde geändert.");
                }
                else {
                    $this->aditur->error("Preise konnten nicht geändert werden. Bitte wende Dich an den Admin!");
                }

                $query -> close();

                $this->priceOfA6 = $value;
            }
        }
    }


    public function index() {
        ?>

        <a class="Setting Box" href="<?php echo $this->aditur->path("/settings/commercials"); ?>">
            <div>
                <h3><i class="fa fa-lg fa-money"></i>&nbsp;Anzeigen</h3>
            </div>
        </a>

        <?php
    }
}

?>
