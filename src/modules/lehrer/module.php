<?php
// Module: Lehrer


class Lehrer extends Module {
    protected $access_code = "";
    protected $id = null;

    protected $CensorshipTeachersIsEnabled = false;
    protected $CommentsFromTeachersEnabled = false;


    public function fromUrl($argv) {
        if ($this->aditur->login->isUserLoggedIn()) {
            $this->aditur->loadModule("teachers", "fromUrl");

            return;
        }


        // check if censorship for teachers is enabled
        $this->CensorshipTeachersIsEnabled = $this->db->query("SELECT value FROM aditur_config WHERE config_key='censorship_teachers_enabled'") -> fetch_object() -> value == 0 ? false : true;

        // check if comments from teachers are enabled
        $this->CommentsFromTeachersIsEnabled = $this->db->query("SELECT value FROM aditur_config WHERE config_key='comments_from_teachers_enabled'") -> fetch_object() -> value == 0 ? false : true;



        $this->id = $this->op($argv[0]);
        $this->access_code = $argv[0];

        if ($this->id === false) {
            $this->showTeachersAuthenticationForm();
        }
        else {
            if (count($argv) >= 2) {
                if ($argv[1] === "censorship") {
                    if ($this->CensorshipTeachersIsEnabled === false) {
                        $this->displayIndex();

                        return;
                    }

                    $this->aditur->loadModule("censorship", "censorTeachersComments", array($this->id, $argv[0]));
                }
                else if ($argv[1] === "profiles") {
                    if ($this->CommentsFromTeachersIsEnabled == false) {
                        $this->displayIndex();

                        return;
                    }

                    $this->aditur->addStylesheet("profiles/base.css");

                    if (count($argv) >= 3) {
                        $this->showProfile($argv[2]);
                    }
                    else {
                        $this->showProfiles();
                    }
                }
                else {
                    $this->displayIndex();
                }
            }
            else {
                $this->displayIndex();
            }
        }
    }


    protected function displayIndex() {
        $id = $this->id;
        $access_code = $this->access_code;

        $object = $this->db->query("SELECT title, name FROM aditur_teachers WHERE id=" . $id) -> fetch_object();

        $name = $object -> title . " " . $object -> name;

        ?>

        <h1>Willkommen, <?php echo $name; ?>!</h1>

        <p>Bitte bewahren Sie Ihren Link sorgfältig auf.<br><br></p>

        <div class="Index">
            <?php if ($this->CensorshipTeachersIsEnabled) { ?>
            <a class="Box" href="<?php echo $this->aditur->path("/lehrer/" . $access_code . "/censorship"); ?>">
                <div>
                    <h3>&nbsp;<i class="fa fa-lg fa-eye-slash"></i>&nbsp;Kommentare zu Ihnen zensieren</h3>
                </div>
            </a>
            <?php }

            if ($this->CommentsFromTeachersIsEnabled) { ?>
            <a class="Box" href="<?php echo $this->aditur->path("/lehrer/" . $access_code . "/profiles"); ?>">
                <div>
                    <h3>&nbsp;<i class="fa fa-lg fa-users"></i>&nbsp;Kommentieren</h3>
                </div>
            </a>
            <?php }

            // if nothing is activated:
            if (!$this->CensorshipTeachersIsEnabled && !$this->CommentsFromTeachersIsEnabled) { ?>
                <p><br><br>Hier gibt es nichts zu tun...<br><br><br>Wenden Sie sich an den Administrator, wenn Sie hier etwas erwartet haben.</p>
            <?php } ?>
        </div>

        <?php
    }


    protected function showProfiles() {
        // show all profiles
        $query = "SELECT id, forename, lastname FROM aditur_members ORDER BY forename";
        if (!$result = $this->db->query($query)) {
            echo $this->db->error;
            $this->aditur->log($this -> db -> error);
        }

        $objects = array();

        while ($object = $result->fetch_object()) {
            $objects[] = $object;
        }

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/lehrer/" . $this->access_code); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Zurück</a>

        <h2>Alle Profile</h2>

        <div class="ProfileFrames">
            <?php
                for ($i = 0; $i < count($objects); $i++) {
                    $object = $objects[$i];

                    $displayName = $object->forename;

                    // if two people share the same forename
                    if ($i > 0 && $objects[$i-1]->forename == $object->forename || $i < count($objects)-1 && $object->forename == $objects[$i+1]->forename) {
                        $displayName .= " " . $object->lastname;
                    }

            ?>
            <div class="ProfileFrame">
                <a href="<?php echo $this->aditur->path("/lehrer/" . $this->access_code . "/profiles/" . $object->id . "/" . $displayName); ?>">
                    <div class="ImageSizer">
                        <img alt="<?php echo $displayName; ?>'s Profilbild" src="<?php echo $this->aditur->path("/media/profilePics/thumbs/" . $object->id. "/" . $object->forename . " " . $object->lastname . ".jpg"); ?>">
                    </div>
                    <p><?php echo $displayName; ?></p>
                </a>
            </div>
            <?php } ?>
        </div>

        <?php
    }


    protected function showProfile($id) {
        // display a specific profile detailed


        // check if $username exists
        $query = $this -> db -> prepare("SELECT forename, lastname FROM aditur_members WHERE id=?");
        $query -> bind_param('i', $id);
        $query -> execute();
        $query -> bind_result($forename, $lastname);
        $query -> store_result();

        // if there is != 1 user or the selected user is == SESSION
        if (!($query->num_rows == 1)) {
            // if user does not exist display all users
            $this->aditur->error("Dieser Benutzer existiert nicht.");
            $query -> close();

            $this -> showProfiles();
            return;
        }

        $query -> fetch();
        $query -> close();

        $this -> opComments($id);

        ?>

        <div class="ProfileDetail">
            <div class="displayRow">
                <a class="Back" href="<?php echo $this->aditur->path("/lehrer/" . $this->access_code . "/profiles"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Profile</a>

                <h2><?php echo $forename; ?>'s Profil</h2>
            </div>
            <div class="ImageSizer">
                <img class="ProfileImage" alt="<?php echo $forename; ?>'s Profilbild" src="<?php echo $this->aditur->path("/media/profilePics/originals/" . $id . "/" . $forename . " " . $lastname . ".jpg"); ?>">
            </div>

            <div class="Comments">

            <?php

            // load existing comment
            $query = $this->db->prepare("SELECT id, comment FROM aditur_comments_from_teachers WHERE author_id=? AND comment_on_id=?");
            $query -> bind_param('ii', $this->id, $id);
            $query -> bind_result($commentId, $comment);
            $query -> execute();
            $query -> store_result();

            if ($query -> num_rows == 0) {
                $query -> close();

                // or if no comment exist, load submit form
                ?>

                <div class="Box">
                    <!-- CommentSubmitForm -->
                    <form class="SubmitForm CommentSubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <div>
                            <textarea name="new_comment" placeholder="Verfassen Sie einen Kommentar zu <?php echo $forename; ?>." required></textarea>
                            <button type="submit" name="comment_operation" value="submit">
                                <i class="fa fa-lg fa-check"></i>&nbsp; Senden
                            </button>
                        </div>
                    </form>
                </div>

            <?php
            }
            else {
                $query -> fetch();
                $query -> close();

                ?>

                <h3>Ihr Kommentar zu <?php echo $forename; ?>:</h3>

                <div class="Comment">
                    <input type="checkbox" class="Expander BlackBoxExpander" id="blackBoxExpander1">
                    <label class="Comment" for="blackBoxExpander1"><?php echo $comment; ?></label>
                    <form class="ChangeComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input value="<?php echo $commentId; ?>" name="comment_id" type="hidden">
                        <textarea name="new_comment" placeholder="Verfasse einen schönen Kommentar!"><?php echo $comment; ?></textarea>
                        <button class="Button Edit" type="submit" value="edit" name="comment_operation"><i class="fa fa-lg fa-pencil"></i>&nbsp;Bearbeiten</button>
                        <label class="Button" for="blackBoxExpander1"><i class="fa fa-lg fa-remove"></i>&nbsp;Abbrechen</label>
                    </form>

                    <form class="DeleteComment" action="<?php echo $this->aditur->url(); ?>" method="post">
                        <input value="<?php echo $commentId; ?>" name="comment_id" type="hidden">

                        <button class="ButtonDelete" type="submit" value="delete" name="comment_operation">
                            <i class="fa fa-lg fa-eraser"></i>&nbsp;Löschen
                        </button>
                    </form>
                </div>

                <p>Klicken Sie auf den Kommentar um ihn zu bearbeiten.</p>

                <?php
            }

            ?>

            </div>
        </div>

        <?php
    }


    protected function opComments($userId) {
        if (isset($_POST['comment_operation'])) {
            if (!empty($_POST['new_comment'])) {
                if ($_POST['comment_operation'] == "submit") {
                    $comment = htmlspecialchars($_POST['new_comment']);

                    // check if there is already a comment from this teacher to this student
                    $query = $this->db->prepare("SELECT id FROM aditur_comments_from_teachers WHERE author_id=? AND comment_on_id=?");
                    $query -> bind_param('ii', $this->id, $userId);
                    $query -> bind_result($check);
                    $query -> execute();
                    $query -> store_result();

                    if ($query -> num_rows > 0) {
                        return;
                    }

                    $query -> close();


                    $query = $this -> db -> prepare("INSERT INTO aditur_comments_from_teachers (author_id, comment_on_id, comment)
                    VALUES (?, ?, ?)");
                    $query -> bind_param('iis', $this->id, $userId, $comment);
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);
                        $this->aditur->error("Ihr Kommentar konnte leider nicht gespeichert werden. Bitte wende Dich an den Admin!");
                        return;
                    }
                }
            }


            if (!empty($_POST['comment_id'])) {
                // get author_id
                $id = $_POST['comment_id'];


                $query = $this->db->prepare("SELECT author_id FROM aditur_comments_from_teachers WHERE id=?");
                $query -> bind_param('i', $id);
                $query -> bind_result($author_id);

                if (!$query -> execute()) {
                    // TODO show error
                    $this->aditur->log($this -> db -> error);
                    $query -> close();
                    return;
                }

                $query -> fetch();

                $query -> close();


                // check if it is the author or a privileged user
                if ($this->id == $author_id) {



                    // perform changes
                    if ($_POST['comment_operation'] == "delete") {
                        $query = $this->db->prepare("DELETE FROM aditur_comments_from_teachers WHERE id=?");
                        $query -> bind_param('i', $id);
                    }
                    else if ($_POST['comment_operation'] == "edit") {
                        $comment = htmlspecialchars($_POST['new_comment']);

                        $query = $this->db->prepare("UPDATE aditur_comments_from_teachers SET comment=? WHERE id=?");
                        $query -> bind_param('si', $comment, $id);
                    }
                    else {
                        $query -> close();
                        return;
                    }
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);
                        $this->aditur->error("Ihr Kommentar konnte nicht bearbeitet werden. Probiere es später noch einmal!");
                    }
                    else {
                        $this->aditur->note("Ihr Kommentar wurde geändert.");
                    }

                    $query -> close();
                }
            }
        }
    }


    protected function op($access_code = null) {
        if (is_null($access_code)) {
            if (isset($_POST['submit_access_code_operation']) && !empty($_POST['access_code'])) {
                ob_end_clean();

                header('Location: ' . $this->aditur->url() . "/" . $_POST['access_code']);
                exit;
            }
            else {
                return false;
            }
        }

        $id = false;

        $query = $this->db->prepare("SELECT id FROM aditur_teachers WHERE access_code=?");
        $query -> bind_param('s', $access_code);
        $query -> execute();
        $query -> bind_result($id);
        $query -> store_result();

        if ($query -> num_rows !== 1) {
            if (isset($_GET['wrapper'])) {
                $query -> close();

                ob_end_clean();

                echo $access_code;
                echo "-1";

                exit;
            }

            return false;
        }

        $query -> fetch();
        $query -> close();

        return $id;
    }





    public function showTeachersAuthenticationForm() {
        ?>

        <section class="Censorship Box">
            <form action="<?php echo $this->aditur->path("/lehrer"); ?>" method="post">
                <p>Bitte geben Sie Ihren Autorisierungscode ein!</p>

                <input type="text" id="accessCode" name="access_code" value="" placeholder="Code" required>

                <button type="submit" name="submit_access_code_operation" onclick="window.location = '<?php echo $this->aditur->path("/lehrer"); ?>/' + document.getElementById('accessCode').value; return false;">
                    <i class="fa fa-lg fa-key"></i>&nbsp;Öffnen
                </button>
            </form>
        </section>

        <?php
    }
}


?>
