<?php
// Settings: Me
// enable/disable Kinderbild, Zeitungsbild
// create, delete Questions


class MeSettings extends Setting {
    private $profileQuestionsEnabled = false;
    private $babyPicsEnabled = false;
    private $choosePaperPhotoEnabled = false;

    public function fromUrl($argv) {
        $this->profileQuestionsEnabled = $this->aditur->config("profileQuestionsEnabled");
        $this->babyPicsEnabled = $this->aditur->config("babyPicsEnabled");
        $this->choosePaperPhotoEnabled = $this->aditur->config("choosePaperPhotoEnabled");


        $this->op();

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/settings"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Einstellungen</a>

        <h2>Persönliches Profil</h2>

        <section class="Tuning Box">
            <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">

                <?php if ($this->profileQuestionsEnabled) {
                    ?>

                    <p>Profilfragen sind <span class="Activated">aktiviert</span>.</p>

                    <button type="submit" class="Deactivate" name="deactivate_profile_questions">
                        Deaktivieren
                    </button>

                    <?php
                }
                else {
                    ?>

                    <p>Profilfragen sind <span class="Deactivated">deaktiviert</span>.</p>

                    <button type="submit" class="Activate" name="activate_profile_questions">
                        Aktivieren
                    </button>

                    <?php
                }

                ?>

            </form>

            <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">

            <?php if ($this->babyPicsEnabled) {
                ?>

                <p>Kinderbilder sind <span class="Activated">aktiviert</span>.</p>

                <button type="submit" class="Deactivate" name="deactivate_baby_pics">
                    Deaktivieren
                </button>

                <?php
            }
            else {
                ?>

                <p>Kinderbilder sind <span class="Deactivated">deaktiviert</span>.</p>

                <button type="submit" class="Activate" name="activate_baby_pics">
                    Aktivieren
                </button>

                <?php
            }

            ?>

            </form>

            <form class="ActivationForm" action="<?php echo $this->aditur->url(); ?>" method="post">

                <?php if ($this->choosePaperPhotoEnabled) {
                ?>

                <p>Bildauswahl für die Abizeitung ist <span class="Activated">aktiviert</span>.</p>

                <button type="submit" class="Deactivate" name="deactivate_choose_paper_photo">
                    Deaktivieren
                </button>

                <?php
                    }
                else {
                ?>

                <p>Bildauswahl für die Abizeitung ist <span class="Deactivated">deaktiviert</span>.</p>

                <button type="submit" class="Activate" name="activate_choose_paper_photo">
                    Aktivieren
                </button>

                <?php
                }

                ?>

            </form>
        </section>



        <?php

                $query = $this->db->query("SELECT id, text FROM aditur_me_questions");

                $questions = array();
                while ($object = $query -> fetch_object()) {
                    $questions[] = $object;
                }

                $query -> close();

        ?>

        <h3>Persönliche Fragen</h3>

        <section class="Questions Box">
            <h3>Neue Frage erstellen</h3>

            <form class="CreateNewQuestionForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input type="text" name="new_question_text" placeholder="Frage" required>

                <button type="submit" name="new_question_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

        <section class="QuestionManagement Box">
            <h3>Fragen</h3>

            <ul class="Questions" id="questions">
                <?php

                $nQuestion = 0;
                foreach ($questions as $question) {
                ?>

                <li>
                    <p class="Title"><?php echo $question->text; ?></p>
                    <input type="checkbox" class="Expander QuestionExpander" id="QuestionExpander<?php echo $nQuestion; ?>">
                    <label class="Toggler" for="QuestionExpander<?php echo $nQuestion; ?>">
                        <i class="fa fa-lg fa-times-circle"></i>
                    </label>
                    <form class="DeleteQuestionForm SubmitForm" action="<?php echo $this->aditur->url() . "#questions"; ?>" method="post">
                        <input type="hidden" name="question_id" value="<?php echo $question->id; ?>">

                        <p>Diese Frage wirklich löschen?</p>

                        <button type="submit" name="delete_question_operation">
                            <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                        </button>
                    </form>
                </li>

                <?php

                    $nQuestion++;
                }

                ?>
            </ul>
        </section>

        <?php
    }


    private function op() {
        // Module Tuning
        if (isset($_POST['activate_profile_questions'])) {
            if ($this->db->query("UPDATE aditur_config SET value=1 WHERE config_key='profileQuestionsEnabled'")) {
                $this->aditur->note("Profilfragen wurden aktiviert.");

                $this->profileQuestionsEnabled = 1;
            }
            else {
                $this->aditur->error("Profilfragen konnten nicht aktiviert werden. Bitte wende Dich an den Admin!");
            }
        }
        if (isset($_POST['deactivate_profile_questions'])) {
            if ($this->db->query("UPDATE aditur_config SET value=0 WHERE config_key='profileQuestionsEnabled'")) {
                $this->aditur->note("Profilfragen wurden deaktiviert.");

                $this->profileQuestionsEnabled = 0;
            }
            else {
                $this->aditur->error("Profilfragen konnten nicht deaktiviert werden. Bitte wende Dich an den Admin!");
            }
        }


        if (isset($_POST['activate_baby_pics'])) {
            if ($this->db->query("UPDATE aditur_config SET value=1 WHERE config_key='babyPicsEnabled'")) {
                $this->aditur->note("Kinderbilder wurden aktiviert.");

                $this->babyPicsEnabled = 1;
            }
            else {
                $this->aditur->error("Kinderbilder konnten nicht aktiviert werden. Bitte wende Dich an den Admin!");
            }
        }
        if (isset($_POST['deactivate_baby_pics'])) {
            if ($this->db->query("UPDATE aditur_config SET value=0 WHERE config_key='babyPicsEnabled'")) {
                $this->aditur->note("Kinderbilder wurden deaktiviert.");

                $this->babyPicsEnabled = 0;
            }
            else {
                $this->aditur->error("Kinderbilder konnten nicht deaktiviert werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['activate_choose_paper_photo'])) {
            if ($this->db->query("UPDATE aditur_config SET value=1 WHERE config_key='choosePaperPhotoEnabled'")) {
                $this->aditur->note("Bildauswahl wurde aktiviert.");

                $this->choosePaperPhotoEnabled = 1;
            }
            else {
                $this->aditur->error("Bildauswahl konnte nicht aktiviert werden. Bitte wende Dich an den Admin!");
            }
        }
        if (isset($_POST['deactivate_choose_paper_photo'])) {
            if ($this->db->query("UPDATE aditur_config SET value=0 WHERE config_key='choosePaperPhotoEnabled'")) {
                $this->aditur->note("Bildauswahl wurde deaktiviert.");

                $this->choosePaperPhotoEnabled = 0;
            }
            else {
                $this->aditur->error("Bildauswahl konnte nicht deaktiviert werden. Bitte wende Dich an den Admin!");
            }
        }


        // Question Management
        if (isset($_POST['new_question_operation']) && !empty($_POST['new_question_text']))  {
            $query = $this->db->prepare("INSERT INTO aditur_me_questions (text) VALUES (?)");
            $query -> bind_param('s', $_POST['new_question_text']);
            if ($query -> execute()) {
                $this->aditur->note("Frage wurde erstellt.");
            }
            else {
                $this->aditur->error("Die neue Frage konnte nicht hinzugefügt werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['delete_question_operation']) && !empty($_POST['question_id'])) {
            $query = $this->db->prepare("DELETE FROM aditur_me_questions WHERE id=?");
            $query -> bind_param('i', $_POST['question_id']);
            if ($query -> execute()) {
                $this->aditur->note("Frage wurde gelöscht.");
            }
            else {
                $this->aditur->error("Ankündigung konnte nicht gelöscht werden! Bitte wende Dich an den Admin!");
            }
        }
    }




    public function index() {
        ?>

        <a class="Setting Box" href="<?php echo $this->aditur->path("/settings/me"); ?>">
            <div>
                <h3><i class="fa fa-lg fa-user-circle-o"></i>&nbsp;Persönliches Profil</h3>
            </div>
        </a>

        <?php
    }
}

?>
