<?php
// Module: Me


class Me extends Module {
    public function fromUrl($argv) {
        ?>

        <h2>Dein Profil</h2>

        <?php

        if ($this->aditur->config("profileQuestionsEnabled")) {
            $this->printSubmitPropertiesForm();
        }

        if ($this->aditur->config("babyPicsEnabled")) {
            $this->printSubmitBabyPicForm();
        }

        if ($this->aditur->config("choosePaperPhotoEnabled")) {
            $this->printChoosePaperPhotoForm();
        }

        $this->printSubmitProfilePicForm();
    }


    // process POST data
    public function post() {
        $success = false;

        // switch different operations

        // Insert answers for a profile question
        if (isset($_POST['submit_properties_operation'])) {
            if (empty($_POST['question_id'])) return false;
            if (empty($_POST['text'])) return false;

            $success = $this -> submitProperties($_POST['question_id'], $_POST['text']);

            unset($_POST['submit_properties_operation']);
        }


        // If the user chooses a photo for the "Abizeitung"
        if (isset($_POST['choose_paper_photo_operation'])) {
            if (empty($_POST['picture_name'])) return false;

            $success = $this -> choosePaperPhoto($_POST['picture_name']);

            unset($_POST['choose_paper_photo_operation']);
        }


        // If the user submits a new profile picture for the website
        if (isset($_POST['submit_profile_pic_operation'])) {
            if (empty($_FILES['new_profile_pic']['tmp_name'])) return false;

            $success = $this -> submitProfilePic($_FILES['new_profile_pic']['tmp_name']);

            unset($_POST['new_profile_pic_operation']);
            unlink($_FILES['new_profile_pic']['tmp_name']);
        }


        // if the user submits a picture from his childhood for the "Abizeitung"
        if (isset($_POST['submit_baby_pic_operation'])) {
            if (empty($_FILES['new_baby_pic']['tmp_name'])) return false;

            $success = $this -> submitBabyPic($_FILES['new_baby_pic']['tmp_name']);

            unset($_POST['submit_baby_pic_operation']);
            unlink($_FILES['new_baby_pic']['tmp_name']);
        }


        return $success;
    }

    // following methods are used by post() to perform certain operations
    private function submitProperties($questionId, $text) {
        $text = htmlspecialchars($text);

        $q = $this -> db -> prepare("INSERT INTO aditur_me_answers (question_id, author_id, text) VALUES (?, ?, ?)");
        $q -> bind_param('iis', $questionId, $_SESSION['id'], $text);

        if (!$q -> execute()) {
            $this->aditur->log($this -> db -> error);

            $this->aditur->error("Deine Antwort konnte nicht gespeichert werden. Bitte wende Dich an den Admin.");

            return false;
        }

        $q -> close();

        $this->aditur->note("Deine Antwort wurde gespeichert.");


        return true;
    }

    private function choosePaperPhoto($pictureName) {
        // check if the user already made a choice
        $q = $this -> db -> query("SELECT COUNT(owner_id) FROM aditur_chosen_picture WHERE owner_id=" . $_SESSION['id']);
        $row = $q -> fetch_row();

        if ($row[0] > 0) {
            // update
            $q = $this -> db -> prepare("UPDATE aditur_chosen_picture SET picture_name=? WHERE owner_id=" . $_SESSION['id']);
            $q -> bind_param('s', $pictureName);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Deine Bildwahl konnte nicht gespeichert werden. Bitte wende Dich an den Admin.");

                return false;
            }

            $q -> close();
        }
        else {
            // insert
            $q = $this -> db -> prepare("INSERT INTO aditur_chosen_picture (owner_id, picture_name) VALUES (" . $_SESSION['id'] . ", ?)");
            $q -> bind_param('s', $pictureName);

            if (!$q -> execute()) {
                $this->aditur->log($this -> db -> error);

                $this->aditur->error("Deine Bildwahl konnte nicht gespeichert werden. Bitte wende Dich an den Admin.");

                return false;
            }

            $q -> close();
        }

        return true;
    }

    private function submitProfilePic($tmp_name) {
        // thumbnail as it is shown in profiles overview
        $thumbnailMaxWidth = 130;
        $thumbnailMaxHeight = 100;

        // image in size in which it is shown on the profile page
        $originalMaxWidth = 700;
        $originalMaxHeight = 400;


        $path = "data/profilePics/raw/" . $_SESSION['id'].".jpg";


        if (file_exists($path)) {
            // remove the old one
            if (!unlink("data/profilePics/raw/" . $_SESSION['id'] . ".jpg")) {
                $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

                return false;
            }
            if (!unlink("data/profilePics/originals/" . $_SESSION['id'] . ".jpg")) {
                $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

                return false;
            }
            if (!unlink("data/profilePics/thumbs/" . $_SESSION['id'] . ".jpg")) {
                $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

                return false;
            }
        }


        $image = $this->aditur->imagecreatefromfile($tmp_name);


        if (is_null($image) or $image === false) {
            $this->aditur->log("ERROR Image is NULL!");

            $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

            return false;
        }


        // create thumbnail

        // landscape
        if (imagesx($image) > imagesy($image)) {
            $thumbnailHeight = imagesy($image) * $thumbnailMaxWidth / imagesx($image);

            $thumbnail = imagecreatetruecolor($thumbnailMaxWidth, $thumbnailHeight);

            imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailMaxWidth, $thumbnailHeight, imagesx($image), imagesy($image));
        }
        // portrait
        else {
            $thumbnailWidth = imagesx($image) * $thumbnailMaxHeight / imagesy($image);

            $thumbnail = imagecreatetruecolor($thumbnailWidth, $thumbnailMaxHeight);

            imagecopyresampled($thumbnail, $image, 0, 0, 0, 0, $thumbnailWidth, $thumbnailMaxHeight, imagesx($image), imagesy($image));
        }


        // output thumbnail
        if (!imagejpeg($thumbnail, $this->aditur->path("data/profilePics/thumbs/" . $_SESSION['id'] . ".jpg"))) {
            $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

            return false;
        }


        // create fitting originals size

        // landscape
        if (imagesx($image) > imagesy($image)) {
            $originalHeight = imagesy($image) * $originalMaxWidth / imagesx($image);

            $original = imagecreatetruecolor($originalMaxWidth, $originalHeight);

            imagecopyresampled($original, $image, 0, 0, 0, 0, $originalMaxWidth, $originalHeight, imagesx($image), imagesy($image));
        }
        // portrait
        else {
            $originalWidth = imagesx($image) * $originalMaxHeight / imagesy($image);

            $original = imagecreatetruecolor($originalWidth, $originalMaxHeight);

            imagecopyresampled($original, $image, 0, 0, 0, 0, $originalWidth, $originalMaxHeight, imagesx($image), imagesy($image));
        }


        // output original
        if (!imagejpeg($original, $this->aditur->path("data/profilePics/originals/" . $_SESSION['id'] . ".jpg"))) {
            $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

            return false;
        }


        // save "raw", the uploaded image
        if (imagejpeg($image, $this->aditur->path("data/profilePics/raw/" . $_SESSION['id'] . ".jpg"))) {
            $this->aditur->note("Schönes Profilbild!");

            return true;
        } else {
            $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

            return false;
        }
    }

    private function submitBabyPic($tmp_name) {
        $path = "data/babyPics/" . $_SESSION['id'].".jpg";


        if (file_exists($path)) {
            // remove the old one
            if (!unlink("data/babyPics/" . $_SESSION['id'] . ".jpg")) {
                $this->aditur->error("Neues Profilbild konnte nicht gespeichert werden!");

                return false;
            }
        }


        $image = $this->aditur->imagecreatefromfile($tmp_name);


        if (is_null($image) or $image === false) {
            $this->aditur->log("ERROR Image is NULL");

            $this->aditur->error("Neues Kinderbild konnte nicht gespeichert werden!");

            return false;
        }


        if (imagejpeg($image, $this->aditur->path("data/babyPics/" . $_SESSION['id'] . ".jpg"))) {
            $this->aditur->note("Schönes Kinderbild!");

            return true;
        } else {
            $this->aditur->error("Neues Kinderbild konnte nicht gespeichert werden!");

            return false;
        }
    }


    protected function printSubmitPropertiesForm() {
        $query = $this->db->query("SELECT id, text FROM aditur_me_questions");

        $questions = array();
        while ($object = $query -> fetch_object()) {
            $questions[] = $object;
        }

        if (empty($questions)) {
            return;
        }

        ?>

        <h3>Ein paar Fragen zu Dir</h3>

        <div class="Questions Group">
            <?php

            foreach ($questions as $question) {
                $query = $this->db->query("SELECT text FROM aditur_me_answers WHERE question_id=" . $question->id . " AND author_id=" . $_SESSION['id'] . " ORDER BY id DESC LIMIT 1");

                ?>

                <div class="Box QuestionPair" id="question<?php echo $question->id; ?>">
                    <form class="SubmitPropertiesForm SubmitForm" action="<?php echo $this->aditur->url(); ?>#question<?php echo $question->id; ?>" method="post">
                        <input type="hidden" name="question_id" value="<?php echo $question->id; ?>">

                        <label class="Question" for="question<?php echo $question->id; ?>"><?php echo $question->text; ?></label>

                        <textarea name="text" placeholder="Gib hier Deine Antwort ein."><?php if ($answer = $query -> fetch_object()) { echo $answer->text; } ?></textarea>

                        <button type="submit" name="submit_properties_operation">
                            <i class="fa fa-lg fa-check"></i>&nbsp;OK
                        </button>
                    </form>
                </div>

                <?php
            }

            ?>
        </div>

        <?php
    }


    public function printSubmitProfilePicForm() {
        ?>

        <form class="SubmitProfilePic SubmitForm" action="<?php echo $this->aditur->path("/me"); ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="MAX_FILE_SIZE" value="8000000">

            <div class="ImageSizer">
                <img style="width: 100%;" class="ProfileImage" alt="Dein Profilbild" src="<?php echo $this->aditur->path("/media/profilePics/raw/" . $_SESSION['id'] . "/" . $_SESSION['forename'] . " " . $_SESSION['lastname'] . ".jpg?reload"); ?>">
            </div>

            <div class="Box" style="margin-top: 1.5rem;">
                <h3 style="margin-top: 0;">Neues Profilbild hochladen</h3>

                <input type="file" name="new_profile_pic" required>

                <button type="submit" name="submit_profile_pic_operation"><i class="fa fa-lg fa-upload"></i>&nbsp;Hochladen</button>
            </div>

        </form>

        <?php
    }


    private function printSubmitBabyPicForm() {
        ?>

        <form class="SubmitBabyPic SubmitForm" action="<?php echo $this->aditur->path("/me"); ?>" method="post" enctype="multipart/form-data">
            <input type="hidden" name="MAX_FILE_SIZE" value="8000000">

            <div class="ImageSizer">
                <img style="width: 100%;" class="BabyPic" alt="Dein Kinderbild" src="<?php echo $this->aditur->path("/media/babyPics/" . $_SESSION['id'] . "/" . $_SESSION['forename'] . " " . $_SESSION['lastname'] . ".jpg?reload"); ?>">
            </div>

            <div class="Box" style="margin-top: 1.5rem;">
                <h3 style="margin-top: 0;">Ein altes Kinderbild hochladen</h3>

                <input type="file" name="new_baby_pic" required>

                <button type="submit" name="submit_baby_pic_operation"><i class="fa fa-lg fa-upload"></i>&nbsp;Hochladen</button>
            </div>

        </form>

        <?php
    }


    private function printChoosePaperPhotoForm() {
        // fetch choice
        $chosenPicture = "";

        $query = $this->db->query("SELECT picture_name FROM aditur_chosen_picture WHERE owner_id=" . $_SESSION['id']);

        if ($query -> num_rows == 1) {
            $chosenPicture = $query -> fetch_object() -> picture_name;
        }

        $query -> close();


        // list images
        $pictures = array();

        $directory = "data/paper/originals/" . $_SESSION['id'];
        if (is_dir($directory)) {
            $pictures = array_diff(scandir($directory), array('..', '.'));
        }


        ?>

        <section class="Box">
            <h3>Wähle dein Bild für die Abizeitung aus</h3>

            <div class="ChoosePicturesFrame">
                <?php
                if (empty($pictures)) {
                    ?>
                    <p>Es stehen keine Bilder zur Auswahl. Bitte wende Dich an das Abizeitungskomitee.</p>
                    <?php
                }
                else {
                    $nPicture = 1;

                    ?>
                    <p>Klick auf das Bild, das Du am schönsten findest!</p>
                    <?php
                    foreach ($pictures as $picture) {
                        ?>
                        <form class="ChoosePaperPhotoForm SubmitForm" action="<?php echo $this->aditur->url() . "#" . $nPicture; ?>" method="post">
                            <input type="hidden" name="picture_name" value="<?php echo $picture; ?>">

                            <p class="N" id="<?php echo $nPicture; ?>">#<?php echo $nPicture; ?>: <?php if ($picture == $chosenPicture) echo "Ausgewählt"; ?></p>

                            <button type="submit" name="choose_paper_photo_operation" class="<?php if ($picture == $chosenPicture) echo "Chosen"; ?>">
                                <img alt="Dein vielleicht schönstes Bild" src="<?php echo $this->aditur->path("media/paper/originals/" . $_SESSION['id'] . "/" . $picture); ?>">
                            </button>
                        </form>
                        <?php

                        $nPicture++;
                    }
                    ?>
                    </div>
                <?php
                }
            ?>
        </section>

        <?php
    }
}



?>
