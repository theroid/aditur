<?php
// Module: Upload


class Upload extends Module {
    public function fromUrl($argv = null) {
        //this file should take photos from $uploadDir
        //process them (convert, resize, make thumbnails, add watermarks, ...)
        //eventually create database entries for them, ...
        //and then copy them to the according directory

        //thumnail dimensions (quadratic)
        $thumbnailDimensions = 300;


        //get all files in the upload directory...
        $uploadDir = "./upload/";
        $files = array_values(array_diff(scandir($uploadDir), array('..', '.', '.htaccess')));

        $destinationDir = "./data/photos/1/";
        $watermarkPath  = "./data/images/watermark.png";

        $watermark = imagecreatefrompng($watermarkPath);


        //...and process them
        foreach ($files as $key => $imagePath) {
            // reset max execution timer
            set_time_limit(30);

            // new filename
            $newFileName = explode(".", $imagePath)[0];

            //original
            $image = imagecreatefromjpeg($uploadDir.$imagePath);

            //create thumbnail
            $thumbnail = imagecreatetruecolor($thumbnailDimensions, $thumbnailDimensions);


            //crop before resampling as I want quadratic thumbnails

            //calculate whether height or width is bigger
            if (imagesx($image) > imagesy($image)) { //width is bigger
                $cropArray = array('x'      => (imagesx($image) - imagesy($image)) / 2,
                                   'y'      => 0,
                                   'width'  => imagesy($image),
                                   'height' => imagesy($image));
            }
            else {  //height is bigger
                $cropArray = array('x'      => 0,
                                   'y'      => (imagesy($image) - imagesx($image)) / 2,
                                   'width'  => imagesx($image),
                                   'height' => imagesx($image));
            }

            $croppedImage = imagecrop($image, $cropArray);


            //add watermark
            // Copy the watermark image onto our image using the margin offsets and the image
            // Set the margins for the watermark and get the height/width of the watermark image
            $marginRight = 50;
            $marginBottom = 50;
            $sx = imagesx($watermark);
            $sy = imagesy($watermark);


            // width to calculate positioning of the watermark.
            imagecopy($croppedImage, $watermark, imagesx($croppedImage) - $sx - $marginRight, imagesy($croppedImage) - $sy - $marginBottom, 0, 0, $sx, $sy);

            imagejpeg($croppedImage, $destinationDir."raw/".$newFileName.".jpg");


            // resize original image
            // imagescale($croppedImage, 1000, 1000);
            $resizedImage = imagecreatetruecolor(1000, 1000);
            imagecopyresampled($resizedImage, $croppedImage, 0, 0, 0, 0, 1000, 1000, imagesx($croppedImage), imagesy($croppedImage));

            //copy to destination
            imagejpeg($resizedImage, $destinationDir."originals/".$newFileName.".jpg");


            //resize
            imagecopyresampled($thumbnail, $croppedImage, 0, 0, 0, 0, $thumbnailDimensions, $thumbnailDimensions, imagesx($croppedImage), imagesy($croppedImage));

            imagejpeg($thumbnail, $destinationDir."thumbs/".$newFileName.".jpg");
            //imagejpeg($croppedImage, $destinationDir."thumbs/".$key.".jpg");


            imagedestroy($image);
            imagedestroy($croppedImage);
            imagedestroy($resizedImage);
            imagedestroy($thumbnail);

            echo "<p>Image created: ".$newFileName."</p>";

            //delete Original from uploadDir
            unlink($uploadDir.$imagePath);
        }

        imagedestroy($watermark);
    }
}


?>
