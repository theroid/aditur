<?php
// Settings: Views
// manage Views (add, delete)


class ViewsSettings extends Setting {
    public function fromUrl($argv) {
        $this->op();


        $query = $this->db->query("SELECT id, name, link, title, needs_login, home FROM aditur_views");

        $views = array();
        while ($object = $query -> fetch_object()) {
            $views[] = $object;
        }

        $query -> close();


        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/settings"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Einstellungen</a>

        <h2>Views</h2>

        <section class="NewView Box">
            <h3>Neuen View erstellen</h3>

            <form class="CreateNewViewForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post" enctype="multipart/form-data">
                <input type="text" name="new_view_name" placeholder="Name" required>

                <input type="text" name="new_view_title" placeholder="Titel" required>

                <input type="text" name="new_view_link" placeholder="Link/URL" required>

                <p><br><input type="checkbox" name="new_view_needs_login" id="needsLogin"><label for="needsLogin"> Login notwendig</label></p>

                <p><br><input type="checkbox" name="new_view_is_home" id="isHome"><label for="isHome"> Index-View</label></p>

                <p><br>Der Index-View wird angezeigt, wenn die Seite nur mit "http://<?php echo $this->aditur->config("domain"); ?>" aufgerufen wird.<br><br></p>

                <p><label for="newViewScript">PHP-Script:</label><input type="file" name="new_view_script" id="newViewScript" required><br><br></p>

                <p><label for="newViewCss">CSS-Stylesheet</label><input type="file" name="new_view_css" id="newViewCss"><br><br></p>

                <button type="submit" name="new_view_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

        <section class="Settings ViewManagement Box">
            <h3>Views</h3>
            <ul class="Views" id="views">
                <?php

                $nView = 0;
                foreach ($views as $view) {
                    ?>

                    <li>
                        <p class="Name"><?php echo $view->name; ?><br><br>
                        Titel: <?php echo $view->title; ?><br>
                        Link: <?php echo $this->aditur->config("domain") . "/" . $view->link; ?><br><br>
                        <?php if ($view->needs_login) echo "Login notwendig<br>";
                              if ($view->home) echo "<strong>Home</strong>"; ?>
                        </p>

                        <input type="checkbox" class="Expander ViewExpander" id="viewExpander<?php echo $nView; ?>">
                        <label class="Toggler" for="viewExpander<?php echo $nView; ?>">
                            <i class="fa fa-lg fa-times-circle"></i>
                        </label>
                        <form class="DeleteViewForm SubmitForm" action="<?php echo $this->aditur->url() . "#views"; ?>" method="post">
                            <input type="hidden" name="view_id" value="<?php echo $view->id; ?>">

                            <p>Diesen View wirklich löschen?</p>

                            <button type="submit" name="delete_view_operation">
                                <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                            </button>
                        </form>
                    </li>

                    <?php

                    $nView++;
                }

                ?>
            </ul>
        </section>

        <?php
    }


    private function op() {
        if (isset($_POST['new_view_operation']) && !empty($_POST['new_view_name'])
            && !empty($_POST['new_view_title']) && !empty($_POST['new_view_link'])
            && !empty($_FILES['new_view_script']['tmp_name'])) {
            $needsLogin = isset($_POST['new_view_needs_login']) ? 1 : 0;
            $isHome = isset($_POST['new_view_is_home']) ? 1 : 0;


            $query = $this->db->prepare("INSERT INTO aditur_views (name, title, link, needs_login, home) VALUES (?, ?, ?, ?, ?)");
            $query -> bind_param('sssii', $_POST['new_view_name'],
                                 $_POST['new_view_title'],
                                 $_POST['new_view_link'],
                                 $needsLogin,
                                 $isHome);
            if (!$query -> execute()) {
                $this->aditur->error("Der neue View konnte nicht hinzugefügt werden. Bitte wende Dich an den Admin!");

                $query -> close();

                return;
            }

            $query -> close();


            copy($_FILES['new_view_script']['tmp_name'], "views/" . $_POST['new_view_name'] . ".php");

            if (!empty($_FILES['new_view_css']['tmp_name'])) {
                copy($_FILES['new_view_css']['tmp_name'], "css/" . $_FILES['new_view_css']['name']);
            }
        }

        if (isset($_POST['delete_view_operation']) && !empty($_POST['view_id'])) {
            $query = $this->db->prepare("SELECT name, home FROM aditur_views WHERE id=?");
            $query -> bind_param('i', $_POST['view_id']);
            $query -> bind_result($name, $home);
            $query -> execute();
            $query -> store_result();

            if ($query -> num_rows !== 1) {
                $this->aditur->error("Der zu löschende View existiert nicht.");

                $query -> close();

                return;
            }

            $query -> fetch();

            if ($home) {
                $this->aditur->error("Der Index-View darf nicht gelöscht werden!");

                $query -> close();

                return;
            }

            $query -> close();


            $query = $this->db->prepare("DELETE FROM aditur_views WHERE id=?");
            $query -> bind_param('i', $_POST['view_id']);

            if ($query -> execute()) {
                $this->aditur->note("View wurde gelöscht.");
            }
            else {
                $this->aditur->error("View konnte nicht gelöscht werden. Bitte wende Dich an den Admin!");

                $query -> close();

                return;
            }

            $query -> close();

            unlink("views/" . $name . ".php");
        }
    }


    public function index() {
        ?>

        <a class="Setting Box" href="<?php echo $this->aditur->path("/settings/views"); ?>">
            <div>
                <h3><i class="fa fa-lg fa-files-o"></i>&nbsp;Views</h3>
            </div>
        </a>

        <?php
    }
}

?>
