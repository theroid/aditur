<?php
// Module: Views


class Views extends Module {
    public function fromUrl($argv) {
        if (!empty($argv[0])) {
            $this->showView($argv[0]);
        }
        else {
            $this->showHome();
        }
    }


    public function showHome() {
        $query = $this->db->query("SELECT name FROM aditur_views WHERE home=1");
        $object = $query -> fetch_object();

        $query -> close();

        $this->printView($object->name, "Home");
    }


    public function showView($view) {
        $query = $this->db->prepare("SELECT name, title, needs_login FROM aditur_views WHERE link=?");
        $query -> bind_param('s', $view);
        $query -> execute();
        $query -> bind_result($name, $title, $needs_login);
        $query -> store_result();

        if ($query -> num_rows !== 1) {
            $query -> close();

            $this->aditur->error("Die aufgerufene Seite existiert nicht.");
            $this->showHome();
            return;
        }

        $query -> fetch();
        $query -> close();

        if ($needs_login && !$this->aditur->login->isUserLoggedIn()) {
            $this->aditur->error("Die aufgerufene Seite existiert nicht.");
            $this->showHome();

            return;
        }

        $this->printView($name, $title);
    }


    private function printView($view, $title) {
        $this->aditur->setTitle($title);

        $this->aditur->aditur_include($this->aditur->path("views/" . $view . ".php"));
    }
}

?>
