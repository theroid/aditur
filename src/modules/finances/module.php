<?php
// Module: Finances


class Finances extends Module {
    public function fromUrl($argv = null) {
        $this->aditur->loadModule("headquarter", "fromUrl");
        return;
    }


    public function showStatus() {
        $query = $this->db->query("SELECT enabled FROM aditur_modules WHERE name='finances'");

        $enabled = $query -> fetch_object() -> enabled;

        if (!$enabled and $this->aditur->mayI("changeFinances") == false) {
            return;
        }


        $this->op();


        // first gather destination value
        $query = "SELECT value, name FROM aditur_finances WHERE destination=1 ORDER BY timestamp DESC LIMIT 1";
        $result = $this->db->query($query);

        if (!$object = $result->fetch_object()) {
            $this->aditur->log("Finances: No Destination set!");

            if (!($this->aditur->mayI("changeFinances"))) {
                // do not display this form
                return;
            }

            ?>

            <section class="Finances Box">
                <h2>Finanzen</h2>

                <?php $this->printSubmitNewValueForm(); ?>
            </section>

            <?php

            return;
        }

        $destination = $object->value;
        $destinationName = $object->name;

        // then our current balance
        $query = "SELECT value, name FROM aditur_finances WHERE balance=1 ORDER BY timestamp DESC LIMIT 1";
        $result = $this->db->query($query);

        if (!$object = $result->fetch_object()) {
            $this->aditur->log("Finances: No balance set!");
            $currentBalance = 0;
        } else {
            $currentBalance = $object->value;
        }



        // compute progress in percent
        $progress = floor(100 * $currentBalance / $destination);


        // show progress bar
        if ($progress < 100) {
            ?>

            <section class="Finances Box">
                <h2>Finanzen</h2>


                <?php $this->printSubmitNewValueForm(); ?>

                <div class="Graph">
                    <div class="Bar">
                        <div class="Markers">
                            <div class="Marker" id="start" style="left: 0">
                                <div class="Name">Anfang</div>
                                <div class="Value">0&nbsp;€</div>
                            </div>

                            <div class="Marker" id="balance" style="left: <?php echo $progress."%"; ?>">
                                <i class="fa fa-lg fa-chevron-down"></i>

                                <div class="Value">
                                    <p><?php echo $currentBalance."&nbsp;€"; ?></p>
                                </div>
                            </div>

                            <div class="Marker" id="destination">
                                <div class="Name"><?php echo $destinationName; ?></div>
                                <div class="Value"><?php echo $destination."&nbsp;€"; ?></div>
                            </div>
                        </div>

                        <div class="Progress"></div>
                    </div>
                </div>
            </section>

            <?php

        }
        // display something like "yeah we have enough money!"
        else {
            ?>

            <section class="Finances Box">
                <h2>Finanzen</h2>

                <?php $this->printSubmitNewValueForm(); ?>

                <div class="Victory"><?php echo $currentBalance; ?>€!!!</div>
            </section>

            <?php
        }

    }


    protected function printSubmitNewValueForm() {
        // if this is not the speaker of the Finanzkomitee neither an administrator
        if (!($this->aditur->mayI("changeFinances"))) {
            // do not display this form
            return;
        }

        ?>

        <input class="Expander" id="financesExpander" type="checkbox">

        <label class="SettingsToggler" for="financesExpander"><i class="fa fa-lg fa-cog"></i></label>

        <div class="SettingsForm Toggled">
            <form class="SubmitForm SubmitNewValueForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <h3>Zielbetrag ändern</h3>

                <input type="text" name="new_name" placeholder="Name">

                <input id="new_value" name="new_value" type="number" placeholder="Betrag in €">

                <button type="submit" name="new_destination"><i class="fa fa-lg fa-check"></i>&nbsp;Ändern</button>
            </form>

            <form class="SubmitForm SubmitNewValueForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <h3>Kontostand ändern</h3>

                <input id="new_balance" name="new_balance" type="number" placeholder="Betrag in €">
                <button type="submit" name="change_balance"><i class="fa fa-lg fa-check"></i>&nbsp;Ändern</button>
            </form>
        </div>

        <?php
    }


    protected function op() {
        // if this is not the speaker of the Finanzkomitee neither an administrator
        if (!($this->aditur->mayI("changeFinances"))) {
            // do not display this form
            return;
        }

        if (isset($_POST['new_destination'])) {

            isset($_POST['new_name']) ? $newName = htmlspecialchars($_POST['new_name']) : $newName = "";
            isset($_POST['new_value']) ? $newValue = $_POST['new_value'] : $newValue = 1;


            // cast to float
            if ($newValue <= 0) $newValue = 1;
            $newValue = floatval($newValue);
            $isDestination = true;

            $time = time();

            // fill in new vlaues
            $query = $this->db->prepare("INSERT INTO aditur_finances (name, value, destination, balance) VALUES (?, ?, ?, 0)");

            $query -> bind_param('sii', $newName, $newValue, $isDestination);

            if (!$query -> execute()) {
                // echo $this->db->error;
            }

            $query -> close();

            $this->aditur->note("Neuer Marker wurde gespeichert.");
        }
        else if (isset($_POST['change_balance']) && !empty($_POST['new_balance'])) {
            $newBalance = $_POST['new_balance'];

            // cast to float
            $newBalance = floatval($newBalance);

            $time = time();
            $name = "Kontostand am " . date("m.d.y");

            // fill in new vlaues
            $query = $this->db->prepare("INSERT INTO aditur_finances (name, value, destination, balance) VALUES (?, ?, 0, 1)");

            $query -> bind_param('si', $name, $newBalance);

            if (!$query -> execute()) {
                // echo $this->db->error;
            }

            $query -> close();

            $this->aditur->note("Kontostand gespeichert.");
        }
    }
};


?>
