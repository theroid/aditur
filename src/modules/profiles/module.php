<?php
// Module: Profiles


class Profiles extends Module {
    public function fromUrl($argv) {
        if (empty($argv)) {
            $this->allProfiles();
        }
        else {
            $this->profile($argv[0]);
        }
    }


    protected function op() {

    }


    public function allProfiles() {
        // show all profiles
        $query = "SELECT id, forename, lastname FROM aditur_members ORDER BY forename";
        if (!$result = $this->db->query($query)) {
            echo $this->db->error;
            $this->aditur->log($this -> db -> error);
        }

        $objects = array();

        while ($object = $result->fetch_object()) {
            if ($object->id == $_SESSION['id']) {
                continue;
            }

            $objects[] = $object;
        }

        ?>

        <h2>Alle Profile</h2>

        <div class="ProfileFrames">
            <?php
                for ($i = 0; $i < count($objects); $i++) {
                    $object = $objects[$i];

                    $displayName = $object->forename;

                    // if two people share the same forename
                    if ($i > 0 && $objects[$i-1]->forename == $object->forename || $i < count($objects)-1 && $object->forename == $objects[$i+1]->forename) {
                        $displayName .= " " . $object->lastname;
                    }

            ?>
            <div class="ProfileFrame">
                <a href="<?php echo $this->aditur->path("/profiles/" . $object->id . "/" . $displayName); ?>">
                    <div class="ImageSizer">
                        <img alt="<?php echo $displayName; ?>'s Profilbild" src="<?php echo $this->aditur->path("/media/profilePics/thumbs/" . $object->id. "/" . $object->forename . " " . $object->lastname . ".jpg"); ?>">
                    </div>
                    <p><?php echo $displayName; ?></p>
                </a>
            </div>
            <?php } ?>
        </div>

        <?php
    }




    public function profile($id) {
        // display a specific profile in Detail.


        // check if $username exists
        $query = $this -> db -> prepare("SELECT forename, lastname FROM aditur_members WHERE id=?");
        $query -> bind_param('i', $id);
        $query -> execute();
        $query -> bind_result($forename, $lastname);
        $query -> store_result();

        // if there is != 1 user or the selected user is == SESSION
        if (!($query->num_rows == 1)) {
            // if user does not exist display all users
            $this->aditur->error("Dieser Benutzer existiert nicht.");
            $query->close();

            $this->allProfiles();

            return;
        }

        if ($id == $_SESSION['id']) {
            $query -> close();

            $this->aditur->loadModule("me", "fromUrl");

            return;
        }
        else {
            $query -> fetch();
            $query -> close();

            ?>

        <div class="ProfileDetail">
            <div class="displayRow">
                <a class="Back" href="<?php echo $this->aditur->path("/profiles"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Profile</a>

                <h2><?php echo $forename; ?>'s Profil</h2>
            </div>
            <div class="ImageSizer">
                <img class="ProfileImage" alt="<?php echo $forename; ?>'s Profilbild" src="<?php echo $this->aditur->path("/media/profilePics/originals/" . $id . "/" . $forename . " " . $lastname . ".jpg"); ?>">
            </div>

            <?php
            // load existing comments
            $this->aditur->loadModule("comments", "printYourCommentsTo", $id);

            // load comments submit form
            $this->aditur->loadModule("comments", "printSubmitForm");

            // TODO implement Rest
            $this->aditur->loadModule("comments", "printNotCommentedProfiles");
            ?>
        </div>

        <?php
        }
    }



    public function profileFrame($profiles) {
    ?>
<div class="ProfileFrames">
    <?php foreach ($profiles as $id) {
        if ($_SESSION['id'] == $id) {
            continue;
        }

        $query = $this->db->prepare("SELECT forename, lastname FROM aditur_members WHERE id=?");
        $query -> bind_param('i', $id);
        $query -> execute();
        $query -> bind_result($forename, $lastname);
        $query -> store_result();

        if ($query -> num_rows != 1) {
            $query -> close();

            continue;
        }

        $query -> fetch();
        $query -> close();

    ?>
    <div class="ProfileFrame">
        <a href="<?php echo $this->aditur->path("/profiles/" . $id . "/" . $forename . " " . $lastname); ?>">
            <div class="ImageSizer">
                <img alt="<?php echo $forename; ?>" src="<?php echo $this->aditur->path("/media/profilePics/thumbs/" . $id . "/" . $forename . " " . $lastname . ".jpg"); ?>">
            </div>
            <p><?php echo $forename; ?></p>
        </a>
    </div>
    <?php } ?>
</div>
    <?php
    }
}


?>
