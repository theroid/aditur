<?php
// Settings: Profiles
// Benutzerverwaltung


class ProfilesSettings extends Setting {
    public function fromUrl($argv) {
        $this->op();


        $query = $this->db->query("SELECT id, name FROM aditur_komitees");

        $komitees = array();
        while ($object = $query -> fetch_object()) {
            $komitees[] = $object;
        }

        $query -> close();

        ?>

        <a class="Back" href="<?php echo $this->aditur->path("/settings"); ?>"><i class="fa fa-lg fa-arrow-left"></i>&nbsp;Alle&nbsp;Einstellungen</a>


        <h2>Profile</h2>

        <h3>Komiteeverwaltung</h3>

        <section class="NewKomitee Box">
            <h3>Neues Komitee erstellen</h3>

            <form class="CreateNewKomiteeForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input type="text" name="new_komitee_name" placeholder="Komiteename" required>

                <button type="submit" name="new_komitee_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

        <section class="KomiteeManagement Box">
            <h3>Komitees</h3>

            <ul class="Komitees" id="komitees">
                <li><p class="Name">1: /</p></li>
                <li><p class="Name">2: Finanzen</p></li>
                <li><p class="Name">3: Abizeitung</p></li>

                <?php

                $nKomitee = 0;
                foreach ($komitees as $komitee) {
                    if ($komitee->id <= 3) {
                        continue;
                    }
                    ?>

                    <li>
                        <p class="Name"><?php echo $komitee->id . ": " . $komitee->name; ?></p>
                        <input type="checkbox" class="Expander KomiteeExpander" id="komiteeExpander<?php echo $nKomitee; ?>">
                        <label class="Toggler" for="komiteeExpander<?php echo $nKomitee; ?>">
                            <i class="fa fa-lg fa-times-circle"></i>
                        </label>
                        <form class="DeleteKomiteeForm SubmitForm" action="<?php echo $this->aditur->url() . "#komitees"; ?>" method="post">
                            <input type="hidden" name="komitee_id" value="<?php echo $komitee->id; ?>">

                            <p>Dieses Komitee wirklich löschen?</p>

                            <button type="submit" name="delete_komitee_operation">
                                <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                            </button>
                        </form>
                    </li>

                    <?php

                    $nKomitee++;
                }

                ?>
            </ul>
        </section>


        <h3>Benutzerverwaltung</h3>

        <section class="NewProfile">
            <form class="CreateNewUserForm SubmitForm Box" action="<?php echo $this->aditur->url(); ?>" method="post">
                <h3>Neuen Benutzer erstellen</h3>

                <input type="text" name="new_profile_forename" value="" placeholder="Vorname" required>

                <input type="text" name="new_profile_lastname" value="" placeholder="Nachname" required>

                <input type="email" name="new_profile_email" value="" placeholder="E-Mail-Adresse" required>

                <select name="new_profile_privileges" required>
                    <option value="normal">Normal</option>
                    <option value="speaker">Komiteesprecher</option>
                    <option value="president">Jahrgangssprecher</option>
                    <option value="administrator">Admin</option>
                </select>

                <select name="new_profile_komitee" required>
                    <?php

        foreach ($komitees as $komitee) {
                    ?>
                    <option value="<?php echo $komitee->id; ?>"><?php echo $komitee->name; ?></option>
                    <?php
        }

                    ?>
                </select>

                <button type="submit" name="new_profile_operation">
                    <i class="fa fa-lg fa-check"></i>&nbsp;Erstellen
                </button>
            </form>
        </section>

        <section class="ManageProfiles">
            <h3>Benutzer</h3>

            <p>Klick auf die <span class="Properties">Eigenschaften</span>, die Du ändern möchtest!</p>

            <ul class="ProfileList" id="form-1">
                <?php

                $query = $this->db->query("SELECT aditur_members.id AS id, forename, lastname, email, privileges, komitee, aditur_komitees.id AS komitee_id, aditur_komitees.name FROM aditur_members, aditur_komitees WHERE aditur_komitees.id=komitee ORDER BY forename");

                $nForms = 0;
                while ($object = $query -> fetch_object()) {
                    ?>

                    <li class="Profile Box" id="form<?php echo $nForms; ?>">
                        <div class="ProfileFrame">
                            <div class="ImageSizer">
                                <img src="<?php echo $this->aditur->path("/media/profilePics/thumbs/" . $object->id . "/" . $object->forename . " " . $object->lastname) . ".jpg"; ?>" alt="">
                            </div>
                            <p class="ProfileName"><?php echo $object->forename . " " . $object->lastname; ?><br>(ID: <?php echo $object->id; ?>)</p>
                        </div>

                        <div class="Forms">
                            <div class="ChangeName">
                                <input type="checkbox" class="Expander NameExpander" id="nameExpander<?php echo $nForms; ?>">
                                <label class="Data" for="nameExpander<?php echo $nForms; ?>">
                                    <?php echo $object->forename . " " . $object->lastname; ?>
                                </label>
                                <form class="ChangeName SubmitForm" action="<?php echo $this->aditur->url() . "#form" . $nForms; ?>" method="post">
                                    <i class="fa fa-lg fa-chevron-up"></i>

                                    <label class="Close" for="nameExpander<?php echo $nForms; ?>">
                                        <i class="fa fa-lg fa-times-circle"></i>
                                    </label>

                                    <p>Profilnamen ändern</p>

                                    <input type="hidden" name="profile_id" value="<?php echo $object->id; ?>">

                                    <input type="text" name="new_profile_forename" value="<?php echo $object->forename; ?>" placeholder="Neuer Vorname" required>
                                    <input type="text" name="new_profile_lastname" value="<?php echo $object->lastname; ?>" placeholder="Neuer Nachname" required>

                                    <button type="submit" name="change_profile_name_operation">
                                        <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                                    </button>
                                </form>
                            </div>

                            <div class="ChangeEmail">
                                <input type="checkbox" class="Expander EmailExpander" id="emailExpander<?php echo $nForms; ?>">
                                <label class="Data" for="emailExpander<?php echo $nForms; ?>">
                                    <?php echo $object->email; ?>
                                </label>
                                <form class="ChangeEmail SubmitForm" action="<?php echo $this->aditur->url() . "#form" . $nForms; ?>" method="post">
                                    <i class="fa fa-lg fa-chevron-up"></i>

                                    <label class="Close" for="emailExpander<?php echo $nForms; ?>">
                                        <i class="fa fa-lg fa-times-circle"></i>
                                    </label>

                                    <p>E-Mail-Adresse ändern</p>

                                    <input type="hidden" name="profile_id" value="<?php echo $object->id; ?>">

                                    <input type="text" name="new_profile_email" value="<?php echo $object->email; ?>" placeholder="Neue E-Mail-Adresse" required>

                                    <button type="submit" name="change_profile_email_operation">
                                        <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                                    </button>
                                </form>
                            </div>

                            <div class="ChangeRights">
                                <input type="checkbox" class="Expander RightsExpander" id="rightsExpander<?php echo $nForms; ?>">
                                <label class="Data" for="rightsExpander<?php echo $nForms; ?>">
                                    <?php

                                    switch ($object->privileges) {
                                        case "administrator":
                                            echo "Admin";
                                            break;

                                        case "president":
                                            echo "Jahrgangssprecher";
                                            break;

                                        case "speaker":
                                            echo "Komiteesprecher";
                                            break;

                                        case "normal":
                                            echo "Normal";
                                            break;

                                        default:
                                            echo "/";
                                            break;
                                    }

                                    ?>
                                    <br>
                                    <?php echo $object->name; ?>
                                </label>
                                <form class="ChangeRights SubmitForm" action="<?php echo $this->aditur->url() . "#form" . $nForms; ?>" method="post">
                                    <i class="fa fa-lg fa-chevron-up"></i>

                                    <label class="Close" for="rightsExpander<?php echo $nForms; ?>">
                                        <i class="fa fa-lg fa-times-circle"></i>
                                    </label>

                                    <p>Rechte und Komitee ändern</p>

                                    <input type="hidden" name="profile_id" value="<?php echo $object->id; ?>">

                                    <select name="new_profile_privileges" required>
                                        <option value="<?php echo $object->privileges; ?>">
                                            <?php

                                            switch ($object->privileges) {
                                                case "administrator":
                                                    echo "Admin";
                                                    break;

                                                case "president":
                                                    echo "Jahrgangssprecher";
                                                    break;

                                                case "speaker":
                                                    echo "Komiteesprecher";
                                                    break;

                                                case "normal":
                                                    echo "Normal";
                                                    break;

                                                default:
                                                    echo "/";
                                                    break;
                                            }

                                            ?>
                                        </option>
                                        <option value="administrator">Admin</option>
                                        <option value="president">Jahrgangssprecher</option>
                                        <option value="speaker">Komiteesprecher</option>
                                        <option value="normal">Normal</option>
                                    </select>

                                    <select name="new_profile_komitee" value="<?php echo $object->lastname; ?>" placeholder="Neuer Nachname" required>
                                        <option value="<?php echo $object->komitee_id; ?>"><?php echo $object->name; ?></option>

                                        <?php foreach ($komitees as $komitee) {
                                        ?>
                                        <option value="<?php echo $komitee->id; ?>"><?php echo $komitee->name; ?></option>
                                        <?php
                                            }

                                        ?>
                                    </select>

                                    <button type="submit" name="change_profile_rights_operation">
                                        <i class="fa fa-lg fa-check"></i>&nbsp;Ändern
                                    </button>
                                </form>
                            </div>
                        </div>

                        <div class="DeleteProfile">
                            <input type="checkbox" class="Expander DeleteProfileExpander" id="deleteProfileExpander<?php echo $nForms; ?>">
                            <label class="Toggler Close" for="deleteProfileExpander<?php echo $nForms; ?>">
                                <i class="fa fa-lg fa-trash"></i>
                            </label>
                            <form class="DeleteProfileForm SubmitForm" action="<?php echo $this->aditur->url(); ?>" method="post">
                                <i class="fa fa-lg fa-chevron-up"></i>

                                <label class="Close" for="deleteProfileExpander<?php echo $nForms; ?>">
                                    <i class="fa fa-lg fa-times-circle"></i>
                                </label>

                                <input type="hidden" name="profile_id" value="<?php echo $object->id; ?>">

                                <p>Dieses Profil wirklich löschen?</p>

                                <button type="submit" name="delete_profile_operation">
                                    <i class="fa fa-lg fa-trash"></i>&nbsp;Löschen
                                </button>
                            </form>
                        </div>
                    </li>

                    <?php

                    $nForms++;
                }

                ?>
            </ul>
        </section>

        <?php
    }


    private function op() {
        // Komitee Management
        if (isset($_POST['new_komitee_operation']) && !empty($_POST['new_komitee_name']))  {
            $newKomiteeName = htmlspecialchars($_POST['new_komitee_name']);

            $query = $this->db->prepare("INSERT INTO aditur_komitees (name) VALUES (?)");
            $query -> bind_param('s', $newKomiteeName);
            if ($query -> execute()) {
                $this->aditur->note("Komitee wurde hinzugefügt.");
            }
            else {
                $this->aditur->error("Das neue Komitee konnte nicht hinzugefügt werden. Bitte wende Dich an den Admin!");
            }
        }

        if (isset($_POST['delete_komitee_operation']) && !empty($_POST['komitee_id'])) {
            if ($_POST['komitee_id'] <= 3) {
                $this->aditur->error("Dieses Komitee darf nicht gelöscht werden!");

                return;
            }

            $query = $this->db->prepare("DELETE FROM aditur_komitees WHERE id=?");
            $query -> bind_param('i', $_POST['komitee_id']);
            if ($query -> execute()) {
                $this->aditur->note("Komitee wurde gelöscht.");
            }
            else {
                $this->aditur->error("Komitee konnte nicht gelöscht werden! Bitte wende Dich an den Admin!");
            }
        }

        // Profile Management
        if (isset($_POST['new_profile_operation'])
        && !empty($_POST['new_profile_forename'])
        && !empty($_POST['new_profile_lastname'])
        && !empty($_POST['new_profile_email'])
        && !empty($_POST['new_profile_privileges'])
        && !empty($_POST['new_profile_komitee'])) {
            // check privileges
            $privileges = "";

            switch (htmlspecialchars($_POST['new_profile_privileges'])) {
                case "administrator":
                    $privileges = "administrator";
                    break;

                case "president":
                    $privileges = "president";
                    break;

                case "speaker":
                    $privileges = "speaker";
                    break;

                case "normal":
                    $privileges = "normal";
                    break;

                default:
                    $this->aditur->error("Die angegebenen Rechte sind ungültig. Sie wurden auf normal gesetzt.");

                    $privileges = "normal";
            }


            // check komitee
            $query = $this->db->prepare("SELECT id FROM aditur_komitees WHERE id=?");
            $query -> bind_param('i', $_POST['new_profile_komitee']);
            $query -> execute();
            $query -> bind_result($komiteeId);
            $query -> store_result();

            if ($query -> num_rows !== 1) {
                $query -> close();

                $this->aditur->error("Das angegebene Komitee existiert nicht.");

                return;
            }

            $query -> fetch();
            $query -> close();


            // lowercase email
            $newEmail = trim(strtolower($_POST['new_profile_email']));
            $newForename = trim(htmlspecialchars($_POST['new_profile_forename']));
            $newLastname = trim(htmlspecialchars($_POST['new_profile_lastname']));


            // insert values
            $query = $this->db->prepare("INSERT INTO aditur_members (forename, lastname, email, privileges, komitee) VALUES (?, ?, ?, ?, ?)");
            $query -> bind_param('sssss', $newForename,
                             $newLastname,
                             $newEmail,
                             $privileges,
                             $komiteeId);
            if (!$query -> execute()) {
                $this->aditur->error("Profil konnte nicht hinzugefügt werden. Bitte gib dem Administrator Bescheid.");

                return;
            }

            $query -> close();


            // send password reset email
            $query = $this->db->prepare("SELECT id, forename, email FROM aditur_members WHERE email=?");
            $query -> bind_param('s', $_POST['new_profile_email']);
            $query -> execute();
            $query -> bind_result($id, $forename, $email);
            $query -> fetch();
            $query -> close();

            $hash = md5($forename . time());

            $query = "INSERT INTO aditur_password_reset (user_id, token) VALUES ($id, '$hash')";
            $this->db->query($query);

            $message = "<h1>Anmeldung im Aditur</h1><p><br><br>Um Dich im Aditur anmelden zu können, klick auf Diesen Link und erstelle ein Passwort!</p><a href=\"http://" . $this->aditur->config("domain") . "/resetPassword/" . $hash . "\">Passwort erstellen</a><p><br><br>Die Gültigkeit dieses Links endet in 48 Stunden. Danach klick einfach unter Login auf \"Passwort vergessen\", um Dein Passwort zu erstellen.</p>";

            if (!$this->aditur->sendEmail($email, "Aditur Registrierung", $message)) {
                $this->aditur->error("Die Registrierungsemail konnte nicht gesendet werden.");
                $this->aditur->note("Um ein Passwort zu setzen kann entweder die \"Passwort vergessen\"-Funktion genutzt werden oder folgender Link auf anderem Wege weitergegeben werden:<br><a href=\"http://" . $this->aditur->config("domain") . "/resetPassword/" . $hash . "\">" . $this->aditur->config("domain") . "/resetPassword/" . $hash . "</a>");
            }
        }

        if (!empty($_POST['profile_id'])) {
            $id = $_POST['profile_id'];

            if (isset($_POST['change_profile_name_operation']) && !empty($_POST['new_profile_forename']) && !empty($_POST['new_profile_lastname'])) {
                $query = $this->db->prepare("UPDATE aditur_members SET forename=?, lastname=? WHERE id=?");
                $query -> bind_param('ssi', trim(htmlspecialchars($_POST['new_profile_forename'])), trim(htmlspecialchars($_POST['new_profile_lastname'])), $id);
                if (!$query -> execute()) {
                    $this->aditur->error("Der Profilname konnte nicht geändert werden. Bitte wende Dich an den Admin.");

                    $query -> close();
                    return;
                }

                $query -> close();

                $this->aditur->note("Der Profilname wurde geändert.");
            }

            if (isset($_POST['change_profile_email_operation']) && !empty($_POST['new_profile_email'])) {
                $newEmail = strtolower($_POST['new_profile_email']);

                $query = $this->db->prepare("UPDATE aditur_members SET email=? WHERE id=?");
                $query -> bind_param('si', $newEmail, $id);
                if (!$query -> execute()) {
                    $this->aditur->error("Die E-Mail-Adresse konnte nicht geändert werden. Bitte wende Dich an den Admin.");

                    $query -> close();
                    return;
                }

                $query -> close();

                $this->aditur->note("E-Mail-Adresse wurde geändert.");
            }

            if (isset($_POST['change_profile_rights_operation']) && !empty($_POST['new_profile_privileges']) && !empty($_POST['new_profile_komitee'])) {
                // check privileges
                $privileges = "";

                switch (htmlspecialchars($_POST['new_profile_privileges'])) {
                    case "administrator":
                        $privileges = "administrator";
                        break;

                    case "president":
                        $privileges = "president";
                        break;

                    case "speaker":
                        $privileges = "speaker";
                        break;

                    case "normal":
                        $privileges = "normal";
                        break;

                    default:
                        $this->aditur->error("Die angegebenen Rechte sind ungültig. Sie wurden auf normal gesetzt.");

                        $privileges = "normal";
                }


                // check komitee
                $query = $this->db->prepare("SELECT id FROM aditur_komitees WHERE id=?");
                $query -> bind_param('i', $_POST['new_profile_komitee']);
                $query -> execute();
                $query -> bind_result($komiteeId);
                $query -> store_result();

                if ($query -> num_rows !== 1) {
                    $query -> close();

                    $this->aditur->error("Das angegebene Komitee existiert nicht.");

                    return;
                }

                $query -> fetch();
                $query -> close();


                // update values
                $query = $this->db->prepare("UPDATE aditur_members SET privileges=?, komitee=? WHERE id=?");
                $query -> bind_param('sii', $privileges, $komiteeId, $id);
                if (!$query -> execute()) {
                    $this->aditur->error("Die Rechte konnten nicht geändert werden. Bitte wende Dich an den Admin.");

                    $query -> close();
                    return;
                }

                $query -> close();

                $this->aditur->note("Rechte wurden geändert.");
            }

            if (isset($_POST['delete_profile_operation'])) {
                $query = $this->db->prepare("SELECT forename, lastname FROM aditur_members WHERE id=?");
                $query -> bind_param('i', $_POST['profile_id']);
                $query -> bind_result($forename, $lastname);
                $query -> execute();
                $query -> store_result();

                if ($query -> num_rows !== 1) {
                    $query -> close();

                    $this->aditur->error("Der zu löschende Benutzer existiert nicht!");

                    return;
                }

                $query -> fetch();
                $query -> close();


                $query = $this->db->prepare("DELETE FROM aditur_members WHERE id=?");

                $query -> bind_param('i', $_POST['profile_id']);

                if (!$query -> execute()) {
                    $query -> close();

                    $this->aditur->error("Der zu löschende Benutzer konnte nicht gelöscht werden! Bitte wende Dich an den Admin.");

                    return;
                }

                $query -> close();

                $this->aditur->note("Der Benutzer \"$forename $lastname\" (ID: " . $_POST['profile_id'] . ") wurde gelöscht.");
            }
        }
    }


    public function index() {
        ?>

        <a class="Setting Box" href="<?php echo $this->aditur->path("/settings/profiles"); ?>">
            <div>
                <h3><i class="fa fa-lg fa-users"></i>&nbsp;Benutzerverwaltung</h3>
            </div>
        </a>

        <?php
    }
}

?>
