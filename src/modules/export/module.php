<?php
// Module: Export


ini_set("memory_limit", "1024M");
ini_set("max_execution_time", 300);


class Export extends Module {
    public function fromUrl($argv) {
        if (!isset($_POST['download_export'])) {
            ob_end_clean();

            header('HTTP/1.0 403 Forbidden');

            echo "No access.";

            exit;
        }


        $this->aditur->aditur_include("lib/phpodt/phpodt.php");

        // parse Parameters

        // if pictures shoudl be included
        $embedPictures = false;
        if (isset($_POST['embed_pictures'])) {
            $embedPictures = true;
        }

        // if censored comments should be included as black bars.
        $blackBars = false;

        if (isset($_POST['black_bars'])) {
            $blackBars = true;
        }


        // create document
        $odt = ODT::getInstance();

        // write Heading(s)
        $headingPStyle = new ParagraphStyle("HeadingPStyle1");
        $headingPStyle -> setTextAlign(StyleConstants::CENTER);
        $headingPStyle -> setVerticalMargin("0.65cm", "0.65cm");

        $heading = new Paragraph($headingPStyle);

        $headingStyle = new TextStyle("HeadingStyle1");
        $headingStyle -> setFontSize("23pt");
        $headingStyle -> setBold();

        $heading -> addText("Kommentar-Export", $headingStyle);

        // write Stats
        $statisticsPStyle = new ParagraphStyle("StatisticsPStyle1");
        $statisticsPStyle -> setBreakAfter(StyleConstants::PAGE);

        $statistics = new Paragraph($statisticsPStyle);

        $statistics -> addText($this->aditur->config("domain"));
        $statistics -> addLineBreak();
        $statistics -> addText("Erstellt am: " . date("d.m.y"));
        $statistics -> addLineBreak();

        $statistics -> addLineBreak();


        // add parameters
        if ($embedPictures) {
            $statistics -> addText("Bilder wurden eingebettet.");
        }
        else {
            $statistics -> addText("Bilder wurden nicht eingebettet.");
        }

        $statistics -> addLineBreak();

        if ($blackBars) {
            $statistics -> addText("Zensierte Kommentare wurden als Schwarze Balken hinzugefügt.");
        }
        else {
            $statistics -> addText("Zensierte Kommentare wurden ausgelassen.");
        }

        $statistics -> addLineBreak();


        $p = new Paragraph();

        $pStyle = new ParagraphStyle("ParagraphStyle1");
        $pStyle -> setBreakAfter(StyleConstants::PAGE);


        // write Index
        $heading2PStyle = new ParagraphStyle("HeadingPStyle2");
        $heading2PStyle -> setTextAlign(StyleConstants::CENTER);

        $heading = new Paragraph($heading2PStyle);

        $heading2Style = new TextStyle("HeadingStyle2");
        $heading2Style -> setFontSize("18pt");
        $heading2Style -> setBold();

        $heading -> addText("Profile", $heading2Style);

        $questionStyle = new TextStyle("QuestionStyle");
        $questionStyle -> setBold();


        // write all Profiles
        $query = $this->db->query("SELECT id, forename, lastname FROM aditur_members ORDER BY forename");

        $profiles = array();
        while ($object = $query -> fetch_object()) {
            $profiles[] = $object;
        }

        $query -> close();


        $index = new ODTList();
        $nProfile = 0;
        foreach ($profiles as $profile) {
            $element = ODT::getInstance() -> getDocumentContent() -> createElement('text:list-item');

            $p = new Paragraph(null, false);
            $p -> addBookmarkRef("profileBookmark" . $nProfile, $profile -> forename . " " . $profile -> lastname);

            $element -> appendChild($p -> getDOMElement());
            $index -> getDOMElement() -> appendChild($element);

            $nProfile++;
        }


        $p = new Paragraph();
        $p -> addText("Insgesamt " . count($profiles) . " Profile.");
        $p -> addLineBreak();
        $p -> addLineBreak();
        $p -> addLineBreak();



        // repeat with Teachers
        $query = $this->db->query("SELECT id, title, name FROM aditur_teachers ORDER BY name");

        $teachers = array();
        while ($object = $query -> fetch_object()) {
            $teachers[] = $object;
        }

        $query -> close();


        $index = new ODTList();
        $nTeacher = 0;
        foreach ($teachers as $teacher) {
            $element = ODT::getInstance() -> getDocumentContent() -> createElement('text:list-item');

            $p = new Paragraph(null, false);
            $p -> addBookmarkRef("teacherBookmark" . $nTeacher, $teacher -> name);

            $element -> appendChild($p -> getDOMElement());
            $index -> getDOMElement() -> appendChild($element);

            $nTeacher++;
        }


        $p = new Paragraph();
        $p -> addText("Insgesamt " . count($teachers) . " Lehrer.");
        $p -> addLineBreak();
        $p -> addLineBreak();
        $p -> addLineBreak();


        // Index Entry Citations
        $heading2 = new Paragraph($headingPStyle);

        $heading2 -> addText("Zitate", $heading2Style);

        $p = new Paragraph($pStyle);
        $p -> addBookmarkRef("citationsBookmark", "Zu den Zitaten");

        // Index Entry Boxes
        $heading3 = new Paragraph($headingPStyle);

        $heading3 -> addText("Boxen", $heading2Style);

        $p = new Paragraph($pStyle);
        $p -> addBookmarkRef("boxesBookmark", "Zu den Boxen");



        $heading4 = new Paragraph($headingPStyle);

        $heading4 -> addText("Umfrage", $heading2Style);

        $p = new Paragraph($pStyle);
        $p -> addBookmarkRef("surveysBookmark", "Zu den Umfragen");


        // write Profiles in Detail

        // get personal questions
        $query = $this->db->query("SELECT id, text FROM aditur_me_questions");

        $questions = array();
        while ($object = $query -> fetch_object()) {
            $questions[] = $object;
        }

        $query -> close();


        // style settings for profiles in Detail
        $separator = " • ";

        $profileHeadingPStyle = new ParagraphStyle("profileHeadingPStyle");
        $profileHeadingPStyle -> setTextAlign(StyleConstants::CENTER);

        $profileHeadingStyle = new TextStyle("profileHeadingStyle");
        $profileHeadingStyle -> setFontSize("18pt");
        $profileHeadingStyle -> setBold();


        $commentsPStyle = new ParagraphStyle("commentsPStyle");
        $commentsPStyle -> setTextAlign(StyleConstants::JUSTIFY);

        $censoredStyle = new TextStyle("censoredStyle");
        $censoredStyle -> setTextBackgroundColor("#000000");

        $nProfile = 0;

        // write every profile
        foreach ($profiles as $profile) {
            $profileHeading = new Paragraph($profileHeadingPStyle);
            $profileHeading -> addBookmark("profileBookmark" . $nProfile);
            $profileHeading -> addText($profile->forename . " " . $profile->lastname, $profileHeadingStyle);


            // chosen Picture
            $p = new Paragraph();
            $p -> addText("Gewünschtes Bild:");
            $p -> addLineBreak();

            $query = $this->db->query("SELECT picture_name FROM aditur_chosen_picture WHERE owner_id=" . $profile -> id);

            if ($object = $query -> fetch_object()) {
                $path = $this->aditur->path("data/paper/raw/" . $profile->id . "/" . $object->picture_name);

                if (file_exists($path)) {
                    $image = $this->aditur->imagecreatefromfile($path);
                    $height = imagesy($image) / imagesx($image) * 17;

                    $p -> addText($path);

                    if ($embedPictures) {
                        $p -> addImage($path, "17cm", $height . "cm");
                    }
                }
                else {
                    $p -> addText("Kein Bild ausgewählt.");
                }
            }
            else {
                $p -> addText("Kein Bild ausgewählt.");
            }

            $p -> addLineBreak();


            // Baby Picture
            $p = new Paragraph();
            $p -> addText("Kinderbild:");
            $p -> addLineBreak();


            $path = $this->aditur->path("data/babyPics/" . $profile->id . ".jpg");

            if (file_exists($path)) {
                $image = $this->aditur->imagecreatefromfile($path);
                $height = imagesy($image) / imagesx($image) * 17;

                $p -> addText($path);

                if ($embedPictures) {
                    $p -> addImage($path, "17cm", $height . "cm");
                }

            }
            else {
                $p -> addText("Kein Kinderbild hochgeladen.");
            }

            $p -> addLineBreak();


            // personal Questions
            $p = new Paragraph();

            foreach ($questions as $question) {
                $p -> addText($question->text, $questionStyle);
                $p -> addLineBreak();

                $query = $this->db->query("SELECT text FROM aditur_me_answers WHERE question_id=" . $question->id . " AND author_id=" . $profile->id . " ORDER BY id DESC LIMIT 1");

                if ($object = $query -> fetch_object()) {
                    $p -> addText($object -> text);
                }

                $query -> close();

                $p -> addLineBreak();
                $p -> addLineBreak();
            }


            // Comments to this person
            $commentsHeading = new Paragraph($heading2PStyle);
            $commentsHeading -> addText("Kommentare", $heading2Style);

            $p = new Paragraph($commentsPStyle);

            if ($blackBars) {
                $query = $this->db->query("SELECT comment, censored FROM aditur_comments WHERE comment_on_id=" . $profile->id . " AND precensored=0");

            }
            else {
                $query = $this->db->query("SELECT comment, censored FROM aditur_comments WHERE comment_on_id=" . $profile->id . " AND precensored=0 AND censored=0");
            }

            $comments = array();
            while ($object = $query -> fetch_object()) {
                $comments[] = $object;
            }

            $first = true;

            foreach ($comments as $comment) {
                // add Separator
                if ($first) {
                    $first = false;
                }
                else {
                    $p -> addText($separator);
                }

                if ($comment -> censored) {
                    $p -> addText(trim($comment -> comment), $censoredStyle);
                }
                else {
                    $p -> addText(trim($comment -> comment));
                }
            }


            $p = new Paragraph($pStyle);
            $p -> addLineBreak();
            $p -> addText("Insgesamt " . count($comments) . " Kommentare.");


            // Comments from teachers to this person
            $commentsHeading = new Paragraph($heading2PStyle);
            $commentsHeading -> addText("Kommentare von Lehrern", $heading2Style);

            $p = new Paragraph($commentsPStyle);

            $query = $this->db->query("SELECT comment, censored FROM aditur_comments_from_teachers WHERE comment_on_id=" . $profile->id . " AND censored=0");

            $comments = array();
            while ($object = $query -> fetch_object()) {
                $comments[] = $object;
            }

            $first = true;

            foreach ($comments as $comment) {
                // add Separator
                if ($first) {
                    $first = false;
                }
                else {
                    $p -> addText($separator);
                }

                $p -> addText(trim($comment -> comment));
            }


            $p = new Paragraph($pStyle);
            $p -> addLineBreak();
            $p -> addText("Insgesamt " . count($comments) . " Kommentare.");



            $nProfile++;
        }


        // same with Teachers in Detail
        $nTeacher = 0;

        foreach ($teachers as $teacher) {
            $profileHeading = new Paragraph($profileHeadingPStyle);
            $profileHeading -> addBookmark("teacherBookmark" . $nTeacher);
            $profileHeading -> addText($teacher->title . " " . $teacher->name, $profileHeadingStyle);


            // Picture
            $p = new Paragraph();
            $p -> addText("Bild:");
            $p -> addLineBreak();

            $path = $this->aditur->path("data/teachers/raw/" . $teacher->id . ".jpg");

            if (file_exists($path)) {
                $image = $this->aditur->imagecreatefromfile($path);
                $height = imagesy($image) / imagesx($image) * 17;

                $p -> addText($path);

                if ($embedPictures)  {
                    $p -> addImage($path, "17cm", $height . "cm");
                }

            }
            else {
                $p -> addText("Kein Bild vorhanden.");
            }

            $p -> addLineBreak();


            // Comments
            $commentsHeading = new Paragraph($heading2PStyle);
            $commentsHeading -> addText("Kommentare", $heading2Style);

            $p = new Paragraph($commentsPStyle);

            if ($blackBars) {
                $query = $this->db->query("SELECT comment, censored FROM aditur_teachers_comments WHERE comment_on_id=" . $teacher->id . " AND precensored=0");

            }
            else {
                $query = $this->db->query("SELECT comment, censored FROM aditur_teachers_comments WHERE comment_on_id=" . $teacher->id . " AND precensored=0 AND censored=0");

            }

            $comments = array();
            while ($object = $query -> fetch_object()) {
                $comments[] = $object;
            }

            $first = true;
            foreach ($comments as $comment) {
                // add Separator
                if ($first) {
                    $first = false;
                }
                else {
                    $p -> addText($separator);
                }

                if ($comment -> censored) {
                    $p -> addText(trim($comment -> comment), $censoredStyle);
                }
                else {
                    $p -> addText(trim($comment -> comment));
                }
            }


            $p = new Paragraph($pStyle);
            $p -> addLineBreak();
            $p -> addText("Insgesamt " . count($comments) . " Kommentare.");


            $nTeacher++;
        }


        // create a section where all censored comments are listed

        // censored by komitee
        $commentsHeading = new Paragraph($heading2PStyle);
        $commentsHeading -> addText("Kommentare, zensiert vom Komitee", $heading2Style);


        $query = $this -> db -> query("SELECT id, comment FROM aditur_comments WHERE precensored=1");

        $comments = array();

        while ($object = $query -> fetch_object()) {
            $comments[] = $object;
        }

        $query -> close();


        $query = $this -> db -> query("SELECT id, comment FROM aditur_teachers_comments WHERE precensored=1");

        while ($object = $query -> fetch_object()) {
            $comments[] = $object;
        }

        $query -> close();


        $query = $this -> db -> query("SELECT id, comment FROM aditur_comments_from_teachers WHERE censored=1");

        while ($object = $query -> fetch_object()) {
            $comments[] = $object;
        }


        // stats
        $p = new Paragraph();

        $p -> addText("Vom Komitee wurden " . count($comments) . " Kommentare zensiert.");


        $p = new Paragraph($commentsPStyle);

        $first = true;

        foreach ($comments as $comment) {
            // add Separator
            if ($first) {
                $first = false;
            }
            else {
                $p -> addText($separator);
            }

            $p -> addText(trim($comment -> comment));
        }


        // censored by members
        $commentsHeading = new Paragraph($heading2PStyle);
        $commentsHeading -> addText("Kommentare, zensiert von den Schülern", $heading2Style);

        $query = $this -> db -> query("SELECT id, comment FROM aditur_comments WHERE censored=1");

        $comments = array();

        while ($object = $query -> fetch_object()) {
            $comments[] = $object;
        }


        // stats
        $p = new Paragraph();

        $p -> addText("Von den Schülern wurden " . count($comments) . " Kommentare zensiert.");


        $p = new Paragraph($commentsPStyle);

        $first = true;

        foreach ($comments as $comment) {
            // add Separator
            if ($first) {
                $first = false;
            }
            else {
                $p -> addText($separator);
            }

            $p -> addText(trim($comment -> comment));
        }


        // censored by teachers
        $commentsHeading = new Paragraph($heading2PStyle);
        $commentsHeading -> addText("Kommentare, zensiert von den Lehrern", $heading2Style);

        $query = $this -> db -> query("SELECT id, comment FROM aditur_teachers_comments WHERE censored=1");

        $comments = array();

        while ($object = $query -> fetch_object()) {
            $comments[] = $object;
        }

        // stats
        $p = new Paragraph();

        $p -> addText("Von den Lehrern wurden " . count($comments) . " Kommentare zensiert.");


        $p = new Paragraph($commentsPStyle);

        $first = true;

        foreach ($comments as $comment) {
            // add Separator
            if ($first) {
                $first = false;
            }
            else {
                $p -> addText($separator);
            }

            $p -> addText(trim($comment -> comment));
        }


        // finished comments.



        // Citations
        $citatorStyle = new TextStyle("CitatorStyle");
        $citatorStyle -> setBold();

        $p = new Paragraph($headingPStyle);
        $p -> addText("Zitate", $headingStyle);
        $p -> addBookmark("citationsBookmark");


        $query = $this->db->query("SELECT citator, citation FROM aditur_citations");

        $nCitations = 0;
        while ($object = $query -> fetch_object()) {
            $p = new Paragraph();

            $p -> addLineBreak();
            $p -> addText($object -> citator);
            $p -> addLineBreak();
            $p -> addText(trim($object -> citation));


            $nCitations++;
        }

        $p = new Paragraph($pStyle);


        // Boxes
        $p = new Paragraph($headingPStyle);
        $p -> addText("Boxen", $headingStyle);
        $p -> addBookmark("boxesBookmark");

        $query = $this->db->query("SELECT id, name FROM aditur_boxes");

        $boxes = array();

        while ($object = $query -> fetch_object()) {
            $boxes[] = $object;
        }

        if (empty($boxes)) {
            $p = new Paragraph();

            $p -> addText("Keine Boxen angelegt.");
        }
        else {
            foreach ($boxes as $box) {
                $p = new Paragraph($heading2PStyle);
                $p -> addText($box->name, $headingStyle);


                $query = $this->db->query("SELECT text FROM aditur_box_entries WHERE box_id=" . $box->id);

                $entries = array();
                while ($object = $query -> fetch_object()) {
                    $entries[] = $object;
                }

                $p = new Paragraph();
                if (empty($entries)) {
                    $p = new Paragraph();
                    $p -> addText("Diese Box ist leer.");
                }
                else {
                    $first = true;
                    foreach ($entries as $entry) {
                        // add Separator
                        if ($first) {
                            $first = false;
                        }
                        else {
                            $p -> addText($separator);
                        }

                        $p -> addText(trim($entry -> text));
                    }
                }

                $p -> addLineBreak();
            }
        }


        // Surveys
        $p = new Paragraph($headingPStyle);
        $p -> addText("Umfragen", $headingStyle);
        $p -> addBookmark("surveysBookmark");


        // get all surveys
        $query = "SELECT id, question, komitee, closed, category, dropdown FROM aditur_surveys WHERE category='Abizeitung' ORDER BY id";
        $result = $this->db->query($query);

        $surveys = array();

        while ($row = $result->fetch_assoc()) {
            $surveys[] = $row;
        }

        if (empty($surveys)) {
            $p = new Paragraph();

            $p -> addText("Keine Boxen angelegt.");
        }
        else {
            foreach ($surveys as $survey) {
                $p = new Paragraph($heading2PStyle);
                $p -> addText($survey['question'], $headingStyle);


                // Select all answers
                $query = "SELECT id, answer FROM aditur_surveys_answers WHERE survey_id=" . $survey['id'];
                $result = $this->db->query($query);

                $answers = array();
                while ($row = $result -> fetch_assoc()) {
                    $answers[] = $row;
                }

                $votes = array();

                // get votes for each answer
                foreach ($answers as $answer) {
                    $query = "SELECT COUNT(id) FROM aditur_surveys_votes WHERE survey_id=" . $survey['id'] . " AND answer_id=" . $answer['id'];
                    $result = $this -> db -> query($query);
                    $count  = $result -> fetch_row()[0];
                    $countAll += $count;

                    $votes[] = array('count' => $count, 'answer' => $answer['answer']);
                }


                // sort answers descending depending on how many votes they have
                $counts = array();
                $answers = array();

                foreach ($votes as $key => $row) {
                    $counts[$key] = $row['count'];
                    $answers[$key] = $row['answer'];
                }

                array_multisort($counts, SORT_DESC, $answers, SORT_ASC, $votes);


                // create a table
                $table = new Table("table" . $survey['id']);
                $table -> createColumns(2);

                $tableRows = array();

                foreach ($votes as $key => $row) {
                    // skip answers with 0 votes
                    if ($row['count'] == 0) {
                        continue;
                    }

                    $tableRows[] = array($row['count'], $row['answer']);
                }

                $table -> addRows($tableRows);
            }
        }



        // Finished document creation.


        // Now create a real odt file...
        $file = $this->aditur->path("data/downloads/Export-" . time() . ".odt");

        $odt -> output($file);

        ob_end_clean();


        // ...add to downloads...
        $q = $this -> db -> query("INSERT INTO aditur_downloads (link, path, privileges, komitee, directory) VALUES ('Export-" . time() . ".odt', '" . $file . "', 'speaker', 3, 'Abizeitung Exportdaten')");


        // ...and initiate download.
        header('Content-Description: File Transfer');
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . basename($file) . '"');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));

        readfile($file);

        exit;
    }
}

?>
