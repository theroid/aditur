<?php
// Module: Citations


class Citations extends Module {
    public function fromUrl($argv) {
        // show submitForm
        $this->printSubmitForm();

        // submitted citations
        $this->printYourCitations();
    }


    protected function op() {
        if (isset($_POST['citation_operation'])) {
            if ($_POST['citation_operation'] == "submit") {
                $citation = htmlspecialchars($_POST['new_citation']);
                $citator  = htmlspecialchars($_POST['new_citator']);

                $query = $this->db->prepare("INSERT INTO aditur_citations (author_id, citator, citation)
                    VALUES (?, ?, ?)");
                $query -> bind_param('iss', $_SESSION['id'], $citator, $citation);
                if (!$query -> execute()) {
                    $this->aditur->error("Dein Zitat konnte leider nicht gespeichert werden. Bitte wende Dich an den Admin!");
                    $this->aditur->log($this -> db -> error);
                }

                $query -> close();

                $this->aditur->note("Dein Zitat wurde gespeichert.");
            }
            else {
                // get author_id
                $id = $_POST['citation_id'];

                $query = $this->db->prepare("SELECT author_id FROM aditur_citations WHERE id=?");
                $query -> bind_param('i', $id);
                $query -> bind_result($author_id);

                if (!$query -> execute()) {
                    $this->aditur->error("Dein Zitat konnte leider nicht bearbeitet werden. Bitte wende Dich an den Admin!");
                    $this->aditur->log($this -> db -> error);
                    $query -> close();
                    return;
                }
                $query -> fetch();
                $query -> close();

                // check if it is the author or a privileged user
                if ($_SESSION['id'] == $author_id) {

                    // perform changes
                    if ($_POST['citation_operation'] == "delete") {
                        $query = $this->db->prepare("DELETE FROM aditur_citations WHERE id=?");
                        $query -> bind_param('i', $id);
                    }
                    else if ($_POST['citation_operation'] == "edit") {
                        $citation = htmlspecialchars($_POST['new_citation']);

                        $query = $this->db->prepare("UPDATE aditur_citations SET citation=? WHERE id=?");
                        $query -> bind_param('si', $citation, $id);
                    }
                    else {
                        $query -> close();
                        return;
                    }
                    if (!$query -> execute()) {
                        $this->aditur->log($this -> db -> error);
                    }

                    $query -> close();

                    // $this->aditur->note("Schon geschehen!");
                }
            }
        }
    }




    public function printYourCitations() {
        // show your Citations
        $query = "SELECT id, citator, citation FROM aditur_citations WHERE author_id=" . $_SESSION['id'] . " ORDER BY citator";

        $result = $this->db->query($query);

        $nCitations = 0;
?>
<div class="Citations">

    <h2>Deine Zitate</h2>

    <p><?php $this->printStatistics(); ?></p>

    <?php
        while ($citation = $result -> fetch_object()) {
            $nCitations++;

        ?>
    <div class="Cite">
        <div class="Citator">
            <div class="ImageSizer">
               <?php // TODO implement sick thumbnail algorithm ?>
                <img alt="" src="<?php echo $this->aditur->path("/media/profilePics/0.jpg"); ?>">
            </div>
            <p><?php echo $citation->citator; ?></p>
        </div>

        <div class="Citation">
            <input type="checkbox" class="Expander BlackBoxExpander" id="blackBoxExpander<?php echo $nCitations; ?>">
            <label class="Citation" for="blackBoxExpander<?php echo $nCitations; ?>"><?php echo $citation->citation; ?></label>

            <form class="ChangeCitation" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input value="<?php echo $citation->id; ?>" name="citation_id" type="hidden">

                <textarea name="new_citation" placeholder="Verfasse einen schönen Kommentar!"><?php echo $citation->citation; ?></textarea>
                            <button class="ButtonEdit" type="submit" value="edit" name="citation_operation"><i class="fa fa-lg fa-pencil"></i>&nbsp; Bearbeiten</button>
                            <label class="Button" for="blackBoxExpander<?php echo $nCitations; ?>"><i class="fa fa-lg fa-remove"></i>&nbsp; Abbrechen</label>
            </form>

            <form class="DeleteCitation" action="<?php echo $this->aditur->url(); ?>" method="post">
                <input value="<?php echo $citation->id; ?>" name="citation_id" type="hidden">
                <button class="ButtonDelete" type="submit" value="delete" name="citation_operation">
                <i class="fa fa-lg fa-eraser"></i>&nbsp;Löschen
                </button>
            </form>
        </div>
    </div>
        <?php
        }

        if (0 == $nCitations) {
    ?>
    <p>Noch keine Zitate verfasst.</p>
    <?php
        }
    ?>
</div>
<?php
    }




    public function printStatistics() {
        $query = "SELECT count(id) FROM aditur_citations WHERE author_id=" . $_SESSION['id'];

        $result = $this->db->query($query);
        ?>Du hast bisher <?php echo $result->fetch_row()[0]; ?> Zitate verfasst.<?php
    }



    public function printSubmitForm() {
        $query = $this->db->query("SELECT enabled FROM aditur_modules WHERE name='citations'");

        $enabled = $query -> fetch_object() -> enabled;

        if (!$enabled) {
            return;
        }


        $this->op();

        ?>
        <div class="Box">
            <h2>Zitat einsenden</h2>

            <form class="SubmitForm SubmitCitationForm" action="<?php echo $this->aditur->url(); ?>" method="post" autocomplete="off">
                <input type="text" name="new_citator" placeholder="Person(en)" autocomplete="off" >
                <textarea name="new_citation" placeholder="Zitat oder Dialog" required></textarea>
                <button type="submit" name="citation_operation" value="submit"><i class="fa fa-lg fa-check"></i>&nbsp; Senden</button>
            </form>
        </div>

        <?php
    }
}



?>
