<?php
// Module: Countdown

// TODO make dummy countdown real!
class Countdown extends Module {
    public function fromUrl($argv) {
        $this->aditur->loadModule("headquarter", "fromUrl");
        return;
    }


    public function display($argv) {
        // get date to display
        if (empty($argv[0])) {
            $query = "SELECT timestamp FROM aditur_events ORDER BY timestamp DESC LIMIT 1";
        }
        else {
            $query = "SELECT timestamp FROM aditur_events WHERE id=" . $argv[0];
        }


        if (!$result = $this->db->query($query)) {
            $this->aditur->log("Countdown: could not load event!");

            return;
        }

        $object = $result->fetch_object();

        // calculate time difference from now until event
        $seconds = strtotime($object->timestamp) - time();

        if ($seconds < 0) {
            $this->aditur->log("Countdown: Event passed.");
            return;
        }

        // extract days
        $days = floor($seconds / (60 * 60 * 24));

        // extract hours
        $divisor_for_hours = $seconds % (60 * 60 * 24);
        $hours = floor($divisor_for_hours / (60 * 60));

        // extract minutes
        $divisor_for_minutes = $seconds % (60 * 60);
        $minutes = floor($divisor_for_minutes / 60);

        // extract the remaining seconds
        $divisor_for_seconds = $divisor_for_minutes % 60;
        $seconds = ceil($divisor_for_seconds);
        ?>

<section class="Countdown fullWidthBar afterWhite">
    <!-- really nice font by pizzadude.dk -->
    <p><?php
        // echo days
        ?><span id="days"><?php
        if ($days > 9) {
            echo $days;
        }
        else {
            echo "0".$days;
        }
        ?></span><span class="Colon">:</span><?php

        // echo hours
        ?><span id="hours"><?php
        if ($hours > 9) {
            echo $hours;
        }
        else {
            echo "0".$hours;
        }
        ?></span><span class="Colon">:</span><span id="minutes"><?php

        // echo minutes
        if ($minutes > 9) {
            echo $minutes;
        }
        else {
            echo "0".$minutes;
        }
        ?></span><span class="Colon">:</span><span id="seconds"><?php

        // echo minutes
        if ($seconds > 9) {
            echo $seconds;
        }
        else {
            echo "0".$seconds;
        }
        ?></span></p>
</section>

<!-- countdown script -->
<script>
    //elements of Countdown
    var days    = document.getElementById("days"),
        hours   = document.getElementById("hours"),
        minutes = document.getElementById("minutes"),
        seconds = document.getElementById("seconds"),

        t,

        colon = true,
        colons = document.getElementsByClassName("Colon"),
        j = 0,
        tock = false;

    function decreaseCountdown() {
        "use strict";

        if (seconds.innerHTML == 0) {
            if (minutes.innerHTML == 0) {
                if (hours.innerHTML == 0) {
                    if (days.innerHTML == 0) {
                        zero();
                    }
                    else {
                        t = days.innerHTML - 1;
                        if (t < 10) {
                            days.innerHTML = "0" + t;
                        }
                        else {
                            days.innerHTML = t;
                        }

                        hours.innerHTML = "23";
                        minutes.innerHTML = "59";
                        seconds.innerHTML = "59";
                    }
                }
                else {
                    t = hours.innerHTML - 1;
                    if (t < 10) {
                        hours.innerHTML = "0" + t;
                    }
                    else {
                        hours.innerHTML = t;
                    }

                    minutes.innerHTML = "59";
                    seconds.innerHTML = "59";
                }
            }
            else {
                t = minutes.innerHTML - 1;
                if (t < 10) {
                    minutes.innerHTML = "0" + t;
                }
                else {
                    minutes.innerHTML = t;
                }

                seconds.innerHTML = "59";
            }
        }
        else {
            t = seconds.innerHTML - 1;
            if (t < 10) {
                seconds.innerHTML = "0" + t;
            }
            else {
                seconds.innerHTML = t;
            }
        }
    }


    /*
                    function tick() {
                        //flashing colons
                        if (j < 4) {
                            if (colon) {
                                //set colons invisible...
                                for (var i = 0; i < colons.length; i++) {
                                    colons[i].style.visibility = 'hidden';
                                }
                                colon = false;
                            } else {
                                //...and visible again
                                for (var i = 0; i < colons.length; i++) {
                                    colons[i].style.visibility = 'visible';
                                }
                                colon = true;
                            }

                            j++;
                        }

                        if (tock) {
                            tock = false;

                            decreaseCountdown();
                        }
                        else {
                            tock = true;
                        }
                    }
                    */


    function zero() {
        "use strict";

        //when countdown reaches 00:00:00:00
        location.reload();
    }

    //window.setInterval(function() { tick(); }, 500);
    window.setInterval(function() { decreaseCountdown(); }, 1000);
</script>

       <?php
    }
}


?>
