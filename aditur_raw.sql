-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 23. Apr 2020 um 01:23
-- Server-Version: 10.4.11-MariaDB
-- PHP-Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `aditur_raw`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_announcements`
--

CREATE TABLE `aditur_announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `content` text NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_boxes`
--

CREATE TABLE `aditur_boxes` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `creator_id` int(11) NOT NULL,
  `secret` tinyint(1) NOT NULL,
  `closed` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_box_entries`
--

CREATE TABLE `aditur_box_entries` (
  `id` int(11) NOT NULL,
  `box_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_chosen_picture`
--

CREATE TABLE `aditur_chosen_picture` (
  `id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `picture_name` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_citations`
--

CREATE TABLE `aditur_citations` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `citator` text COLLATE latin1_german1_ci NOT NULL,
  `citation` text COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_comments`
--

CREATE TABLE `aditur_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment_on_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `precensored` tinyint(1) NOT NULL,
  `censored` tinyint(1) NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_comments_from_teachers`
--

CREATE TABLE `aditur_comments_from_teachers` (
  `id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `comment_on_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `censored` tinyint(1) NOT NULL,
  `reason` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_commercials`
--

CREATE TABLE `aditur_commercials` (
  `id` int(11) NOT NULL,
  `advertiser` tinytext COLLATE latin1_german1_ci NOT NULL,
  `responsible` int(11) NOT NULL,
  `asked` varchar(3) COLLATE latin1_german1_ci NOT NULL DEFAULT 'no',
  `commitment` varchar(3) COLLATE latin1_german1_ci NOT NULL DEFAULT 'no',
  `payed` varchar(3) COLLATE latin1_german1_ci NOT NULL DEFAULT 'no',
  `ad_in` varchar(3) COLLATE latin1_german1_ci NOT NULL DEFAULT 'no',
  `copy` varchar(3) COLLATE latin1_german1_ci NOT NULL DEFAULT 'no',
  `format` varchar(12) COLLATE latin1_german1_ci NOT NULL DEFAULT 'keins'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_config`
--

CREATE TABLE `aditur_config` (
  `id` int(11) NOT NULL,
  `config_key` varchar(128) NOT NULL,
  `value` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aditur_config`
--

INSERT INTO `aditur_config` (`id`, `config_key`, `value`) VALUES
(1, 'PEPPER', ''),
(2, 'COST', ''),
(3, 'title', 'Aditur'),
(4, 'domain', ''),
(5, 'admin_email_address', ''),
(6, 'priceOfA3', '380'),
(7, 'priceOfA4', '200'),
(8, 'priceOfA5', '120'),
(9, 'priceOfA6', '65'),
(10, 'impressum_data', '<h3>Angaben gemäß § 5 TMG</h3>'),
(11, 'babyPicsEnabled', '1'),
(12, 'choosePaperPhotoEnabled', '0'),
(13, 'censorship_komitee_enabled', '0'),
(14, 'censorship_students_enabled', '0'),
(15, 'censorship_teachers_enabled', '0'),
(16, 'comments_from_teachers_enabled', '0'),
(17, 'profileQuestionsEnabled', '1'),
(18, 'upload_images_enabled', '1'),
(19, 'error_reporting', '0');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_downloads`
--

CREATE TABLE `aditur_downloads` (
  `id` int(11) NOT NULL,
  `link` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `path` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `directory` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `privileges` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `komitee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_events`
--

CREATE TABLE `aditur_events` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_finances`
--

CREATE TABLE `aditur_finances` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `value` double NOT NULL,
  `destination` tinyint(1) NOT NULL,
  `balance` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_ideas`
--

CREATE TABLE `aditur_ideas` (
  `id` int(11) NOT NULL,
  `idea` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_komitees`
--

CREATE TABLE `aditur_komitees` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aditur_komitees`
--

INSERT INTO `aditur_komitees` (`id`, `name`) VALUES
(1, '/'),
(2, 'Finanzen'),
(3, 'Abizeitung'),
(4, 'Abiball'),
(5, 'Abigag'),
(6, 'Jahrgangskonzert'),
(7, 'Sondereinnahmen'),
(8, 'Halle'),
(9, 'Kreativ');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_login_attempts`
--

CREATE TABLE `aditur_login_attempts` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_members`
--

CREATE TABLE `aditur_members` (
  `id` int(10) UNSIGNED NOT NULL,
  `forename` varchar(256) COLLATE utf8_bin NOT NULL,
  `lastname` varchar(256) COLLATE utf8_bin NOT NULL,
  `email` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_cs NOT NULL,
  `password` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `privileges` varchar(256) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `komitee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_me_answers`
--

CREATE TABLE `aditur_me_answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_me_questions`
--

CREATE TABLE `aditur_me_questions` (
  `id` int(11) NOT NULL,
  `text` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_modules`
--

CREATE TABLE `aditur_modules` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `link` varchar(128) NOT NULL,
  `description` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `in_header` tinyint(1) NOT NULL,
  `in_menu` tinyint(1) NOT NULL,
  `can_be_disabled` tinyint(1) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `needs_login` tinyint(1) NOT NULL,
  `html` tinyint(1) NOT NULL,
  `frame` tinyint(1) NOT NULL,
  `privileges` varchar(128) NOT NULL,
  `komitee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aditur_modules`
--

INSERT INTO `aditur_modules` (`id`, `name`, `link`, `description`, `icon`, `in_header`, `in_menu`, `can_be_disabled`, `enabled`, `needs_login`, `html`, `frame`, `privileges`, `komitee`) VALUES
(1, 'home', '', 'Home', 'home', 1, 0, 0, 1, 0, 1, 0, '', 0),
(2, 'headquarter', 'headquarter', 'Headquarter', 'university', 1, 0, 0, 1, 1, 1, 1, '', 0),
(3, 'me', 'me', 'Mein Profil', 'user-circle-o', 1, 0, 0, 1, 1, 1, 1, '', 0),
(4, 'logout', '?logout', 'Abmelden', 'power-off', 1, 0, 0, 1, 1, 1, 0, '', 0),
(5, 'profiles', 'profiles', 'Profile', 'users', 0, 1, 1, 1, 1, 1, 1, '', 0),
(6, 'comments', 'comments', 'Kommentare', 'comment', 0, 1, 1, 1, 1, 1, 1, '', 0),
(7, 'censorship', 'censorship', 'Zensieren', 'eye-slash', 0, 1, 1, 0, 1, 1, 1, '', 0),
(8, 'teachers', 'teachers', 'Lehrer', 'graduation-cap', 0, 1, 1, 1, 1, 1, 1, '', 0),
(9, 'citations', 'citations', 'Zitate', 'comments-o', 0, 1, 1, 1, 1, 1, 1, '', 0),
(10, 'collections', 'collections', 'Bilder', 'photo', 0, 1, 1, 1, 1, 1, 1, '', 0),
(11, 'surveys', 'surveys', 'Umfragen', 'archive', 0, 1, 1, 1, 1, 1, 1, '', 0),
(12, 'boxes', 'boxes', 'Boxen', 'th-large', 0, 1, 1, 1, 1, 1, 1, '', 0),
(13, 'commercials', 'commercials', 'Anzeigen', 'money', 0, 1, 1, 1, 1, 1, 1, 'normal', 3),
(14, 'sites', 'sites', 'Seiten', 'file-text', 0, 1, 1, 0, 1, 1, 1, '', 0),
(15, 'downloads', 'downloads', 'Downloads', 'download', 0, 1, 1, 1, 1, 1, 1, 'normal', 0),
(16, 'countdown', 'countdown', 'Countdown', 'hourglass-half', 0, 0, 0, 1, 0, 1, 1, '', 0),
(17, 'css', 'css', 'CSS', 'css3', 0, 0, 0, 1, 0, 0, 0, '', 0),
(18, 'finances', 'finances', 'Finanzen', 'euro', 0, 0, 1, 1, 1, 1, 1, '', 0),
(19, 'resetPassword', 'resetPassword', 'Passwort vergessen', 'key', 0, 0, 0, 1, 0, 1, 1, '', 0),
(20, 'ideabox', 'ideabox', 'Ideenbox', 'idea', 0, 0, 1, 1, 0, 1, 1, '', 0),
(21, 'imageGallery', 'imageGallery', 'Bildergallerie', 'camera', 0, 0, 0, 1, 0, 0, 0, '', 0),
(22, 'announcements', 'announcements', 'Ankündigungen', 'info', 0, 0, 0, 1, 1, 1, 1, '', 0),
(23, 'media', 'media', 'Medien', 'file', 0, 0, 0, 1, 0, 0, 0, '', 0),
(24, 'upload', 'upload', 'Hochladen', 'upload', 0, 0, 0, 1, 1, 1, 1, 'administrator', 0),
(25, 'views', 'views', 'Views', 'files-o', 0, 0, 0, 1, 0, 1, 0, '', 0),
(26, 'export', 'export', 'Exportieren', 'download', 0, 0, 0, 1, 1, 0, 0, 'administrator', 0),
(27, 'members', 'members', 'Mitglieder', 'envelope', 0, 1, 1, 1, 1, 1, 1, 'speaker', 0),
(28, 'lehrer', 'lehrer', 'Lehrerzugang', 'graduation-cap', 0, 0, 1, 1, 0, 1, 1, '', 0),
(29, 'settings', 'settings', 'Einstellungen', 'wrench', 0, 1, 0, 1, 1, 1, 1, 'administrator', 0),
(30, 'register', 'register', 'Registrierung', 'plus-circle', 0, 0, 1, 0, 0, 1, 1, '', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_password_reset`
--

CREATE TABLE `aditur_password_reset` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(60) COLLATE latin1_german1_ci NOT NULL,
  `timestamp` int(12) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_rights`
--

CREATE TABLE `aditur_rights` (
  `id` int(11) NOT NULL,
  `operation_key` varchar(128) COLLATE latin1_german1_ci NOT NULL,
  `privileges` varchar(128) COLLATE latin1_german1_ci NOT NULL,
  `komitee` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

--
-- Daten für Tabelle `aditur_rights`
--

INSERT INTO `aditur_rights` (`id`, `operation_key`, `privileges`, `komitee`) VALUES
(1, 'changeFinances', 'speaker', 2),
(2, 'createSurvey', 'speaker', 3),
(3, 'deleteSurvey', 'speaker', 3),
(4, 'seeResults', 'speaker', 3),
(5, 'createCollection', 'administrator', 0),
(6, 'changeAdInfo', 'speaker', 3),
(7, 'changeAdFinances', 'speaker', 2),
(8, 'deleteCollectionFile', 'administrator', 0),
(9, 'deleteCollection', 'administrator', 0),
(10, 'censorComments', 'speaker', 3),
(12, 'deleteIdeas', 'administrator', 0),
(13, 'createSite', 'administrator', 0),
(14, 'deleteSite', 'administrator', 0),
(15, 'createBoxes', 'administrator', 0),
(16, 'seeBoxEntries', 'speaker', 3),
(17, 'deleteBoxEntries', 'administrator', 0),
(18, 'uploadFiles', 'speaker', 0),
(19, 'deleteFile', 'president', 0);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_sites`
--

CREATE TABLE `aditur_sites` (
  `id` int(11) NOT NULL,
  `link` varchar(256) NOT NULL,
  `heading` varchar(256) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_surveys`
--

CREATE TABLE `aditur_surveys` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `komitee` varchar(256) NOT NULL,
  `closed` tinyint(1) NOT NULL,
  `category` text NOT NULL,
  `dropdown` int(1) NOT NULL DEFAULT 0,
  `suggestion` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_surveys_answers`
--

CREATE TABLE `aditur_surveys_answers` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `answer` text NOT NULL,
  `accepted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_surveys_votes`
--

CREATE TABLE `aditur_surveys_votes` (
  `id` int(11) NOT NULL,
  `survey_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_teachers`
--

CREATE TABLE `aditur_teachers` (
  `id` int(10) NOT NULL,
  `title` varchar(128) COLLATE latin1_german1_ci NOT NULL,
  `name` varchar(128) COLLATE latin1_german1_ci NOT NULL,
  `access_code` varchar(8) COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_teachers_comments`
--

CREATE TABLE `aditur_teachers_comments` (
  `id` int(11) NOT NULL,
  `comment_on_id` int(11) NOT NULL,
  `author_id` int(11) NOT NULL,
  `comment` text COLLATE latin1_german1_ci NOT NULL,
  `precensored` tinyint(1) NOT NULL,
  `censored` tinyint(1) NOT NULL,
  `reason` text COLLATE latin1_german1_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_german1_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `aditur_views`
--

CREATE TABLE `aditur_views` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `link` varchar(256) NOT NULL,
  `title` varchar(256) NOT NULL,
  `needs_login` tinyint(1) NOT NULL,
  `home` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Daten für Tabelle `aditur_views`
--

INSERT INTO `aditur_views` (`id`, `name`, `link`, `title`, `needs_login`, `home`) VALUES
(1, 'home', 'home', 'Home', 0, 1);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `aditur_announcements`
--
ALTER TABLE `aditur_announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_boxes`
--
ALTER TABLE `aditur_boxes`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_box_entries`
--
ALTER TABLE `aditur_box_entries`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_chosen_picture`
--
ALTER TABLE `aditur_chosen_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_citations`
--
ALTER TABLE `aditur_citations`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_comments`
--
ALTER TABLE `aditur_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_comments_from_teachers`
--
ALTER TABLE `aditur_comments_from_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_commercials`
--
ALTER TABLE `aditur_commercials`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_config`
--
ALTER TABLE `aditur_config`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_downloads`
--
ALTER TABLE `aditur_downloads`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_events`
--
ALTER TABLE `aditur_events`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_finances`
--
ALTER TABLE `aditur_finances`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_ideas`
--
ALTER TABLE `aditur_ideas`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_komitees`
--
ALTER TABLE `aditur_komitees`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_login_attempts`
--
ALTER TABLE `aditur_login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_members`
--
ALTER TABLE `aditur_members`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indizes für die Tabelle `aditur_me_answers`
--
ALTER TABLE `aditur_me_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_me_questions`
--
ALTER TABLE `aditur_me_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_modules`
--
ALTER TABLE `aditur_modules`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_password_reset`
--
ALTER TABLE `aditur_password_reset`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_rights`
--
ALTER TABLE `aditur_rights`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_sites`
--
ALTER TABLE `aditur_sites`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_surveys`
--
ALTER TABLE `aditur_surveys`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_surveys_answers`
--
ALTER TABLE `aditur_surveys_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_surveys_votes`
--
ALTER TABLE `aditur_surveys_votes`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_teachers`
--
ALTER TABLE `aditur_teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_teachers_comments`
--
ALTER TABLE `aditur_teachers_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `aditur_views`
--
ALTER TABLE `aditur_views`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `aditur_announcements`
--
ALTER TABLE `aditur_announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `aditur_boxes`
--
ALTER TABLE `aditur_boxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT für Tabelle `aditur_box_entries`
--
ALTER TABLE `aditur_box_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_chosen_picture`
--
ALTER TABLE `aditur_chosen_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_citations`
--
ALTER TABLE `aditur_citations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_comments`
--
ALTER TABLE `aditur_comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_comments_from_teachers`
--
ALTER TABLE `aditur_comments_from_teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_commercials`
--
ALTER TABLE `aditur_commercials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_config`
--
ALTER TABLE `aditur_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT für Tabelle `aditur_downloads`
--
ALTER TABLE `aditur_downloads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_events`
--
ALTER TABLE `aditur_events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_finances`
--
ALTER TABLE `aditur_finances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_ideas`
--
ALTER TABLE `aditur_ideas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_komitees`
--
ALTER TABLE `aditur_komitees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT für Tabelle `aditur_login_attempts`
--
ALTER TABLE `aditur_login_attempts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_members`
--
ALTER TABLE `aditur_members`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_me_answers`
--
ALTER TABLE `aditur_me_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_me_questions`
--
ALTER TABLE `aditur_me_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_modules`
--
ALTER TABLE `aditur_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT für Tabelle `aditur_password_reset`
--
ALTER TABLE `aditur_password_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT für Tabelle `aditur_rights`
--
ALTER TABLE `aditur_rights`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT für Tabelle `aditur_sites`
--
ALTER TABLE `aditur_sites`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_surveys`
--
ALTER TABLE `aditur_surveys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_surveys_answers`
--
ALTER TABLE `aditur_surveys_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_surveys_votes`
--
ALTER TABLE `aditur_surveys_votes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_teachers`
--
ALTER TABLE `aditur_teachers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_teachers_comments`
--
ALTER TABLE `aditur_teachers_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `aditur_views`
--
ALTER TABLE `aditur_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
